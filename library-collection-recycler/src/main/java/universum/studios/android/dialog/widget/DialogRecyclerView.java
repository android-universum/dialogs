/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.dialog.view.DialogView;

/**
 * A {@link RecyclerView} simple implementation that tints its over-scroll effect using the accent
 * color presented within the current dialog theme. This extended recycler view also draws a shadow
 * at its both vertical (top and bottom) edges.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogRecyclerView extends RecyclerView implements DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogRecyclerView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Helper used to draw vertical edge shadows for this view.
	 */
	private VerticalEdgeShadowHelper mEdgeShadowHelper;

	/*
	 * Constructors ================================================================================
	 */

    /**
     * Same as {@link #DialogRecyclerView(Context, AttributeSet)}  without attributes.
     */
    public DialogRecyclerView(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #DialogRecyclerView(Context, AttributeSet, int)} with {@code 0} as attribute
     * for default style.
     */
    public DialogRecyclerView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Creates a new instance of DialogRecyclerView for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     */
    public DialogRecyclerView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
	    this.mEdgeShadowHelper = new VerticalEdgeShadowHelper(this);
        OverScrollTintManager.applyOverScrollTint(context);
    }

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
		super.onSizeChanged(width, height, oldWidth, oldHeight);
		mEdgeShadowHelper.onViewSizeChanged(width, height);
	}

	/**
	 */
	@Override protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		final boolean shadowsVisible = ViewCompat.canScrollVertically(this, -1) || ViewCompat.canScrollVertically(this, 1);
		mEdgeShadowHelper.setTopShadowVisible(shadowsVisible);
		mEdgeShadowHelper.setBottomShadowVisible(shadowsVisible);
	}

	/**
	 */
	@Override public boolean verifyDrawable(@NonNull final Drawable drawable) {
		return mEdgeShadowHelper.verifyShadowDrawable(drawable) || super.verifyDrawable(drawable);
	}

	/**
	 */
	@Override public void draw(@NonNull final Canvas canvas) {
		super.draw(canvas);
		mEdgeShadowHelper.drawShadows(canvas);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}