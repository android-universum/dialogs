/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.dialog.RecyclerDialog;
import universum.studios.android.dialog.widget.RecyclerDialogContentView;

/**
 * A {@link BaseAdapterDialogView} extension used by {@link RecyclerDialog RecyclerDialog} as interface
 * layer for its content view.
 * <p>
 * See default {@link RecyclerDialogContentView implementation} for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface RecyclerDialogView extends BaseAdapterDialogView<RecyclerView.Adapter, RecyclerView> {

    /*
     * Interface ===================================================================================
     */

    /**
     * Simple interface for recycler adapters that are to be used within context of {@link RecyclerDialog}.
     * This adapter interface specifies simple API layer to provide a {@link androidx.recyclerview.widget.RecyclerView.LayoutManager LayoutManager}
     * via {@link #provideLayoutManager()} and a list of {@link androidx.recyclerview.widget.RecyclerView.ItemDecoration ItemDecorations}
     * via {@link #provideItemDecorations()} for the {@link RecyclerView} to which will be this
     * RecyclerAdapter implementation attached.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @see RecyclerView#setLayoutManager(RecyclerView.LayoutManager)
     * @see RecyclerView#addItemDecoration(RecyclerView.ItemDecoration)
     */
    interface RecyclerAdapter {

        /**
         * Provides an instance of layout manager that should be set to the {@link RecyclerView} to
         * which is this adapter attached.
         *
         * @return Layout manager to be used for the recycler view or {@code null} to use default
         * {@link androidx.recyclerview.widget LinearLayoutManager}.
         * @see RecyclerView#setLayoutManager(RecyclerView.LayoutManager)
         */
        @Nullable RecyclerView.LayoutManager provideLayoutManager();

        /**
         * Provides a list of item decorations that should be added into the {@link RecyclerView} to
         * which is this adapter attached.
         *
         * @return List of item decorations or {@code null} to not add any item decorations.
         * @see RecyclerView#addItemDecoration(RecyclerView.ItemDecoration)
         */
        @Nullable List<RecyclerView.ItemDecoration> provideItemDecorations();
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets a layout manager for the RecyclerView of this dialog view.
     *
     * @param layoutManager The desired layout manager. May be {@code null} to clear the current one.
     * @see RecyclerView#setLayoutManager(RecyclerView.LayoutManager)
     * @see #getLayoutManager()
     */
    void setLayoutManager(@Nullable RecyclerView.LayoutManager layoutManager);

    /**
     * Returns the RecyclerView's layout manager specified for this dialog view.
     *
     * @return Current layout manager or {@code null} if no layout manager has been specified yet.
     * @see #setLayoutManager(RecyclerView.LayoutManager)
     */
    @Nullable RecyclerView.LayoutManager getLayoutManager();
}