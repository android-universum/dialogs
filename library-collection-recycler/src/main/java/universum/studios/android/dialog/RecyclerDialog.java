/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.dialog.view.RecyclerDialogView;

/**
 * A {@link BaseAdapterDialog} implementation that can be used to present data set of items within
 * {@link RecyclerView}.
 * <p>
 * Custom {@link androidx.recyclerview.widget.RecyclerView.LayoutManager LayoutManager} or set of
 * {@link androidx.recyclerview.widget.RecyclerView.ItemDecoration ItemDecorations} can be provided
 * via {@link RecyclerDialogView.RecyclerAdapter RecyclerAdapter} interface that should be in such
 * case implemented by an adapter provided for a particular RecyclerDialog via
 * {@link AdapterProvider AdapterProvider}.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogRecyclerOptions dialogRecyclerOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class RecyclerDialog extends BaseAdapterDialog<RecyclerView.Adapter, RecyclerView> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "RecyclerDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of RecyclerDialog with {@link RecyclerView} to present data from
	 * {@link RecyclerView.Adapter}.
	 */
	public RecyclerDialog() {
		super(R.attr.dialogRecyclerOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of RecyclerDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New RecyclerDialog instance.
	 */
	@NonNull public static RecyclerDialog newInstance(@NonNull final RecyclerOptions options) {
		final RecyclerDialog dialog = new RecyclerDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new RecyclerOptions(resources);
	}

	/**
	 */
	@Override protected void onAttachProvidedAdapter(@NonNull final Object adapter) {
		if (!(adapter instanceof RecyclerView.Adapter)) {
			throw new IllegalArgumentException(
					"Provided adapter of unsupported type for RecyclerDialog!" +
							" Dialog adapter must be instance of " + RecyclerView.Adapter.class.getName() + "."
			);
		}
		setAdapter((RecyclerView.Adapter) adapter);
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_recycler, container, false);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link BaseAdapterOptions} implementation specific for the {@link RecyclerDialog}.
	 * <p>
	 * These options are here only for convenience and do not add any extra options to the parent's
	 * {@link BaseAdapterOptions}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class RecyclerOptions extends BaseAdapterOptions<RecyclerOptions> {

		/**
		 * Creator used to create an instance or array of instances of RecyclerOptions from {@link Parcel}.
		 */
		public static final Creator<RecyclerOptions> CREATOR = new Creator<RecyclerOptions>() {

			/**
			 */
			@Override public RecyclerOptions createFromParcel(@NonNull final Parcel source) { return new RecyclerOptions(source); }

			/**
			 */
			@Override public RecyclerOptions[] newArray(final int size) { return new RecyclerOptions[size]; }
		};

		/**
		 * Creates a new instance of RecyclerOptions.
		 */
		public RecyclerOptions() {
			super();
		}

		/**
		 * Creates a new instance of RecyclerOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public RecyclerOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of RecyclerOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected RecyclerOptions(@NonNull final Parcel source) {
			super(source);
		}
	}
}