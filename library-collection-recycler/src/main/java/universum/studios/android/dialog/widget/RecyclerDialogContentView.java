/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.List;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.RecyclerDialog;
import universum.studios.android.dialog.view.RecyclerDialogView;
import universum.studios.android.ui.widget.StatefulAdapter;

/**
 * A {@link BaseAdapterDialogContentView} implementation used by {@link RecyclerDialog RecyclerDialog}
 * as its content view.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class RecyclerDialogContentView extends BaseAdapterDialogContentView<RecyclerView.Adapter, RecyclerView> implements RecyclerDialogView {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "RecyclerDialogContentView";

	/*
	 * Interface ===================================================================================
	 */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #RecyclerDialogContentView(Context, AttributeSet)} without attributes.
     */
    public RecyclerDialogContentView(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #RecyclerDialogContentView(Context, AttributeSet, int)} with {@code 0} as
     * attribute for default style.
     */
    public RecyclerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Same as {@link #RecyclerDialogContentView(Context, AttributeSet, int, int)} with {@code 0}
     * as default style.
     */
    public RecyclerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    /**
     * Creates a new instance of RecyclerDialogContentView for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     * @param defStyleRes  Resource id of the default style for the new view.
     */
    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RecyclerDialogContentView(
            @NonNull final Context context,
            @Nullable final AttributeSet attrs,
            @AttrRes final int defStyleAttr,
            @StyleRes final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Methods =====================================================================================
     */

    /**
     * Called from one of constructors of this view to perform its initialization.
     * <p>
     * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
     * this view specific data from it that can be used to configure this new view instance. The
     * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
     * from the current theme provided by the specified <var>context</var>.
     */
    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        final Resources.Theme theme = context.getTheme();
        final TypedValue typedValue = new TypedValue();
        // Resolve layout resource from which to inflate view hierarchy.
        int layoutResource = R.layout.dialog_layout_recycler;
        if (theme.resolveAttribute(R.attr.dialogLayoutRecyclerContent, typedValue, true)) {
            layoutResource = typedValue.resourceId;
        }
        this.inflateHierarchy(context, layoutResource);
    }

    /**
     */
    @Override boolean onHierarchyInflationFinished(@Nullable final View adapterView) {
        return adapterView instanceof RecyclerView;
    }

    /**
     */
    @Override public void setLayoutManager(@Nullable final RecyclerView.LayoutManager layoutManager) {
        if (mAdapterView != null && mAdapterView.getLayoutManager() != layoutManager) {
            mAdapterView.setLayoutManager(layoutManager);
        }
    }

    /**
     */
    @Override @Nullable public RecyclerView.LayoutManager getLayoutManager() {
        return mAdapterView != null ? mAdapterView.getLayoutManager() : null;
    }

    @Override public void setAdapter(@Nullable final RecyclerView.Adapter adapter) {
        super.setAdapter(adapter);
        if (mAdapterView != null && adapter instanceof RecyclerAdapter) {
            final RecyclerAdapter recyclerAdapter = (RecyclerAdapter) adapter;
            final RecyclerView.LayoutManager layoutManager = recyclerAdapter.provideLayoutManager();
            if (layoutManager != null) {
                mAdapterView.setLayoutManager(layoutManager);
            }
            final List<RecyclerView.ItemDecoration> itemDecorations = recyclerAdapter.provideItemDecorations();
            if (itemDecorations != null && !itemDecorations.isEmpty()) {
                for (RecyclerView.ItemDecoration decoration : itemDecorations) {
                    mAdapterView.addItemDecoration(decoration);
                }
            }
        }
    }

    /**
     */
    @Override protected void onAttachAdapter(@NonNull final RecyclerView adapterView, @Nullable final RecyclerView.Adapter adapter) {
        adapterView.setAdapter(adapter);
    }

    /**
     */
    @Override @Nullable protected Parcelable onSaveAdapterState(@NonNull final RecyclerView.Adapter adapter) {
        return saveAdapterState(adapter);
    }

    /**
     * Saves the current state of the specified <var>adapter</var>.
     *
     * @param adapter The adapter of which state to save. Should be instance of {@link StatefulAdapter}.
     * @return Adapter's saved state or {@code null} or empty state if the adapter does not save its
     * state or it is not a stateful adapter.
     */
    private Parcelable saveAdapterState(final RecyclerView.Adapter adapter) {
        if (adapter instanceof StatefulAdapter) {
            return ((StatefulAdapter) adapter).saveInstanceState();
        }
        return null;
    }

    /**
     */
    @Override protected void onRestoreAdapterState(@NonNull final RecyclerView.Adapter adapter, @NonNull final Parcelable adapterState) {
        this.restoreAdapterState(adapter, adapterState);
    }

    /**
     * Restores the saved state of the specified <var>adapter</var>.
     *
     * @param adapter      The adapter of which state to restore. Should be instance of {@link StatefulAdapter}.
     * @param adapterState The previously saved adapter state via {@link #saveAdapterState(RecyclerView.Adapter)}.
     */
    private void restoreAdapterState(final RecyclerView.Adapter adapter, final Parcelable adapterState) {
        if (adapter instanceof StatefulAdapter) {
            ((StatefulAdapter) adapter).restoreInstanceState(adapterState);
        }
    }

    /**
     * Inner classes ===============================================================================
     */
}