/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import androidx.annotation.NonNull;

/**
 * A {@link SimpleDialog} implementation used only to allow parsing of global options from the dialog
 * theme specific for this type of dialog.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogErrorOptions dialogErrorOptions} attribute.
 *
 * @author Martin Albedinsky
 */
public class ErrorDialog extends SimpleDialog {

	/**
	 * Creates a new instance of ErrorDialog.
	 */
	public ErrorDialog() {
		super(R.attr.dialogErrorOptions);
	}

	/**
	 * Creates a new instance of ErrorDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New ErrorDialog instance.
	 */
	@NonNull public static ErrorDialog newInstance(@NonNull final DialogOptions options) {
		final ErrorDialog dialog = new ErrorDialog();
		dialog.setOptions(options);
		return dialog;
	}
}