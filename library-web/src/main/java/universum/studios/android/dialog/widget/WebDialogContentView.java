/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.DialogsConfig;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.WebDialog;
import universum.studios.android.dialog.view.WebDialogView;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link WebDialogView} implementation used by {@link WebDialog WebDialog} as content view.
 * <p>
 * This class represents a container {@link WebView} used to present either HTML content or URL.
 * The desired content can be loaded into web view via {@link #loadContent(String)} or its resource
 * equivalent {@link #loadContent(int)} and the WebDialogView will automatically resolve its type
 * and according to it will use proper loading method of WebView specific for that particular type.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 * <p>
 * <h3>Callbacks</h3>
 * If you need to listener for loading progress you can specify {@link OnContentLoadingListener} via
 * {@link #setOnContentLoadingListener(WebDialogView.OnContentLoadingListener)} of which callbacks
 * will be fired whenever loading of web content started, finished or its progress changed.
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutWebContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_web} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class WebDialogContentView extends FrameLayout implements WebDialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "WebDialogContentView";

	/**
	 * Content data encoding.
	 */
	private static final String DATA_ENCODING = "UTF-8";

	/**
	 * Content data mime type.
	 */
	private static final String DATA_MIME_TYPE = "text/html";

	/**
	 * Max length of content text to print out when logging.
	 */
	private static final int LOG_CONTENT_MAX_LENGTH = 50;

	/**
	 * Duration for fade in/out animation for loading view.
	 */
	private static final int ANIM_LOADING_FADE_DURATION = 500;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Matcher for <b>filer</b> uri scheme.
	 */
	private static final Matcher FILE_URI_MATCHER = Pattern.compile("file://(.*)").matcher("");

	/*
	 * Source code copied from Android SDK [START] =================================================
	 * to preserve min library SDK version at 7.
	 */
	private static final String GOOD_IRI_CHAR = "a-zA-Z0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF";

	/**
	 * Regular expression to match all IANA top-level domains for WEB_URL_MATCHER.
	 * List accurate as of 2011/07/18.  List taken from:
	 * http://data.iana.org/TLD/tlds-alpha-by-domain.txt
	 * This pattern is auto-generated by frameworks/ex/common/tools/make-iana-tld-pattern.py
	 */
	private static final String TOP_LEVEL_DOMAIN_STR_FOR_WEB_URL =
			"(?:"
					+ "(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])"
					+ "|(?:biz|b[abdefghijmnorstvwyz])"
					+ "|(?:cat|com|coop|c[acdfghiklmnoruvxyz])"
					+ "|d[ejkmoz]"
					+ "|(?:edu|e[cegrstu])"
					+ "|f[ijkmor]"
					+ "|(?:gov|g[abdefghilmnpqrstuwy])"
					+ "|h[kmnrtu]"
					+ "|(?:info|int|i[delmnoqrst])"
					+ "|(?:jobs|j[emop])"
					+ "|k[eghimnprwyz]"
					+ "|l[abcikrstuvy]"
					+ "|(?:mil|mobi|museum|m[acdeghklmnopqrstuvwxyz])"
					+ "|(?:name|net|n[acefgilopruz])"
					+ "|(?:org|om)"
					+ "|(?:pro|p[aefghklmnrstwy])"
					+ "|qa"
					+ "|r[eosuw]"
					+ "|s[abcdeghijklmnortuvyz]"
					+ "|(?:tel|travel|t[cdfghjklmnoprtvwz])"
					+ "|u[agksyz]"
					+ "|v[aceginu]"
					+ "|w[fs]"
					+ "|(?:\u03b4\u03bf\u03ba\u03b9\u03bc\u03ae|\u0438\u0441\u043f\u044b\u0442\u0430\u043d\u0438\u0435|\u0440\u0444|\u0441\u0440\u0431|\u05d8\u05e2\u05e1\u05d8|\u0622\u0632\u0645\u0627\u06cc\u0634\u06cc|\u0625\u062e\u062a\u0628\u0627\u0631|\u0627\u0644\u0627\u0631\u062f\u0646|\u0627\u0644\u062c\u0632\u0627\u0626\u0631|\u0627\u0644\u0633\u0639\u0648\u062f\u064a\u0629|\u0627\u0644\u0645\u063a\u0631\u0628|\u0627\u0645\u0627\u0631\u0627\u062a|\u0628\u06be\u0627\u0631\u062a|\u062a\u0648\u0646\u0633|\u0633\u0648\u0631\u064a\u0629|\u0641\u0644\u0633\u0637\u064a\u0646|\u0642\u0637\u0631|\u0645\u0635\u0631|\u092a\u0930\u0940\u0915\u094d\u0937\u093e|\u092d\u093e\u0930\u0924|\u09ad\u09be\u09b0\u09a4|\u0a2d\u0a3e\u0a30\u0a24|\u0aad\u0abe\u0ab0\u0aa4|\u0b87\u0ba8\u0bcd\u0ba4\u0bbf\u0baf\u0bbe|\u0b87\u0bb2\u0b99\u0bcd\u0b95\u0bc8|\u0b9a\u0bbf\u0b99\u0bcd\u0b95\u0baa\u0bcd\u0baa\u0bc2\u0bb0\u0bcd|\u0baa\u0bb0\u0bbf\u0b9f\u0bcd\u0b9a\u0bc8|\u0c2d\u0c3e\u0c30\u0c24\u0c4d|\u0dbd\u0d82\u0d9a\u0dcf|\u0e44\u0e17\u0e22|\u30c6\u30b9\u30c8|\u4e2d\u56fd|\u4e2d\u570b|\u53f0\u6e7e|\u53f0\u7063|\u65b0\u52a0\u5761|\u6d4b\u8bd5|\u6e2c\u8a66|\u9999\u6e2f|\ud14c\uc2a4\ud2b8|\ud55c\uad6d|xn\\-\\-0zwm56d|xn\\-\\-11b5bs3a9aj6g|xn\\-\\-3e0b707e|xn\\-\\-45brj9c|xn\\-\\-80akhbyknj4f|xn\\-\\-90a3ac|xn\\-\\-9t4b11yi5a|xn\\-\\-clchc0ea0b2g2a9gcd|xn\\-\\-deba0ad|xn\\-\\-fiqs8s|xn\\-\\-fiqz9s|xn\\-\\-fpcrj9c3d|xn\\-\\-fzc2c9e2c|xn\\-\\-g6w251d|xn\\-\\-gecrj9c|xn\\-\\-h2brj9c|xn\\-\\-hgbk6aj7f53bba|xn\\-\\-hlcj6aya9esc7a|xn\\-\\-j6w193g|xn\\-\\-jxalpdlp|xn\\-\\-kgbechtv|xn\\-\\-kprw13d|xn\\-\\-kpry57d|xn\\-\\-lgbbat1ad8j|xn\\-\\-mgbaam7a8h|xn\\-\\-mgbayh7gpa|xn\\-\\-mgbbh1a71e|xn\\-\\-mgbc0a9azcg|xn\\-\\-mgberp4a5d4ar|xn\\-\\-o3cw4h|xn\\-\\-ogbpf8fl|xn\\-\\-p1ai|xn\\-\\-pgbs0dh|xn\\-\\-s9brj9c|xn\\-\\-wgbh1c|xn\\-\\-wgbl6a|xn\\-\\-xkc2al3hye2a|xn\\-\\-xkc2dl3a5ee0h|xn\\-\\-yfro4i67o|xn\\-\\-ygbi2ammx|xn\\-\\-zckzah|xxx)"
					+ "|y[et]"
					+ "|z[amw]))";

	private static final Matcher WEB_URL_MATCHER = Pattern.compile(
			"((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
					+ "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
					+ "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
					+ "((?:(?:[" + GOOD_IRI_CHAR + "][" + GOOD_IRI_CHAR + "\\-]{0,64}\\.)+"   // named host
					+ TOP_LEVEL_DOMAIN_STR_FOR_WEB_URL
					+ "|(?:(?:25[0-5]|2[0-4]" // or ip address
					+ "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
					+ "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
					+ "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
					+ "|[1-9][0-9]|[0-9])))"
					+ "(?:\\:\\d{1,5})?)" // plus option port numbers
					+ "(\\/(?:(?:[" + GOOD_IRI_CHAR + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
					+ "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
					+ "(?:\\b|$)"
	).matcher(""); // and finally, a word boundary or end of input.  This is to stop foo.sure from matching as foo.su
	/*
	 * Source code copied from Android SDK [END] ===================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Fade in animation for loading view.
	 */
	private final AlphaAnimation LOADING_FADE_IN_ANIMATION = new AlphaAnimation(0.0f, 1.0f);

	/**
	 * Fade out animation for loading view.
	 */
	private final AlphaAnimation LOADING_FADE_OUT_ANIMATION = new AlphaAnimation(1.0f, 0.0f);

	/**
	 * Animation listener for {@link #LOADING_FADE_OUT_ANIMATION} animation. This listener hides
	 * the loading view (or progress bar) within {@link #onAnimationEnd()}.
	 */
	private final Animation.AnimationListener LOADING_FADE_OUT_ANIM_LISTENER = new AnimationWatcher() {

		/**
		 */
		@Override public void onAnimationEnd(@NonNull final Animation animation) {
			if (mLoadingLayout != null) {
				mLoadingLayout.setVisibility(View.GONE);
			} else if (mProgressBar != null) {
				mProgressBar.setVisibility(View.GONE);
			}
		}
	};

	{
		LOADING_FADE_IN_ANIMATION.setDuration(ANIM_LOADING_FADE_DURATION);
		LOADING_FADE_OUT_ANIMATION.setDuration(ANIM_LOADING_FADE_DURATION);
		LOADING_FADE_OUT_ANIMATION.setAnimationListener(LOADING_FADE_OUT_ANIM_LISTENER);
	}

	/**
	 * Wrapper for {@link WebChromeClient} instance attached to this dialog body view.
	 */
	private final WebChromeClientWrapper WEB_CHROME_CLIENT_WRAPPER = new WebChromeClientWrapper(this);

	/**
	 * Wrapper for {@link WebViewClient} instance attached to this dialog body view.
	 */
	private final WebViewClientWrapper WEB_CLIENT_WRAPPER = new WebViewClientWrapper(this);

	/**
	 * Boolean flag indicating whether a java-script for web view is enabled or not.
	 */
	private boolean mJavaScriptEnabled;

	/**
	 * Web view to present Html content from file, url or as a raw content.
	 */
	private WebView mWebView;

	/**
	 * Layout with progress bar to indicate loading.
	 */
	private ViewGroup mLoadingLayout;

	/**
	 * Progress bar to indicate content loading.
	 */
	private View mProgressBar;

	/**
	 * Callback to be invoked whenever loading of the current content has been started/finished.
	 */
	private OnContentLoadingListener mLoadingListener;

	/**
	 * Content presented within this dialog body's web view. This can a raw Html, Url, or file uri.
	 */
	private String mContent = "";

	/**
	 * Type of the content presented within this dialog body's web view.
	 */
	private int mContentType = CONTENT_EMPTY;

	/**
	 * Boolean flag indicating whether the current content has been changed or not.
	 */
	private boolean mContentChanged;

	/**
	 * Current progress of content loading into web view.
	 */
	private int mProgress;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #WebDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public WebDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #WebDialogContentView(Context, AttributeSet, int)} with {@code 0} as attribute
	 * for default style.
	 */
	public WebDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Same as {@link #WebDialogContentView(Context, AttributeSet, int, int)} with {@code 0} as default
	 * style.
	 */
	public WebDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of WebDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public WebDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		int layoutResource = R.layout.dialog_layout_web;
		if (theme.resolveAttribute(R.attr.dialogLayoutWebContent, typedValue, true)) {
			layoutResource = typedValue.resourceId;
		}
		this.inflateHierarchy(context, layoutResource);
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mWebView = (WebView) findViewById(R.id.dialog_web_view);
			this.mLoadingLayout = (ViewGroup) findViewById(R.id.dialog_loading);
			this.mProgressBar = findViewById(R.id.dialog_progress_bar);
			if (mWebView != null) {
				mWebView.setWebChromeClient(WEB_CHROME_CLIENT_WRAPPER);
				mWebView.setWebViewClient(WEB_CLIENT_WRAPPER);
			}
			onFinishInflate();
		}
	}

	/**
	 */
	@Override public void setWebViewClient(@Nullable final WebViewClient client) {
		WEB_CLIENT_WRAPPER.mClient = client;
	}

	/**
	 */
	@Override @Nullable public WebViewClient getWebViewClient() {
		return WEB_CLIENT_WRAPPER.mClient;
	}

	/**
	 */
	@Override public void setWebChromeClient(@Nullable final WebChromeClient chromeClient) {
		WEB_CHROME_CLIENT_WRAPPER.mClient = chromeClient;
	}

	/**
	 */
	@Override @Nullable public WebChromeClient getWebChromeClient() {
		return WEB_CHROME_CLIENT_WRAPPER.mClient;
	}

	/**
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@Override public void setJavaScriptEnabled(final boolean enabled) {
		if (mJavaScriptEnabled != enabled) {
			this.mJavaScriptEnabled = enabled;
			if (mWebView != null) {
				mWebView.getSettings().setJavaScriptEnabled(enabled);
			}
		}
	}

	/**
	 */
	@Override public boolean isJavaScriptEnabled() {
		return mJavaScriptEnabled;
	}

	/**
	 */
	@Override public void setOnContentLoadingListener(@Nullable final OnContentLoadingListener listener) {
		this.mLoadingListener = listener;
	}

	/**
	 */
	@Override public void setLoadingVisible(final boolean visible) {
		if (mLoadingLayout != null) setLoadingViewVisible(mLoadingLayout, visible);
		else if (mProgressBar != null) setLoadingViewVisible(mProgressBar, visible);
	}

	/**
	 * Updates a visibility of the given <var>loadingView</var>.
	 * <p>
	 * <b>Note</b>, that this will animate the given loading view with fade in/out animation.
	 *
	 * @param loadingView View which represents loading of which visibility to update.
	 * @param visible     {@code True} to set visible, {@code false} otherwise.
	 */
	private void setLoadingViewVisible(final View loadingView, final boolean visible) {
		if (loadingView != null) {
			final Animation animation = loadingView.getAnimation();
			if (animation != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
				animation.cancel();
			}
			if (visible) {
				loadingView.setVisibility(View.VISIBLE);
				loadingView.startAnimation(LOADING_FADE_IN_ANIMATION);
			} else {
				loadingView.startAnimation(LOADING_FADE_OUT_ANIMATION);
			}
		}
	}

	/**
	 */
	@Override public boolean isLoadingVisible() {
		return DialogWidgetUtils.isVisible(mLoadingLayout) || DialogWidgetUtils.isVisible(mProgressBar);
	}

	/**
	 */
	@Override public void loadContent(@StringRes final int resId) {
		loadContent(getResources().getString(resId));
	}

	/**
	 */
	@Override public void loadContent(@NonNull final String content) {
		this.mContent = content;
		this.mContentChanged = true;
		onLoadContent(mContent, resolveContentType());
	}

	/**
	 * Resolves the current type of content that will be loaded into web view.
	 *
	 * @return Resolved content type. One of {@link #CONTENT_URL}, {@link #CONTENT_HTML}, {@link #CONTENT_FILE}
	 * or {@link #CONTENT_EMPTY} if the current content is empty.
	 */
	@ContentType private int resolveContentType() {
		if (mContentChanged) {
			if (!TextUtils.isEmpty(mContent)) {
				if (WEB_URL_MATCHER.reset(mContent).matches()) {
					this.mContentType = CONTENT_URL;
				} else if (FILE_URI_MATCHER.reset(mContent).matches()) {
					this.mContentType = CONTENT_FILE;
				} else {
					this.mContentType = CONTENT_HTML;
				}
			} else {
				this.mContentType = CONTENT_EMPTY;
			}
		}
		return mContentType;
	}

	/**
	 * Invoked whenever {@link #loadContent(String)} is called.
	 *
	 * @param content The current content which should be loaded into web view.
	 * @param type    Type of the content. One of {@link #CONTENT_URL}, {@link #CONTENT_HTML}, {@link #CONTENT_FILE}
	 *                or {@link #CONTENT_EMPTY} if the content is empty.
	 */
	protected void onLoadContent(@NonNull final String content, @ContentType final int type) {
		if (mWebView != null) {
			if (DialogsConfig.LOG_ENABLED && !TextUtils.isEmpty(content)) {
				if (content.length() > LOG_CONTENT_MAX_LENGTH) {
					Log.i(TAG, "Loading content('" + content.substring(0, LOG_CONTENT_MAX_LENGTH) + "...') into web view.");
				} else {
					Log.i(TAG, "Loading content('" + content + "') into web view.");
				}
			}
			switch (type) {
				case CONTENT_EMPTY:
					mWebView.loadDataWithBaseURL("", "", DATA_MIME_TYPE, DATA_ENCODING, "");
					break;
				case CONTENT_URL:
				case CONTENT_FILE:
					mWebView.loadUrl(content);
					break;
				case CONTENT_HTML:
					mWebView.loadDataWithBaseURL("", content, DATA_MIME_TYPE, DATA_ENCODING, "");
					break;
			}
		}
	}

	/**
	 */
	@Override @NonNull public String getContent() {
		return mContent;
	}

	/**
	 */
	@Override @ContentType public int getContentType() {
		return mContentType;
	}

	/**
	 */
	@Override public boolean dispatchBackPress() {
		if (mWebView != null && mWebView.canGoBack()) {
			mWebView.goBack();
			return true;
		}
		return false;
	}

	/**
	 * Called from the {@link #WEB_CHROME_CLIENT_WRAPPER} to dispatch that loading of the specified
	 * <var>url</var> has been started.
	 *
	 * @param url The url of which loading just started.
	 */
	final void handleUrlLoadingStarted(final String url) {
		if (mLoadingListener != null) mLoadingListener.onContentLoadingStarted(url, CONTENT_URL);
	}

	/**
	 * Called from the {@link #WEB_CHROME_CLIENT_WRAPPER} to dispatch that loading of the specified
	 * <var>url</var> has been finished.
	 *
	 * @param url The url of which loading just finished.
	 */
	final void handleUrlLoadingFinished(final String url) {
		if (mLoadingListener != null) mLoadingListener.onContentLoadingFinished(url, CONTENT_URL);
	}

	/**
	 * Called from the {@link #WEB_CLIENT_WRAPPER} to dispatch current loading progress.
	 *
	 * @param progress The current loading progress.
	 */
	final void handleProgressChanged(final int progress) {
		if (mProgress != progress) {
			this.mProgress = progress;
			if (mLoadingListener != null) mLoadingListener.onLoadingProgressChange(progress);
		}
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.content = mContent;
		savedState.javaScriptEnabled = mJavaScriptEnabled;
		if (mWebView != null) {
			final Bundle state = new Bundle();
			mWebView.saveState(state);
			savedState.webViewState = state;
		}
		return savedState;
	}

	/**
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		setJavaScriptEnabled(savedState.javaScriptEnabled);
		this.mContent = savedState.content;
		this.mContentChanged = true;
		this.resolveContentType();
		if (savedState.webViewState != null && mWebView != null) {
			mWebView.restoreState(savedState.webViewState);
		}
		if (!TextUtils.isEmpty(mContent)) {
			switch (mContentType) {
				case CONTENT_HTML:
					loadContent(mContent);
					break;
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link WebDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) {
				return new SavedState(source);
			}

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		String content;

		/**
		 */
		boolean javaScriptEnabled;

		/**
		 */
		Bundle webViewState;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.content = source.readString();
			this.javaScriptEnabled = source.readInt() == 1;
			this.webViewState = source.readBundle(DialogsConfig.class.getClassLoader());
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeString(content);
			dest.writeInt(javaScriptEnabled ? 1 : 0);
			dest.writeBundle(webViewState);
		}
	}
}