/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;

/**
 * A {@link WebViewClient} implementation used by {@link WebDialogContentView} to wrap a single
 * WebViewClient and dispatch all received callbacks to the wrapped client and some of them to
 * the DialogWebContent by which is this wrapper used.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("deprecation")
final class WebViewClientWrapper extends WebViewClient {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "WebViewClientWrapper";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Web dialog body view to which is this wrapper attached.
     */
    private final WebDialogContentView mWebDialogContent;

    /**
     * Wrapped instance of WebViewClient.
     */
    WebViewClient mClient;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of WebViewClientWrapper.
     *
     * @param dialogContent Web dialog body to which will be this wrapper attached.
     */
    WebViewClientWrapper(final WebDialogContentView dialogContent) {
        this.mWebDialogContent = dialogContent;
    }

    /*
     * Methods =====================================================================================
     */

    /**
     */
    @Override public boolean shouldOverrideUrlLoading(@NonNull final WebView view, @NonNull final String url) {
        return (mClient != null && mClient.shouldOverrideUrlLoading(view, url)) ||
                super.shouldOverrideUrlLoading(view, url);
    }

    /**
     */
    @Override public void onPageStarted(@NonNull final WebView view, @NonNull final String url, @NonNull final Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (mWebDialogContent != null) {
            mWebDialogContent.handleUrlLoadingStarted(url);
        }
        if (mClient != null) {
            mClient.onPageStarted(view, url, favicon);
        }
    }

    /**
     */
    @Override public void onPageFinished(@NonNull final WebView view, @NonNull final String url) {
        super.onPageFinished(view, url);
        if (mWebDialogContent != null) {
            mWebDialogContent.handleUrlLoadingFinished(url);
        }
        if (mClient != null) {
            mClient.onPageFinished(view, url);
        }
    }

    /**
     */
    @Override public void onLoadResource(@NonNull final WebView view, @NonNull final String url) {
        super.onLoadResource(view, url);
        if (mClient != null) mClient.onLoadResource(view, url);
    }

    /**
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    @Override public WebResourceResponse shouldInterceptRequest(@NonNull final WebView view, @NonNull final String url) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && mClient != null ?
                mClient.shouldInterceptRequest(view, url) :
                super.shouldInterceptRequest(view, url);
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public void onTooManyRedirects(@NonNull final WebView view, @NonNull final Message cancelMsg, @NonNull final Message continueMsg) {
        super.onTooManyRedirects(view, cancelMsg, continueMsg);
        if (mClient != null) mClient.onTooManyRedirects(view, cancelMsg, continueMsg);
    }

    /**
     */
    @Override public void onReceivedError(
            @NonNull final WebView view,
            final int errorCode,
            @NonNull final String description,
            @NonNull final String failingUrl
    ) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        if (mClient != null) mClient.onReceivedError(view, errorCode, description, failingUrl);
    }

    /**
     */
    @Override public void onFormResubmission(@NonNull final WebView view, @NonNull final Message dontResend, @NonNull final Message resend) {
        super.onFormResubmission(view, dontResend, resend);
    }

    /**
     */
    @Override public void doUpdateVisitedHistory(@NonNull final WebView view, @NonNull final String url, final boolean isReload) {
        super.doUpdateVisitedHistory(view, url, isReload);
        if (mClient != null) mClient.doUpdateVisitedHistory(view, url, isReload);
    }

    /**
     */
    @Override public void onReceivedSslError(@NonNull final WebView view, @NonNull final SslErrorHandler handler, @NonNull final SslError error) {
        super.onReceivedSslError(view, handler, error);
        if (mClient != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mClient.onReceivedSslError(view, handler, error);
        }
    }

    /**
     */
    @Override public void onReceivedHttpAuthRequest(
    		@NonNull final WebView view,
		    @NonNull final HttpAuthHandler handler,
		    @NonNull final String host,
		    @NonNull final String realm
    ) {
        super.onReceivedHttpAuthRequest(view, handler, host, realm);
        if (mClient != null) mClient.onReceivedHttpAuthRequest(view, handler, host, realm);
    }

    /**
     */
    @Override public boolean shouldOverrideKeyEvent(@NonNull final WebView view, @NonNull final KeyEvent event) {
        return (mClient != null && mClient.shouldOverrideKeyEvent(view, event)) ||
                super.shouldOverrideKeyEvent(view, event);
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public void onUnhandledKeyEvent(@NonNull final WebView view, @NonNull final KeyEvent event) {
        super.onUnhandledKeyEvent(view, event);
        if (mClient != null) mClient.onUnhandledKeyEvent(view, event);
    }

    /**
     */
    @Override public void onScaleChanged(@NonNull final WebView view, final float oldScale, final float newScale) {
        super.onScaleChanged(view, oldScale, newScale);
        if (mClient != null) mClient.onScaleChanged(view, oldScale, newScale);
    }

    /**
     */
    @Override public void onReceivedLoginRequest(
		    @NonNull final WebView view,
		    @NonNull final String realm,
		    @NonNull final String account,
		    @NonNull final String args
    ) {
        super.onReceivedLoginRequest(view, realm, account, args);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1 && mClient != null) {
            mClient.onReceivedLoginRequest(view, realm, account, args);
        }
    }

    /*
     * Inner classes ===============================================================================
     */
}