/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

import androidx.annotation.AttrRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.DialogTitleView;
import universum.studios.android.dialog.view.WebDialogView;
import universum.studios.android.dialog.widget.DialogDynamicLayout;

/**
 * A {@link SimpleDialog} implementation that can be used to present a web content in HTML format or
 * via URL to a user. This dialog uses default {@link WebDialogView} implementation as its content
 * view which consists of {@link android.webkit.WebView WebView} and circular progress bar used as
 * loading indicator.
 * <p>
 * The WebDialog will show a progress bar during content loading process, if its title is visible and
 * if the title is not visible, it will show progress bar within the {@link WebDialogView}. Also the
 * back button press event will be handled to allow navigation through web view browsing history.
 * <p>
 * See {@link WebDialog.WebOptions} for options that can be supplied to the WebDialog from the
 * desired context via {@link #newInstance(WebDialog.WebOptions)}
 * or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogWebOptions dialogWebOptions} attribute. This attribute should reference to
 * a style that can contain following attributes, including attributes of the parent dialog:
 * <ul>
 * <li>{@link R.attr#dialogJavaScriptEnabled dialogJavaScriptEnabled}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class WebDialog extends SimpleDialog implements android.content.DialogInterface.OnKeyListener, WebDialogView.OnContentLoadingListener {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "WebDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Web dialog view implementation.
	 */
	private WebDialogView mWebView;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of WebDialog with single {@link android.webkit.WebView} within body
	 * view.
	 */
	public WebDialog() {
		super(R.attr.dialogWebOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of WebDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New WebDialog instance.
	 */
	@NonNull public static WebDialog newInstance(@NonNull final WebOptions options) {
		final WebDialog dialog = new WebDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new WebOptions(resources);
	}

	/**
	 */
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof WebOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Web);
			final WebOptions options = (WebOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Web_dialogJavaScriptEnabled) {
					if (!options.isSet(WebOptions.JAVASCRIPT_ENABLED)) {
						options.javascriptEnabled(attributes.getBoolean(index, options.javascriptEnabled));
					}
				}
			}
			attributes.recycle();
		}
	}

	/**
	 */
	@Override @NonNull protected ViewGroup onCreateLayout(
			@NonNull final LayoutInflater inflater,
			@Nullable final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return new DialogDynamicLayout(inflater.getContext());
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_web, container, false);
	}

	/**
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
		if (contentView instanceof WebDialogView && options instanceof WebOptions) {
			final WebOptions webOptions = (WebOptions) options;
			final WebDialogView webView = (WebDialogView) contentView;
			webView.setJavaScriptEnabled(webOptions.javascriptEnabled);
			if (webOptions.mContent != null) {
				webView.loadContent(webOptions.mContent.toString());
			}
		}
	}

	/**
	 */
	@Override protected void onUpdateContentViewPadding() {
		// Do not update content view padding.
	}

	/**
	 */
	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final View contentView = getContentView();
		if (contentView instanceof WebDialogView) {
			this.mWebView = (WebDialogView) contentView;
			mWebView.setOnContentLoadingListener(this);
			// Attach web clients.
			final WebViewClient client = onCreateWebViewClient();
			if (client != null) {
				mWebView.setWebViewClient(client);
			}
			final WebChromeClient chromeClient = onCreateWebChromeClient();
			if (chromeClient != null) {
				mWebView.setWebChromeClient(chromeClient);
			}
		}
		// Set up key listener to handle also web view history.
		android.app.Dialog dialog = getDialog();
		if (dialog != null) {
			dialog.setOnKeyListener(this);
		}
	}

	/**
	 * Invoked during web view's initialization process. You can create here your custom implementation
	 * of WebViewClient to manage specific callbacks for such a client.
	 * <p>
	 * This implementation returns {@code null}.
	 *
	 * @see #onCreateWebChromeClient()
	 */
	@Nullable protected WebViewClient onCreateWebViewClient() {
		return null;
	}

	/**
	 * Invoked during web view's initialization process. You can create here your custom implementation
	 * of WebChromeClient to manage specific callbacks for such a client.
	 * <p>
	 * This implementation returns {@code null}.
	 *
	 * @see #onCreateWebViewClient()
	 */
	@Nullable protected WebChromeClient onCreateWebChromeClient() {
		return null;
	}

	/**
	 */
	@Override public boolean onKey(@NonNull final android.content.DialogInterface dialog, final int keyCode, @NonNull final KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
				case KeyEvent.KEYCODE_BACK:
					return mWebView != null && mWebView.dispatchBackPress();
			}
		}
		return false;
	}

	/**
	 */
	@Override public void onContentLoadingStarted(@NonNull final String content, @WebDialogView.ContentType final int contentType) {
		setLoadingVisible(true);
	}

	/**
	 */
	@Override public void onLoadingProgressChange(@IntRange(from = 0, to = 100) int progress) {
		// Ignored for the current implementation.
		// In the feature, the progress value can be possibly shown within title view, maybe.
	}

	/**
	 */
	@Override public void onContentLoadingFinished(@NonNull final String content, @WebDialogView.ContentType final int contentType) {
		setLoadingVisible(false);
	}

	/**
	 * Set the loading progress bar visibility.
	 *
	 * @param visible {@code True} to show loading, {@code false} to hide it.
	 */
	private void setLoadingVisible(final boolean visible) {
		final View titleView = getTitleView();
		if (titleView != null && titleView.getVisibility() == View.VISIBLE && titleView instanceof DialogTitleView) {
			((DialogTitleView) titleView).setIndeterminateProgressBarVisible(visible);
		} else if (mWebView != null) {
			mWebView.setLoadingVisible(visible);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link DialogOptions} implementation specific for the {@link WebDialog}.
	 * <p>
	 * Below are listed setters which may be used to supply desired data to {@link WebDialog}
	 * through these options:
	 * <ul>
	 * <li>{@link #content(CharSequence)} (used as web content)</li>
	 * <li>{@link #content(int)} (used as web content)</li>
	 * <li>{@link #javascriptEnabled(boolean)}</li>
	 * </ul>
	 *
	 * <h3>Xml attributes</h3>
	 * See {@link R.styleable#Dialog_Options_Web WebOptions Attributes},
	 * {@link R.styleable#Dialog_Options DialogOptions Attributes}
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class WebOptions extends DialogOptions<WebOptions> {

		/**
		 * Creator used to create an instance or array of instances of WebOptions from {@link android.os.Parcel}.
		 */
		public static final Parcelable.Creator<WebOptions> CREATOR = new Parcelable.Creator<WebOptions>() {

			/**
			 */
			@Override public WebOptions createFromParcel(@NonNull final Parcel source) { return new WebOptions(source); }

			/**
			 */
			@Override public WebOptions[] newArray(final int size) { return new WebOptions[size]; }
		};

		/**
		 * Flag for <b>java-script enabled</b> parameter.
		 */
		static final int JAVASCRIPT_ENABLED = 0x00000001;

		/**
		 * Flag indicating whether a java-script should be enabled in WebView or not.
		 */
		boolean javascriptEnabled = true;

		/**
		 * Creates a new instance of WebOptions.
		 */
		public WebOptions() {
			super();
		}

		/**
		 * Creates a new instance of WebOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public WebOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of WebOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected WebOptions(@NonNull final Parcel source) {
			super(source);
			this.javascriptEnabled = source.readInt() == 1;
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(javascriptEnabled ? 1 : 0);
		}

		/**
		 */
		@Override public WebOptions merge(@NonNull final DialogOptions options) {
			if (!(options instanceof WebOptions)) {
				return super.merge(options);
			}
			final WebOptions webOptions = (WebOptions) options;
			if (webOptions.isSet(JAVASCRIPT_ENABLED)) {
				this.javascriptEnabled = webOptions.javascriptEnabled;
				updateIsSet(JAVASCRIPT_ENABLED);
			}
			return super.merge(options);
		}

		/**
		 * Sets a flag indicating whether a java-script should be enabled in WebView of dialog
		 * associated with these options or not.
		 *
		 * @param enabled {@code True} to enable, {@code false} otherwise.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogJavaScriptEnabled dialog:dialogJavaScriptEnabled
		 * @see #shouldEnableJavascript()
		 */
		public WebOptions javascriptEnabled(final boolean enabled) {
			updateIsSet(JAVASCRIPT_ENABLED);
			if (javascriptEnabled != enabled) {
				this.javascriptEnabled = enabled;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns a flag indicating whether a java-script should be enabled or not.
		 * <p>
		 * Default value: <b>true</b>
		 *
		 * @return {@code True} if enabled, {@code false} otherwise.
		 * @see #javascriptEnabled(boolean)
		 */
		public boolean shouldEnableJavascript() {
			return javascriptEnabled;
		}

		/**
		 */
		@Override protected void onParseXmlAttribute(
				@NonNull final XmlResourceParser xmlParser,
				final int index,
				@AttrRes final int attr,
				@NonNull final Resources resources,
				@Nullable final Resources.Theme theme
		) {
			if (attr == R.attr.dialogJavaScriptEnabled) {
				javascriptEnabled(xmlParser.getAttributeBooleanValue(index, true));
			} else {
				super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
			}
		}
	}
}