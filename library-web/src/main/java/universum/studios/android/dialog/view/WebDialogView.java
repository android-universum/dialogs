/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.WebDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link WebDialog WebDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.WebDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface WebDialogView extends DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Flag indicating empty web content.
	 */
	int CONTENT_EMPTY = 0x00;

	/**
	 * Flag indicating raw <b>Html</b> content.
	 */
	int CONTENT_HTML = 0x01;

	/**
	 * Flag indicating web <b>Url</b> content.
	 */
	int CONTENT_URL = 0x02;

	/**
	 * Flag indicating <b>Uri</b> to file with raw <b>Html</b> content.
	 */
	int CONTENT_FILE = 0x03;

	/**
	 * Defines an annotation for determining set of content types for
	 * {@link OnContentLoadingListener#onContentLoadingStarted(String, int)} and
	 * {@link OnContentLoadingListener#onContentLoadingFinished(String, int)} callbacks.
	 */
	@IntDef({CONTENT_EMPTY, CONTENT_HTML, CONTENT_URL, CONTENT_FILE})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface ContentType {}

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that can receive callbacks about started or finished loading of content into web view
	 * and also callback with change in progress of content's loading process.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface OnContentLoadingListener {

		/**
		 * Invoked whenever loading of the specified <var>content</var> has been started.
		 *
		 * @param content     The content of which loading just started.
		 * @param contentType Type of the specified content.
		 */
		void onContentLoadingStarted(@NonNull String content, @ContentType int contentType);

		/**
		 * Invoked whenever the loading progress has been changed.
		 *
		 * @param progress The current loading progress.
		 */
		void onLoadingProgressChange(@IntRange(from = 0, to = 100) int progress);

		/**
		 * Invoked whenever loading of the specified <var>content</var> has been finished.
		 *
		 * @param content     The content of which loading just finished.
		 * @param contentType Type of the specified content.
		 */
		void onContentLoadingFinished(@NonNull String content, @ContentType int contentType);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a web view client for this dialog view.
	 *
	 * @param client The desired WebView client. May be {@code null} to clear the current one.
	 * @see #getWebViewClient()
	 * @see #setWebChromeClient(WebChromeClient)
	 */
	void setWebViewClient(@Nullable WebViewClient client);

	/**
	 * Returns the web view client specified for this dialog view.
	 *
	 * @return Current WebView client or {@code null} if no client has been specified.
	 * @see #setWebViewClient(WebViewClient)
	 */
	@Nullable WebViewClient getWebViewClient();

	/**
	 * Sets a web chrome client for this dialog view.
	 *
	 * @param chromeClient The desired WebChrome client. May be {@code null} to clear the current one.
	 * @see #getWebChromeClient()
	 * @see #setWebViewClient(WebViewClient)
	 */
	void setWebChromeClient(@Nullable WebChromeClient chromeClient);

	/**
	 * Returns the web chrome client specified for this dialog view.
	 *
	 * @return Current WebChrome client or {@code null} if no client has been specified.
	 * @see #setWebChromeClient(WebChromeClient)
	 */
	@Nullable WebChromeClient getWebChromeClient();

	/**
	 * Sets a boolean flag indicating whether to enable Java-Script for this dialog view's {@link android.webkit.WebView WebView}
	 * or not.
	 *
	 * @param enabled {@code True} to enable Java-Script, {@code false} to disable it.
	 * @see #isJavaScriptEnabled()
	 */
	void setJavaScriptEnabled(boolean enabled);

	/**
	 * Returns a boolean flag indicating whether the Java-Script is enabled or not.
	 * <p>
	 * Default value: <b>true</b>
	 *
	 * @return {@code True} if Java-Script is enabled, {@code false} otherwise.
	 * @see #setJavaScriptEnabled(boolean)
	 */
	boolean isJavaScriptEnabled();

	/**
	 * Registers a callback to be invoked whenever loading of current content passed to this dialog
	 * view has been started/finished or its loading progress has changed.
	 *
	 * @param listener Listener callback. May be {@code null} to clear the current one.
	 */
	void setOnContentLoadingListener(@Nullable OnContentLoadingListener listener);

	/**
	 * Sets a boolean flag indicating whether to show loading for this dialog view or not.
	 *
	 * @param visible {@code True} to show loading, {@code false} to hide it.
	 * @see #isLoadingVisible()
	 * @see #setOnContentLoadingListener(OnContentLoadingListener)
	 */
	void setLoadingVisible(boolean visible);

	/**
	 * Returns a boolean flag indicating whether the loading is visible or not.
	 *
	 * @return {@code True} if loading is visible, {@code false} otherwise.
	 */
	boolean isLoadingVisible();

	/**
	 * Same as {@link #loadContent(String)} for resource id.
	 *
	 * @param resId Resource id of the desired content to load.
	 * @see #getContent()
	 * @see #getContentType()
	 * @see #setOnContentLoadingListener(OnContentLoadingListener)
	 */
	void loadContent(@StringRes int resId);

	/**
	 * Loads the specified <var>content</var> into the WebView of this dialog view.
	 *
	 * @param content The desired content to load. This can be either a <b>raw Html</b>, <b>web Url</b>
	 *                or <b>Uri to file with raw Html</b>.
	 * @see #getContent()
	 * @see #getContentType()
	 * @see #setOnContentLoadingListener(OnContentLoadingListener)
	 */
	void loadContent(@NonNull String content);

	/**
	 * Returns the current content loaded into the WebView of this dialog view.
	 *
	 * @return Web content or empty string if not content has been loaded yet.
	 * @see #loadContent(int)
	 * @see #loadContent(String)
	 * @see #getContentType()
	 */
	@NonNull String getContent();

	/**
	 * Returns the type of the current content loaded into WebView of this dialog view.
	 *
	 * @return One of {@link #CONTENT_HTML}, {@link #CONTENT_URL}, {@link #CONTENT_FILE} or
	 * {@link #CONTENT_EMPTY} if no content has been loaded yet.
	 * @see #loadContent(int)
	 * @see #loadContent(String)
	 */
	@ContentType int getContentType();

	/**
	 * Called to dispatch back press event to this dialog view.
	 *
	 * @return {@code True} if this dialog view has processed the dispatched event, {@code false} otherwise.
	 */
	boolean dispatchBackPress();
}