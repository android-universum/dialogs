/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Message;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;

import androidx.annotation.NonNull;

/**
 * A {@link WebChromeClient} implementation used by {@link WebDialogContentView} to wrap a single
 * WebChromeClient and dispatch all received callbacks to the wrapped client and some of them to
 * the DialogWebContent by which is this wrapper used.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class WebChromeClientWrapper extends WebChromeClient {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "WebChromeClientWrapper";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Web dialog body view to which is this wrapper attached.
     */
    private final WebDialogContentView mWebDialogContent;

    /**
     * Wrapped instance of WebChromeClient.
     */
    WebChromeClient mClient;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of WebChromeClientWrapper.
     *
     * @param dialogContent Web dialog body to which will be this wrapper attached.
     */
    WebChromeClientWrapper(final WebDialogContentView dialogContent) {
        this.mWebDialogContent = dialogContent;
    }

    /*
     * Methods =====================================================================================
     */

    /**
     */
    @Override public void onProgressChanged(@NonNull final WebView view, final int newProgress) {
        super.onProgressChanged(view, newProgress);
        if (mWebDialogContent != null) {
            mWebDialogContent.handleProgressChanged(newProgress);
        }
        if (mClient != null) {
            mClient.onProgressChanged(view, newProgress);
        }
    }

    /**
     */
    @Override public void onReceivedTitle(@NonNull final WebView view, @NonNull final String title) {
        super.onReceivedTitle(view, title);
        if (mClient != null) mClient.onReceivedTitle(view, title);
    }

    /**
     */
    @Override public void onReceivedIcon(@NonNull final WebView view, @NonNull final Bitmap icon) {
        super.onReceivedIcon(view, icon);
        if (mClient != null) mClient.onReceivedIcon(view, icon);
    }

    /**
     */
    @Override public void onReceivedTouchIconUrl(@NonNull final WebView view, @NonNull final String url, final boolean precomposed) {
        super.onReceivedTouchIconUrl(view, url, precomposed);
        if (mClient != null) mClient.onReceivedTouchIconUrl(view, url, precomposed);
    }

    /**
     */
    @Override public void onShowCustomView(@NonNull final View view, @NonNull final CustomViewCallback callback) {
        super.onShowCustomView(view, callback);
        if (mClient != null) mClient.onShowCustomView(view, callback);
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public void onShowCustomView(@NonNull final View view, final int requestedOrientation, @NonNull final CustomViewCallback callback) {
        super.onShowCustomView(view, requestedOrientation, callback);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && mClient != null) {
            mClient.onShowCustomView(view, requestedOrientation, callback);
        }
    }

    /**
     */
    @Override public void onHideCustomView() {
        super.onHideCustomView();
        if (mClient != null) mClient.onHideCustomView();
    }

    /**
     */
    @Override public boolean onCreateWindow(
    		@NonNull final WebView view,
		    final boolean isDialog,
		    final boolean isUserGesture,
		    @NonNull final Message resultMsg
    ) {
        return (mClient != null && mClient.onCreateWindow(view, isDialog, isUserGesture, resultMsg)) ||
                super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
    }

    /**
     */
    @Override public void onRequestFocus(@NonNull final WebView view) {
        super.onRequestFocus(view);
        if (mClient != null) mClient.onRequestFocus(view);
    }

    /**
     */
    @Override public void onCloseWindow(@NonNull final WebView window) {
        super.onCloseWindow(window);
        if (mClient != null) mClient.onCloseWindow(window);
    }

    /**
     */
    @Override public boolean onJsAlert(
    		@NonNull final WebView view,
		    @NonNull final String url,
		    @NonNull final String content,
		    @NonNull final JsResult result
    ) {
        return (mClient != null && mClient.onJsAlert(view, url, content, result)) ||
                super.onJsAlert(view, url, content, result);
    }

    /**
     */
    @Override public boolean onJsConfirm(
    		@NonNull final WebView view,
		    @NonNull final String url,
		    @NonNull final String content,
		    @NonNull final JsResult result
    ) {
        return (mClient != null && mClient.onJsConfirm(view, url, content, result)) ||
                super.onJsConfirm(view, url, content, result);
    }

    /**
     */
    @Override public boolean onJsPrompt(
		    @NonNull final WebView view,
		    @NonNull final String url,
		    @NonNull final String content,
		    @NonNull final String defaultValue,
		    @NonNull final JsPromptResult result
    ) {
        return (mClient != null && mClient.onJsPrompt(view, url, content, defaultValue, result)) ||
                super.onJsPrompt(view, url, content, defaultValue, result);
    }

    /**
     */
    @Override public boolean onJsBeforeUnload(
    		@NonNull final WebView view,
		    @NonNull final String url,
		    @NonNull final String content,
		    @NonNull final JsResult result
    ) {
        return (mClient != null && mClient.onJsBeforeUnload(view, url, content, result)) ||
                super.onJsBeforeUnload(view, url, content, result);
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public void onExceededDatabaseQuota(
		    @NonNull final String url,
		    @NonNull final String databaseIdentifier,
		    final long quota,
		    final long estimatedDatabaseSize,
		    final long totalQuota,
		    @NonNull final WebStorage.QuotaUpdater quotaUpdater
    ) {
        super.onExceededDatabaseQuota(url, databaseIdentifier, quota, estimatedDatabaseSize, totalQuota, quotaUpdater);
        if (mClient != null) mClient.onExceededDatabaseQuota(url, databaseIdentifier, quota, estimatedDatabaseSize, totalQuota, quotaUpdater);
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public void onReachedMaxAppCacheSize(final long requiredStorage, final long quota, @NonNull final WebStorage.QuotaUpdater quotaUpdater) {
        super.onReachedMaxAppCacheSize(requiredStorage, quota, quotaUpdater);
        if (mClient != null) mClient.onReachedMaxAppCacheSize(requiredStorage, quota, quotaUpdater);
    }

    /**
     */
    @Override public void onGeolocationPermissionsShowPrompt(@NonNull final String origin, @NonNull final GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(origin, callback);
        if (mClient != null) mClient.onGeolocationPermissionsShowPrompt(origin, callback);
    }

    /**
     */
    @Override public void onGeolocationPermissionsHidePrompt() {
        super.onGeolocationPermissionsHidePrompt();
        if (mClient != null) mClient.onGeolocationPermissionsHidePrompt();
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public boolean onJsTimeout() {
        return (mClient != null && mClient.onJsTimeout()) || super.onJsTimeout();
    }

    /**
     */
    @SuppressWarnings("deprecation")
    @Override public void onConsoleMessage(@NonNull final String content, final int lineNumber, @NonNull final String sourceId) {
        super.onConsoleMessage(content, lineNumber, sourceId);
        if (mClient != null) mClient.onConsoleMessage(content, lineNumber, sourceId);
    }

    /**
     */
    @Override public boolean onConsoleMessage(@NonNull final ConsoleMessage consoleMessage) {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO && mClient != null && mClient.onConsoleMessage(consoleMessage)) ||
                super.onConsoleMessage(consoleMessage);
    }

    /**
     */
    @Override public Bitmap getDefaultVideoPoster() {
        return mClient != null ?
                mClient.getDefaultVideoPoster() :
                super.getDefaultVideoPoster();
    }

    /**
     */
    @Override public View getVideoLoadingProgressView() {
        return mClient != null ?
                mClient.getVideoLoadingProgressView() :
                super.getVideoLoadingProgressView();
    }

    /**
     */
    @Override public void getVisitedHistory(@NonNull final ValueCallback<String[]> callback) {
        super.getVisitedHistory(callback);
        if (mClient != null) mClient.getVisitedHistory(callback);
    }

    /*
     * Inner classes ===============================================================================
     */
}