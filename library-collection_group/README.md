@Dialogs-Collection
===============

This module groups the following modules into one **single group**:

- [Collection-Core](https://bitbucket.org/android-universum/dialogs/src/main/library-collection-core)
- [Collection-Grid](https://bitbucket.org/android-universum/dialogs/src/main/library-collection-grid)
- [Collection-List](https://bitbucket.org/android-universum/dialogs/src/main/library-collection-list)
- [Collection-Recycler](https://bitbucket.org/android-universum/dialogs/src/main/library-collection-recycler)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adialogs/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adialogs/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:dialogs-collection:${DESIRED_VERSION}@aar"
