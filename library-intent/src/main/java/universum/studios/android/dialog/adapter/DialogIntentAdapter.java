/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.adapter;

import android.content.Intent;
import android.widget.ListAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.IntentDialog;
import universum.studios.android.dialog.intent.IntentAssistant;
import universum.studios.android.dialog.intent.IntentCandidate;

/**
 * Required interface for adapters that are tend to be used within {@link IntentDialog IntentDialog}
 * to provide data set of intent candidates for such a dialog.
 * <p>
 * The IntentDialog will use {@link #getItem(int)} to access intent action for a clicked position and
 * {@link #changeIntent(Intent)} to pass an instance of {@link Intent} that should be
 * used by the adapter to filter appropriate candidates for it.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface DialogIntentAdapter extends ListAdapter {

    /*
     * Listeners ===================================================================================
     */

    /**
     * Listener that can receive a callback whenever a set of filtered intent candidates is ready.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface OnCandidatesListener {

        /**
         * Invoked whenever {@link #changeIntent(Intent)} has been called upon an
         * instance of IntentAdapter to which is this listener attached and candidates were successfully
         * filtered for that particular intent.
         *
         * @param candidates List with filtered intent candidates which are now available by the adapter.
         */
        void onIntentCandidatesReady(@NonNull List<IntentCandidate> candidates);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Registers a callback to be invoked whenever intent candidates for this adapter are ready after
     * {@link #changeIntent(Intent)} has been called.
     *
     * @param listener Listener callback.
     */
    void setOnCandidatesListener(@NonNull OnCandidatesListener listener);

    /**
     * Changes the intent of this adapter. This will cause to be data set of this adapter changed and
     * populated with {@link universum.studios.android.dialog.intent.IntentCandidate}s filtered
     * for the specified <var>intent</var>.
     *
     * @param intent The desired intent for which to filter intent candidates.
     * @see IntentAssistant#filterCandidates(Intent, android.content.Context)
     * @see #setOnCandidatesListener(OnCandidatesListener)
     */
    void changeIntent(@NonNull Intent intent);

    /**
     * Returns the IntentCandidate for the specified <var>position</var>.
     *
     * @param position Position of the desired candidate within this adapter's data set.
     * @return IntentCandidate at the specified position or {@code null} if there is no candidate at
     * the requested position.
     */
    @Nullable IntentCandidate getItem(int position);
}