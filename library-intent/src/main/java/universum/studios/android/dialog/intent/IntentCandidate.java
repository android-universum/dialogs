/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.intent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * The IntentCandidate represents an abstraction for item that can be registered in {@link IntentAssistant}
 * in order to allow filtering of desired (registered) candidates for a particular {@link Intent}.
 * <p>
 * Basically, this class holds only pattern containing package name/-s of an external {@link Activity}.
 * The package pattern is used to identify (target) a specific candidate when the IntentAssistant is
 * running filtering process to find all candidates for the specific Intent.
 * <p>
 * This class also provides API allowing to start the represented Activity via {@link #startActivity(Context, Intent)}
 * or {@link #startActivityForResult(Activity, Intent, int)} with the
 * desired intent. Also Activity's (application's) data like icon and label can be loaded via
 * {@link #loadActivityIcon(PackageManager)} and {@link #loadActivityLabel(PackageManager)}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("unused")
public class IntentCandidate {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "IntentAction";

    /**
     * Regular expression to match package of the <b>Google Documents</b> intent action.
     */
    public static final String GOOGLE_DOCS = "^com\\.google\\.android\\.apps\\.docs$";

    /**
     * Regular expression to match package of the <b>Google GMail</b> intent action.
     */
    public static final String GOOGLE_GMAIL = "^com\\.google\\.android\\.gm$";

    /**
     * Regular expression to match package of the <b>Google+</b> intent action.
     */
    public static final String GOOGLE_PLUS = "^com\\.google\\.android\\.apps\\.plus";

    /**
     * Regular expression to match package of the <b>Google Keep</b> intent action.
     */
    public static final String GOOGLE_KEEP = "^com\\.google\\.android\\.keep$";

    /**
     * Regular expression to match package of the <b>Instagram</b> intent action.
     */
    public static final String INSTAGRAM = "^com\\.instagram\\.android$";

    /**
     * Regular expression to match package of the <b>Dropbox</b> intent action.
     */
    public static final String DROPBOX = "^com\\.dropbox\\.android$";

    /**
     * Regular expression to match package of the <b>LinkedIn</b> intent action.
     */
    public static final String LINKEDIN = "^com\\.linked\\.android$";

    /**
     * Regular expression to match package of the <b>Twitter</b> intent action.
     */
    public static final String TWITTER = "^com\\.twitter\\.android$";

    /**
     * Regular expression to match package of the <b>Facebook</b> intent action.
     */
    public static final String FACEBOOK = "^com\\.facebook\\.katana$";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Instance of package manager used to obtain data from this action's activity info.
     */
    private Matcher mPackageMatcher;

    /**
     * Activity info attached to this action.
     */
    private ActivityInfo mActivityInfo;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of IntentAction with the specified <var>packagePattern</var>.
     *
     * @param packagePattern The pattern used when calling {@link #matches(String)} upon an instance
     *                       of this action.
     */
    public IntentCandidate(@NonNull final String packagePattern) {
        this.mPackageMatcher = Pattern.compile(packagePattern).matcher("");
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns a flag indicating whether the specified <var>packageName</var> matches with the package
     * pattern of this intent candidate or not.
     *
     * @param packageName The desired package name to check if matches.
     * @return {@code True} if package name matches, {@code false} otherwise.
     */
    public boolean matches(@NonNull final String packageName) {
        return mPackageMatcher.reset(packageName.toLowerCase()).matches();
    }

    /**
     * Starts the given <var>intent</var> with package name of the currently attached ActivityInfo to
     * this candidate.
     * <p>
     * <b>Note</b>, that ActivityInfo is attached to this candidate only if this candidate passes the
     * filter process of {@link IntentAssistant} with package
     * name pattern passed to this candidate during initialization.
     *
     * @param caller Context used to start the requested intent.
     * @param intent Intent to be started.
     * @return {@code True} if intent was started, {@code false} if the current ActivityInfo is invalid.
     * @see #startActivityForResult(Activity, Intent, int)
     */
    public boolean startActivity(@NonNull final Context caller, @NonNull final Intent intent) {
        if (mActivityInfo != null) {
            intent.setPackage(mActivityInfo.packageName);
            caller.startActivity(intent);
            return true;
        }
        return false;
    }

    /**
     * Same as {@link #startActivity(Context, Intent)} but for Fragment context.
     */
    public boolean startActivity(@NonNull final Fragment caller, @NonNull final Intent intent) {
        if (mActivityInfo != null) {
            intent.setPackage(mActivityInfo.packageName);
            caller.startActivity(intent);
            return true;
        }
        return false;
    }

    /**
     * Starts the given <var>intent</var> for result with package name of the currently attached
     * ActivityInfo to this candidate.
     * <p>
     * <b>Note</b>, that ActivityInfo is attached to this candidate only if this candidate passes the
     * filter process of {@link IntentAssistant} with package
     * name pattern passed to this candidate during initialization.
     *
     * @param caller      Activity used to start the requested intent for result.
     * @param intent      Intent to be started for result.
     * @param requestCode A request code for the starting intent for result.
     * @return {@code True} if activity was started, {@code false} if the current ActivityInfo is invalid.
     * @see #startActivity(Context, Intent)
     */
    public boolean startActivityForResult(@NonNull final Activity caller, @NonNull final Intent intent, final int requestCode) {
        if (mActivityInfo != null) {
            intent.setPackage(mActivityInfo.packageName);
            caller.startActivityForResult(intent, requestCode);
            return true;
        }
        return false;
    }

    /**
     * Same as {@link #startActivityForResult(Activity, Intent, int)},
     * but for Fragment context.
     */
    public boolean startActivityForResult(@NonNull final Fragment caller, @NonNull final Intent intent, final int requestCode) {
        if (mActivityInfo != null) {
            intent.setPackage(mActivityInfo.packageName);
            caller.startActivityForResult(intent, requestCode);
            return true;
        }
        return false;
    }

    /**
     * Loads a label from the currently attached ActivityInfo to this candidate.
     * <p>
     * <b>Note</b>, that ActivityInfo is attached to this candidate only if this candidate passes the
     * filter process of {@link IntentAssistant} with package
     * name pattern passed to this candidate during initialization.
     *
     * @param packageManager An instance of package manager used to obtain the requested label.
     * @return Label text or {@code null} if the current ActivityInfo is invalid.
     * @see #loadActivityIcon(PackageManager)
     */
    @Nullable public CharSequence loadActivityLabel(@NonNull final PackageManager packageManager) {
        return mActivityInfo != null ? mActivityInfo.loadLabel(packageManager) : null;
    }

    /**
     * Loads an icon from the currently attached ActivityInfo to this candidate.
     * <p>
     * <b>Note</b>, that ActivityInfo is attached to this candidate only if this candidate passes the
     * filter process of {@link IntentAssistant} with package
     * name pattern passed to this candidate during initialization.
     *
     * @param packageManager An instance of package manager used to obtain the requested icon.
     * @return Icon drawable or {@code null} if the current ActivityInfo is invalid.
     * @see #loadActivityLabel(PackageManager)
     */
    @Nullable public Drawable loadActivityIcon(@NonNull final PackageManager packageManager) {
        return mActivityInfo != null ? mActivityInfo.loadIcon(packageManager) : null;
    }

    /**
     * Attaches the given <var>activityInfo</var> to this candidate.
     *
     * @param activityInfo Activity info to be attached to this candidate.
     */
    void attachActivityInfo(final ActivityInfo activityInfo) {
        this.mActivityInfo = activityInfo;
    }

    /*
     * Inner classes ===============================================================================
     */
}