/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import androidx.annotation.AttrRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.fragment.app.Fragment;
import universum.studios.android.dialog.adapter.DialogBaseAdapter;
import universum.studios.android.dialog.adapter.DialogIntentAdapter;
import universum.studios.android.dialog.adapter.DialogViewHolder;
import universum.studios.android.dialog.intent.IntentAssistant;
import universum.studios.android.dialog.intent.IntentCandidate;

/**
 * An {@link AdapterDialog} implementation that can be used to allow to a user to pick from a set of
 * applications (already installed) that can handle a specific intent (share, maps, ...).
 * <p>
 * Whether the IntentDialog should show a list or a grid of applications can be specified through
 * {@link IntentOptions} via {@link IntentOptions#adapterViewType(int)}.
 * <p>
 * See {@link IntentOptions} for options that can be supplied to the IntentDialog
 * from the desired context via {@link #newInstance(IntentOptions)} or via
 * {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogIntentOptions dialogIntentOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class IntentDialog extends AdapterDialog<AdapterView> {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "IntentDialog";

    /**
     * Type flag to request {@link android.widget.ListView} as adapter view for IntentDialog.
     */
    public static final int LIST = 0x01;

    /**
     * Type flag to request {@link android.widget.GridView} as adapter view for IntentDialog.
     */
    public static final int GRID = 0x02;

    /**
     * Defines an annotation for determining set of allowed flags for requesting adapter view type.
     */
    @IntDef({LIST, GRID})
    @Retention(RetentionPolicy.SOURCE)
    public @interface AdapterViewType {}

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of IntentDialog with {@link android.widget.ListView} or {@link android.widget.GridView}
     * to present data set of {@link IntentCandidate}s.
     */
    public IntentDialog() {
        super(R.attr.dialogIntentOptions);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Creates a new instance of IntentDialog with the specified options.
     *
     * @param options Specific options for this type of dialog.
     * @return New IntentDialog instance.
     */
    @NonNull public static IntentDialog newInstance(@NonNull final IntentOptions options) {
        final IntentDialog dialog = new IntentDialog();
        dialog.setOptions(options);
        return dialog;
    }

    /**
     */
    @Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
        return new IntentOptions(resources);
    }

    /**
     */
    @SuppressWarnings("ResourceType")
    @Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
        super.onProcessOptionsStyle(context, optionsStyle);
        if (mOptions instanceof IntentOptions) {
            final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Intent);
	        final IntentOptions options = (IntentOptions) mOptions;
	        for (int i = 0; i < attributes.getIndexCount(); i++) {
		        int index = attributes.getIndex(i);
		        if (index == R.styleable.Dialog_Options_Intent_dialogAdapterViewType) {
			        if (!options.isSet(IntentOptions.ADAPTER_VIEW_TYPE)) {
				        options.adapterViewType(attributes.getInteger(index, options.adapterViewType));
			        }
		        }
	        }
	        attributes.recycle();
        }
    }

    /**
     */
    @Override @NonNull protected View onCreateContentView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        if (mOptions instanceof IntentOptions) {
            switch (((IntentOptions) mOptions).adapterViewType) {
                case GRID:
                    return inflater.inflate(R.layout.dialog_content_grid, container, false);
            }
        }
        return inflater.inflate(R.layout.dialog_content_list, container, false);
    }

    /**
     */
    @Override protected void onBindView(
    		@NonNull final View dialogView,
		    @NonNull final DialogOptions options,
		    @Nullable final Bundle savedInstanceState
    ) {
        super.onBindView(dialogView, options, savedInstanceState);
        if (options instanceof IntentOptions) {
            final IntentOptions intentOptions = (IntentOptions) options;
            // Ensure adapter.
            DialogIntentAdapter adapter = null;
            if (mAdapter instanceof DialogIntentAdapter) {
                adapter = (DialogIntentAdapter) mAdapter;
            } else if (mAdapter == null) {
                // Create default adapter.
                final Adapter defaultAdapter = new Adapter(getContext(), mLayoutInflater);
                defaultAdapter.adapterViewType = intentOptions.adapterViewType;
                setAdapter(adapter = defaultAdapter);
            }
            if (adapter != null && adapter.getCount() == 0) {
                if (intentOptions.intent == null) {
                    throw new IllegalStateException("No intent specified within the current IntentOptions.");
                }
                adapter.setOnCandidatesListener(new DialogIntentAdapter.OnCandidatesListener() {

	                /**
                     */
                    @Override
                    public void onIntentCandidatesReady(@NonNull List<IntentCandidate> candidates) {
                        setIndeterminateProgressBarVisible(false);
                    }
                });
                // Request to show available applications which can handle the desired intent.
                setIndeterminateProgressBarVisible(true);
                adapter.changeIntent(intentOptions.intent);
            }
        }
    }

    /**
     */
    @Override protected boolean onItemClick(
    		@NonNull final AdapterView<?> parent,
		    @NonNull final View view,
		    final int position,
		    final long id
    ) {
        if (mAdapter instanceof DialogIntentAdapter && mOptions instanceof IntentOptions) {
            final IntentCandidate intentAction = ((DialogIntentAdapter) mAdapter).getItem(position);
            if (intentAction != null) {
                final Fragment contextFragment = findContextFragment();
                final IntentOptions options = (IntentOptions) mOptions;
                if (options.requestCode >= 0) {
                    if (contextFragment != null) {
                        intentAction.startActivityForResult(contextFragment, options.intent, options.requestCode);
                    } else {
                        intentAction.startActivityForResult(getActivity(), options.intent, options.requestCode);
                    }
                } else {
                    if (contextFragment != null) {
                        intentAction.startActivity(contextFragment, options.intent);
                    } else {
                        intentAction.startActivity(getActivity(), options.intent);
                    }
                }
                dismiss();
            }
        }
        return super.onItemClick(parent, view, position, id);
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link BaseAdapterOptions} implementation specific for the {@link IntentDialog}.
     * <p>
     * Below are listed setters which may be used to supply desired data to {@link IntentOptions}
     * through these options:
     * <ul>
     * <li>{@link #intent(Intent)}</li>
     * <li>{@link #requestCode(int)}</li>
     * <li>{@link #adapterViewType(int)}</li>
     * </ul>
     * <p>
     * <h3>Xml attributes</h3>
     * See {@link R.styleable#Dialog_Options_Intent IntentOptions Attributes},
     * {@link R.styleable#Dialog_Options DialogOptions Attributes}
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    @SuppressWarnings("ResourceType")
    public static class IntentOptions extends BaseAdapterOptions<IntentOptions> {

        /**
         * Creator used to create an instance or array of instances of IntentOptions from {@link Parcel}.
         */
        public static final Parcelable.Creator<IntentOptions> CREATOR = new Parcelable.Creator<IntentOptions>() {

	        /**
             */
            @Override public IntentOptions createFromParcel(@NonNull final Parcel source) { return new IntentOptions(source); }

            /**
             */
            @Override public IntentOptions[] newArray(final int size) { return new IntentOptions[size]; }
        };

        /**
         * Flag for <b>intent</b> parameter.
         */
        static final int INTENT = 0x00000001;

        /**
         * Flag for <b>adapter view type</b> parameter.
         */
        static final int ADAPTER_VIEW_TYPE = 0x00000001 << 1;

        /**
         * Flag for <b>request code</b> parameter.
         */
        static final int REQUEST_CODE = 0x00000001 << 2;

        /**
         * Intent used to filter set of {@link IntentCandidate}s which should be available for a user
         * within IntentDialog.
         */
        Intent intent;

        /**
         * Type of the AdapterView which should present IntentDialog actions data set.
         */
        int adapterViewType = LIST;

        /**
         * In none negative, this code will be used when starting activity for result by
         * {@link android.app.Activity#startActivityForResult(Intent, int)} or
         * {@link Fragment#startActivityForResult(Intent, int)}.
         */
        private int requestCode = -1;

        /**
         * Creates a new instance of IntentOptions.
         */
        public IntentOptions() {
            super();
        }

        /**
         * Creates a new instance of IntentOptions.
         *
         * @param resources Application resources used to obtain values for these options when they
         *                  are requested by theirs resource ids.
         */
        public IntentOptions(@NonNull final Resources resources) {
            super(resources);
        }

        /**
         * Called from {@link #CREATOR} to create an instance of IntentOptions from the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected IntentOptions(@NonNull final Parcel source) {
            super(source);
            this.intent = source.readParcelable(DialogsConfig.class.getClassLoader());
            this.adapterViewType = source.readInt();
            this.requestCode = source.readInt();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(intent, flags);
            dest.writeInt(adapterViewType);
            dest.writeInt(requestCode);
        }

        /**
         */
        @Override public IntentOptions merge(@NonNull final DialogOptions options) {
            if (!(options instanceof IntentOptions)) {
                return super.merge(options);
            }
            final IntentOptions intentOptions = (IntentOptions) options;
            if (intentOptions.isSet(INTENT)) {
                if (intentOptions.intent != null) {
                    this.intent = new Intent(intentOptions.intent);
                } else {
                    this.intent = null;
                }
                updateIsSet(INTENT);
            }
            if (intentOptions.isSet(ADAPTER_VIEW_TYPE)) {
                this.adapterViewType = intentOptions.adapterViewType;
                updateIsSet(ADAPTER_VIEW_TYPE);
            }
            if (intentOptions.isSet(REQUEST_CODE)) {
                this.requestCode = intentOptions.requestCode;
                updateIsSet(REQUEST_CODE);
            }
            return super.merge(options);
        }

        /**
         * Sets the intent used when filtering {@link IntentCandidate}s which will be presented within
         * AdapterView of IntentDialog instance.
         *
         * @param intent The desired intent with predefined parameters.
         * @return These options to allow methods chaining.
         * @see #intent()
         */
        public IntentOptions intent(@NonNull final Intent intent) {
            this.intent = intent;
            updateIsSet(INTENT);
            notifyChanged();
            return this;
        }

        /**
         * Returns the intent ,set to these options.
         *
         * @return Intent used to filter IntentActions or {@code null} by default.
         * @see #intent(Intent)
         */
        @Nullable public Intent intent() {
            return intent;
        }

        /**
         * Sets the request code indicating that a selected IntentAction within IntentDialog
         * instance should be started as activity for result by {@link android.app.Activity#startActivityForResult(Intent, int)}
         * or {@link Fragment#startActivityForResult(Intent, int)},
         * depends on within which context will be IntentDialog shown.
         * <p>
         * <b>Note</b>, that use <b>none-negative</b> request code to set up one, <b>negative</b>
         * to cancel the current one.
         *
         * @param code The desired request code.
         * @return These options to allow methods chaining.
         * @see #requestCode()
         */
        public IntentOptions requestCode(final int code) {
            updateIsSet(REQUEST_CODE);
            if (requestCode != code) {
                this.requestCode = code;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the request code, set to these options.
         *
         * @return Request code for <b>startActivityForResult(...)</b> call or {@code -1} by default.
         * @see #requestCode(int)
         */
        public int requestCode() {
            return requestCode;
        }

        /**
         * Sets the type of adapter view which should be created to present set of {@link IntentCandidate}s
         * within a dialog associated with these options.
         *
         * @param type The desired type. One of {@link #LIST} or {@link #GRID}.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogAdapterViewType dialog:dialogAdapterViewType
         * @see #adapterViewType()
         */
        public IntentOptions adapterViewType(@AdapterViewType final int type) {
            updateIsSet(ADAPTER_VIEW_TYPE);
            switch (type) {
                case LIST:
                case GRID:
                    if (adapterViewType != type) {
                        this.adapterViewType = type;
                        notifyChanged();
                    }
            }
            return this;
        }

        /**
         * Returns the adapter view type flag, set to these options.
         *
         * @return One of {@link #LIST} or {@link #GRID}. Returns {@link #LIST LIST} by default.
         * @see #adapterViewType(int)
         */
        @AdapterViewType public int adapterViewType() {
            return adapterViewType;
        }

        /**
         */
        @Override protected void onParseXmlAttribute(
        		@NonNull final XmlResourceParser xmlParser,
		        final int index,
		        @AttrRes final int attr,
		        @NonNull final Resources resources,
		        @Nullable final Resources.Theme theme
        ) {
            if (attr == R.attr.dialogAdapterViewType) {
                adapterViewType(xmlParser.getAttributeIntValue(index, adapterViewType));
            } else {
                super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
            }
        }
    }

    /**
     * A {@link DialogIntentAdapter} default implementation used by {@link IntentDialog} to populate
     * its adapter view with data set of {@link IntentCandidate IntentCandidates}, filtered for an
     * intent supplied via {@link #changeIntent(Intent)}.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class Adapter extends DialogBaseAdapter<IntentCandidate, ViewHolder> implements DialogIntentAdapter {

        /**
         * Defines the count of view types provided by this adapter by {@link #getView(int, View, ViewGroup)}.
         */
        public static final int VIEW_TYPE_COUNT = 2;

        /**
         * View type indicating view for list item.
         */
        public static final int VIEW_TYPE_ITEM_LIST = 0x00;

        /**
         * View type indicating view for grid item.
         */
        public static final int VIEW_TYPE_ITEM_GRID = 0x01;

        /**
         * Assistant used to filter IntentActions based on the current Intent of this adapter.
         */
        protected final IntentAssistant INTENT_ASSISTANT = new IntentAssistant();

        /**
         * Filtering callback for {@link #INTENT_ASSISTANT}.
         */
        private final IntentAssistant.FilterCallback INTENT_FILTER_CALLBACK = new IntentAssistant.FilterCallback() {

            /**
             */
            @Override public void onIntentCandidatesFiltered(@NonNull final List<IntentCandidate> candidates, @NonNull final Intent intent) {
                changeItems(candidates);
                if (actionsListener != null) {
                    actionsListener.onIntentCandidatesReady(candidates);
                }
            }
        };

        /**
         * Package manager used to obtain data about intent action activities.
         */
        protected final PackageManager packageManager;

        /**
         * Callback to be notified when the current data set of IntentActions has been successfully
         * filtered.
         */
        private OnCandidatesListener actionsListener;

        /**
         * Type of the AdapterView for which provides this adapter data set.
         */
        private int adapterViewType;

        /**
         * Creates a new instance of IntentActionsAdapter to provide data set of IntentActions.
         *
         * @param context  Context in which will be this adapter presented.
         * @param inflater Layout inflater which can be used to inflate views for this adapter.
         */
        Adapter(@NonNull final Context context, @NonNull final LayoutInflater inflater) {
            super(context, inflater);
            this.packageManager = context.getPackageManager();
        }

        /**
         */
        @Override public void changeIntent(@NonNull final Intent intent) {
            INTENT_ASSISTANT.asyncFilterCandidates(intent, mContext, INTENT_FILTER_CALLBACK);
        }

        /**
         */
        @Override public void setOnCandidatesListener(@NonNull final OnCandidatesListener listener) {
            this.actionsListener = listener;
        }

        /**
         */
        @Override public int getViewTypeCount() {
            return VIEW_TYPE_COUNT;
        }

        /**
         */
        @Override public int getItemViewType(final int position) {
            return adapterViewType == LIST ? VIEW_TYPE_ITEM_LIST : VIEW_TYPE_ITEM_GRID;
        }

        /**
         */
        @Override @NonNull protected View onCreateView(@NonNull final ViewGroup parent, final int position) {
            switch (currentViewType()) {
                case VIEW_TYPE_ITEM_GRID: return inflate(R.layout.dialog_item_grid_intent, parent);
                default: return inflate(R.layout.dialog_item_list_intent, parent);
            }
        }

        /**
         */
        @Override @Nullable protected ViewHolder onCreateViewHolder(@NonNull final View view, final int position) {
            return new ViewHolder(view);
        }

        /**
         */
        @Override protected void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final IntentCandidate candidate = getItem(position);
            viewHolder.icon.setImageDrawable(candidate.loadActivityIcon(packageManager));
            viewHolder.label.setText(candidate.loadActivityLabel(packageManager));
        }
    }

    /**
     * A {@link DialogViewHolder} implementation for item views of {@link Adapter}.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class ViewHolder extends DialogViewHolder {

        /**
         * Label view hold by this holder for better access.
         */
        public final TextView label;

        /**
         * Icon view hold by this holder for better access.
         */
        public final ImageView icon;

        /**
         * Creates a new ViewHolder for intent item view.
         */
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            this.label = (TextView) itemView.findViewById(R.id.dialog_text_view_item_label);
            this.icon = (ImageView) itemView.findViewById(R.id.dialog_image_view_item_icon);
        }
    }
}