/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.intent;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * The IntentAssistant represents a manager that allows to filter only desired {@link IntentCandidate IntentCandidates}
 * for a specific {@link Intent}. This can be used to constrain a set of activities (choices) from
 * which can a user pick one, whenever he wants to perform an action that can be handled by multiple
 * applications on its Android device.
 * <p>
 * Set of desired candidates that should be filtered can be specified via methods listed below:
 * <ul>
 * <li>{@link #registerCandidate(IntentCandidate)}</li>
 * <li>{@link #registerCandidate(String)}</li>
 * <li>{@link #registerCandidate(Class)}</li>
 * <li>{@link #registerCandidates(IntentCandidate...)}</li>
 * <li>{@link #registerCandidates(String...)}</li>
 * <li>{@link #registerCandidates(Class[])}</li>
 * </ul>
 * To filter candidates for the desired intent, call {@link #filterCandidates(Intent, Context)}
 * or {@link #asyncFilterCandidates(Intent, Context, FilterCallback)}.
 * <p>
 * <b>Note</b>, that this is not replacement for {@link Intent#createChooser(Intent, CharSequence)}
 * method, which creates an intent that after it is started shows a dialog with all applications, that
 * can handle an action specified by such an intent, but rather an extension allowing to constrain
 * set of these items but using implementation that is not dependent on {@code Intent.createChooser(...)}
 * logic.
 * <p>
 * If you want to obtain set of default intent candidates for your desired intent and you want to
 * show these in your custom collection (not supported using {@code Intent.createChooser(...)}), you
 * can do so via {@link #filterDefaultCandidates(Intent, Context)}
 * which will return set of IntentCandidates representing items that would {@code Intent.createChooser(...)}
 * show within the mentioned chooser dialog.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class IntentAssistant {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "IntentAssistant";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener which receives a callback when filter process of the intent candidates finishes.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface FilterCallback {

		/**
		 * Invoked whenever {@link #asyncFilterDefaultCandidates(Intent, Context, FilterCallback)}
		 * or {@link #asyncFilterCandidates(Intent, Context, FilterCallback)}
		 * is called upon an instance of IntentAssistant with this callback.
		 *
		 * @param candidates List with filtered intent candidates.
		 * @param intent     Intent for which was filter process requested.
		 */
		void onIntentCandidatesFiltered(@NonNull List<IntentCandidate> candidates, @NonNull Intent intent);
	}

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Executor used to execute filter process on the background thread.
	 */
	private static ExecutorService sExecutor;

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler used to dispatch filter result on the UI thread when running filter process on the
	 * background thread.
	 */
	private Handler mFilterResultHandler;

	/**
	 * Set of intent candidates registered to this assistant.
	 */
	private List<IntentCandidate> mCandidates;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Same as {@link #registerCandidate(IntentCandidate)} where instance of intent candidate will be created
	 * using the specified <var>classOfCandidate</var>.
	 *
	 * @param classOfCandidate Class of candidate from which to create new IntentCandidate instance.
	 * @see #registerCandidates(Class[])
	 */
	public final void registerCandidate(@NonNull final Class<? extends IntentCandidate> classOfCandidate) {
		registerCandidate(createCandidate(classOfCandidate));
	}

	/**
	 * Registers an instance of {@link IntentCandidate} with the specified <var>packagePattern</var>
	 * for this assistant, so it can be used to filter intent candidates for a specific {@link Intent}
	 * whenever {@link #filterCandidates(Intent, Context)} is called.
	 *
	 * @param packagePattern The package pattern for the intent candidate to register.
	 */
	public final void registerCandidate(@NonNull final String packagePattern) {
		if (packagePattern.length() > 0) {
			registerCandidate(new IntentCandidate(packagePattern));
		}
	}

	/**
	 * Registers the specified intent <var>candidate</var> for this assistant, so it can be used to
	 * filter intent candidates for a specific {@link Intent} whenever {@link #filterCandidates(Intent, Context)}
	 * is called.
	 *
	 * @param candidate The desired candidate to be registered to this assistant.
	 */
	public final void registerCandidate(@NonNull final IntentCandidate candidate) {
		this.ensureCandidates(1);
		mCandidates.add(candidate);
	}

	/**
	 * Ensures that the array of candidates is initialized.
	 *
	 * @param initialSize Initial size of candidates list if it is not initialized yet.
	 */
	private void ensureCandidates(final int initialSize) {
		if (mCandidates == null) mCandidates = new ArrayList<>(initialSize);
	}

	/**
	 * Bulk operation for {@link #registerCandidate(Class)}.
	 *
	 * @param classesOfCandidates Set of classes of intent candidates to be registered to this assistant.
	 */
	@SafeVarargs public final void registerCandidates(@NonNull final Class<? extends IntentCandidate>... classesOfCandidates) {
		if (classesOfCandidates.length > 0) {
			this.ensureCandidates(classesOfCandidates.length);
			for (Class<? extends IntentCandidate> classOfHandler : classesOfCandidates) {
				mCandidates.add(createCandidate(classOfHandler));
			}
		}
	}

	/**
	 * Bulk operation for {@link #registerCandidate(IntentCandidate)}.
	 *
	 * @param candidates Set of candidates to be registered to this assistant.
	 */
	public final void registerCandidates(@NonNull final IntentCandidate... candidates) {
		if (candidates.length > 0) {
			this.ensureCandidates(candidates.length);
			mCandidates.addAll(Arrays.asList(candidates));
		}
	}

	/**
	 * Bulk operation for {@link #registerCandidate(String)}.
	 *
	 * @param packagesPatterns Set of packages patterns of intent candidates to be registered to
	 *                         this assistant.
	 */
	public final void registerCandidates(@NonNull final String... packagesPatterns) {
		if (packagesPatterns.length > 0) {
			this.ensureCandidates(packagesPatterns.length);
			for (String pattern : packagesPatterns) {
				mCandidates.add(new IntentCandidate(pattern));
			}
		}
	}

	/**
	 * Filters intent candidates from the currently registered candidates to this assistant for the
	 * specified <var>intent</var>. Each of the filtered candidates (if any) will have attached
	 * {@link android.content.pm.ActivityInfo ActivityInfo} which can be used to obtain <b>icon</b>
	 * and <b>label</b> of activity which that particular candidate represents.
	 * <p>
	 * <b>Note</b>, that filtering process can take some time (depends on how much candidates need to
	 * be filtered). It is a good approach to run this method on the background thread or use
	 * {@link #asyncFilterCandidates(Intent, Context, FilterCallback)}
	 * to free the UI thread form this 'heavy' operation.
	 * </p>
	 *
	 * @param intent  The intent for which to perform filtering.
	 * @param context Context used to access package manager which is needed to obtain ActivityInfo
	 *                for each of the filtered candidates.
	 * @return List of filtered intent candidates for the specified intent or empty list if no candidates
	 * were filtered for the specified intent.
	 * @see #asyncFilterCandidates(Intent, Context, FilterCallback)
	 */
	@NonNull public List<IntentCandidate> filterCandidates(@NonNull final Intent intent, @NonNull final Context context) {
		if (isEmpty()) {
			final List<IntentCandidate> filtered = new ArrayList<>();
			final List<ResolveInfo> infoList = context.getPackageManager().queryIntentActivities(intent, 0);
			if (!infoList.isEmpty()) {
				for (IntentCandidate candidate : mCandidates) {
					for (ResolveInfo info : infoList) {
						if (info.activityInfo != null) {
							if (candidate.matches(info.activityInfo.packageName) && !filtered.contains(candidate)) {
								// Attach activity info.
								candidate.attachActivityInfo(info.activityInfo);
								filtered.add(candidate);
								break;
							}
						}
					}
				}
			}
			return filtered;
		}
		return filterDefaultCandidates(intent, context);
	}

	/**
	 * Checks whether this assistant has some intent candidates registered or not.
	 *
	 * @return {@code True} if there are no candidates registered, {@code false} otherwise.
	 */
	public boolean isEmpty() {
		return mCandidates != null && !mCandidates.isEmpty();
	}

	/**
	 * Same as {@link #filterCandidates(Intent, Context)} but this is
	 * executed on the background thread.
	 *
	 * @param callback A callback to be invoked when the filter process finishes.
	 */
	public void asyncFilterCandidates(@NonNull final Intent intent, @NonNull final Context context, @NonNull final FilterCallback callback) {
		this.executeTask(new Runnable() {

			/**
			 */
			@Override public void run() {
				mFilterResultHandler.obtainMessage(
						0,
						new FilterResult(
								intent,
								filterCandidates(intent, context),
								callback
						)
				).sendToTarget();
			}
		});
	}

	/**
	 * Executes the given Runnable task on the background thread using Executor.
	 *
	 * @param task The desired task to execute.
	 */
	private void executeTask(final Runnable task) {
		if (mFilterResultHandler == null) {
			this.mFilterResultHandler = new FilterResultHandler();
			sExecutor = Executors.newSingleThreadExecutor();
		}
		// Execute on the background thread.
		sExecutor.execute(task);
	}

	/**
	 * Filters the default intent candidates for the specified <var>intent</var>. Each of the filtered
	 * candidates (if any) will have attached {@link android.content.pm.ActivityInfo ActivityInfo}
	 * which can be used to obtain <b>icon</b> and <b>label</b> of activity which that particular
	 * candidate represents.
	 * <p>
	 * <b>Note</b>, that filtering process can take some time (depends on how much candidates need to
	 * be filtered). It is a good approach to run this method on the background thread or use
	 * {@link #asyncFilterDefaultCandidates(Intent, Context, FilterCallback)}
	 * to free the UI thread form this 'heavy' operation.
	 * </p>
	 *
	 * @param intent  The intent for which to perform filtering.
	 * @param context Context used to access package manager which is needed to obtain ActivityInfo
	 *                for each of the filtered IntentHandlers.
	 * @return List of filtered intent candidates for the specified intent or empty list of no candidates
	 * were filtered for the specified intent.
     * @see #asyncFilterDefaultCandidates(Intent, Context, FilterCallback)
     * @see #filterCandidates(Intent, Context)
	 */
	@NonNull public List<IntentCandidate> filterDefaultCandidates(@NonNull final Intent intent, @NonNull final Context context) {
		final List<IntentCandidate> candidates = new ArrayList<>();
		final List<ResolveInfo> infoList = context.getPackageManager().queryIntentActivities(intent, 0);
		if (!infoList.isEmpty()) {
			final List<String> packageNames = new ArrayList<>();
			for (ResolveInfo info : infoList) {
				if (info.activityInfo != null && !packageNames.contains(info.activityInfo.packageName)) {
					packageNames.add(info.activityInfo.packageName);
					final IntentCandidate candidate = new IntentCandidate(info.activityInfo.packageName);
					// Attach activity info.
					candidate.attachActivityInfo(info.activityInfo);
					candidates.add(candidate);
				}
			}
		}
		return candidates;
	}

	/**
	 * Same as {@link #filterDefaultCandidates(Intent, Context)} but
	 * this is executed on the background thread.
	 *
	 * @param callback A callback to be invoked when the filter process finishes.
	 */
	public void asyncFilterDefaultCandidates(@NonNull final Intent intent, @NonNull final Context context, @NonNull final FilterCallback callback) {
		this.executeTask(new Runnable() {

			/**
			 */
			@Override public void run() {
				mFilterResultHandler.obtainMessage(
						0,
						new FilterResult(
								intent,
								filterDefaultCandidates(intent, context),
								callback
						)
				).sendToTarget();
			}
		});
	}

	/**
	 * Returns the current set of intent candidates of this assistant.
	 *
	 * @return List of registered IntentCandidates.
	 */
	@Nullable public List<IntentCandidate> getCandidates() {
		return mCandidates;
	}

    /**
     * Clears the current set of intent candidates of this assistant.
     * @see #getCandidates()
     */
    public void clearCandidates() {
        if (mCandidates != null) mCandidates.clear();
    }

	/**
	 * Creates a new instance of IntentCandidate from the given <var>classOfCandidate</var>.
	 *
	 * @param classOfCandidate Class of candidate from which to create new instance.
	 * @throws RuntimeException If instantiation fails.
	 */
	private IntentCandidate createCandidate(final Class<? extends IntentCandidate> classOfCandidate) {
		try {
			return classOfCandidate.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(
					"Failed to instantiate IntentCandidate class of(" + classOfCandidate.getSimpleName() + ")" +
							"Make sure this class has public empty constructor.",
					e
			);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Handler used to dispatch filter results on the UI thread when running filter process on the
	 * background thread.
	 */
	private static class FilterResultHandler extends Handler {

		/**
		 */
		@Override public void handleMessage(@NonNull final Message msg) {
			final FilterResult result = (FilterResult) msg.obj;
			if (result != null) {
				result.callback.onIntentCandidatesFiltered(result.candidates, result.intent);
			}
		}
	}

	/**
	 * Filter result holder for {@link FilterResultHandler}.
	 */
	private static class FilterResult {

		/**
		 * Intent for which was filter process executed.
		 */
		final Intent intent;

		/**
		 * Filtered list of candidates.
		 */
		final List<IntentCandidate> candidates;

		/**
		 * Callback to be invoked when filter process finishes.
		 */
		final FilterCallback callback;

		/**
		 * Creates a new instance of FilterResult with the given parameters.
		 */
		FilterResult(final Intent intent, final List<IntentCandidate> candidates, final FilterCallback callback) {
			this.intent = intent;
			this.candidates = candidates;
			this.callback = callback;
		}
	}
}