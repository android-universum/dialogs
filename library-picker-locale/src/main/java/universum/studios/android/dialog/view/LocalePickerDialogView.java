/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.LocalePickerDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for
 * {@link LocalePickerDialog LocalePickerDialog} to ensure proper working of binding process of the
 * content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.LocalePickerDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface LocalePickerDialogView extends AdapterDialogView<AdapterView> {

	/**
	 * Same as {@link #setSearchQueryHint(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired search hint.
	 * @see #getSearchQueryHint()
	 */
	void setSearchQueryHint(@StringRes int resId);

	/**
	 * Sets a hint for this dialog view's {@link universum.studios.android.ui.widget.SearchView SearchView}.
	 *
	 * @param hint The desired search hint. May be {@code null} to clear the current one.
	 * @see #getSearchQueryHint()
	 */
	void setSearchQueryHint(@Nullable CharSequence hint);

	/**
	 * Returns the hint specified for this dialog view's {@link universum.studios.android.ui.widget.SearchView SearchView}.
	 *
	 * @return Search hint or empty text if no hint has been specified.
	 */
	@Nullable CharSequence getSearchQueryHint();

	/**
	 * Same as {@link #setSearchQuery(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired query text.
	 * @see #getSearchQuery()
	 */
	void setSearchQuery(@StringRes int resId);

	/**
	 * Sets an initial query text for this dialog view's {@link universum.studios.android.ui.widget.SearchView SearchView}.
	 *
	 * @param query The desired query text. May be {@code null} to clear the current one.
	 * @see #getSearchQuery()
	 */
	void setSearchQuery(@Nullable CharSequence query);

	/**
	 * Returns the current search query text presented within this dialog view's {@link universum.studios.android.ui.widget.SearchView SearchView}.
	 *
	 * @return Current search query text or empty text if no query has been specified/entered yet.
	 */
	@NonNull CharSequence getSearchQuery();
}