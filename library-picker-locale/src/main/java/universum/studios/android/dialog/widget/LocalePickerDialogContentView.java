/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Filterable;
import android.widget.ListView;

import universum.studios.android.dialog.LocalePickerDialog;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.LocalePickerDialogView;
import universum.studios.android.ui.widget.SearchView;

/**
 * A {@link LocalePickerDialogView} implementation used by {@link LocalePickerDialog LocalePickerDialog}
 * as content view.
 * <p>
 * This class represents a container for {@link SearchView} and {@link ListView} allowing to pick and
 * filter from a set of available system locales. This layout basically connects search queries fired
 * from SearchView presented above the ListView with locales to filter a user's desired locales.
 * <p>
 * Initial search query text for search view can be specified via {@link #setSearchQuery(CharSequence)}
 * or its resource equivalent {@link #setSearchQuery(int)}. You can specify also hint for the search
 * view via {@link #setSearchQueryHint(CharSequence)} or its resource equivalent {@link #setSearchQueryHint(int)}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutLocaleContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_locale} and {@link AdapterDialogContentView} for the
 * info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class LocalePickerDialogContentView extends AdapterDialogContentView<AdapterView> implements LocalePickerDialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LocalePickerDialogContentView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Search view used to search through set of available locales.
	 */
	private SearchView mSearchView;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #LocalePickerDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public LocalePickerDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #LocalePickerDialogContentView(Context, AttributeSet, int)} with {@code 0} as
	 * attribute for default style.
	 */
	public LocalePickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Same as {@link #LocalePickerDialogContentView(Context, AttributeSet, int, int)} with {@code 0}
	 * as default style.
	 */
	public LocalePickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of LocalePickerDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public LocalePickerDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p/>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		int layoutResource = R.layout.dialog_layout_locale;
		if (theme.resolveAttribute(R.attr.dialogLayoutLocaleContent, typedValue, true)) {
			layoutResource = typedValue.resourceId;
		}
		this.inflateHierarchy(context, layoutResource);
	}

	/**
	 */
	@Override boolean onHierarchyInflationFinished(@NonNull final View adapterView) {
		if (adapterView instanceof ListView) {
			this.mSearchView = (SearchView) findViewById(R.id.dialog_search_view);
			if (mSearchView != null) {
				mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

					/**
					 */
					@Override public void onQueryTextChanged(@NonNull final SearchView searchView, @NonNull final CharSequence text) {
						handleSearchTextChange(text);
					}

					/**
					 */
					@Override public void onQueryTextConfirmed(@NonNull final SearchView searchView, @NonNull final CharSequence text) {
						handleSearchTextChange(text);
					}

					/**
					 */
					@Override public void onQueryTextCleared(@NonNull final SearchView searchView) {
						handleSearchTextChange("");
					}
				});
			}
			return true;
		}
		return false;
	}

	/**
	 * Handles change in the search query text. This will update filtered locales within the current
	 * {@link #mAdapter}.
	 *
	 * @param text The changed search query text.
	 */
	void handleSearchTextChange(final CharSequence text) {
		if (mAdapter instanceof Filterable) ((Filterable) mAdapter).getFilter().filter(text);
	}

	/**
	 */
	@Override @NonNull public ListView getAdapterView() {
		return (ListView) super.getAdapterView();
	}

	/**
	 */
	@Override public void setSearchQueryHint(@StringRes final int resId) {
		setSearchQueryHint(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setSearchQueryHint(@Nullable final CharSequence hint) {
		if (mSearchView != null) mSearchView.setQueryHint(hint);
	}

	/**
	 */
	@Override @Nullable public CharSequence getSearchQueryHint() {
		return mSearchView == null ? "" : mSearchView.getQueryHint();
	}

	/**
	 */
	@Override public void setSearchQuery(@StringRes final int resId) {
		setSearchQuery(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setSearchQuery(@Nullable final CharSequence query) {
		if (mSearchView != null) mSearchView.setQuery(query);
	}

	/**
	 */
	@Override @NonNull public CharSequence getSearchQuery() {
		return mSearchView == null ? "" : mSearchView.getQuery();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}