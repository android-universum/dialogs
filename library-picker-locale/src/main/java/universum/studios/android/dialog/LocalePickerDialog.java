/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.ArrayRes;
import androidx.annotation.AttrRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.util.DialogLocale;
import universum.studios.android.dialog.util.DialogLocales;
import universum.studios.android.dialog.view.LocalePickerDialogView;

/**
 * A {@link SelectionDialog} implementation that can be used to allow to a user to pick from a set of
 * available {@link DialogLocale DialogLocales} its
 * desired one.
 * <p>
 * The LocalePickerDialog can be shown in two modes determining what kind of data set should be presented
 * to the user. The desired mode can be specified through {@link LocaleOptions} via
 * {@link LocaleOptions#mode(int)} by supplying one of {@link #MODE_COUNTRY} or {@link #MODE_LANGUAGE}.
 * <p>
 * See {@link LocaleOptions} for options that can be supplied to the LocalePickerDialog
 * from the desired context via {@link #newInstance(LocaleOptions)} or via
 * {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogLocaleOptions dialogLocaleOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class LocalePickerDialog extends SelectionDialog {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LocalePickerDialog";

	/**
	 * Mode flag for {@link LocaleOptions#mode(int)} to request, that the {@link LocalesAdapter}
	 * should bind its views with data containing country name of a specific {@link DialogLocale}.
	 */
	public static final int MODE_COUNTRY = 0x01;

	/**
	 * Mode flag for {@link LocaleOptions#mode(int)} to request, that the {@link LocalesAdapter}
	 * should bind its views with data containing language name of a specific {@link DialogLocale}.
	 */
	public static final int MODE_LANGUAGE = 0x02;

	/**
	 * Defines an annotation for determining set of allowed flags for locale picker mode.
	 */
	@IntDef({MODE_COUNTRY, MODE_LANGUAGE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Mode {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Locale dialog view implementation.
	 */
	private LocalePickerDialogView mLocaleView;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of LocalePickerDialog with {@link android.widget.ListView} and data set
	 * of available {@link DialogLocale DialogLocales}.
	 */
	public LocalePickerDialog() {
		super(R.attr.dialogLocaleOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of LocalePickerDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New LocalePickerDialog instance.
	 */
	@NonNull public static LocalePickerDialog newInstance(@NonNull final LocaleOptions options) {
		final LocalePickerDialog dialog = new LocalePickerDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new LocaleOptions(resources);
	}

	/**
	 */
	@SuppressWarnings("ResourceType")
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof LocaleOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Locale);
			final LocaleOptions options = (LocaleOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Locale_dialogLocaleMode) {
					if (!options.isSet(LocaleOptions.MODE)) {
						options.mode(attributes.getInt(index, options.mode));
					}
				} else if (index == R.styleable.Dialog_Options_Locale_dialogExcludeCountryCodes) {
					if (!options.isSet(LocaleOptions.EXCLUDE_COUNTRY_CODES)) {
						final int arrayResId = attributes.getResourceId(index, -1);
						if (arrayResId != -1) options.excludeCountryCodes(arrayResId);
					}
				} else if (index == R.styleable.Dialog_Options_Locale_dialogSearchHint) {
					if (!options.isSet(LocaleOptions.SEARCH_HINT)) {
						options.searchHint(attributes.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_Locale_dialogSearchQuery) {
					if (!options.isSet(LocaleOptions.SEARCH_QUERY)) {
						options.searchQuery(attributes.getText(index));
					}
				}
			}
			attributes.recycle();
		}
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_locale, container, false);
	}

	/**
	 */
	@Override protected void onBindView(
			@NonNull final View dialogView,
			@NonNull final DialogOptions options,
			@Nullable final Bundle savedInstanceState
	) {
		super.onBindView(dialogView, options, savedInstanceState);
		if (options instanceof LocaleOptions && getAdapter() == null) {
			final LocaleOptions localeOptions = (LocaleOptions) options;
			final LocalesAdapter adapter = new LocalesAdapter(
					getContext(),
					mLayoutInflater,
					localeOptions.mode,
					localeOptions.excludeCountryCodes
			);
			if (localeOptions.selection >= 0) {
				adapter.setSelection(new long[]{localeOptions.selection});
			}
			setAdapter(adapter);
		}
	}

	/**
	 */
	@Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
		super.onBindContentView(contentView, options);
		if (contentView instanceof LocalePickerDialogView && options instanceof LocaleOptions) {
			final LocalePickerDialogView localeView = (LocalePickerDialogView) contentView;
			final LocaleOptions localeOptions = (LocaleOptions) options;
			localeView.setSearchQueryHint(localeOptions.searchHint);
		}
	}

	/**
	 */
	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final View contentView = getContentView();
		if (contentView instanceof LocalePickerDialogView) {
			this.mLocaleView = (LocalePickerDialogView) contentView;
			final LocaleOptions options = (LocaleOptions) getOptions();
			if (savedInstanceState == null && options != null && options.searchQuery != null) {
				mLocaleView.setSearchQuery(options.searchQuery);
			}
		}
	}

	/**
	 * Returns the current search query text presented within this dialog's {@link universum.studios.android.ui.widget.SearchView SearchView}.
	 *
	 * @return Current search query text or empty text if no query has been specified/entered yet.
	 */
	@NonNull public CharSequence getSearchQuery() {
		return mLocaleView == null ? "" : mLocaleView.getSearchQuery();
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * An {@link BaseAdapterOptions} implementation specific for the {@link LocalePickerDialog}.
	 * <p>
	 * Below are listed setters which may be used to supply desired data to {@link LocalePickerDialog}
	 * through these options:
	 * <ul>
	 * <li>{@link #mode(int)}</li>
	 * <li>{@link #excludeCountryCodes(String[])}</li>
	 * <li>{@link #excludeCountryCodes(int)}</li>
	 * <li>{@link #searchHint(CharSequence)}</li>
	 * <li>{@link #searchHint(int)}</li>
	 * <li>{@link #searchQuery(CharSequence)}</li>
	 * <li>{@link #searchQuery(int)}</li>
	 * </ul>
	 *
	 * <h3>Xml attributes</h3>
	 * See {@link R.styleable#Dialog_Options_Locale LocaleOptions Attributes},
	 * {@link R.styleable#Dialog_Options DialogOptions Attributes}
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	@SuppressWarnings("ResourceType")
	public static class LocaleOptions extends BaseAdapterOptions<LocaleOptions> {

		/**
		 * Creator used to create an instance or array of instances of LocaleOptions from {@link Parcel}.
		 */
		public static final Creator<LocaleOptions> CREATOR = new Creator<LocaleOptions>() {

			/**
			 */
			@Override public LocaleOptions createFromParcel(@NonNull final Parcel source) { return new LocaleOptions(source); }

			/**
			 */
			@Override public LocaleOptions[] newArray(final int size) { return new LocaleOptions[size]; }
		};

		/**
		 * Flag for <b>mode</b> parameter.
		 */
		static final int MODE = 0x00000001;

		/**
		 * Flag for <b>exclude country codes</b> parameter.
		 */
		static final int EXCLUDE_COUNTRY_CODES = 0x00000001 << 1;

		/**
		 * Flag for <b>selection</b> parameter.
		 */
		static final int SELECTION = 0x00000001 << 2;

		/**
		 * Flag for <b>search hint</b> parameter.
		 */
		static final int SEARCH_HINT = 0x00000001 << 3;

		/**
		 * Flag for <b>search query</b> parameter.
		 */
		static final int SEARCH_QUERY = 0x00000001 << 4;

		/**
		 * Mode determining whether to present data set of locales as countries or languages.
		 */
		int mode = MODE_COUNTRY;

		/**
		 * Hint text for the search query.
		 */
		CharSequence searchHint;

		/**
		 * Initial query for the search view.
		 */
		CharSequence searchQuery;

		/**
		 * Set of country codes determining which locales should be excluded by theirs country code
		 * from the available data set.
		 */
		String[] excludeCountryCodes;

		/**
		 * Id of the locale item that should be selected.
		 */
		long selection = -1;

		/**
		 * Creates a new instance of LocaleOptions.
		 */
		public LocaleOptions() {
			super();
		}

		/**
		 * Creates a new instance of LocaleOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public LocaleOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of LocaleOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected LocaleOptions(@NonNull final Parcel source) {
			super(source);
			this.mode = source.readInt();
			excludeCountryCodes = new String[source.readInt()];
			source.readStringArray(excludeCountryCodes);
			this.selection = source.readLong();
			this.searchHint = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.searchQuery = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(mode);
			dest.writeInt(excludeCountryCodes != null ? excludeCountryCodes.length : 0);
			dest.writeLong(selection);
			dest.writeStringArray(excludeCountryCodes);
			TextUtils.writeToParcel(searchHint, dest, flags);
			TextUtils.writeToParcel(searchQuery, dest, flags);
		}

		/**
		 */
		@Override public LocaleOptions merge(@NonNull final DialogOptions options) {
			if (!(options instanceof LocaleOptions)) {
				return super.merge(options);
			}
			final LocaleOptions localeOptions = (LocaleOptions) options;
			if (localeOptions.isSet(MODE)) {
				this.mode = localeOptions.mode;
				updateIsSet(MODE);
			}
			if (localeOptions.isSet(EXCLUDE_COUNTRY_CODES)) {
				if (localeOptions.excludeCountryCodes != null) {
					this.excludeCountryCodes = new String[localeOptions.excludeCountryCodes.length];
					System.arraycopy(localeOptions.excludeCountryCodes, 0, excludeCountryCodes, 0, localeOptions.excludeCountryCodes.length);
				} else {
					this.excludeCountryCodes = null;
				}
				updateIsSet(EXCLUDE_COUNTRY_CODES);
			}
			if (localeOptions.isSet(SELECTION)) {
				this.selection = localeOptions.selection;
				updateIsSet(SELECTION);
			}
			if (localeOptions.isSet(SEARCH_HINT)) {
				this.searchHint = copyCharSequence(localeOptions.searchHint);
				updateIsSet(SEARCH_HINT);
			}
			if (localeOptions.isSet(SEARCH_QUERY)) {
				this.searchQuery = copyCharSequence(localeOptions.searchQuery);
				updateIsSet(SEARCH_QUERY);
			}
			return super.merge(options);
		}

		/**
		 * Sets the mode determining whether to present data set of countries or languages by
		 * adapter of a dialog associated with these options.
		 *
		 * @param mode The desired locale mode. One of {@link #MODE_COUNTRY} or {@link #MODE_LANGUAGE}.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogLocaleMode dialog:dialogLocaleMode
		 * @see #mode()
		 */
		public LocaleOptions mode(@Mode final int mode) {
			updateIsSet(MODE);
			if (this.mode != mode) {
				this.mode = mode;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the locale mode, set to these options.
		 * <p>
		 * Default value: <b>{@link #MODE_COUNTRY}</b>
		 *
		 * @return One of {@link #MODE_COUNTRY} or {@link #MODE_LANGUAGE}.
		 * @see #mode(int)
		 */
		@Mode public int mode() {
			return mode;
		}

		/**
		 * Same as {@link #excludeCountryCodes(String[])} for resource id.
		 *
		 * @param resId Resource id of the string array holding the country codes.
		 * @see #excludedCountryCodes()
		 */
		public LocaleOptions excludeCountryCodes(@ArrayRes final int resId) {
			checkRequiredResources();
			return excludeCountryCodes(mResources.getStringArray(resId));
		}

		/**
		 * Sets the set of country codes determining which locales should be excluded from the data
		 * set of the locales adapter of a dialog associated with these options.
		 *
		 * @param countryCodes The desired set of country codes.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogExcludeCountryCodes dialog:dialogExcludeCountryCodes
		 * @see #excludedCountryCodes()
		 */
		public LocaleOptions excludeCountryCodes(@NonNull final String[] countryCodes) {
			updateIsSet(EXCLUDE_COUNTRY_CODES);
			this.excludeCountryCodes = countryCodes;
			notifyChanged();
			return this;
		}

		/**
		 * Returns the set of country codes, set to these options.
		 *
		 * @return Set of country codes determining which locales to exclude from the data set.
		 * @see #excludeCountryCodes(int)
		 * @see #excludeCountryCodes(String[])
		 */
		@Nullable public String[] excludedCountryCodes() {
			return excludeCountryCodes;
		}

		/**
		 * Specifies an id of the locale item that should be initially selected within a dialog
		 * associated with these options.
		 *
		 * @param selection The desired id of the locale to be selected.
		 * @return These options to allow methods chaining.
		 * @see #selection()
		 */
		public LocaleOptions selection(final long selection) {
			updateIsSet(SELECTION);
			if (this.selection != selection) {
				this.selection = selection;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the initial selection id, set to these options.
		 * <p>
		 * Default value: <b>-1</b>
		 *
		 * @return Id of the locale that should be initially selected.
		 * @see #selection(long)
		 */
		public long selection() {
			return selection;
		}

		/**
		 * Same as {@link #searchHint(CharSequence)}, but for resource id.
		 *
		 * @param resId Resource id of the desired hint text.
		 * @see R.attr#dialogSearchHint dialog:dialogSearchHint
		 * @see #searchHint()
		 */
		public LocaleOptions searchHint(@StringRes final int resId) {
			return searchHint(text(resId));
		}

		/**
		 * Sets a hint text for the SearchView of a dialog associated with these options.
		 *
		 * @param hint The desired hint text.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogSearchHint dialog:dialogSearchHint
		 * @see #searchHint()
		 */
		public LocaleOptions searchHint(@Nullable final CharSequence hint) {
			updateIsSet(SEARCH_HINT);
			if (searchHint == null || !searchHint.equals(hint)) {
				this.searchHint = hint;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the hint text, set to these options.
		 *
		 * @return Hint text for the SearchView.
		 * @see #searchHint(int)
		 * @see #searchHint(CharSequence)
		 */
		@Nullable public CharSequence searchHint() {
			return searchHint;
		}

		/**
		 * Same as {@link #searchQuery(CharSequence)}, but for resource id.
		 *
		 * @param resId Resource id of the desired query text.
		 * @see R.attr#dialogSearchQuery dialog:dialogSearchQuery
		 * @see #searchQuery()
		 */
		public LocaleOptions searchQuery(@StringRes final int resId) {
			return searchQuery(text(resId));
		}

		/**
		 * Sets a query text for the SearchView of a dialog associated with these options.
		 *
		 * @param query The desired query text.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogSearchQuery dialog:dialogSearchQuery
		 * @see #searchQuery()
		 */
		public LocaleOptions searchQuery(@Nullable final CharSequence query) {
			updateIsSet(SEARCH_QUERY);
			if (searchQuery == null || !searchQuery.equals(query)) {
				this.searchQuery = query;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the query text, set to these options.
		 *
		 * @return Query text for the SearchView.
		 * @see #searchQuery(int)
		 * @see #searchQuery(CharSequence)
		 */
		@Nullable public CharSequence searchQuery() {
			return searchQuery;
		}

		/**
		 */
		@Override protected void onParseXmlAttribute(
				@NonNull final XmlResourceParser xmlParser,
				final int index,
				@AttrRes final int attr,
				@NonNull final Resources resources,
				@Nullable final Resources.Theme theme
		) {
			if (attr == R.attr.dialogLocaleMode) {
				mode(xmlParser.getAttributeIntValue(index, mode));
			} else if (attr == R.attr.dialogExcludeCountryCodes) {
				final int arrayResId = xmlParser.getAttributeResourceValue(index, -1);
				if (arrayResId != -1) excludeCountryCodes(arrayResId);
			} else if (attr == R.attr.dialogSearchHint) {
				searchHint(obtainXmlAttributeText(xmlParser, index, resources));
			} else if (attr == R.attr.dialogSearchQuery) {
				searchQuery(obtainXmlAttributeText(xmlParser, index, resources));
			} else {
				super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
			}
		}
	}

	/**
	 * An {@link ItemsAdapter} implementation used by {@link LocalePickerDialog} to populate its
	 * adapter view with data set of {@link DialogLocale DialogLocales}. The adapter can be created
	 * in two modes:
	 * <ul>
	 * <li>{@link #MODE_COUNTRY}
	 * - the adapter produces item views bound with only country name,
	 * </li>
	 * <li>{@link #MODE_LANGUAGE}
	 * - the adapter produces item views bound with language name followed by country name for the
	 * locales that have the same country name.
	 * </li>
	 * </ul>
	 * Filtering of the data set will be handled accordingly to the presented data.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class LocalesAdapter extends ItemsAdapter<DialogLocale> implements Filterable {

		/**
		 * Filter used to filter current data set of locales.
		 */
		private final Filter FILTER = new Filter() {

			/**
			 */
			@Override protected FilterResults performFiltering(@Nullable final CharSequence constraint) {
				final FilterResults results = new FilterResults();
				if (locales != null && !locales.isEmpty()) {
					LOCALES_FILTER.query(constraint);
					final List<DialogLocale> items = DialogLocales.filterLocales(locales, LOCALES_FILTER);
					results.values = items;
					results.count = items.size();
				}
				return results;
			}

			/**
			 */
			@SuppressWarnings("unchecked")
			@Override protected void publishResults(@Nullable final CharSequence constraint, @NonNull final FilterResults results) {
				if (results.values != null) {
					LocalesAdapter.super.changeItems((List<DialogLocale>) results.values);
				}
			}
		};

		/**
		 * Filter for the {@link DialogLocales} factory determining how should be the set of locales
		 * filtered.
		 */
		final DialogLocales.Filter LOCALES_FILTER = new DialogLocales.Filter();

		/**
		 * Mode determining which data should be used from the data set to bound item views produced
		 * by this adapter.
		 */
		final int mode;

		/**
		 * Set of locales provided by this adapter.
		 */
		List<DialogLocale> locales;

		/**
		 * Same as {@link #LocalesAdapter(Context, LayoutInflater, int, String[])}
		 * with {@code null} <var>excludeCountryCodes</var> parameter so all available locales will
		 * be provided by this adapter.
		 */
		public LocalesAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @Mode final int mode) {
			this(context, inflater, mode, null);
		}

		/**
		 * Creates a new instance of LocalesAdapter that provides data set of {@link DialogLocale DialogLocales}.
		 * Based on the specified <var>mode</var>, the adapter will threat the data set accordingly.
		 *
		 * @param context             Context in which will be the new adapter used.
		 * @param inflater            Layout inflater that should be used to inflate views for this
		 *                            adapter.
		 * @param mode                The desired locale mode. One of {@link #MODE_COUNTRY} or
		 *                            {@link #MODE_LANGUAGE}.
		 * @param excludeCountryCodes Set of country codes used to determine which locales should
		 *                            be excluded from the data set. The adapter will compare these
		 *                            codes with {@link DialogLocale#countryCode} and will exclude
		 *                            those who matches.
		 */
		public LocalesAdapter(
				@NonNull final Context context,
				@NonNull final LayoutInflater inflater,
				@Mode final int mode,
				@Nullable final String[] excludeCountryCodes
		) {
			super(context, inflater);
			switch (this.mode = mode) {
				case MODE_COUNTRY:
					this.locales = DialogLocales.listAvailableLocales(DialogLocale.COUNTRY_COMPARATOR);
					LOCALES_FILTER.queryFlags(DialogLocales.Filter.COUNTRY);
					break;
				case MODE_LANGUAGE:
					this.locales = DialogLocales.listAvailableLocales(DialogLocale.LANGUAGE_COMPARATOR);
					LOCALES_FILTER.queryFlags(DialogLocales.Filter.LANGUAGE);
					break;
				default:
					throw new IllegalArgumentException("Unknown mode(" + mode + ").");
			}
			if (excludeCountryCodes != null && excludeCountryCodes.length > 0) {
				final List<DialogLocale> remove = new ArrayList<>();
				final List<String> exclude = Arrays.asList(excludeCountryCodes);
				for (DialogLocale locale : locales) {
					if (exclude.contains(locale.countryCode)) {
						remove.add(locale);
					}
				}
				locales.removeAll(remove);
			}
			LocalesAdapter.super.changeItems(new ArrayList<>(locales));
		}

		/**
		 */
		@Override public void changeItems(@Nullable final List<DialogLocale> dialogLocales) {
			throw new UnsupportedOperationException("Cannot change fixed data set of DialogLocales.");
		}

		/**
		 */
		@Override public Filter getFilter() {
			return FILTER;
		}

		/**
		 */
		@Override protected void onBindViewHolder(@NonNull final ItemHolder viewHolder, final boolean selected, final int position) {
			final DialogLocale locale = getItem(position);
			if (viewHolder.textView instanceof CompoundButton) {
				((CompoundButton) viewHolder.textView).setChecked(selected);
			}
			switch (mode) {
				case MODE_COUNTRY:
					viewHolder.textView.setText(locale.countryName);
					break;
				case MODE_LANGUAGE:
					if (position == 0) {
						this.bindLanguageViewHolder(viewHolder, locale, null, getItem(position + 1));
					} else if (position == getCount() - 1) {
						this.bindLanguageViewHolder(viewHolder, locale, getItem(position - 1), null);
					} else {
						this.bindLanguageViewHolder(viewHolder, locale, getItem(position - 1), getItem(position + 1));
					}
					break;
			}
		}

		/**
		 * Called from {@link #onBindViewHolder(ItemHolder, boolean, int)} in {@link #MODE_LANGUAGE}
		 * mode to bind the given <var>holder</var>.
		 *
		 * @param holder       Item holder to bind.
		 * @param locale       Current iterated locale of which data to bind to the holder.
		 * @param localeBefore Locale form the data set before position of the current locale. May be
		 *                     {@code null} if current iterated position is {@code 0}.
		 * @param localeAfter  Locale form the data set after position of the current locale. May be
		 *                     {@code null} if current iterated position is {@code getCount() - 1}.
		 */
		private void bindLanguageViewHolder(final ItemHolder holder, final DialogLocale locale, final DialogLocale localeBefore, final DialogLocale localeAfter) {
			final String languageName = locale.languageName;
			// Show country name only for same languages.
			if ((localeBefore != null && languageName.equals(localeBefore.languageName))
					|| (localeAfter != null && languageName.equals(localeAfter.languageName))) {
				holder.textView.setText(mResources.getString(
						R.string.dialog_locale_language_format,
						languageName,
						locale.countryName
				));
			} else {
				holder.textView.setText(languageName);
			}
		}
	}
}