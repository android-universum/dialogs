/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.Comparator;
import java.util.Locale;
import java.util.MissingResourceException;

import androidx.annotation.NonNull;
import universum.studios.android.dialog.LocalePickerDialog;
import universum.studios.android.dialog.adapter.DialogSelectionAdapter;

/**
 * The DialogLocale class represents a wrapper for {@link Locale} used primarily within
 * {@link LocalePickerDialog LocalePickerDialog} that allows
 * to a user to pick from set of available locales by country name or language name.
 * <p>
 * This wrapper extracts most needed data from the wrapped {@link Locale} instance for better accessibility.
 * The extracted data are listed below:
 * <ul>
 * <li>{@link #countryName}</li>
 * <li>{@link #languageName}</li>
 * <li>{@link #countryCode}</li>
 * <li>{@link #countryISO3Code} (if available)</li>
 * </ul>
 *
 * <h3>Sorting</h3>
 * You can use following comparators to sort list of available dialog locales that can be obtained
 * via {@link DialogLocales} factory:
 * <ul>
 * <li>{@link #COUNTRY_COMPARATOR}</li>
 * <li>{@link #LANGUAGE_COMPARATOR}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogLocale implements DialogSelectionAdapter.Item, Comparable<DialogLocale> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogLocale";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Creator used to create an instance or array of instances of DialogLocale from {@link Parcel}.
	 */
	public static final Parcelable.Creator<DialogLocale> CREATOR = new Parcelable.Creator<DialogLocale>() {

		/**
		 */
		@Override public DialogLocale createFromParcel(@NonNull final Parcel source) {
			return new DialogLocale(source); }

		/**
		 */
		@Override public DialogLocale[] newArray(final int size) { return new DialogLocale[size]; }
	};

	/**
	 * Comparator implementation that can be used to compare (sort) set of {@link DialogLocale DialogLocales}
	 * instances by theirs {@link #countryName}.
	 */
	public static final Comparator<DialogLocale> COUNTRY_COMPARATOR = new Comparator<DialogLocale>() {

		/**
		 */
		@Override public int compare(@NonNull final DialogLocale first, @NonNull final DialogLocale second) {
			if (!TextUtils.isEmpty(first.countryName) && !TextUtils.isEmpty(second.countryName)) {
				return first.countryName.compareToIgnoreCase(second.countryName);
			}
			return 0;
		}
	};

	/**
	 * Comparator implementation that can be used to compare (sort) set of {@link DialogLocale DialogLocales}
	 * instances by theirs {@link #languageName}.
	 * <p>
	 * <b>Note</b>, that this comparator takes into count also {@link #countryName} if both comparing
	 * locale instances have same language name.
	 */
	public static final Comparator<DialogLocale> LANGUAGE_COMPARATOR = new Comparator<DialogLocale>() {

		/**
		 */
		@Override public int compare(@NonNull final DialogLocale first, @NonNull final DialogLocale second) {
			if (!TextUtils.isEmpty(first.languageName) && !TextUtils.isEmpty(second.languageName)) {
				if (first.languageName.compareToIgnoreCase(second.languageName) == 0) {
					return first.countryName.compareToIgnoreCase(second.countryName);
				}
				return first.languageName.compareToIgnoreCase(second.languageName);
			}
			return 0;
		}
	};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Id of this locale wrapper.
	 */
	final int mId;

	/**
	 * Country name of this locale wrapper obtained via {@link Locale#getDisplayCountry()}.
	 */
	public final String countryName;

	/**
	 * Language name of this locale wrapper obtained via {@link Locale#getDisplayLanguage()}.
	 */
	public final String languageName;

	/**
	 * Two-letter country code of this locale wrapper obtained via {@link #extractCountryCode(Locale)}.
	 */
	public final String countryCode;

	/**
	 * Three-letter country ISO3 based code of this locale wrapper obtained via {@link Locale#getISO3Country()}
	 * if possible.
	 * <p>
	 * May be empty string if the wrapped locale does not posses country code in ISO3 format.
	 */
	public final String countryISO3Code;

	/**
	 * Raw locale of this locale wrapper.
	 */
	public final Locale locale;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DialogLocale wrapper with data obtained from the passed <var>locale</var>.
	 *
	 * @param id     An id for the new locale.
	 * @param locale Instance of locale which will be wrapped by this DialogLocale.
	 */
	DialogLocale(final int id, @NonNull final Locale locale) {
		this.mId = id;
		this.countryName = locale.getDisplayCountry();
		this.languageName = locale.getDisplayLanguage();
		this.countryCode = extractCountryCode(locale);
		this.locale = locale;
		String iso3Code = "";
		try {
			iso3Code = locale.getISO3Country();
		} catch (MissingResourceException ignored) {
			// This country does not have ISO3 country code resource available.
		}
		this.countryISO3Code = iso3Code;
	}

	/**
	 * Called form {@link #CREATOR} to create an instance of DialogLocale form the given parcel
	 * <var>source</var>.
	 *
	 * @param source Parcel with data for the new instance.
	 */
	DialogLocale(@NonNull final Parcel source) {
		this.mId = source.readInt();
		this.countryName = source.readString();
		this.languageName = source.readString();
		this.countryCode = source.readString();
		this.countryISO3Code = source.readString();
		this.locale = (Locale) source.readSerializable();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Extracts a country code from the given <var>locale</var>.
	 * <p>
	 * See {@link Locale#getVariant()} and {@link Locale#getISOCountries()} for
	 * more info.
	 *
	 * @param locale Instance of locale from which should be country code extracted.
	 * @return Extracted country code.
	 */
	@NonNull public static String extractCountryCode(@NonNull final Locale locale) {
		final String variant = locale.getVariant();
		return TextUtils.isEmpty(variant) ? locale.getCountry() : variant;
	}

	/**
	 */
	@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
		dest.writeString(countryName);
		dest.writeString(languageName);
		dest.writeString(countryCode);
		dest.writeString(countryISO3Code);
		dest.writeSerializable(locale);
	}

	/**
	 */
	@Override public int describeContents() {
		return 0;
	}

	/**
	 */
	@Override public int compareTo(@NonNull final DialogLocale other) {
		if (!TextUtils.isEmpty(countryName) && !TextUtils.isEmpty(other.countryName)) {
			return countryName.compareToIgnoreCase(other.countryName);
		}
		return 0;
	}

	/**
	 */
	@Override public long getId() {
		return mId;
	}

	/**
	 */
	@Override @NonNull public CharSequence getText() {
		return "";
	}

	/*
	 * Inner classes ===============================================================================
	 */
}