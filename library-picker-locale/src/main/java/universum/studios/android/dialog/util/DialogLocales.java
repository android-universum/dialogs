/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.util;

import android.text.TextUtils;
import android.util.SparseArray;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.LocalePickerDialog;

/**
 * The DialogLocales class represents a factory for {@link DialogLocale DialogLocales} implemented
 * primarily for purpose of supplying set of available locales for adapter of {@link LocalePickerDialog LocalePickerDialog}.
 * <p>
 * List of available system locales can be obtained via {@link #listAvailableSystemLocales()} which
 * returns locales for "unique" country only obtained from the system via {@link Locale#getAvailableLocales()}.
 * As mentioned above, the LocalePickerDialog dialog uses this factory to filter available locales
 * that are presented within its adapter via {@link #filterLocales(List, Filter)}.
 * <p>
 * All DialogLocale instances provided by this factory have theirs unique id, and you can obtain
 * instance of DialogLocale for a specific id via {@link #obtainLocale(int)}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class DialogLocales {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogLocales";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Lock used for synchronized operations.
	 */
	private static final Object LOCK = new Object();

	/**
	 * Set of cached system locales.
	 */
	private static List<Locale> sCachedSystemLocales;

	/**
	 * Current system locale for which are locales within this factory cached.
	 */
	private static Locale sCurrentLocale;

	/**
	 * Set of cached dialog locales.
	 */
	private static SparseArray<DialogLocale> sCachedLocales;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private DialogLocales() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns a list of all available locales provided by the current system.
	 * <p>
	 * All currently available locales are requested via {@link Locale#getAvailableLocales()}
	 * and then filtered so the returned list contains only locales with valid and unique country
	 * name.
	 *
	 * @return List of available locales without duplicates with the same country name.
	 */
	@NonNull public static List<Locale> listAvailableSystemLocales() {
		synchronized (LOCK) {
			checkLocaleChange();
			if (sCachedSystemLocales == null) {
				sCachedSystemLocales = new ArrayList<>();
				final List<String> foundCountries = new ArrayList<>();
				for (Locale locale : Locale.getAvailableLocales()) {
					final String displayCountry = locale.getDisplayCountry();
					final String country = locale.getCountry();
					if (!TextUtils.isEmpty(displayCountry) && !TextUtils.isDigitsOnly(country) && !foundCountries.contains(displayCountry)) {
						foundCountries.add(displayCountry);
						sCachedSystemLocales.add(locale);
					}
				}
			}
		}
		return sCachedSystemLocales;
	}

	/**
	 * Same as {@link #listAvailableLocales(Comparator)} with {@code null} comparator for sort.
	 */
	@NonNull public static List<DialogLocale> listAvailableLocales() {
		return listAvailableLocales(null);
	}

	/**
	 * Returns a list of DialogLocale instances that wraps all currently available system locales
	 * provided by {@link #listAvailableSystemLocales()} method.
	 *
	 * @return List of DialogLocale wrappers for the current available system locales.
	 */
	@NonNull public static List<DialogLocale> listAvailableLocales(@Nullable final Comparator<DialogLocale> sortComparator) {
		ensureLocales();
		final List<DialogLocale> dialogLocales = new ArrayList<>(sCachedLocales.size());
		for (int i = 0; i < sCachedLocales.size(); i++) {
			dialogLocales.add(sCachedLocales.valueAt(i));
		}

		if (sortComparator != null) {
			Collections.sort(dialogLocales, sortComparator);
		}
		return dialogLocales;
	}

	/**
	 * Ensures that the cached DialogLocale instances are initialized and ready to obtain.
	 */
	private static void ensureLocales() {
		synchronized (LOCK) {
			checkLocaleChange();
			if (sCachedLocales == null) {
				final List<Locale> locales = listAvailableSystemLocales();
				sCachedLocales = new SparseArray<>(locales.size());
				int id = 0;
				for (Locale locale : locales) {
					sCachedLocales.put(id, new DialogLocale(id, locale));
					id++;
				}
			}
		}
	}

	/**
	 * Clears the current cache with the locales.
	 */
	public static void clearCachedLocales() {
		if (sCachedLocales != null) {
			sCachedLocales.clear();
			sCachedLocales = null;
		}
		if (sCachedSystemLocales != null) {
			sCachedSystemLocales.clear();
			sCachedSystemLocales = null;
		}
	}

	/**
	 * Same as {@link #filterLocales(List, Filter)}
	 * where as <var>locales</var> will be passed locales obtained via {@link #listAvailableLocales()}.
	 */
	public static List<DialogLocale> filterLocales(@NonNull final Filter filter) {
		return filterLocales(listAvailableLocales(), filter);
	}

	/**
	 * Returns a list of DialogLocale instances that passes the specified <var>filter</var> using
	 * the specified <var>locales</var> list as source.
	 *
	 * @param locales List of locales to be filtered.
	 * @param filter  The filter to be used.
	 * @return Filtered locales by the specified filter.
	 */
	public static List<DialogLocale> filterLocales(@NonNull final List<DialogLocale> locales, @NonNull final Filter filter) {
		final List<DialogLocale> filtered = new ArrayList<>();
		if (locales.isEmpty() || filter.queryFlags == 0) {
			return filtered;
		}

		if (filter.query.length() == 0) {
			filtered.addAll(locales);
			return filtered;
		}

		final String query = filter.query.toString();
		final int flags = filter.queryFlags;

		for (DialogLocale locale : locales) {
			if ((flags & Filter.COUNTRY) != 0 && containsIgnoreCase(locale.countryName, query)) {
				filtered.add(locale);
			} else if ((flags & Filter.LANGUAGE) != 0 && containsIgnoreCase(locale.languageName, query)) {
				filtered.add(locale);
			}
		}

		if (filter.sortComparator != null && !filtered.isEmpty()) {
			Collections.sort(filtered, filter.sortComparator);
		}
		return filtered;
	}

	/**
	 * Checks whether the specified <var>source</var> string contains the specified <var>search</var>
	 * string ignoring letters case.
	 *
	 * @param source The source string where to search.
	 * @param search The search string which to search for.
	 * @return {@code True} if the search string is contained within the source string ignoring
	 * letters case, {@code false} otherwise.
	 */
	private static boolean containsIgnoreCase(final String source, final String search) {
		return source.toLowerCase().contains(search.toLowerCase());
	}

	/**
	 * Obtains locale for the specified <var>id</var>. Id of a particular dialog locale can be obtained
	 * via {@link DialogLocale#getId()}.
	 *
	 * @param id The id of the desired DialogLocale to obtain.
	 * @return Obtained dialog locale for the requested id or {@code null} if there is no dialog
	 * locale with the specified id currently available.
	 */
	@Nullable public static DialogLocale obtainLocale(final int id) {
		ensureLocales();
		return sCachedLocales.get(id);
	}

	/**
	 * Checks whether the current default system locale does not change since the last time this
	 * check has been performed. If default locale has been changed, this will de-initialize the
	 * current cached locales so we will need to re-create them again.
	 */
	private static void checkLocaleChange() {
		if (sCurrentLocale != Locale.getDefault()) {
			sCurrentLocale = Locale.getDefault();
			clearCachedLocales();
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Filter used to supply query, comparator and query flags to {@link DialogLocales} factory to
	 * properly filter list of dialog locales according to the desired parameters.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static final class Filter {

		/**
		 * Query flag determining that a query specified via {@link #query(CharSequence)} should be
		 * also used for lookup within country name of the filtered dialog locale.
		 */
		public static final int COUNTRY = 0x01;

		/**
		 * Query flag determining that a query specified via {@link #query(CharSequence)} should be
		 * also used for lookup within language name of the filtered dialog locale.
		 */
		public static final int LANGUAGE = 0x02;

		/**
		 * Defines an annotation for determining set of allowed query flags for dialog locales filter.
		 */
		@Retention(RetentionPolicy.SOURCE)
		@IntDef({COUNTRY, LANGUAGE})
		public @interface QueryFlags {}

		/**
		 * Text used to filter locales by theirs country or language or both, depending on the
		 * {@link #queryFlags} value.
		 */
		CharSequence query = "";

		/**
		 * Comparator used to sort filtered locales.
		 */
		Comparator<DialogLocale> sortComparator;

		/**
		 * Set of flags determining filtering behaviour.
		 */
		int queryFlags = COUNTRY;

		/**
		 * Specifies the query used to filter dialog locales. Handling of the query will depend on
		 * the specified query flags via {@link #queryFlags(int)}.
		 *
		 * @param query The desired query text.
		 * @return This filter to allow methods chaining.
		 */
		public Filter query(@NonNull final CharSequence query) {
			this.query = query;
			return this;
		}

		/**
		 * Specifies a set of flags determining how to perform filtering with the query specified
		 * via {@link #query(CharSequence)}.
		 * <p>
		 * Default flags: {@link #COUNTRY}
		 *
		 * @param flags Set of query flags.
		 * @return This filter to allow methods chaining.
		 */
		public Filter queryFlags(@QueryFlags final int flags) {
			this.queryFlags = flags;
			return this;
		}

		/**
		 * Specifies a comparator that should be used to sort filtered locales.
		 *
		 * @param comparator The desired comparator. May be {@code null} if no sorting should be
		 *                   performed.
		 * @return This filter to allow methods chaining.
		 */
		public Filter sortComparator(@Nullable final Comparator<DialogLocale> comparator) {
			this.sortComparator = comparator;
			return this;
		}
	}
}