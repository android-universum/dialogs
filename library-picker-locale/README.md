Dialogs-Picker-Locale
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adialogs/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adialogs/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:dialogs-picker-locale:${DESIRED_VERSION}@aar"
