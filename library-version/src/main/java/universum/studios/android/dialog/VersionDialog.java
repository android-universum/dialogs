/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.VersionDialogView;

/**
 * A {@link SimpleDialog} implementation that can be used to show the current application version
 * info. This dialog uses default {@link VersionDialogView} implementation as its content view which
 * consists of a single {@link android.widget.ImageView} that displays application's icon and set of
 * {@link android.widget.TextView TextViews} displaying application's name, version info and copyright
 * (if specified).
 * <p>
 * See {@link VersionOptions} for options that can be supplied to the VersionDialog from the desired
 * context via {@link #newInstance(VersionOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogVersionOptions dialogVersionOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class VersionDialog extends SimpleDialog {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "VersionDialog";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of VersionDialog that shows current application version info.
     */
    public VersionDialog() {
        super(R.attr.dialogVersionOptions);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Creates a new instance of VersionDialog with the specified options.
     *
     * @param options Specific options for this type of dialog.
     * @return New VersionDialog instance.
     */
    @NonNull public static VersionDialog newInstance(@NonNull final VersionOptions options) {
        final VersionDialog dialog = new VersionDialog();
        dialog.setOptions(options);
        return dialog;
    }

    /**
     */
    @Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
        return new VersionOptions(resources);
    }

    /**
     */
    @SuppressWarnings("ResourceType")
    @Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
        super.onProcessOptionsStyle(context, optionsStyle);
        if (mOptions instanceof VersionOptions) {
	        final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Version);
	        final VersionOptions options = (VersionOptions) mOptions;
	        for (int i = 0; i < attributes.getIndexCount(); i++) {
		        int index = attributes.getIndex(i);
		        if (index == R.styleable.Dialog_Options_Version_dialogVersionVisibility) {
			        if (!options.isSet(VersionOptions.VERSION_VISIBILITY)) {
				        options.versionVisibility(attributes.getInt(index, options.versionVisibility));
			        }
		        } else if (index == R.styleable.Dialog_Options_Version_dialogCopyright) {
			        if (!options.isSet(VersionOptions.COPYRIGHT)) {
				        options.copyright(attributes.getText(index));
			        }
		        }
	        }
	        attributes.recycle();
        }
    }

    /**
     */
    @Override @NonNull protected View onCreateContentView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.dialog_content_version, container, false);
    }

    /**
     */
    @Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
        if (contentView instanceof VersionDialogView && options instanceof VersionOptions) {
            final VersionOptions versionOptions = (VersionOptions) options;
            final VersionDialogView infoView = (VersionDialogView) contentView;
            infoView.setVersionVisibility(versionOptions.versionVisibility);
            infoView.setCopyright(versionOptions.copyright);
        }
    }

    /**
     */
    @Override protected void onUpdateContentViewPadding() {
        // Do not update content view padding.
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link DialogOptions} implementation specific for the {@link VersionDialog}.
     * <p>
     * Below are listed setters which may be used to supply desired data to {@link VersionDialog}
     * through these options:
     * <ul>
     * <li>{@link #versionVisibility(int)}</li>
     * <li>{@link #copyright(int)}</li>
     * <li>{@link #copyright(CharSequence)}</li>
     * </ul>
     *
     * <h3>Xml attributes</h3>
     * See {@link R.styleable#Dialog_Options_Version VersionOptions Attributes},
     * {@link R.styleable#Dialog_Options DialogOptions Attributes}
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    @SuppressWarnings("ResourceType")
    public static class VersionOptions extends DialogOptions<VersionOptions> {

        /**
         * Creator used to create an instance or array of instances of VersionOptions from {@link Parcel}.
         */
        public static final Creator<VersionOptions> CREATOR = new Creator<VersionOptions>() {

            /**
             */
            @Override public VersionOptions createFromParcel(@NonNull final Parcel source) { return new VersionOptions(source); }

            /**
             */
            @Override public VersionOptions[] newArray(final int size) { return new VersionOptions[size]; }
        };

        /**
         * Flag for <b>version visibility</b> parameter.
         */
        static final int VERSION_VISIBILITY = 0x00000001;

        /**
         * Flag for <b>copyright</b> parameter.
         */
        static final int COPYRIGHT = 0x00000001 << 1;

        /**
         * Version visibility flags.
         */
        int versionVisibility = VersionDialogView.VERSION_NAME | VersionDialogView.VERSION_CODE;

        /**
         * Copyright info text.
         */
        CharSequence copyright;

        /**
         * Creates a new instance of VersionOptions.
         *
         * @param resources Application resources used to obtain values for these options when they
         *                  are requested by theirs resource ids.
         */
        public VersionOptions(@NonNull final Resources resources) {
            super(resources);
        }

        /**
         * Called form {@link #CREATOR} to create an instance of VersionOptions form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected VersionOptions(@NonNull final Parcel source) {
            super(source);
            this.versionVisibility = source.readInt();
            this.copyright = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(versionVisibility);
            TextUtils.writeToParcel(copyright, dest, flags);
        }

        /**
         */
        @Override public VersionOptions merge(@NonNull final DialogOptions options) {
            if (!(options instanceof VersionOptions)) {
                return super.merge(options);
            }
            final VersionOptions versionOptions = (VersionOptions) options;
            if (versionOptions.isSet(VERSION_VISIBILITY)) {
                this.versionVisibility = versionOptions.versionVisibility;
                updateIsSet(VERSION_VISIBILITY);
            }
            if (versionOptions.isSet(COPYRIGHT)) {
                if (versionOptions.copyright != null) {
                    this.copyright = new SpannableStringBuilder(versionOptions.copyright);
                } else {
                    this.copyright = null;
                }
                updateIsSet(COPYRIGHT);
            }
            return super.merge(options);
        }

        /**
         * Sets version visibility flags determining which version info should be displayed in a
         * dialog associated with these options.
         *
         * @param visibility The desired visibility flags. One of {@link VersionDialogView#VERSION_NAME},
         * {@link VersionDialogView#VERSION_CODE} or theirs combination.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogVersionVisibility dialog:dialogVersionVisibility
         * @see #versionVisibility()
         */
        public VersionOptions versionVisibility(@VersionDialogView.VersionVisibility final int visibility) {
            updateIsSet(VERSION_VISIBILITY);
            if (versionVisibility != visibility) {
                this.versionVisibility = visibility;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the version visibility flags, set to these options.
         *
         * @return Version visibility flags.
         * @see #versionVisibility(int)
         */
        @VersionDialogView.VersionVisibility public int versionVisibility() {
            return versionVisibility;
        }

        /**
         * Same as {@link #copyright(CharSequence)} for resource id.
         *
         * @param resId Resource id of the desired copyright info text.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogCopyright dialog:dialogCopyright
         * @see #copyright()
         */
        public VersionOptions copyright(@StringRes final int resId) {
            return copyright(text(resId));
        }

        /**
         * Sets a copyright text for a dialog associated with these options
         *
         * @param copyright The desired copyright text.
         * @return These options to allow methods chaining.
         * @see #copyright()
         */
        public VersionOptions copyright(@Nullable final CharSequence copyright) {
            updateIsSet(COPYRIGHT);
            if (this.copyright == null || !this.copyright.equals(copyright)) {
                this.copyright = copyright;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the copyright info text, set to these options.
         *
         * @return Copyright text.
         */
        @Nullable public CharSequence copyright() {
            return copyright;
        }

        /**
         */
        @Override protected void onParseXmlAttribute(
        		@NonNull final XmlResourceParser xmlParser,
		        final int index,
		        @AttrRes final int attr,
		        @NonNull final Resources resources,
		        @Nullable final Resources.Theme theme
        ) {
            if (attr == R.attr.dialogVersionVisibility) {
                versionVisibility(xmlParser.getAttributeIntValue(index, versionVisibility));
            } else if (attr == R.attr.dialogCopyright) {
                copyright(obtainXmlAttributeText(xmlParser, index, resources));
            } else {
                super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
            }
        }
    }
}