/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.VersionDialog;
import universum.studios.android.dialog.widget.VersionDialogContentView;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link VersionDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link VersionDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface VersionDialogView {

    /*
     * Constants ===================================================================================
     */

    /**
     * Flag determining that version name should be included into version info text.
     */
    int VERSION_NAME = 0x00000001;

    /**
     * Flag determining that version code should be included into version info text.
     */
    int VERSION_CODE = 0x00000001 << 1;

    /**
     * Defines an annotation for determining set of allowed version visibility flags for
     * {@link #setVersionVisibility(int)} method.
     */
    @IntDef(flag = true, value = {
            VERSION_NAME,
            VERSION_CODE
    })
    @Retention(RetentionPolicy.SOURCE)
    @SuppressWarnings("UnnecessaryInterfaceModifier")
    public @interface VersionVisibility {}

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets a version info visibility flags determining which version info should be displayed.
     * <ul>
     * <li>{@link #VERSION_NAME} displays version name from {@link android.content.pm.PackageInfo#versionName PackageInfo.versionName}</li>
     * <li>{@link #VERSION_CODE} displays version code from {@link android.content.pm.PackageInfo#versionCode PackageInfo.versionCode}</li>
     * <li>{@link #VERSION_NAME} | {@link #VERSION_CODE} displays version info in format: <b>VERSION_NAME (VERSION_CODE)</b></li>
     * </ul>
     *
     * @param visibility The desired visibility flags. One of {@link #VERSION_NAME}, {@link #VERSION_CODE}
     * or theirs combination.
     */
    void setVersionVisibility(@VersionVisibility int visibility);

    /**
     * Returns the version info visibility flags.
     * <p>
     * Default value: <b>{@link #VERSION_NAME} | {@link #VERSION_CODE}</b>
     *
     * @return Current version visibility flags. One of {@link #VERSION_NAME}, {@link #VERSION_CODE}
     * or theirs combination.
     * @see #setVersionVisibility(int)
     */
    @VersionVisibility int getVersionVisibility();

    /**
     * Same as {@link #setCopyright(CharSequence)} for resource id.
     *
     * @param resId Resource id of the desired copyright text.
     * @see #getCopyright()
     */
    void setCopyright(@StringRes int resId);

    /**
     * Sets a copyright text to be displayed below the version info text.
     *
     * @param copyright The desired copyright text. May be {@code null} to clear the current one.
     * @see #getCopyright()
     */
    void setCopyright(@Nullable CharSequence copyright);

    /**
     * Returns the copyright text specified for this dialog view.
     *
     * @return Current copyright text displayed below version info text or empty if no copyright
     * has been specified yet.
     * @see #setCopyright(int)
     * @see #setCopyright(CharSequence)
     */
    @NonNull CharSequence getCopyright();
}