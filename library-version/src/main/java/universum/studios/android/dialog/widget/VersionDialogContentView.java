/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.VersionDialog;
import universum.studios.android.dialog.view.VersionDialogView;

/**
 * A {@link VersionDialogView} implementation used by {@link VersionDialog VersionInfoDialog}
 * as content view.
 * <p>
 * This class represents a container for an icon view (ImageView) and set of TextViews used to display
 * current application's name, version info and copyright (if specified). Copyright text can be specified
 * via {@link #setCopyright(CharSequence)} or its resource equivalent {@link #setCopyright(int)}.
 * <p>
 * Whether to show also version code in version info text can be specified via {@link #setVersionVisibility(int)}
 * method by supplying one of {@link #VERSION_NAME}, {@link #VERSION_CODE} flags or theirs combination.
 * By default both, <b>version name</b> and <b>version code</b> are displayed in format:
 * <b>{@code VERSION_NAME (VERSION_CODE)}</b>.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutVersionContent} within the current dialog theme. As long as the
 * default view hierarchy (same view types with same ids) is preserved the proper working of this view
 * is guaranteed. See {@link R.layout#dialog_layout_version} for the info about the default view
 * hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class VersionDialogContentView extends LinearLayout implements VersionDialogView {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    private static final String TAG = "VersionDialogContentView";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * View displaying application icon.
     */
    private ImageView mIconView;

    /**
     * View displaying application name text.
     */
    private TextView mTitleView;

    /**
     * View displaying application version (name + code) depending on {@link #mVersionVisibility}.
     */
    private TextView mVersionView;

    /**
     * Visibility flags determining which version info (name, code) to show.
     */
    private int mVersionVisibility = VERSION_NAME | VERSION_CODE;

    /**
     * View displaying copyright text.
     */
    private TextView mCopyrightView;

    /**
     * Information about the current application's package.
     */
    private PackageInfo mPackageInfo;

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #VersionDialogContentView(Context, AttributeSet)} without attributes.
     */
    public VersionDialogContentView(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #VersionDialogContentView(Context, AttributeSet, int)} with {@code 0} as attribute
     * for default style.
     */
    public VersionDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs, 0, 0);
    }

    /**
     * Same as {@link #VersionDialogContentView(Context, AttributeSet, int, int)} with {@code 0} as
     * default style.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public VersionDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    /**
     * Creates a new instance of VersionDialogContentView for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     * @param defStyleRes  Resource id of the default style for the new view.
     */
    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VersionDialogContentView(
    		@NonNull final Context context,
		    @Nullable final AttributeSet attrs,
		    @AttrRes final int defStyleAttr,
		    @StyleRes final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Called from one of constructors of this view to perform its initialization.
     * <p>
     * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
     * this view specific data from it that can be used to configure this new view instance. The
     * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
     * from the current theme provided by the specified <var>context</var>.
     */
    @SuppressLint("LongLogTag")
    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        final Resources.Theme theme = context.getTheme();
        final TypedValue typedValue = new TypedValue();
        // Resolve layout resource from which to inflate view hierarchy.
        int layoutResource = R.layout.dialog_layout_version;
        if (theme.resolveAttribute(R.attr.dialogLayoutVersionContent, typedValue, true)) {
            layoutResource = typedValue.resourceId;
        }
        this.inflateHierarchy(context, layoutResource);
        // Default set up.
        final PackageManager packageManager = context.getPackageManager();
        final String packageName = context.getPackageName();
        try {
            this.mPackageInfo = packageManager.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to retrieve current application's package info.", e);
        }
        this.updateVersionInfo();
        if (mPackageInfo != null && mPackageInfo.applicationInfo != null) {
            if (mTitleView != null) {
                mTitleView.setText(mPackageInfo.applicationInfo.loadLabel(packageManager));
            }
            if (mIconView != null) {
                mIconView.setImageDrawable(mPackageInfo.applicationInfo.loadIcon(packageManager));
            }
        }
    }

    /**
     * Called to inflate a view hierarchy of this view.
     *
     * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
     *                 layout resource as view hierarchy for this view.
     * @param resource Resource id of the layout which should represent a view hierarchy of this view.
     */
    private void inflateHierarchy(final Context context, final int resource) {
        if (resource > 0) {
            LayoutInflater.from(context).inflate(resource, this);
            this.mIconView = (ImageView) findViewById(R.id.dialog_version_info_icon);
            this.mTitleView = (TextView) findViewById(R.id.dialog_version_info_title);
            this.mVersionView = (TextView) findViewById(R.id.dialog_version_info_version);
            this.mCopyrightView = (TextView) findViewById(R.id.dialog_version_info_copyright);
            onFinishInflate();
        }
    }

    /**
     */
    @Override public void setVersionVisibility(@VersionVisibility final int visibility) {
        if (mVersionVisibility != visibility) {
            this.mVersionVisibility = visibility;
            this.updateVersionInfo();
        }
    }

    /**
     * Updates the version view with the current version info, if available, according to the current
     * {@link #mVersionVisibility} flags.
     */
    private void updateVersionInfo() {
        if (mVersionView != null && mPackageInfo != null) {
            String versionInfo = "";
            if ((mVersionVisibility & VERSION_NAME) != 0) {
                versionInfo += mPackageInfo.versionName;
            }
            if ((mVersionVisibility & VERSION_CODE) != 0) {
                if (versionInfo.length() > 0) {
                    versionInfo += " (" + Integer.toString(mPackageInfo.versionCode) + ")";
                } else {
                    versionInfo += Integer.toString(mPackageInfo.versionCode);
                }
            }
            if (versionInfo.length() > 0) {
                mVersionView.setText(getResources().getString(R.string.dialog_version_format, versionInfo));
            }
        }
    }

    /**
     */
    @Override public int getVersionVisibility() {
        return mVersionVisibility;
    }

    /**
     */
    @Override public void setCopyright(@StringRes final int resId) {
        setCopyright(getResources().getText(resId));
    }

    /**
     */
    @Override public void setCopyright(@Nullable final CharSequence copyright) {
        if (mCopyrightView != null) {
            mCopyrightView.setText(copyright);
            mCopyrightView.setVisibility(TextUtils.isEmpty(copyright) ? GONE : VISIBLE);
        }
    }

    /**
     */
    @Override @NonNull public CharSequence getCopyright() {
        return mCopyrightView != null ? mCopyrightView.getText() : "";
    }

    /*
     * Inner classes ===============================================================================
     */
}