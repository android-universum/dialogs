/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.DatePickerDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link DatePickerDialog DatePickerDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.DatePickerDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface DatePickerDialogView extends DialogView {

    /**
     * todo:
     *
     * @param timeZone
     */
    void setTimeZone(@NonNull TimeZone timeZone);

    /**
     * Same as {@link #setDate(long)} for {@link Date} object.
     *
     * @param date The desired date to be selected.
     * @see #getDateInMillis()
     */
    void setDate(@NonNull Date date);

    /**
     * Sets a date in milliseconds to be used to set values of <b>year, month, day</b> in
     * {@link universum.studios.android.ui.widget.CalendarView CalendarView} of this dialog view.
     * CalendarView will be shown at the requested year and month with a day of the specified
     * date selected.
     *
     * @param milliseconds The desired date in milliseconds.
     * @see #getDateInMillis()
     */
    void setDate(long milliseconds);

    /**
     * Returns the current picked date via {@link universum.studios.android.ui.widget.CalendarView CalendarView}
     * in milliseconds.
     *
     * @return Picked date in milliseconds or {@code null} if there is no picked date yet.
     * @see #setDate(long)
     * @see #setDate(Date)
     */
    @Nullable Long getDateInMillis();

    /**
     * Same as {@link #setMinMaxDate(long, long)} for {@link Date Dates} object.
     *
     * @param minDate The desired minimum date allowed to be picked.
     * @param maxDate The desired maximum date allowed to be picked.
     * @see #getMinDateInMillis()
     * @see #getMaxDateInMillis()
     */
    void setMinMaxDate(@NonNull Date minDate, @NonNull Date maxDate);

    /**
     * Sets the minimum and maximum dates in milliseconds allowed to be picked within this dialog view.
     *
     * @param minMilliseconds The desired minimum date in milliseconds allowed to be picked.
     * @param maxMilliseconds The desired maximum date in milliseconds allowed to be picked. Specify
     *                        {@code 0} if "infinite" date range should be provided.
     * @see #getMinDateInMillis()
     * @see #getMaxDateInMillis()
     */
    void setMinMaxDate(long minMilliseconds, long maxMilliseconds);

    /**
     * Returns the minimum date in milliseconds that is allowed to be picked.
     *
     * @return Date in milliseconds.
     * @see #setMinMaxDate(long, long)
     * @see #setMinMaxDate(Date, Date)
     */
    long getMinDateInMillis();

    /**
     * Returns the maximum date in milliseconds that is allowed to be picked.
     *
     * @return Date in milliseconds.
     * @see #setMinMaxDate(long, long)
     * @see #setMinMaxDate(Date, Date)
     */
    long getMaxDateInMillis();
}