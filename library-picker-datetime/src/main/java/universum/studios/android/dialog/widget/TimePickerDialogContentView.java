/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.AttrRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.PluralsRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.TimePickerDialog;
import universum.studios.android.dialog.view.TimePickerDialogView;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link TimePickerDialogView} implementation used by {@link TimePickerDialog TimePickerDialog}
 * as content view.
 * <p>
 * This class represents a container for views like {@link universum.studios.android.ui.widget.CircularNumberPicker CircularNumberPicker}
 * to allow to a user to pick its desired time. Whether hour + minute or just minute can be picked can
 * be specified via {@link #setPickers(int)} and supplying one of {@link #PICKER_HOUR} or {@link #PICKER_MINUTE}
 * or theirs combination. Initial time can be supplied via {@link #setTime(Date)} or
 * {@link #setTime(long)} and the current picked can be obtained via {@link #getTime()}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutTimeContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_time} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class TimePickerDialogContentView extends LinearLayout
		implements
		TimePickerDialogView,
		TimePickerDialogTitleView.OnSelectionChangeListener,
		TimePickersLayout.TimeValueWatcher {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "TimePickerDialogContentView";

	/**
	 * Number format used to format picked hour or minute number to text visible within title view.
	 */
	private static final DecimalFormat NUMBER_FORMAT = new DecimalFormat("00");

	/**
	 * Delay for change to minute picker from hour picker after the hour number has been selected.
	 */
	private static final int CHANGE_PICKERS_ON_HOUR_SELECTION_DELAY = 200;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Action which changes the current picker to minute picker if it is not visible yet.
	 */
	private final Runnable CHANGE_TO_MINUTE_PICKER = new Runnable() {

		/**
		 */
		@Override public void run() {
			if (mPickersLayout != null && mPickersLayout.currentPicker() == PICKER_HOUR && mTitleView != null) {
				mTitleView.selectMinuteView();
			}
		}
	};

	/**
	 * Title view to present the current picked hour and minute number in the text form.
	 */
	TimePickerDialogTitleView mTitleView;

	/**
	 * Layout containing two number pickers. One to allow picking of hour number, the other one to
	 * allow picking of minute number.
	 */
	TimePickersLayout mPickersLayout;

	/**
	 * Boolean flag indicating whether the change of time via one of number pickers should be ignored
	 * or not.
	 */
	boolean mIgnoreTimePickerValueChange = false;

	/**
	 * Calendar used to resolve picked time value.
	 */
	private Calendar mCalendar;

	/**
	 * Current time value presented by the Number pickers.
	 */
	private long mTime;

	/**
	 * Pickers flag. Determines which pickers to present within this view.
	 */
	private int mPickers = PICKER_HOUR | PICKER_MINUTE;

	/**
	 * Resource id of plurals string used to format pocked number value in case when only one picker
	 * is presented by this view.
	 */
	private int mTimeQuantityTextRes;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #TimePickerDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public TimePickerDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #TimePickerDialogContentView(Context, AttributeSet, int)} with {@code 0} as
	 * attribute for default style.
	 */
	public TimePickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, 0, 0);
	}

	/**
	 * Same as {@link #TimePickerDialogContentView(Context, AttributeSet, int, int)} with {@code 0}
	 * as default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public TimePickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of TimePickerDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public TimePickerDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.mCalendar = Calendar.getInstance();
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		int layoutResource = R.layout.dialog_layout_time;
		if (theme.resolveAttribute(R.attr.dialogLayoutTimeContent, typedValue, true)) {
			layoutResource = typedValue.resourceId;
		}
		this.inflateHierarchy(context, layoutResource);
		// Default set up.
		setTime(System.currentTimeMillis());
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mPickersLayout = (TimePickersLayout) findViewById(R.id.dialog_time_pickers);
			this.mTitleView = (TimePickerDialogTitleView) findViewById(R.id.dialog_time_title);
			if (mPickersLayout != null) {
				mPickersLayout.setTimeValueWatcher(this);
			}
			if (mTitleView != null) {
				mTitleView.selectHourView();
				mTitleView.setOnSelectionChangeListener(this);
			}
			onFinishInflate();
		}
	}

	/**
	 */
	@Override public void setPickers(@Pickers final int pickers) {
		if (mPickers != pickers) handlePickersUpdate(pickers);
	}

	/**
	 * Handles the change in pickers flag. This will perform all changes needed to set up current
	 * layout for the specified <var>pickers</var> flag, so it will show/hide necessary views and
	 * perform needed changes in selection and so on.
	 *
	 * @param pickers The pickers flag by which to update the current layout.
	 */
	private void handlePickersUpdate(@Pickers final int pickers) {
		if (mPickersLayout == null || mTitleView == null) {
			return;
		}
		switch (mPickers = pickers) {
			case PICKER_HOUR | PICKER_MINUTE:
				mTitleView.setMode(TimePickerDialogTitleView.MODE_DEFAULT);
				switch (mPickersLayout.currentPicker()) {
					case PICKER_HOUR:
						mTitleView.selectHourView();
						break;
					case PICKER_MINUTE:
						mTitleView.selectMinuteView();
						break;
				}
				mTitleView.setPluralsText("");
				break;
			case PICKER_HOUR:
				if (mPickersLayout.currentPicker() == PICKER_MINUTE) {
					mPickersLayout.changePickersAnimated(false);
					mTitleView.setMode(TimePickerDialogTitleView.MODE_HOUR);
					mTitleView.selectHourView();
					if (mTimeQuantityTextRes > 0) {
						updateTitleViewPluralsText(getHour());
					} else {
						mTitleView.setPluralsText("");
					}
				}
				break;
			case PICKER_MINUTE:
				if (mPickersLayout.currentPicker() == PICKER_HOUR) {
					mPickersLayout.changePickersAnimated(false);
					mTitleView.setMode(TimePickerDialogTitleView.MODE_MINUTE);
					mTitleView.selectMinuteView();
					if (mTimeQuantityTextRes > 0) {
						updateTitleViewPluralsText(getMinute());
					} else {
						mTitleView.setPluralsText("");
					}
				}
				break;
		}
	}

	/**
	 */
	@Override @Pickers public int getPickers() {
		return mPickers;
	}

	/**
	 */
	@Override public void setTimeQuantityTextRes(@PluralsRes final int resId) {
		if (mTimeQuantityTextRes != resId) {
			this.mTimeQuantityTextRes = resId;
			switch (mPickers) {
				case PICKER_HOUR:
				case PICKER_MINUTE:
					if (mTimeQuantityTextRes > 0) {
						updateTitleViewPluralsText(mPickers == PICKER_HOUR ? getHour() : getMinute());
					} else if (mTitleView != null) {
						mTitleView.setPluralsText("");
					}
					break;
			}
		}
	}

	/**
	 */
	@Override public void setTimeZone(@NonNull TimeZone timeZone) {
		mCalendar.setTimeZone(timeZone);
	}

	/**
	 */
	@Override public void setTime(@NonNull final Date time) {
		setTime(time.getTime());
	}

	/**
	 */
	@Override public void setTime(final long milliseconds) {
		if (mTime != milliseconds) {
			mCalendar.clear();
			mCalendar.setTimeInMillis(milliseconds);
			mTime = milliseconds;

			updateTitleView();

			if (mPickersLayout != null) {
				final int hour = mCalendar.get(Calendar.HOUR);
				final int minute = mCalendar.get(Calendar.MINUTE);

				this.mIgnoreTimePickerValueChange = true;

				mPickersLayout.setHour(hour);
				mPickersLayout.setMinute(minute);

				this.mIgnoreTimePickerValueChange = false;
			}
		}
	}

	/**
	 * Updates the current picked time to the given values.
	 *
	 * @param hour   The current picked hour number.
	 * @param minute The current picked minute number.
	 * @param amPm   The AM/PM flag. One of {@link Calendar#AM} or {@link Calendar#PM}.
	 */
	final void updateTimeTo(final int hour, final int minute, final int amPm) {
		this.mCalendar.clear();
		this.mCalendar.set(Calendar.HOUR, hour);
		this.mCalendar.set(Calendar.MINUTE, minute);
		this.mCalendar.set(Calendar.AM_PM, amPm);
		this.mTime = mCalendar.getTimeInMillis();
	}

	/**
	 * Updates title view to present current picked time.
	 */
	final void updateTitleView() {
		if (mTitleView != null) {
			final int hour = getHour();
			final int minute = getMinute();
			switch (mPickers) {
				case PICKER_HOUR | PICKER_MINUTE:
					mTitleView.setHourText(NUMBER_FORMAT.format(hour != 0 ? hour : 12));
					mTitleView.setMinuteText(NUMBER_FORMAT.format(minute));
					switch (getAmPm()) {
						case Calendar.AM:
							mTitleView.selectAmButton();
							break;
						case Calendar.PM:
							mTitleView.selectPmButton();
							break;
					}
					break;
				case PICKER_HOUR:
					mTitleView.setHourText(NUMBER_FORMAT.format(hour != 0 ? hour : 12));
					updateTitleViewPluralsText(hour);
					break;
				case PICKER_MINUTE:
					mTitleView.setMinuteText(NUMBER_FORMAT.format(minute));
					updateTitleViewPluralsText(minute);
					break;
			}
		}
	}

	/**
	 * Updates the AM/PM text within title view with quantity text obtained from an application resources
	 * for the current {@link #mTimeQuantityTextRes} resource id (if valid) for the given number.
	 *
	 * @param number The number to be used as argument for the quantity text.
	 */
	private void updateTitleViewPluralsText(final int number) {
		if (mTimeQuantityTextRes > 0 && mTitleView != null) {
			mTitleView.setPluralsText(getResources().getQuantityString(
					mTimeQuantityTextRes,
					number,
					number
			).replace(Integer.toString(number) + " ", ""));
		}
	}

	/**
	 */
	@Override public long getTime() {
		return mTime;
	}

	/**
	 * Extras value of hour from the current picked time.
	 *
	 * @return Hour of the current picked time.
	 */
	private int getHour() {
		mCalendar.clear();
		mCalendar.setTimeInMillis(mTime);
		return mCalendar.get(Calendar.HOUR);
	}

	/**
	 * Extras value of minute from the current picked time.
	 *
	 * @return Minute of the current picked time.
	 */
	private int getMinute() {
		mCalendar.clear();
		mCalendar.setTimeInMillis(mTime);
		return mCalendar.get(Calendar.MINUTE);
	}

	/**
	 * Extras AM/PM value from the current picked time.
	 *
	 * @return {@link Calendar#AM} or {@link Calendar#PM}.
	 */
	private int getAmPm() {
		mCalendar.clear();
		mCalendar.setTimeInMillis(mTime);
		return mCalendar.get(Calendar.AM_PM);
	}

	/**
	 */
	@Override public void onHourMinuteSelectionChanged(@NonNull final TimePickerDialogTitleView titleView, final boolean hourSelected) {
		if (mPickersLayout != null) {
			boolean changePickers = false;
			switch (mPickersLayout.currentPicker()) {
				case PICKER_HOUR:
					changePickers = !hourSelected;
					break;
				case PICKER_MINUTE:
					changePickers = hourSelected;
					break;
			}
			if (changePickers) {
				mPickersLayout.changePickersAnimated(true);
			}
		}
	}

	/**
	 */
	@Override public void onAmPmSelectionChanged(@NonNull final TimePickerDialogTitleView titleView, final boolean amSelected) {
		updateTimeTo(getHour(), getMinute(), amSelected ? Calendar.AM : Calendar.PM);
	}

	/**
	 */
	@Override public void onHourChanged(@IntRange(from = 1, to = 12) final int hour) {
		if (!mIgnoreTimePickerValueChange) {
			updateTimeTo(hour, getMinute(), getAmPm());
			updateTitleView();
		}
	}

	/**
	 */
	@Override public void onHourSelected(@IntRange(from = 1, to = 12) final int hour) {
		if (!mIgnoreTimePickerValueChange) {
			updateTimeTo(hour, getMinute(), getAmPm());
			updateTitleView();
			postDelayed(CHANGE_TO_MINUTE_PICKER, CHANGE_PICKERS_ON_HOUR_SELECTION_DELAY);
		}
	}

	/**
	 */
	@Override public void onMinuteChanged(@IntRange(from = 0, to = 59) final int minute) {
		if (!mIgnoreTimePickerValueChange) {
			updateTimeTo(getHour(), minute, getAmPm());
			updateTitleView();
		}
	}

	/**
	 */
	@Override public void onMinuteSelected(@IntRange(from = 0, to = 59) final int minute) {
		if (!mIgnoreTimePickerValueChange) {
			updateTimeTo(getHour(), minute, getAmPm());
			updateTitleView();
		}
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.time = mTime;
		savedState.pickers = mPickers;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());

		this.mTime = savedState.time;

		if (savedState.pickers != mPickers) {
			this.handlePickersUpdate(savedState.pickers);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link TimePickerDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		long time;

		/**
		 */
		int pickers;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.time = source.readLong();
			this.pickers = source.readInt();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeLong(time);
			dest.writeInt(pickers);
		}
	}
}