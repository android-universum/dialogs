/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.PluralsRes;
import universum.studios.android.dialog.TimePickerDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link TimePickerDialog TimePickerDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.TimePickerDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface TimePickerDialogView extends DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Flag to determine <b>HOUR</b> number picker.
	 */
	int PICKER_HOUR = 0x01;

	/**
	 * Flag to determine <b>MINUTE</b> number picker.
	 */
	int PICKER_MINUTE = 0x02;

	/**
	 * Defines an annotation for determining set of allowed picker flags for {@link #setPickers(int)}
	 * method.
	 */
	@IntDef(flag = true, value = {PICKER_HOUR, PICKER_MINUTE})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface Pickers {}

	/**
	 * Defines an annotation for determining set of possible AM/PM types.
	 */
	@IntDef({Calendar.AM, Calendar.PM})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface AmPm {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a pickers flags to determine which number pickers should be presented within this dialog
	 * view.
	 *
	 * @param pickers The desired pickers flags. Combination of {@link TimePickerDialogView#PICKER_HOUR}
	 *                and {@link TimePickerDialogView#PICKER_MINUTE}.
	 */
	void setPickers(@Pickers int pickers);

	/**
	 * Returns the current pickers flags.
	 * <p>
	 * Default value: <b>{@link #PICKER_HOUR PICKER_HOUR} | {@link #PICKER_MINUTE PICKER_MINUTE}</b>
	 *
	 * @return Combination of {@link #PICKER_HOUR} and {@link #PICKER_MINUTE}.
	 */
	@Pickers int getPickers();

	/**
	 * Sets a resource id of the quantity text to be used for formatting of current picked time value.
	 * <p>
	 * <b>Note</b>, that this quantity text is used only in case when there is only one number picker
	 * presented.
	 *
	 * @param resId Resource id of the desired quantity text.
	 */
	void setTimeQuantityTextRes(@PluralsRes int resId);

	/**
	 * todo:
	 *
	 * @param timeZone
	 */
	void setTimeZone(@NonNull TimeZone timeZone);

	/**
	 * Same as {@link #setTime(long)} for {@link Date} object.
	 *
	 * @param time The desired time to be selected.
	 * @see #getTime()
	 */
	void setTime(@NonNull Date time);

	/**
	 * Sets a time in milliseconds to be used to set values of <b>hour and minute</b> in
	 * {@link universum.studios.android.ui.widget.CircularNumberPicker CircularNumberPickers} of this
	 * dialog view. Corresponding CircularNumberPicker will be shown with associated number (hour, minute)
	 * selected.
	 *
	 * @param milliseconds The desired date in milliseconds.
	 * @see #getTime()
	 */
	void setTime(long milliseconds);

	/**
	 * Returns the current picked time via {@link universum.studios.android.ui.widget.CircularNumberPicker CircularNumberPickers}
	 * in milliseconds.
	 *
	 * @return Picked time in milliseconds.
	 * @see #setTime(long)
	 * @see #setTime(Date)
	 */
	long getTime();
}