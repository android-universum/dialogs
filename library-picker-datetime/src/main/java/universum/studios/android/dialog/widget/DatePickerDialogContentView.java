/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.AttrRes;
import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.DatePickerDialog;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.adapter.DialogBaseAdapter;
import universum.studios.android.dialog.view.DatePickerDialogView;
import universum.studios.android.ui.UiConfig;
import universum.studios.android.ui.util.ResourceUtils;
import universum.studios.android.ui.widget.CalendarView;
import universum.studios.android.ui.widget.MonthView;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link universum.studios.android.dialog.view.DatePickerDialogView} implementation used by
 * {@link DatePickerDialog DatePickerDialog} as content view.
 * <p>
 * This class represents a container for views like {@link universum.studios.android.ui.widget.CalendarView},
 * {@link DatePickerDialogTitleView} to allow to a user to pick its desired date. A date range to be provided
 * by the calendar view can be specified via {@link #setMinMaxDate(Date, Date)} or
 * {@link #setMinMaxDate(long, long)}. Initial date that should be displayed to the user can be specified
 * via {@link #setDate(Date)} and the current picked can be obtained via {@link #getDateInMillis()}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutDateContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_date} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DatePickerDialogContentView extends LinearLayout implements DatePickerDialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DatePickerDialogContentView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Listener for {@link #ANIM_FADE_OUT} alpha animation used to update visibility of faded out view
	 * after animation finishes.
	 */
	private final FadeOutAnimationListener FADE_OUT_ANIM_LISTENER = new FadeOutAnimationListener();

	/**
	 * Animation used to fade in view.
	 */
	private final Animation ANIM_FADE_IN = new AlphaAnimation(0, 1);

	/**
	 * Animation used to fade out view.
	 */
	private final Animation ANIM_FADE_OUT = new AlphaAnimation(1, 0);

	/**
	 * View to present picked date in a better form. This view also allows to change the year of the
	 * CalendarView presented within this dialog content view.
	 */
	DatePickerDialogTitleView mTitleView;

	/**
	 * Calendar view which allows picking of a specific date.
	 */
	CalendarView mCalendarView;

	/**
	 * List view presenting data set of years provided by {@link #mYearsAdapter}.
	 */
	ListView mYearsListView;

	/**
	 * Adapter providing data set of years for {@link #mYearsListView}.
	 */
	YearsAdapter mYearsAdapter;

	/**
	 * Button used to switch to the previous available month in CalendarView (if possible).
	 */
	ImageButton mMonthButtonPrev;

	/**
	 * Button used to switch to the next available month in CalendarView (if possible).
	 */
	ImageButton mMonthButtonNext;

	/**
	 * Container layout in which is placed {@link #mCalendarView} with {@link #mMonthButtonPrev} and
	 * {@link #mMonthButtonNext}.
	 */
	private View mCalendarContainer;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #DatePickerDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public DatePickerDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #DatePickerDialogContentView(Context, AttributeSet, int)} with {@code 0} as
	 * attribute for default style.
	 */
	public DatePickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, 0, 0);
	}

	/**
	 * Same as {@link #DatePickerDialogContentView(Context, AttributeSet, int, int)} with {@code 0}
	 * as default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public DatePickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of DatePickerDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public DatePickerDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		int layoutResource = R.layout.dialog_layout_date;
		if (theme.resolveAttribute(R.attr.dialogLayoutDateContent, typedValue, true)) {
			layoutResource = typedValue.resourceId;
		}
		this.inflateHierarchy(context, layoutResource);
		// Default set up.
		ANIM_FADE_IN.setDuration(UiConfig.ANIMATION_DURATION_LONG);
		ANIM_FADE_OUT.setDuration(100);
		ANIM_FADE_OUT.setAnimationListener(FADE_OUT_ANIM_LISTENER);
		this.mYearsAdapter = new YearsAdapter(context, LayoutInflater.from(context));
		if (mYearsListView != null) {
			mYearsListView.setAdapter(mYearsAdapter);
		}
		if (mCalendarView != null && mTitleView != null) {
			mCalendarView.setSelectedDate(System.currentTimeMillis());
			mYearsAdapter.changeYearBoundaries(mCalendarView.getCalendar().get(Calendar.YEAR), 0);
		}
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mTitleView = (DatePickerDialogTitleView) findViewById(R.id.dialog_date_title);
			this.mCalendarContainer = findViewById(R.id.dialog_calendar_container);
			if (mCalendarContainer != null) {
				this.mMonthButtonPrev = (ImageButton) mCalendarContainer.findViewById(R.id.dialog_button_prev);
				this.mMonthButtonNext = (ImageButton) mCalendarContainer.findViewById(R.id.dialog_button_next);
				this.mCalendarView = (CalendarView) mCalendarContainer.findViewById(R.id.dialog_calendar_view);
			}
			this.mYearsListView = (ListView) findViewById(R.id.dialog_adapter_view);
			final ListenerCallbacks listenerCallbacks = new ListenerCallbacks();
			if (mCalendarView != null) {
				mCalendarView.setOnDateSelectionListener(listenerCallbacks);
				mCalendarView.setOnMonthChangeListener(listenerCallbacks);
				if (mMonthButtonPrev != null)
					mMonthButtonPrev.setOnClickListener(listenerCallbacks);
				if (mMonthButtonNext != null)
					mMonthButtonNext.setOnClickListener(listenerCallbacks);
			}
			if (mYearsListView != null) mYearsListView.setOnItemClickListener(listenerCallbacks);
			if (mTitleView != null) mTitleView.setOnViewsSelectionListener(listenerCallbacks);
			onFinishInflate();
		}
	}

	/**
	 */
	@Override public void setTimeZone(@NonNull final TimeZone timeZone) {
		if (mCalendarView != null) {
			this.mCalendarView.setTimeZone(timeZone);
		}
	}

	/**
	 */
	@Override public void setDate(@NonNull final Date date) {
		setDate(date.getTime());
	}

	/**
	 */
	@Override public void setDate(final long milliseconds) {
		if (mCalendarView != null) mCalendarView.setSelectedDate(milliseconds);
	}

	/**
	 */
	@Override @Nullable public Long getDateInMillis() {
		return mCalendarView != null ? mCalendarView.getSelectedDateInMillis() : null;
	}

	/**
	 */
	@Override public void setMinMaxDate(@NonNull final Date minDate, @NonNull final Date maxDate) {
		setMinMaxDate(minDate.getTime(), maxDate.getTime());
	}

	/**
	 */
	@Override public void setMinMaxDate(final long minMilliseconds, final long maxMilliseconds) {
		if (mCalendarView != null) {
			mCalendarView.getCalendarAdapter().setMinMaxDate(minMilliseconds, maxMilliseconds);
			final Calendar calendar = mCalendarView.getCalendar();
			calendar.clear();
			calendar.setTimeInMillis(minMilliseconds);
			final int minYear = calendar.get(Calendar.YEAR);
			if (maxMilliseconds != 0) {
				calendar.clear();
				calendar.setTimeInMillis(maxMilliseconds);
				final int maxYear = calendar.get(Calendar.YEAR);
				mYearsAdapter.changeYearBoundaries(minYear, maxYear);
			} else {
				mYearsAdapter.changeYearBoundaries(minYear, 0);
			}
		}
	}

	/**
	 */
	@Override public long getMinDateInMillis() {
		return mCalendarView != null ? mCalendarView.getCalendarAdapter().getDateMinInMillis() : 0;
	}

	/**
	 */
	@Override public long getMaxDateInMillis() {
		return mCalendarView != null ? mCalendarView.getCalendarAdapter().getDateMaxInMillis() : 0;
	}

	/**
	 */
	@Override protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mCalendarView == null || mYearsListView == null) {
			return;
		}
		final int calendarViewWidth = mCalendarView.getMeasuredWidth();
		final int calendarViewHeight = mCalendarView.getMeasuredHeight();
		final ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mYearsListView.getLayoutParams();
		layoutParams.width = calendarViewWidth;
		layoutParams.height = calendarViewHeight;
		mYearsListView.measure(
				View.MeasureSpec.makeMeasureSpec(calendarViewWidth, View.MeasureSpec.EXACTLY),
				View.MeasureSpec.makeMeasureSpec(calendarViewHeight, View.MeasureSpec.EXACTLY)
		);
	}

	/**
	 * Handles selection of the specified <var>date</var> within the currently visible MonthView.
	 * This will update values within {@link #mTitleView} and also updates selected year within
	 * {@link #mYearsListView}.
	 *
	 * @param calendarView       Calendar view within which was the date selected.
	 * @param dateInMilliseconds The selected date. May be {@code null} if the previous selected date
	 *                           has been cleared.
	 */
	final void handleDateSelected(final CalendarView calendarView, final Long dateInMilliseconds) {
		if (mTitleView == null || dateInMilliseconds == null) {
			return;
		}
		final Calendar calendar = calendarView.getCalendar();
		calendar.setTimeInMillis(dateInMilliseconds);
		String dayOfWeekName = onObtainCalendarFieldName(
				calendar,
				Calendar.DAY_OF_WEEK,
				MonthView.CALENDAR_STYLE_SHORT,
				calendarView.getLocale()
		);
		dayOfWeekName = dayOfWeekName.substring(0, 1).toUpperCase() + dayOfWeekName.substring(1, dayOfWeekName.length());
		String primaryText = dayOfWeekName;
		String monthName = onObtainCalendarFieldName(
				calendar,
				Calendar.MONTH,
				MonthView.CALENDAR_STYLE_SHORT,
				calendarView.getLocale()
		);
		monthName = monthName.substring(0, 1).toUpperCase() + monthName.substring(1, monthName.length());
		primaryText += ", " + monthName;
		final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		primaryText += " " + Integer.toString(dayOfMonth);
		mTitleView.setPrimaryText(primaryText);
		final int year = calendar.get(Calendar.YEAR);
		mTitleView.setSecondaryText(Integer.toString(year));
		if (mYearsAdapter != null) {
			updateYearsAdapterSelectedPosition(mYearsAdapter.positionOfYear(year));
		}
	}

	/**
	 * Invoked to obtain a name of a calendar field with the specified <var>field</var> identifier
	 * from the given <var>calendar</var> for the specified <var>locale</var>.
	 * <p>
	 * This method is here primarily to support displaying of calendar field names in this view also
	 * for pre {@link Build.VERSION_CODES#GINGERBREAD GINGERBREAD}.
	 * <p>
	 * <b>Note</b>, that for pre {@link Build.VERSION_CODES#GINGERBREAD GINGERBREAD} Android
	 * version, this method will returns only English name regardless the specified <var>locale</var>.
	 *
	 * @param calendar The calendar from which to obtain the field's name.
	 * @param field    The desired field identifier.
	 * @param style    Style flag for the requested name. One of {@link MonthView#CALENDAR_STYLE_SHORT}
	 *                 or {@link MonthView#CALENDAR_STYLE_LONG}.
	 * @param locale   The locale for which to obtain the requested name.
	 * @return Name of the requested calendar field.
	 */
	@NonNull protected String onObtainCalendarFieldName(@NonNull final Calendar calendar, final int field, final int style, @NonNull final Locale locale) {
		return MonthView.resolveCalendarFieldName(calendar, field, style, locale);
	}

	/**
	 * Updates the current selection of the {@link #mYearsAdapter} to the specified <var>position</var>.
	 *
	 * @param position The position that should be selected within years adapter.
	 */
	void updateYearsAdapterSelectedPosition(final int position) {
		if (mYearsAdapter != null) mYearsAdapter.updateSelectedPosition(position);
	}

	/**
	 * Changes current visibility presence of the content views (calendar, years list). The change
	 * will be animated using cross-fade animation.
	 *
	 * @param showCalendarView {@code True} to make calendar view visible, {@code false} to make
	 *                         years list view visible.
	 */
	final void switchContentViews(final boolean showCalendarView) {
		if (mCalendarView == null || mYearsListView == null) {
			return;
		}
		if (showCalendarView && mYearsListView.getAnimation() != null ||
				!showCalendarView && mCalendarView.getAnimation() != null) {
			return;
		}
		if (showCalendarView) {
			FADE_OUT_ANIM_LISTENER.view = mYearsListView;
			FADE_OUT_ANIM_LISTENER.visibility = INVISIBLE;
			this.animateView(mYearsListView, ANIM_FADE_OUT);
			mCalendarContainer.setVisibility(View.VISIBLE);
			this.animateView(mCalendarContainer, ANIM_FADE_IN);
		} else {
			FADE_OUT_ANIM_LISTENER.view = mCalendarContainer;
			FADE_OUT_ANIM_LISTENER.visibility = INVISIBLE;
			this.animateView(mCalendarContainer, ANIM_FADE_OUT);
			mYearsAdapter.notifyDataSetChangeIfPending();
			mYearsListView.setVisibility(View.VISIBLE);
			final int visibleItems = mYearsListView.getLastVisiblePosition() - mYearsListView.getFirstVisiblePosition();
			mYearsListView.setSelection(mYearsAdapter.selectedPosition - visibleItems / 2);
			this.animateView(mYearsListView, ANIM_FADE_IN);
		}
	}

	/**
	 * Animates the specified view using the specified animation.
	 *
	 * @param view      The view to animate.
	 * @param animation Animations used to animate the view.
	 */
	private void animateView(final View view, final Animation animation) {
		if (view.getAnimation() != null) {
			view.clearAnimation();
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			view.setLayerType(LAYER_TYPE_HARDWARE, null);
		}
		view.startAnimation(animation);
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.minYear = mYearsAdapter.minYear;
		savedState.maxYear = mYearsAdapter.maxYear;
		savedState.yearSelection = mYearsAdapter.selectedPosition;
		savedState.calendarViewVisible = mCalendarView != null && mCalendarView.getVisibility() == VISIBLE;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		mYearsAdapter.selectedPosition = savedState.yearSelection;
		mYearsAdapter.changeYearBoundaries(savedState.minYear, savedState.maxYear);
		if (!savedState.calendarViewVisible) {
			switchContentViews(false);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link DatePickerDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		int minYear, maxYear, yearSelection;

		/**
		 */
		boolean calendarViewVisible = true;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.minYear = source.readInt();
			this.maxYear = source.readInt();
			this.yearSelection = source.readInt();
			this.calendarViewVisible = source.readInt() == 1;
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(minYear);
			dest.writeInt(maxYear);
			dest.writeInt(yearSelection);
			dest.writeInt(calendarViewVisible ? 1 : 0);
		}
	}

	/**
	 * Adapter implementation for the {@link #mYearsListView} that provides data set of years.
	 */
	private static final class YearsAdapter extends DialogBaseAdapter<Integer, TextView> {

		/**
		 * Scale for text size of item's text view used to highlight current selected item.
		 */
		static final float TEXT_SCALE_SELECTED = 1.75f;

		/**
		 * Year value boundary.
		 */
		int minYear, maxYear;

		/**
		 * Count of years provided by this adapter.
		 */
		int yearsCount = Integer.MAX_VALUE;

		/**
		 * Position of the currently selected year.
		 */
		int selectedPosition;

		/**
		 * Boolean flag indicating whether there is pending data set change registered or not.
		 *
		 * @see #notifyDataSetChangeIfPending()
		 */
		boolean pendingDataSetChange;

		/**
		 * Original text size of the item's text view.
		 */
		float textSize;

		/**
		 * Color state list for the item's text view used by default if the text view has specified
		 * transparent color.
		 */
		ColorStateList textColor;

		/**
		 * Color used for the selected item's text view by default if the text view has specified
		 * transparent color.
		 */
		int selectedTextColor;

		/**
		 * Boolean flag indicating whether to use {@link #textColor} and {@link #selectedTextColor}
		 * for the item's text view by default or not.
		 */
		boolean useDefaultTextColors;

		/**
		 * Creates a new instance of YearsAdapter to provide data set of years from which can user
		 * pick the desired one.
		 *
		 * @param context  Context in which will be this adapter used.
		 * @param inflater Layout inflater which can be used to inflate views for this adapter.
		 */
		YearsAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater) {
			super(context, inflater);
			final Resources.Theme theme = context.getTheme();
			final TypedValue typedValue = new TypedValue();
			if (theme.resolveAttribute(android.R.attr.textColorSecondary, typedValue, true)) {
				this.textColor = ResourceUtils.getColorStateList(
						context.getResources(),
						typedValue.resourceId,
						theme
				);
			}
			if (theme.resolveAttribute(R.attr.colorControlActivated, typedValue, true)) {
				this.selectedTextColor = typedValue.data;
			}
		}

		/**
		 */
		@Override public int getCount() {
			return yearsCount;
		}

		/**
		 */
		@Override @NonNull public Integer getItem(final int position) {
			return minYear + position;
		}

		/**
		 */
		@Override @NonNull protected View onCreateView(@NonNull final ViewGroup parent, final int position) {
			final TextView textView = (TextView) mLayoutInflater.inflate(R.layout.dialog_item_list_date_year, parent, false);
			this.textSize = textView.getTextSize();
			this.useDefaultTextColors = textView.getTextColors().getDefaultColor() == Color.TRANSPARENT;
			if (useDefaultTextColors && textColor != null) {
				textView.setTextColor(textColor);
			}
			return textView;
		}

		/**
		 * Updates the current selected position to the specified one.
		 *
		 * @param position Position of the year that should be selected.
		 */
		final void updateSelectedPosition(final int position) {
			if (selectedPosition != position) {
				this.selectedPosition = position;
				this.pendingDataSetChange = true;
			}
		}

		/**
		 * Fires {@link #notifyDataSetChanged()} if there is pending data set change registered. If
		 * not, this method does nothing.
		 */
		private void notifyDataSetChangeIfPending() {
			if (pendingDataSetChange) {
				this.pendingDataSetChange = false;
				notifyDataSetChanged();
			}
		}

		/**
		 */
		@SuppressLint("SetTextI18n")
		@Override protected void onBindViewHolder(@NonNull final TextView view, final int position) {
			final boolean selected = selectedPosition == position;
			view.setText(createItemText(getItem(position), selected));
			view.setSelected(selected);
			view.setTextSize(
					TypedValue.COMPLEX_UNIT_PX,
					selected ? (textSize * TEXT_SCALE_SELECTED) : textSize
			);
			if (useDefaultTextColors && textColor != null) {
				if (selected) view.setTextColor(selectedTextColor);
				else view.setTextColor(textColor);
			}
		}

		/**
		 * Creates a text for an item with the <var>yearNumber</var>.
		 *
		 * @param yearNumber The number of year which should be set as text for item.
		 * @param selected   {@code True} if item with the associated year number is at this time selected,
		 *                   {@code false} otherwise.
		 * @return Text representation of the specified year number.
		 */
		private CharSequence createItemText(final int yearNumber, final boolean selected) {
			final String text = Integer.toString(yearNumber);
			if (selected) {
				final Spannable spannable = new SpannableStringBuilder(text);
				spannable.setSpan(
						new BoldTextSpan(),
						0, text.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
				);
				return spannable;
			}
			return text;
		}

		/**
		 * Computes position for the requested <var>year</var>.
		 *
		 * @param year The desired year of which position to find out.
		 * @return Computed position or {@code negative number} if the given year is smaller than the
		 * current {@code minYear}.
		 */
		final int positionOfYear(final int year) {
			return year - minYear;
		}

		/**
		 * Changes the current min + max values for the year. These values will be used to determine
		 * count of years to be provided by this adapter and also to determine which year to return
		 * by {@link #getItem(int)} for the requested position.
		 *
		 * @param minY The desired value for minimum year.
		 * @param maxY The desired value for maximum year. Pass here {@code 0} if this adapter should
		 *             provide 'infinite' count of years.
		 */
		final void changeYearBoundaries(final int minY, final int maxY) {
			if (minY >= 0 && (minY < maxY || maxY == 0)) {
				if (minYear != minY || maxYear != maxY) {
					// Update bottom boundary.
					this.minYear = minY;

					// Update top boundary by total count of years for this adapter.
					if ((maxYear = maxY) != 0) {
						this.yearsCount = maxYear - minYear + 1;
					} else {
						this.yearsCount = Integer.MAX_VALUE;
					}
					notifyDataSetChanged();
				}
			}
		}
	}

	/**
	 * A {@link MetricAffectingSpan} implementation used to apply <b>bold</b> style to a specific text.
	 */
	private static final class BoldTextSpan extends MetricAffectingSpan {

		/**
		 * Creates a new instance of BoldTextSpan.
		 */
		BoldTextSpan() {}

		/**
		 */
		@Override public void updateDrawState(@NonNull final TextPaint paint) {
			updateMeasureState(paint);
		}

		/**
		 */
		@Override public void updateMeasureState(@NonNull final TextPaint paint) {
			paint.setFakeBoldText(true);
		}
	}

	/**
	 * A {@link CalendarView.OnDateSelectionListener}, {@link DatePickerDialogTitleView.OnViewsSelectionListener}
	 * and {@link AdapterView.OnItemClickListener} implementation dispatching relevant callbacks to
	 * the associated {@link DatePickerDialogContentView} layout.
	 */
	private final class ListenerCallbacks
			implements
			View.OnClickListener,
			CalendarView.OnDateSelectionListener,
			CalendarView.OnMonthChangeListener,
			DatePickerDialogTitleView.OnViewsSelectionListener,
			AdapterView.OnItemClickListener {

		/**
		 */
		@Override public void onClick(@NonNull final View view) {
			final int viewId = view.getId();
			if (viewId == R.id.dialog_button_prev) {
				mCalendarView.smoothScrollToPreviousMonth();
			} else if (viewId == R.id.dialog_button_next) {
				mCalendarView.smoothScrollToNextMonth();
			}
		}

		/**
		 */
		@SuppressWarnings("deprecation")
		@Override public void onMonthScrolled(
				@NonNull final CalendarView calendarView,
				@IntRange(from = 0L, to = 11L) final int month,
				@FloatRange(from = 0.0, to = 1.0) final float offset
		) {
			if (mMonthButtonPrev != null && mMonthButtonNext != null) {
				int alpha = 0;
				if (offset <= 0.25) {
					alpha = 255 - (int) (255 * (offset / 0.25));
				} else if (offset >= 0.75) {
					alpha = (int) (255 * ((offset - 0.75) / 0.25));
				}
				mMonthButtonPrev.setAlpha(alpha);
				mMonthButtonNext.setAlpha(alpha);
			}
		}

		/**
		 */
		@Override public void onMonthChanged(@NonNull CalendarView calendarView, @IntRange(from = 0L, to = 11L) int month) {
			// Ignored.
		}

		/**
		 */
		@Override public void onDateSelected(@NonNull final CalendarView calendarView, final long dateInMillis) {
			handleDateSelected(calendarView, dateInMillis);
		}

		/**
		 */
		@Override public void onNoDateSelected(@NonNull final CalendarView calendarView) {
			handleDateSelected(calendarView, null);
		}

		/**
		 */
		@Override public void onPrimaryViewSelected() {
			switchContentViews(true);
		}

		/**
		 */
		@Override public void onSecondaryViewSelected() {
			switchContentViews(false);
		}

		/**
		 */
		@SuppressWarnings("ResourceType")
		@Override public void onItemClick(@NonNull final AdapterView<?> parent, @Nullable final View view, final int position, final long id) {
			// Update year of the currently selected date.
			final int year = mYearsAdapter.getItem(position);
			final Calendar calendar = mCalendarView.getCalendar();
			calendar.setTime(mCalendarView.getSelectedDate());
			if (calendar.get(Calendar.YEAR) != year) {
				calendar.set(Calendar.YEAR, year);
				mCalendarView.setSelectedDate(calendar.getTime());
			}
			updateYearsAdapterSelectedPosition(position);
			mTitleView.selectPrimaryView();
		}
	}

	/**
	 * Animation listener for {@link #ANIM_FADE_OUT} alpha animation.
	 */
	private static final class FadeOutAnimationListener extends AnimationWatcher {

		/**
		 * View of which visibility should be changed to the {@link #visibility} flag after fade out
		 * animation finishes.
		 */
		View view;

		/**
		 * The visibility flag to be set after animation finished.
		 */
		int visibility;

		/**
		 */
		@Override public void onAnimationEnd(@NonNull final Animation animation) {
			if (view != null) {
				view.setVisibility(visibility);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					view.postDelayed(new Runnable() {

						/**
						 */
						@SuppressLint("NewApi")
						@Override public void run() {
							view.setLayerType(LAYER_TYPE_NONE, null);
							view = null;
						}
					}, 100);
				} else {
					this.view = null;
				}
			}
		}
	}
}