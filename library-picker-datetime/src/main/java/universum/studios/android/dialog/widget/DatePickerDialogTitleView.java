/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.DialogView;
import universum.studios.android.ui.widget.LinearLayoutWidget;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link LinearLayoutWidget} implementation that represents a container for {@link TextView TextViews}
 * used to present the current picked date value within {@link DatePickerDialogContentView} view.
 * <p>
 * Values of the picked date can be specified via following methods:
 * <ul>
 * <li>{@link #setPrimaryText(CharSequence)}</li>
 * <li>{@link #setSecondaryText(CharSequence)}</li>
 * </ul>
 *
 * <h3>Callbacks</h3>
 * You can listen for a selection events on primary and secondary title view by registering
 * {@link OnViewsSelectionListener} listener via {@link #setOnViewsSelectionListener(OnViewsSelectionListener)}.
 * Its callbacks will be fired whenever the other title view is clicked while the current is selected.
 * If the selected title view is again clicked, the callback will be not fired.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#dialogDateTitleStyle dialogDateTitleStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DatePickerDialogTitleView extends LinearLayout implements DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DatePickerDialogTitleView";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that can receive callbacks about selected year or month + day view within
	 * {@link DatePickerDialogTitleView} widget.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface OnViewsSelectionListener {

		/**
		 * Invoked whenever the <b>year</b> view has been selected within the associated DialogDateTitleView.
		 */
		void onSecondaryViewSelected();

		/**
		 * Invoked whenever the <b>month + day</b> view has been selected within the associated DialogDateTitleView.
		 */
		void onPrimaryViewSelected();
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Text view to present year number for the picked date.
	 */
	private TextView mSecondaryView;

	/**
	 * Text view to present day name, name of a month with day number for the picked date.
	 */
	private TextView mPrimaryView;

	/**
	 * Callback to be invoked whenever a primary or secondary title view is selected within this view.
	 */
	private OnViewsSelectionListener mListener;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #DatePickerDialogTitleView(Context, AttributeSet)} without attributes.
	 */
	public DatePickerDialogTitleView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #DatePickerDialogTitleView(Context, AttributeSet, int)} with
	 * {@link R.attr#dialogDateTitleStyle dialogDateTitleStyle} as attribute for default style.
	 */
	public DatePickerDialogTitleView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, R.attr.dialogDateTitleStyle, 0);
	}

	/**
	 * Same as {@link #DatePickerDialogTitleView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public DatePickerDialogTitleView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of DatePickerDialogTitleView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public DatePickerDialogTitleView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.inflateHierarchy(context, R.layout.dialog_layout_date_title);
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mPrimaryView = (TextView) findViewById(R.id.dialog_text_view_date_primary);
			this.mSecondaryView = (TextView) findViewById(R.id.dialog_text_view_date_secondary);
			if (mPrimaryView != null && mSecondaryView != null) {
				final View.OnClickListener clickListener = new View.OnClickListener() {

					/**
					 */
					@Override public void onClick(@NonNull final View view) {
						final int id = view.getId();
						if (id == R.id.dialog_text_view_date_primary) {
							selectPrimaryView();
						} else if (id == R.id.dialog_text_view_date_secondary) {
							selectSecondaryView();
						}
					}
				};
				mPrimaryView.setSelected(true);
				mPrimaryView.setOnClickListener(clickListener);
				mSecondaryView.setOnClickListener(clickListener);
			}
			onFinishInflate();
		}
	}

	/**
	 * Selects the primary title view, if it is not selected yet.
	 * <p>
	 * Also {@link OnViewsSelectionListener} will be notified via {@link OnViewsSelectionListener#onPrimaryViewSelected()}
	 * callback.
	 *
	 * @see #selectSecondaryView()
	 */
	public void selectPrimaryView() {
		if (mPrimaryView.isSelected()) {
			return;
		}
		mSecondaryView.setSelected(false);
		mPrimaryView.setSelected(true);
		if (mListener != null) {
			mListener.onPrimaryViewSelected();
		}
	}

	/**
	 * Selects the secondary title view, if it is not selected yet.
	 * <p>
	 * Also {@link OnViewsSelectionListener} will be notified via {@link OnViewsSelectionListener#onSecondaryViewSelected()}
	 * callback.
	 *
	 * @see #selectPrimaryView()
	 */
	public void selectSecondaryView() {
		if (mSecondaryView.isSelected()) {
			return;
		}
		mPrimaryView.setSelected(false);
		mSecondaryView.setSelected(true);
		if (mListener != null) {
			mListener.onSecondaryViewSelected();
		}
	}

	/**
	 * Registers a callback to be invoked whenever primary or secondary view is selected.
	 *
	 * @param listener Listener callback. May be {@code null} to clear the current one.
	 * @see #isPrimaryViewSelected()
	 */
	public void setOnViewsSelectionListener(@Nullable final OnViewsSelectionListener listener) {
		this.mListener = listener;
	}

	/**
	 * Checks whether the primary title view selected or not.
	 *
	 * @return {@code True} if primary title view is selected, {@code false} if the secondary title
	 * view is selected.
	 */
	public boolean isPrimaryViewSelected() {
		return mPrimaryView != null && mPrimaryView.isSelected();
	}

	/**
	 * Sets a text for the primary title text view.
	 *
	 * @param text The desired text. May be {@code null} to clear the current one.
	 * @see #getPrimaryText()
	 */
	public void setPrimaryText(@Nullable final CharSequence text) {
		if (mPrimaryView != null) mPrimaryView.setText(text);
	}

	/**
	 * Returns the current text presented within primary title text view.
	 *
	 * @return Current primary text.
	 * @see #setPrimaryText(CharSequence)
	 */
	@NonNull public CharSequence getPrimaryText() {
		return mPrimaryView != null ? mPrimaryView.getText() : "";
	}

	/**
	 * Sets a text for the secondary title text view.
	 *
	 * @param text The desired text. May be {@code null} to clear the current one.
	 * @see #getSecondaryText()
	 */
	public void setSecondaryText(@Nullable final CharSequence text) {
		if (mSecondaryView != null) mSecondaryView.setText(text);
	}

	/**
	 * Returns the current text presented within the month text view.
	 *
	 * @return Current secondary title text.
	 * @see #setSecondaryText(CharSequence)
	 */
	@NonNull public CharSequence getSecondaryText() {
		return mSecondaryView != null ? mSecondaryView.getText() : "";
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.primaryText = mPrimaryView != null ? mPrimaryView.getText() : "";
		savedState.secondaryText = mSecondaryView != null ? mSecondaryView.getText() : "";
		savedState.secondaryViewSelected = mSecondaryView != null && mSecondaryView.isSelected();
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		setPrimaryText(savedState.primaryText);
		setSecondaryText(savedState.secondaryText);
		if (mPrimaryView != null && mSecondaryView != null) {
			if (savedState.secondaryViewSelected) {
				mSecondaryView.setSelected(true);
				if (mListener != null) {
					mListener.onSecondaryViewSelected();
				}
			} else {
				// Primary view is by default selected so do not notify listener.
				mPrimaryView.setSelected(true);
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link DatePickerDialogTitleView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		CharSequence primaryText, secondaryText;

		/**
		 */
		boolean secondaryViewSelected;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.primaryText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.secondaryText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.secondaryViewSelected = source.readInt() == 1;
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			TextUtils.writeToParcel(primaryText, dest, flags);
			TextUtils.writeToParcel(secondaryText, dest, flags);
			dest.writeInt(secondaryViewSelected ? 1 : 0);
		}
	}
}