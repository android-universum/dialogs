/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.AttrRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.PluralsRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.TimePickerDialogView;

/**
 * A {@link SimpleDialog} implementation that can be used to allow to a user to pick its desired time
 * by picking values for hour and minute. This dialog uses default {@link TimePickerDialogView}
 * implementation as its content view which consists of two
 * {@link universum.studios.android.ui.widget.CircularNumberPicker CircularNumberPickers},
 * one to pick number for <b>hour</b> the second one to pick number for <b>minute</b>.
 * <p>
 * The picked time can be obtained via {@link #getTime()}.
 * <p>
 * See {@link TimeOptions} for options that can be supplied to the TimePickerDialog from the desired
 * context via {@link #newInstance(TimeOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogTimeOptions dialogTimeOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class TimePickerDialog extends SimpleDialog {

    /*
     * Constants ===================================================================================
     */

    /*
     * Log TAG.
     */
    // private static final String TAG = "TimePickerDialog";

	/*
	 * Interface ===================================================================================
	 */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Time dialog view implementation.
     */
    private TimePickerDialogView mTimeView;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of TimePickerDialog with two {@link universum.studios.android.ui.widget.CircularNumberPicker CircularNumberPickers}.
     * One for <b>hour</b> number and the other one for <b>minute</b> value picking.
     */
    public TimePickerDialog() {
        super(R.attr.dialogTimeOptions);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Creates a new instance of TimePickerDialog with the specified options.
     *
     * @param options Specific options for this type of dialog.
     * @return New TimePickerDialog instance.
     */
    @NonNull public static TimePickerDialog newInstance(@NonNull final TimeOptions options) {
        final TimePickerDialog dialog = new TimePickerDialog();
        dialog.setOptions(options);
        return dialog;
    }

    /**
     */
    @Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
        return new TimeOptions(resources);
    }

	/**
	 */
	@SuppressWarnings("ResourceType")
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof TimeOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Time);
			final TimeOptions options = (TimeOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Date_dialogDate) {
					if (!options.isSet(TimeOptions.TIME)) {
						final Long time = TimeParser.parse(attributes.getString(index));
						if (time != null) options.time(time);
					}
				} else if (index == R.styleable.Dialog_Options_Date_dialogDateMin) {
					if (!options.isSet(TimeOptions.PICKERS)) {
						options.timePickers(attributes.getInt(index, options.pickers));
					}
				} else if (index == R.styleable.Dialog_Options_Date_dialogDateMax) {
					if (!options.isSet(TimeOptions.QUANTITY_TEXT_RES)) {
						options.timeQuantityText(attributes.getResourceId(index, options.quantityTextRes));
					}
				}
			}
			attributes.recycle();
		}
	}

    /**
     */
    @Override @NonNull protected View onCreateContentView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.dialog_content_time, container, false);
    }

    /**
     */
    @Override @NonNull protected View onCreateButtonsView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        // Content view already contains buttons view.
        final View view = new Space(inflater.getContext());
        view.setVisibility(View.GONE);
        return view;
    }

    /**
     */
    @Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
        if (contentView instanceof TimePickerDialogView && options instanceof TimeOptions) {
            final TimeOptions timeOptions = (TimeOptions) options;
            final TimePickerDialogView timeView = (TimePickerDialogView) contentView;
            if (timeOptions.timeZone != null) {
                timeView.setTimeZone(timeOptions.timeZone);
            }
            timeView.setTime(timeOptions.time);
            timeView.setPickers(timeOptions.pickers);
            timeView.setTimeQuantityTextRes(timeOptions.quantityTextRes);
        }
    }

    /**
     */
    @Override protected void onUpdateContentViewPadding() {
        // Do not update content view padding.
    }

    /**
     */
    @Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final View contentView = getContentView();
        if (contentView instanceof TimePickerDialogView) {
            this.mTimeView = (TimePickerDialogView) contentView;
        }
    }

    /**
     * Returns the current value of <b>time</b> picked within this picker dialog.
     *
     * @return Picked time either by user or initially specified via options.
     */
    public long getTime() {
        return mTimeView != null ? mTimeView.getTime() : 0;
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link DialogOptions} implementation specific for the {@link TimePickerDialog}.
     * <p>
     * Below are listed setters which may be used to supply desired data to {@link TimePickerDialog}
     * through these options:
     * <ul>
     * <li>{@link #time(Date)}</li>
     * <li>{@link #time(long)}</li>
     * <li>{@link #timePickers(int)}</li>
     * <li>{@link #timeQuantityText(int)}</li>
     * </ul>
     *
     * <h3>Xml attributes</h3>
     * See {@link R.styleable#Dialog_Options_Time TimeOptions Attributes},
     * {@link R.styleable#Dialog_Options DialogOptions Attributes}
     * <p>
     * Note, that the time related values specified within Xml should be in format: <b>'hh:mm a'</b>
     * or <b>'current[+|-](HH:mm)'</b> otherwise they will not be properly parsed.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    @SuppressWarnings("ResourceType")
    public static class TimeOptions extends DialogOptions<TimeOptions> {

        /**
         * Creator used to create an instance or array of instances of TimeOptions from {@link Parcel}.
         */
        public static final Creator<TimeOptions> CREATOR = new Creator<TimeOptions>() {

            /**
             */
            @Override public TimeOptions createFromParcel(@NonNull final Parcel source) { return new TimeOptions(source); }

            /**
             */
            @Override public TimeOptions[] newArray(final int size) { return new TimeOptions[size]; }
        };

        /**
         * Flag for <b>time zone</b> attribute.
         */
        static final int TIME_ZONE = 0x00000001;

        /**
         * Flag for <b>time</b> parameter.
         */
        static final int TIME = 0x00000001 << 1;

        /**
         * Flag for <b>pickers</b> parameter.
         */
        static final int PICKERS = 0x00000001 << 2;

        /**
         * Flag for <b>time quantity text resource</b> parameter.
         */
        static final int QUANTITY_TEXT_RES = 0x00000001 << 3;

        /**
         * todo:
         */
        TimeZone timeZone;

        /**
         * Time value determining values for <b>hour, minute, am/pm</b> to be presented by number
         * pickers.
         */
        long time = System.currentTimeMillis();

        /**
         * Number pickers flags determining which pickers should be presented within associated time
         * picker dialog content view.
         */
        int pickers = TimePickerDialogView.PICKER_HOUR | TimePickerDialogView.PICKER_MINUTE;

        /**
         * Resource id of the quantity text used to format current value of picked number within specific
         * number picker. This resource id is used only in case when there is only one number picker
         * presented within dialog content view.
         */
        int quantityTextRes;

        /**
         * Creates a new instance of TimeOptions.
         */
        public TimeOptions() {
            super();
        }

        /**
         * Creates a new instance of TimeOptions.
         *
         * @param resources Application resources used to obtain values for these options when they
         *                  are requested by theirs resource ids.
         */
        public TimeOptions(@NonNull final Resources resources) {
            super(resources);
        }

        /**
         * Called form {@link #CREATOR} to create an instance of TimeOptions form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected TimeOptions(@NonNull final Parcel source) {
            super(source);
            this.timeZone = (TimeZone) source.readSerializable();
            this.time = source.readLong();
            this.pickers = source.readInt();
            this.quantityTextRes = source.readInt();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeSerializable(timeZone);
            dest.writeLong(time);
            dest.writeInt(pickers);
            dest.writeInt(quantityTextRes);
        }

        /**
         * Creates a new instance of Date with the given <var>hour, minute, amPm</var> values.
         *
         * @param hour   The desired hour number for the new time. Should be from the range {@code [1, 12]}.
         * @param minute The desired minute number for the new time. Should be from the range {@code [0, 59]}.
         * @param amPm   One of {@link Calendar#AM} or {@link Calendar#PM}.
         * @return New time in milliseconds with the requested values.
         */
        public static long newTime(
        		@IntRange(from = 1, to = 12) final int hour,
		        @IntRange(from = 0, to = 59) final int minute,
		        @TimePickerDialogView.AmPm final int amPm
        ) {
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.AM_PM, amPm);
            return calendar.getTimeInMillis();
        }

        /**
         */
        @Override public TimeOptions merge(@NonNull final DialogOptions options) {
            if (!(options instanceof TimeOptions)) {
                return super.merge(options);
            }
            final TimeOptions timeOptions = (TimeOptions) options;
            if (timeOptions.isSet(TIME_ZONE)) {
                this.timeZone = timeOptions.timeZone;
                updateIsSet(TIME_ZONE);
            }
            if (timeOptions.isSet(TIME)) {
                this.time = timeOptions.time;
                updateIsSet(TIME);
            }
            if (timeOptions.isSet(PICKERS)) {
                this.pickers = timeOptions.pickers;
                updateIsSet(PICKERS);
            }
            if (timeOptions.isSet(QUANTITY_TEXT_RES)) {
                this.quantityTextRes = timeOptions.quantityTextRes;
                updateIsSet(QUANTITY_TEXT_RES);
            }
            return super.merge(options);
        }

        /**
         * todo:
         *
         * @param timeZone
         * @return
         */
        public TimeOptions timeZone(@NonNull final TimeZone timeZone) {
            updateIsSet(TIME_ZONE);
            this.timeZone = timeZone;
            notifyChanged();
            return this;
        }

        /**
         * Same as {@link #time(long)} for {@link Date} object.
         *
         * @param time The desired initial time.
         * @return These options to allow methods chaining.
         * @see #time()
         */
        public TimeOptions time(@NonNull final Date time) {
            return time(time.getTime());
        }

        /**
         * Sets an initial time to be selected within a dialog associated with these options.
         * <p>
         * When the dialog is first time shown, this time will be visible to a user.
         *
         * @param milliseconds The desired initial time in milliseconds.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogTime dialog:dialogTime
         * @see #time()
         */
        public TimeOptions time(final long milliseconds) {
            updateIsSet(TIME);
            if (time != milliseconds) {
                this.time = milliseconds;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the initial time, set to these options.
         * <p>
         * Default value: <b>{@link System#currentTimeMillis()}</b>
         *
         * @return Time value in milliseconds.
         * @see #time(long)
         * @see #time(Date)
         */
        public long time() {
            return time;
        }

        /**
         * Sets the pickers flags to determine which number pickers should be presented within a dialog
         * associated with these options.
         *
         * @param pickers The desired pickers flags. Combination of {@link TimePickerDialogView#PICKER_HOUR}
         *                and {@link TimePickerDialogView#PICKER_MINUTE}.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogTimePickers dialog:dialogTimePickers
         * @see #timePickers()
         */
        public TimeOptions timePickers(@TimePickerDialogView.Pickers final int pickers) {
            updateIsSet(PICKERS);
            if (this.pickers != pickers) {
                this.pickers = pickers;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the pickers flags, set to these options.
         * <p>
         * Default value: <b>{@link TimePickerDialogView#PICKER_HOUR PICKER_HOUR} | {@link TimePickerDialogView#PICKER_MINUTE PICKER_MINUTE}</b>
         *
         * @return Combination of {@link TimePickerDialogView#PICKER_HOUR} and {@link TimePickerDialogView#PICKER_MINUTE}.
         * @see #timePickers(int)
         */
        @TimePickerDialogView.Pickers public int timePickers() {
            return pickers;
        }

        /**
         * Sets the resource id of quantity text used to format current value of picked number.
         * This resource id is used only in case when there is only one number picker presented within
         * the content view of a dialog associated with these options.
         *
         * @param resId Resource id of the desired plurals text.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogTimeQuantityText dialog:dialogTimeQuantityText
         * @see #timeQuantityText()
         */
        public TimeOptions timeQuantityText(@PluralsRes final int resId) {
            updateIsSet(QUANTITY_TEXT_RES);
            if (quantityTextRes != resId) {
                this.quantityTextRes = resId;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns resource id of the quantity text used to format value of picked number, set to this
         * options.
         *
         * @return Plurals text resource id or {@code 0} by default.
         * @see #timeQuantityText(int)
         */
        @PluralsRes public int timeQuantityText() {
            return quantityTextRes;
        }

        /**
         */
        @Override protected void onParseXmlAttribute(
        		@NonNull final XmlResourceParser xmlParser,
		        final int index,
		        @AttrRes final int attr,
		        @NonNull final Resources resources,
		        @Nullable final Resources.Theme theme
        ) {
            if (attr == R.attr.dialogTime) {
                final String timeString = obtainXmlAttributeString(xmlParser, index, resources);
                if (!TextUtils.isEmpty(timeString)) {
                    final Long time = TimeParser.parse(timeString);
                    if (time != null) {
                        time(time);
                    }
                }
            } else if (attr == R.attr.dialogTimePickers) {
                timePickers(xmlParser.getAttributeIntValue(index, pickers));
            } else if (attr == R.attr.dialogTimeQuantityText) {
                timeQuantityText(xmlParser.getAttributeResourceValue(index, quantityTextRes));
            } else {
                super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
            }
        }
    }

    /**
     * Parser used to parse time string into time value.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static final class TimeParser {

        /**
         * Template for the format used to parse time string from the Xml.
         */
        public static final String FORMAT_TEMPLATE = "hh:mm a";

        /**
         * Regular expression used to parse time string containing the <b>current</b> keyword.
         */
        private static final String KEYWORD_PATTERN = "^current([-+])(\\(\\d{1,2}:\\d{1,2}\\))$";

        /**
         * Calendar used to offset current date by the requested amount of years, months and days.
         */
        private static Calendar calendar;

        /**
         * Format used to parse time string from the Xml.
         */
        private static SimpleDateFormat format;

        /**
         * Matcher used to parse time string containing the <b>current</b> keyword.
         */
        private static Matcher keywordMatcher;

        /**
         */
        private TimeParser() {
	        // Instances of this parser are not allowed to be created.
        }

        /**
         * Parses the specified <var>timeString</var> into time value in milliseconds. The specified
         * string should be in format <b>hh:mm a</b> or one of keyword expressions specified below.
         * <p>
         * <h3>Accepted key word expressions</h3>
         * <ul>
         * <li>current</li>
         * <li>current[-+](HH:mm)</li>
         * </ul>
         *
         * @param timeString The desired time string to parse into time value. May be {@code null}
         *                   in which case {@code null} is also returned.
         * @return Parsed time value in milliseconds or {@code null} if the specified string cannot
         * be parsed into time.
         * @see SimpleDateFormat
         */
        @Nullable
        public static Long parse(@Nullable final String timeString) {
	        if (TextUtils.isEmpty(timeString)) {
		        return null;
	        }
            if (timeString.contentEquals("current")) {
                return System.currentTimeMillis();
            }
            ensureInitialized();
            if (keywordMatcher.reset(timeString).matches()) {
                String groupTime = keywordMatcher.group(2);
                // Get rid of the brackets at the start and at the end of the time string.
                groupTime = groupTime.substring(1, groupTime.length() - 1);
                final String[] groupTimeItems = groupTime.split(":");
                final String groupSign = keywordMatcher.group(1);
                final int offsetDirection = groupSign.contentEquals("+") ? 1 : -1;
                final int offsetHours = Integer.parseInt(groupTimeItems[0]) * offsetDirection;
                final int offsetMinutes = Integer.parseInt(groupTimeItems[1]) * offsetDirection;
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + offsetHours);
                calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + offsetMinutes);
                return calendar.getTimeInMillis();
            }
            return parseInner(timeString);
        }

        /**
         * Ensures that this parser is initialized.
         */
        private static void ensureInitialized() {
            if (calendar == null) {
                TimeParser.calendar = Calendar.getInstance();
                TimeParser.format = new SimpleDateFormat(FORMAT_TEMPLATE, Locale.getDefault());
                TimeParser.keywordMatcher = Pattern.compile(KEYWORD_PATTERN).matcher("");
            }
        }

        /**
         * Parses the specified <var>timeString</var> using {@link #format}.
         *
         * @param timeString The string to parse into time value.
         * @return Parsed time value in milliseconds as Long or {@code null} if the specified string
         * cannot be parsed into time value.
         */
        private static Long parseInner(final String timeString) {
            try {
                return format.parse(timeString).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}