/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.DialogView;
import universum.studios.android.dialog.view.TimePickerDialogView;
import universum.studios.android.ui.widget.CircularNumberPicker;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * This view class represents a container for two {@link CircularNumberPicker CircularNumberPickers}
 * used by {@link TimePickerDialogContentView} view that allows to pick one number for hour and another
 * one for minute. This class is primarily created for purpose of animated changes between these two
 * pickers. Current picker can be changed for the other one via {@link #changePickersAnimated(boolean)}.
 * Initial values for hour and minute picker can be specified via {@link #setHour(int)} and {@link #setMinute(int)}
 * and the current picked values can be obtained via {@link #getHour()} and {@link #getMinute()}.
 * If you need to check which picker is currently visible, invoke {@link #currentPicker()} that will
 * return one of {@link TimePickerDialogView#PICKER_HOUR} or {@link TimePickerDialogView#PICKER_MINUTE}.
 * <p>
 * The hour picker will by default show hours from 12, 1, 2, ..., 11 and minute picker minutes from
 * 0, 5, 10, ..., 55.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#dialogTimePickersLayoutStyle dialogTimePickersLayoutStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class TimePickersLayout extends FrameLayout
		implements
		DialogView,
		CircularNumberPicker.OnNumberSelectionListener,
		CircularNumberPicker.OnNumberChangeListener {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "TimePickersLayout";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Watcher that can listen for hour or minute value changes or selections within {@link TimePickersLayout}
	 * widget.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface TimeValueWatcher {

		/**
		 * Invoked whenever the value of hour has been changed within hour number picker. This is
		 * fired whenever the selection indicator is dragged in the hour number picker's circle.
		 *
		 * @param hour The changed hour number.
		 */
		void onHourChanged(@IntRange(from = 1, to = 12) int hour);

		/**
		 * Invoked whenever the value of hour has been selected within hour number picker. This is
		 * fired only when the selection indicator is released.
		 *
		 * @param hour The selected hour number.
		 */
		void onHourSelected(@IntRange(from = 1, to = 12) int hour);

		/**
		 * Invoked whenever the value of minute has been changed within hour minute picker. This is
		 * fired whenever the selection indicator is dragged in the minute number picker's circle.
		 *
		 * @param minute The changed hour number.
		 */
		void onMinuteChanged(@IntRange(from = 0, to = 59) int minute);

		/**
		 * Invoked whenever the value of minute has been selected within minute number picker. This
		 * is fired only when the selection indicator is released.
		 *
		 * @param minute The selected minute number.
		 */
		void onMinuteSelected(@IntRange(from = 0, to = 59) int minute);
	}

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Set of numbers for hour picker.
	 */
	private static final int[] HOURS = {12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

	/**
	 * Set of numbers for minute picker.
	 */
	private static final int[] MINUTES = {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Paint used to paint the background circle behind the pickers.
	 */
	private final Paint BACKGROUND_PAINT = new Paint(Paint.ANTI_ALIAS_FLAG);

	/**
	 * Number picker which allows to pick value for hour.
	 */
	CircularNumberPicker mHourPicker;

	/**
	 * Number picker which allows to pick value for minute.
	 */
	CircularNumberPicker mMinutePicker;

	/**
	 * Current picked value within hour number picker.
	 */
	int mHour;

	/**
	 * Current picked value within minute number picker.
	 */
	int mMinute;

	/**
	 * Value of the center of this layout.
	 */
	private int mCenter;

	/**
	 * Callback to be invoked whenever the current value of the hour or minute number is changed or
	 * selected within related number picker.
	 */
	private TimeValueWatcher mWatcher;

	/**
	 * Flag indicating the current visible picker within this layout.
	 */
	private int mCurrentPicker = TimePickerDialogView.PICKER_HOUR;

	/**
	 * Animation to show/hide number picker.
	 */
	private Animation mAnimHourPickerIn, mAnimHourPickerOut, mAnimMinutePickerIn, mAnimMinutePickerOut;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #TimePickersLayout(Context, AttributeSet)} without attributes.
	 */
	public TimePickersLayout(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #TimePickersLayout(Context, AttributeSet, int)} with
	 * {@link R.attr#dialogTimePickersLayoutStyle dialogTimePickersLayoutStyle} as attribute for
	 * default style.
	 */
	public TimePickersLayout(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.dialogTimePickersLayoutStyle);
	}

	/**
	 * Same as {@link #TimePickersLayout(Context, AttributeSet, int, int)} with {@code 0} as default
	 * style.
	 */
	public TimePickersLayout(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of TimePickersLayout for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public TimePickersLayout(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.inflateHierarchy(context, R.layout.dialog_layout_time_pickers);
		this.mHourPicker.setNumbers(HOURS);
		this.mMinutePicker.setNumbers(MINUTES);
		this.mMinutePicker.setNumberFormat("00");
		this.mMinutePicker.setSelectionRange(MINUTES.length * 5);
		this.mAnimHourPickerIn = AnimationUtils.loadAnimation(context, R.anim.dialog_hour_picker_in);
		this.mAnimHourPickerOut = AnimationUtils.loadAnimation(context, R.anim.dialog_hour_picker_out);
		mAnimHourPickerOut.setAnimationListener(new AnimationWatcher() {

			/**
			 */
			@Override public void onAnimationEnd(@NonNull final Animation animation) {
				mHourPicker.setVisibility(View.GONE);
			}
		});
		this.mAnimMinutePickerIn = AnimationUtils.loadAnimation(context, R.anim.dialog_minute_picker_in);
		this.mAnimMinutePickerOut = AnimationUtils.loadAnimation(context, R.anim.dialog_minute_picker_out);
		mAnimMinutePickerOut.setAnimationListener(new AnimationWatcher() {

			/**
			 */
			@Override public void onAnimationEnd(@NonNull final Animation animation) {
				mMinutePicker.setVisibility(View.GONE);
			}
		});

		final TypedArray attributes = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.colorBackground}, defStyleAttr, defStyleRes);
		final int color = attributes.getColor(0, 0);
		if (color != 0) {
			BACKGROUND_PAINT.setColor(color);
		}
		attributes.recycle();
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			if (getChildCount() > 0) {
				removeAllViews();
			}
			LayoutInflater.from(context).inflate(resource, this);
			this.mHourPicker = (CircularNumberPicker) findViewById(R.id.dialog_time_picker_hour);
			this.mMinutePicker = (CircularNumberPicker) findViewById(R.id.dialog_time_picker_minute);
			mHourPicker.setOnNumberSelectionListener(this);
			mHourPicker.setOnNumberChangeListener(this);
			mMinutePicker.setOnNumberSelectionListener(this);
			mMinutePicker.setOnNumberChangeListener(this);
			onFinishInflate();
		}
	}

	/**
	 * Registers a callback to be invoked whenever the current value of the hour or minute number is
	 * changed or selected within related number picker.
	 *
	 * @param watcher Watcher callback. May be {@code null} to clear the current one.
	 */
	public void setTimeValueWatcher(@Nullable final TimeValueWatcher watcher) {
		this.mWatcher = watcher;
	}

	/**
	 */
	@Override public void setBackgroundColor(final int color) {
		if (BACKGROUND_PAINT.getColor() != color) {
			BACKGROUND_PAINT.setColor(color);
			invalidate();
		}
	}

	/**
	 * Set a hour value to be selected within the hour number picker.
	 *
	 * @param hour The desired hour number to be selected. Should be from the range {@code [1, 12]}.
	 * @see #getHour()
	 * @see #setMinute(int)
	 */
	public void setHour(@IntRange(from = 1, to = 12) final int hour) {
		if (mHour != hour) {
			this.mHour = hour;
			if (mHourPicker != null) mHourPicker.setSelection(hour);
		}
	}

	/**
	 * Returns the current selected hour value within the hour number picker.
	 *
	 * @return Selected hour number from the range {@code [1, 12]}.
	 * @see #setHour(int)
	 */
	@IntRange(from = 1, to = 12) public int getHour() {
		return mHour;
	}

	/**
	 * Set a minute value to be selected within the minute number picker.
	 *
	 * @param minute The desired minute number to be selected. Should be from the range {@code [0, 59]}.
	 * @see #getMinute()
	 * @see #setHour(int)
	 */
	public void setMinute(@IntRange(from = 0, to = 59) final int minute) {
		if (mMinute != minute) {
			this.mMinute = minute;
			if (mMinutePicker != null) mMinutePicker.setSelection(minute);
		}
	}

	/**
	 * Returns the current selected minute value within the minute number picker.
	 *
	 * @return Selected minute number from the range {@code [0, 59]}.
	 * @see #setMinute(int)
	 */
	@IntRange(from = 0, to = 59) public int getMinute() {
		return mMinute;
	}

	/**
	 */
	@Override protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
		super.onSizeChanged(width, height, oldWidth, oldHeight);
		this.mCenter = Math.max(width, height) / 2;
	}

	/**
	 */
	@Override protected void dispatchDraw(@NonNull final Canvas canvas) {
		canvas.drawCircle(mCenter, mCenter, mCenter, BACKGROUND_PAINT);
		super.dispatchDraw(canvas);
	}

	/**
	 * Changes number pickers of this layout. The current active picker will be hided and the other
	 * one will be showed.
	 *
	 * @param animated {@code True} to animate the change between pickers, {@code false} otherwise.
	 */
	public void changePickersAnimated(final boolean animated) {
		switch (mCurrentPicker) {
			case TimePickerDialogView.PICKER_HOUR:
				this.handlePickersChange(mCurrentPicker = TimePickerDialogView.PICKER_MINUTE, animated);
				break;
			case TimePickerDialogView.PICKER_MINUTE:
				this.handlePickersChange(mCurrentPicker = TimePickerDialogView.PICKER_HOUR, animated);
				break;
		}
	}

	/**
	 * Handles the pickers change requested by {@link #changePickersAnimated(boolean)}. This will perform
	 * hiding of the current picker and showing of the other one, depending on the given <var>toPicker</var>
	 * flag.
	 *
	 * @param toPicker The flag of the picker which should be visible after this change. One of
	 *                 {@link TimePickerDialogView#PICKER_HOUR} or {@link TimePickerDialogView#PICKER_MINUTE}.
	 * @param animate  {@code True} to animate the change between pickers, {@code false} otherwise.
	 */
	private void handlePickersChange(final int toPicker, final boolean animate) {
		if (mHourPicker == null || mMinutePicker == null) {
			return;
		}
		if (!animate) {
			switch (toPicker) {
				case TimePickerDialogView.PICKER_HOUR:
					mHourPicker.setVisibility(View.VISIBLE);
					mMinutePicker.setVisibility(View.GONE);
					break;
				case TimePickerDialogView.PICKER_MINUTE:
					mHourPicker.setVisibility(View.GONE);
					mMinutePicker.setVisibility(View.VISIBLE);
					break;
			}
			return;
		}
		// Animate pickers change.
		onAnimatePickersChange(toPicker, mHourPicker, mMinutePicker);
	}

	/**
	 * Invoked whenever {@link #changePickersAnimated(boolean)} has been called with {@code true}.
	 *
	 * @param toPicker     A flag of the picker which should be visible after this change. One of
	 *                     {@link TimePickerDialogView#PICKER_HOUR} or {@link TimePickerDialogView#PICKER_MINUTE}.
	 * @param hourPicker   Hour picker view to be animated.
	 * @param minutePicker Minute picker view to be animated.
	 */
	void onAnimatePickersChange(final int toPicker, @NonNull final CircularNumberPicker hourPicker, @NonNull final CircularNumberPicker minutePicker) {
		switch (toPicker) {
			case TimePickerDialogView.PICKER_HOUR:
				hourPicker.setVisibility(VISIBLE);
				hourPicker.startAnimation(mAnimHourPickerIn);
				minutePicker.startAnimation(mAnimMinutePickerOut);
				break;
			case TimePickerDialogView.PICKER_MINUTE:
				minutePicker.setVisibility(VISIBLE);
				minutePicker.startAnimation(mAnimMinutePickerIn);
				hourPicker.startAnimation(mAnimHourPickerOut);
				break;
		}
	}

	/**
	 * Returns the number picker view for the requested <var>picker</var> flag.
	 *
	 * @param picker The picker flag. One of {@link TimePickerDialogView#PICKER_HOUR} or {@link TimePickerDialogView#PICKER_MINUTE}.
	 * @return Number picker view for the requested picker.
	 */
	@Nullable public CircularNumberPicker getPicker(@TimePickerDialogView.Pickers final int picker) {
		switch (picker) {
			case TimePickerDialogView.PICKER_HOUR:
				return mHourPicker;
			case TimePickerDialogView.PICKER_MINUTE:
				return mMinutePicker;
		}
		return null;
	}

	/**
	 * Returns the picker flag of the current visible picker.
	 *
	 * @return Current visible picker's flag. One of {@link TimePickerDialogView#PICKER_HOUR} or
	 * {@link TimePickerDialogView#PICKER_MINUTE}.
	 */
	@TimePickerDialogView.Pickers public int currentPicker() {
		return mCurrentPicker;
	}

	/**
	 */
	@Override public void onNumberChanged(@NonNull final CircularNumberPicker picker, final int number) {
		final int id = picker.getId();
		if (id == R.id.dialog_time_picker_hour) {
			this.mHour = (number > 0 ? number : 12);
			if (mWatcher != null) {
				mWatcher.onHourChanged(mHour);
			}
		} else if (id == R.id.dialog_time_picker_minute) {
			this.mMinute = (number < 60 ? number : 0);
			if (mWatcher != null) {
				mWatcher.onMinuteChanged(mMinute);
			}
		}
	}

	/**
	 */
	@Override public void onNumberSelected(@NonNull final CircularNumberPicker picker, final int number) {
		final int id = picker.getId();
		if (id == R.id.dialog_time_picker_hour) {
			this.mHour = (number > 0 ? number : 12);
			if (mWatcher != null) {
				mWatcher.onHourSelected(mHour);
			}
		} else if (id == R.id.dialog_time_picker_minute) {
			this.mMinute = (number < 60 ? number : 0);
			if (mWatcher != null) {
				mWatcher.onMinuteSelected(mMinute);
			}
		}
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.hour = mHour;
		savedState.minute = mMinute;
		savedState.currentPicker = mCurrentPicker;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		this.mHour = savedState.hour;
		this.mMinute = savedState.minute;
		if (savedState.currentPicker != mCurrentPicker) {
			this.handlePickersChange(mCurrentPicker = savedState.currentPicker, false);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link TimePickersLayout}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		int hour, minute, currentPicker;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.hour = source.readInt();
			this.minute = source.readInt();
			this.currentPicker = source.readInt();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(hour);
			dest.writeInt(minute);
			dest.writeInt(currentPicker);
		}
	}
}