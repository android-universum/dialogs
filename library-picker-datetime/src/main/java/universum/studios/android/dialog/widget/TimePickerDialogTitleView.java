/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.AttrRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.DialogView;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link RelativeLayout} implementation that represents a container for {@link TextView TextViews}
 * used to present the current picked time value within {@link TimePickerDialogContentView} view.
 * <p>
 * Values of the picked time can be specified via following methods:
 * <ul>
 * <li>{@link #setHourText(CharSequence)}</li>
 * <li>{@link #setMinuteText(CharSequence)}</li>
 * <li>{@link #setPluralsText(CharSequence)}</li>
 * </ul>
 * <p>
 * To change selection state of the TextViews, invoke either {@link #selectHourView()} or
 * {@link #selectMinuteView()}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#dialogTimeTitleStyle dialogTimeTitleStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class TimePickerDialogTitleView extends RelativeLayout implements DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "TimePickerDialogTitleView";

	/**
	 * Flag for default mode to determine that all text views (hour, minute, separator, am/pm) should
	 * be visible within the DialogTimeTitleView.
	 */
	public static final int MODE_DEFAULT = 0x00;

	/**
	 * Flag for mode to determine that only hour text view (+ am/pm for formatted quantity text)
	 * should be visible within the DialogTimeTitleView.
	 */
	public static final int MODE_HOUR = 0x01;

	/**
	 * Flag for mode to determine that only minute text view (+ am/pm for formatted quantity text)
	 * should be visible within the DialogTimeTitleView.
	 */
	public static final int MODE_MINUTE = 0x02;

	/**
	 * Defines an annotation for determining set of allowed flags for {@link #setMode(int)} method.
	 */
	@IntDef({MODE_DEFAULT, MODE_HOUR, MODE_MINUTE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Mode {}

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that can receive callbacks about selection change of hour/minute view or about selection
	 * change of AM/PM view within {@link TimePickerDialogTitleView}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface OnSelectionChangeListener {

		/**
		 * Invoked whenever hour/minute view selection changes within the given <var>titleView</var>.
		 *
		 * @param titleView    The title view where the hour/minute view selection has changed.
		 * @param hourSelected {@code True} if hour view is selected, {@code false} if minute view is.
		 */
		void onHourMinuteSelectionChanged(@NonNull TimePickerDialogTitleView titleView, boolean hourSelected);

		/**
		 * Invoked whenever AM/PM button selection changes within the given <var>titleView</var>.
		 *
		 * @param titleView  The title view where the AM/PM selection has changed.
		 * @param amSelected {@code True} if AM is selected, {@code false} if PM is.
		 */
		void onAmPmSelectionChanged(@NonNull TimePickerDialogTitleView titleView, boolean amSelected);
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Text view for time element value.
	 */
	private TextView mHourView, mMinuteView;

	/**
	 * Text view for additional plurals text.
	 */
	private TextView mPluralsView;

	/**
	 * Button for selecting AM/PM time value.
	 */
	private Button mButtonAm, mButtonPm;

	/**
	 * Text view for time separator ":".
	 */
	private TextView mTextViewSeparator;

	/**
	 * Current mode of this title view. This mode determines which text views should be visible to
	 * present picked number values from the NumberPickers for hour and minute.
	 */
	private int mMode = MODE_DEFAULT;

	/**
	 * Listener callback to be invoked whenever the hour/minute view or AM/PM view selection changes.
	 */
	private OnSelectionChangeListener mListener;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #TimePickerDialogTitleView(Context, AttributeSet)} without attributes.
	 */
	public TimePickerDialogTitleView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #TimePickerDialogTitleView(Context, AttributeSet, int)} with
	 * {@link R.attr#dialogTimeTitleStyle dialogTimeTitleStyle} as attribute for default style.
	 */
	public TimePickerDialogTitleView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.dialogTimeTitleStyle);
	}

	/**
	 * Same as {@link #TimePickerDialogTitleView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public TimePickerDialogTitleView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of TimePickerDialogTitleView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public TimePickerDialogTitleView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.inflateHierarchy(context, R.layout.dialog_layout_time_title);
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mHourView = (TextView) findViewById(R.id.dialog_text_view_time_hour);
			this.mMinuteView = (TextView) findViewById(R.id.dialog_text_view_time_minute);
			this.mTextViewSeparator = (TextView) findViewById(R.id.dialog_text_view_time_separator);
			this.mPluralsView = (TextView) findViewById(R.id.dialog_text_view_time_plurals);
			this.mButtonAm = (Button) findViewById(R.id.dialog_button_am);
			this.mButtonPm = (Button) findViewById(R.id.dialog_button_pm);
			final ListenerCallbacks listenerCallbacks = new ListenerCallbacks();
			if (mHourView != null) {
				mHourView.setSelected(true);
				mHourView.setOnClickListener(listenerCallbacks);
			}
			if (mMinuteView != null) {
				mMinuteView.setOnClickListener(listenerCallbacks);
			}
			if (mButtonAm != null) {
				mButtonAm.setSelected(true);
				mButtonAm.setOnClickListener(listenerCallbacks);
			}
			if (mButtonPm != null) {
				mButtonPm.setOnClickListener(listenerCallbacks);
			}
			onFinishInflate();
		}
	}

	/**
	 * Sets a mode for this title view. The requested mode will determine which text views should
	 * be visible within this view to present current picked time values.
	 *
	 * @param mode The desired mode. One of {@link #MODE_DEFAULT}, {@link #MODE_HOUR}, {@link #MODE_MINUTE}.
	 */
	public void setMode(@Mode final int mode) {
		if (mMode != mode) this.handleModeUpdate(mode);
	}

	/**
	 * Handles update to the specified <var>mode</var>. This will make changes in the text views
	 * visibility so only text views specific for the specified mode will remain visible.
	 *
	 * @param mode The mode to which to update. One of {@link #MODE_DEFAULT}, {@link #MODE_HOUR} or
	 *             {@link #MODE_MINUTE}.
	 * @see #getMode()
	 */
	private void handleModeUpdate(final int mode) {
		if (mHourView == null || mMinuteView == null || mTextViewSeparator == null) {
			return;
		}
		boolean showPluralsText = true;
		boolean showAmPmButtons = false;
		switch (mMode = mode) {
			case MODE_DEFAULT:
				mHourView.setVisibility(VISIBLE);
				mTextViewSeparator.setVisibility(VISIBLE);
				mMinuteView.setVisibility(VISIBLE);
				showPluralsText = false;
				showAmPmButtons = true;
				break;
			case MODE_HOUR:
				mHourView.setVisibility(VISIBLE);
				mTextViewSeparator.setVisibility(GONE);
				mMinuteView.setVisibility(GONE);
				break;
			case MODE_MINUTE:
				mHourView.setVisibility(GONE);
				mTextViewSeparator.setVisibility(GONE);
				mMinuteView.setVisibility(VISIBLE);
				break;
		}
		if (mPluralsView != null) {
			mPluralsView.setVisibility(showPluralsText ? VISIBLE : GONE);
		}
		if (mButtonAm != null && mButtonPm != null) {
			mButtonAm.setVisibility(showAmPmButtons ? VISIBLE : GONE);
			mButtonPm.setVisibility(showAmPmButtons ? VISIBLE : GONE);
		}
	}

	/**
	 * Returns the current mode in which is this title view.
	 *
	 * @return Current mode. One of {@link #MODE_DEFAULT}, {@link #MODE_HOUR}, {@link #MODE_MINUTE}.
	 * @see #setMode(int)
	 */
	@Mode public int getMode() {
		return mMode;
	}

	/**
	 * Registers a callback to be invoked whenever the hour/minute view or AM/PM view selection changes.
	 *
	 * @param listener Listener callback. May be {@code null} to clear the current one.
	 * @see #selectHourView()
	 * @see #selectMinuteView()
	 * @see #selectAmButton()
	 * @see #selectPmButton()
	 */
	public void setOnSelectionChangeListener(@Nullable final OnSelectionChangeListener listener) {
		this.mListener = listener;
	}

	/**
	 * Sets a text for hour text view.
	 *
	 * @param text The desired text for hour view. May be {@code null} to clear the current one.
	 */
	public void setHourText(@Nullable final CharSequence text) {
		if (mHourView != null) mHourView.setText(text);
	}

	/**
	 * Sets a text for minute text view.
	 *
	 * @param text The desired text for minute view. May be {@code null} to clear the current one.
	 */
	public void setMinuteText(@Nullable final CharSequence text) {
		if (mMinuteView != null) mMinuteView.setText(text);
	}

	/**
	 * Sets a text for additional plurals text view.
	 *
	 * @param text The desired text for plurals view. May be {@code null} to clear the current one.
	 */
	public void setPluralsText(@Nullable final CharSequence text) {
		if (mPluralsView != null) {
			mPluralsView.setText(text);
			mPluralsView.setVisibility(TextUtils.isEmpty(text) ? GONE : VISIBLE);
		}
	}

	/**
	 * Updates a selection state of the hour view to <b>selected</b>. If the minute view is currently
	 * the selected one, this will cancel its selection.
	 */
	public void selectHourView() {
		if (mMinuteView == null || mHourView == null || mHourView.isSelected()) {
			return;
		}
		mHourView.setSelected(true);
		mMinuteView.setSelected(false);
		if (mListener != null) {
			mListener.onHourMinuteSelectionChanged(this, true);
		}
	}

	/**
	 * Updates a selection state of the minute view to <b>selected</b>. If the hour view is currently
	 * the selected one, this will cancel its selection.
	 */
	public void selectMinuteView() {
		if (mHourView == null || mMinuteView == null || mMinuteView.isSelected()) {
			return;
		}
		mHourView.setSelected(false);
		mMinuteView.setSelected(true);
		if (mListener != null) {
			mListener.onHourMinuteSelectionChanged(this, false);
		}
	}

	/**
	 * Selects the AM button, if it is not selected yet, and fires {@link OnSelectionChangeListener#onAmPmSelectionChanged(TimePickerDialogTitleView, boolean)}
	 * with {@code true} value.
	 *
	 * @see #selectPmButton()
	 */
	public void selectAmButton() {
		if (mButtonPm == null || mButtonAm == null || mButtonAm.isSelected()) {
			return;
		}
		mButtonAm.setSelected(true);
		mButtonPm.setSelected(false);
		if (mListener != null) {
			mListener.onAmPmSelectionChanged(this, true);
		}
	}

	/**
	 * Selects the PM button, if it is not selected yet, and fires {@link OnSelectionChangeListener#onAmPmSelectionChanged(TimePickerDialogTitleView, boolean)}
	 * with {@code false} value.
	 *
	 * @see #selectAmButton()
	 */
	public void selectPmButton() {
		if (mButtonAm == null || mButtonPm == null || mButtonPm.isSelected()) {
			return;
		}
		mButtonAm.setSelected(false);
		mButtonPm.setSelected(true);
		if (mListener != null) {
			mListener.onAmPmSelectionChanged(this, false);
		}
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.mode = mMode;
		savedState.hourText = mHourView != null ? mHourView.getText() : "";
		savedState.minuteText = mMinuteView != null ? mMinuteView.getText() : "";
		savedState.hourSelected = mHourView != null && mHourView.isSelected();
		savedState.amSelected = mButtonAm != null && mButtonAm.isSelected();
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());

		if (mMode != savedState.mode) {
			this.handleModeUpdate(savedState.mode);
		}

		setHourText(savedState.hourText);
		setMinuteText(savedState.minuteText);
		setPluralsText(savedState.pluralsText);

		if (savedState.hourSelected) {
			selectHourView();
		} else {
			selectMinuteView();
		}

		if (savedState.amSelected) {
			selectAmButton();
		} else {
			selectPmButton();
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link TimePickerDialogTitleView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) {
				return new SavedState[size];
			}
		};

		/**
		 */
		CharSequence hourText, minuteText, pluralsText;

		/**
		 */
		boolean hourSelected, amSelected;

		/**
		 */
		int mode;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.mode = source.readInt();
			this.hourText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.minuteText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.pluralsText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.hourSelected = source.readInt() == 1;
			this.amSelected = source.readInt() == 1;
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(mode);
			TextUtils.writeToParcel(hourText, dest, flags);
			TextUtils.writeToParcel(minuteText, dest, flags);
			TextUtils.writeToParcel(pluralsText, dest, flags);
			dest.writeInt(hourSelected ? 1 : 0);
			dest.writeInt(amSelected ? 1 : 0);
		}
	}

	/**
	 * A {@link View.OnClickListener} implementation dispatching relevant callbacks to the associated
	 * {@link TimePickerDialogTitleView} layout.
	 */
	private final class ListenerCallbacks implements View.OnClickListener {

		/**
		 */
		@Override public void onClick(@NonNull final View view) {
			final int id = view.getId();
			if (id == R.id.dialog_text_view_time_hour) {
				selectHourView();
			} else if (id == R.id.dialog_text_view_time_minute) {
				selectMinuteView();
			} else if (id == R.id.dialog_button_am) {
				selectAmButton();
			} else if (id == R.id.dialog_button_pm) {
				selectPmButton();
			}
		}
	}
}