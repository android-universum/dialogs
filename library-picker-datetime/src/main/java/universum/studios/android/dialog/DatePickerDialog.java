/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.AttrRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.DatePickerDialogView;

/**
 * A {@link SimpleDialog} implementation that can be used to allow to a user to pick its desired date
 * from the available range within calendar. This dialog uses default {@link DatePickerDialogView}
 * implementation as its content view which consists of {@link universum.studios.android.ui.widget.CalendarView CalendarView}
 * used to present a specific range (data set) of months from where can the user pick one day.
 * <p>
 * The picked date can be obtained via {@link #getDate()}.
 * <p>
 * See {@link DateOptions} for options that can be supplied to the DatePickerDialog from the desired
 * context via {@link #newInstance(DateOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogDateOptions dialogDateOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DatePickerDialog extends SimpleDialog {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "DatePickerDialog";

	/*
	 * Interface ===================================================================================
	 */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Date dialog view implementation.
     */
    private DatePickerDialogView mDateView;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of DatePickerDialog with {@link universum.studios.android.ui.widget.CalendarView CalendarView}
     * that allows to pick the desired date.
     */
    public DatePickerDialog() {
        super(R.attr.dialogDateOptions);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Creates a new instance of DatePickerDialog with the specified options.
     *
     * @param options Specific options for this type of dialog.
     * @return New DatePickerDialog instance.
     */
    @NonNull public static DatePickerDialog newInstance(@NonNull final DateOptions options) {
        final DatePickerDialog dialog = new DatePickerDialog();
        dialog.setOptions(options);
        return dialog;
    }

    /**
     */
    @Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
        return new DateOptions(resources);
    }

	/**
	 */
	@SuppressWarnings("ResourceType")
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof DateOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Date);
			final DateOptions options = (DateOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Date_dialogDate) {
					if (!options.isSet(DateOptions.DATE)) {
						final Long date = DateParser.parse(attributes.getString(index));
						if (date != null) options.date(date);
					}
				} else if (index == R.styleable.Dialog_Options_Date_dialogDateMin) {
					if (!options.isSet(DateOptions.MIN_DATE)) {
						final Long date = DateParser.parse(attributes.getString(index));
						if (date != null) options.minDate(date);
					}
				} else if (index == R.styleable.Dialog_Options_Date_dialogDateMax) {
					if (!options.isSet(DateOptions.MAX_DATE)) {
						final Long date = DateParser.parse(attributes.getString(index));
						if (date != null) {
							options.maxDate(date);
							options.date(Math.min(options.date, date));
						}
					}
				}
			}
			attributes.recycle();
		}
	}

    /**
     */
    @Override @NonNull protected View onCreateContentView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.dialog_content_date, container, false);
    }

    /**
     */
    @Override @NonNull protected View onCreateButtonsView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        // Content view already contains buttons view.
        final View view = new Space(inflater.getContext());
        view.setVisibility(View.GONE);
        return view;
    }

    /**
     */
    @Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
        if (contentView instanceof DatePickerDialogView && options instanceof DateOptions) {
            final DateOptions dateOptions = (DateOptions) options;
            final DatePickerDialogView dateView = (DatePickerDialogView) contentView;
            if (dateOptions.timeZone != null) {
	            dateView.setTimeZone(dateOptions.timeZone);
            }
            dateView.setMinMaxDate(dateOptions.minDate, dateOptions.maxDate);
            dateView.setDate(dateOptions.date);
        }
    }

    /**
     */
    @Override protected void onUpdateContentViewPadding() {
        // Do not update content view padding.
    }

    /**
     */
    @Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final View contentView = getContentView();
        if (contentView instanceof DatePickerDialogView) {
            this.mDateView = (DatePickerDialogView) contentView;
        }
    }

    /**
     * Returns the current value of <b>date</b> picked within this picker dialog.
     *
     * @return Picked date either by user or initially specified via options.
     */
    public long getDate() {
	    final Long date = mDateView == null ? null : mDateView.getDateInMillis();
        return date == null ? 0 : date;
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link DialogOptions} implementation specific for the {@link DatePickerDialog}.
     * <p>
     * Below are listed setters which may be used to supply desired data to {@link DatePickerDialog}
     * through these options:
     * <ul>
     * <li>{@link #date(Date)}</li>
     * <li>{@link #date(long)}</li>
     * <li>{@link #minDate(Date)}</li>
     * <li>{@link #minDate(long)}</li>
     * <li>{@link #maxDate(Date)}</li>
     * <li>{@link #maxDate(long)}</li>
     * </ul>
     * <p>
     * <h3>Xml attributes</h3>
     * See {@link R.styleable#Dialog_Options_Date DateOptions Attributes},
     * {@link R.styleable#Dialog_Options DialogOptions Attributes}
     * <p>
     * Note, that the date related values specified within Xml should be in format: <b>'yyyy-MM-dd'</b>
     * or <b>'current[+|-](yyyy-MM-dd)'</b> otherwise they will be not properly parsed.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class DateOptions extends DialogOptions<DateOptions> {

        /**
         * Creator used to create an instance or array of instances of DateOptions from {@link Parcel}.
         */
        public static final Creator<DateOptions> CREATOR = new Creator<DateOptions>() {

            /**
             */
            @Override public DateOptions createFromParcel(@NonNull final Parcel source) { return new DateOptions(source); }

            /**
             */
            @Override public DateOptions[] newArray(final int size) { return new DateOptions[size]; }
        };

        /**
         * Flag for <b>time zone</b> attribute.
         */
        static final int TIME_ZONE = 0x00000001;

        /**
         * Flag for <b>date</b> parameter.
         */
        static final int DATE = 0x00000001 << 1;

        /**
         * Flag for <b>min date</b> parameter.
         */
        static final int MIN_DATE = 0x00000001 << 2;

        /**
         * Flag for <b>max date</b> parameter.
         */
        static final int MAX_DATE = 0x00000001 << 3;

        /**
         * todo:
         */
        TimeZone timeZone;

        /**
         * Date value determining values for <b>year, month, day</b> to be presented by CalendarView.
         */
        long date = System.currentTimeMillis();

        /**
         * Minimum date allowed to be picked within dialog.
         */
        long minDate = date;

        /**
         * Maximum date allowed to be picked within dialog. If {@code 0} will be infinite.
         */
        long maxDate = 0;

        /**
         * Creates a new instance of DateOptions.
         */
        public DateOptions() {
            super();
        }

        /**
         * Creates a new instance of DateOptions.
         *
         * @param resources Application resources used to obtain values for these options when they
         *                  are requested by theirs resource ids.
         */
        public DateOptions(@NonNull final Resources resources) {
            super(resources);
        }

        /**
         * Called form {@link #CREATOR} to create an instance of DateOptions form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected DateOptions(@NonNull final Parcel source) {
            super(source);
            this.timeZone = (TimeZone) source.readSerializable();
            this.date = source.readLong();
            this.minDate = source.readLong();
            this.maxDate = source.readLong();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeSerializable(timeZone);
            dest.writeLong(date);
            dest.writeLong(minDate);
            dest.writeLong(maxDate);
        }

        /**
         * Creates a new instance of Date with the given <var>year, month, day</var> values.
         *
         * @param year       The desired year number for the new date.
         * @param month      The desired month number for the new date. Should be from the range {@code [0, 11]}.
         *                   Use months provided by {@link Calendar} API for proper usage.
         * @param dayOfMonth The desired day number for the new date. Should be from the range {@code [1, 31]}.
         * @return New date in milliseconds with the requested values.
         */
        public static long newDate(final int year, @IntRange(from = 0, to = 11) final int month, @IntRange(from = 1, to = 31) final int dayOfMonth) {
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            return calendar.getTimeInMillis();
        }

        /**
         */
        @Override public DateOptions merge(@NonNull final DialogOptions options) {
            if (!(options instanceof DateOptions)) {
                return super.merge(options);
            }
            final DateOptions dateOptions = (DateOptions) options;
            if (dateOptions.isSet(TIME_ZONE)) {
                this.timeZone = dateOptions.timeZone;
                updateIsSet(TIME_ZONE);
            }
            if (dateOptions.isSet(DATE)) {
                this.date = dateOptions.date;
                updateIsSet(DATE);
            }
            if (dateOptions.isSet(MIN_DATE)) {
                this.minDate = dateOptions.minDate;
                updateIsSet(MIN_DATE);
            }
            if (dateOptions.isSet(MAX_DATE)) {
                this.maxDate = dateOptions.maxDate;
                updateIsSet(MAX_DATE);
            }
            return super.merge(options);
        }

        /**
         * todo:
         *
         * @param timeZone
         * @return
         */
        public DateOptions timeZone(@NonNull final TimeZone timeZone) {
            updateIsSet(TIME_ZONE);
            this.timeZone = timeZone;
            notifyChanged();
            return this;
        }

        /**
         * Same as {@link #date(long)} for {@link Date} object.
         *
         * @param date The desired initial date.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogDate dialog:dialogDate
         * @see #date()
         */
        public DateOptions date(@NonNull final Date date) {
            return date(date.getTime());
        }

        /**
         * Sets an initial date to be selected within a dialog associated with these options.
         * <p>
         * When the dialog is first time shown, this date will be visible to a user.
         *
         * @param milliseconds The desired initial date in milliseconds.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogDate dialog:dialogDate
         * @see #date()
         */
        public DateOptions date(final long milliseconds) {
            updateIsSet(DATE);
            if (date != milliseconds) {
                this.date = milliseconds;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the initial date, set to these options.
         * <p>
         * Default value: <b>{@link System#currentTimeMillis()}</b>
         *
         * @return Date value in milliseconds.
         * @see #date(long)
         * @see #date(Date)
         */
        public long date() {
            return date;
        }

        /**
         * Same as {@link #minDate(long)} for {@link Date} object.
         *
         * @param minDate The desired minimum date.
         * @see R.attr#dialogDateMin dialog:dialogDateMin
         * @see #minDate()
         */
        public DateOptions minDate(@NonNull final Date minDate) {
            return minDate(minDate.getTime());
        }

        /**
         * Sets the minimum date allowed to be picked within a dialog associated with these options.
         *
         * @param minMilliseconds The desired minimum date in milliseconds.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogDateMin dialog:dialogDateMin
         * @see #minDate()
         */
        public DateOptions minDate(final long minMilliseconds) {
            updateIsSet(MIN_DATE);
            if (minDate != minMilliseconds) {
                this.minDate = minMilliseconds;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the minimum date allowed to be picked within dialog, set to these options.
         *
         * @return Minimum date in milliseconds or same as {@link #date()} by default.
         * @see #minDate(long)
         * @see #minDate(Date)
         */
        public long minDate() {
            return minDate;
        }

        /**
         * Same as {@link #maxDate(long)} for {@link Date} object.
         *
         * @param maxDate The desired maximum date.
         * @see R.attr#dialogDateMax dialog:dialogDateMax
         * @see #maxDate()
         */
        public DateOptions maxDate(@NonNull final Date maxDate) {
            return maxDate(maxDate.getTime());
        }

        /**
         * Sets the maximum date allowed to be picked within a dialog associated with these options.
         *
         * @param maxMilliseconds The desired maximum date in milliseconds. Pass here {@code 0} if
         *                        a user can pick from an 'infinite' date.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogDateMax dialog:dialogDateMax
         * @see #maxDate()
         */
        public DateOptions maxDate(final long maxMilliseconds) {
            updateIsSet(MAX_DATE);
            if (maxDate != maxMilliseconds) {
                this.maxDate = maxMilliseconds;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the maximum date allowed to be picked within dialog, set to these options.
         * <p>
         * Default value: <b>0</b>
         *
         * @return Maximum date in milliseconds.
         * @see #maxDate(Date)
         * @see #maxDate(long)
         */
        public long maxDate() {
            return maxDate;
        }

	    /**
	     */
	    @Override protected void onParseXmlAttribute(
	    		@NonNull final XmlResourceParser xmlParser,
			    final int index,
			    @AttrRes final int attr,
			    @NonNull final Resources resources,
			    @Nullable final Resources.Theme theme
	    ) {
		    if (attr == R.attr.dialogTimeZone) {
			    final String timeZoneId = obtainXmlAttributeString(xmlParser, index, resources);
			    if (timeZoneId != null) timeZone(TimeZone.getTimeZone(timeZoneId));
		    } else if (attr == R.attr.dialogDate) {
			    final Long date = DateParser.parse(obtainXmlAttributeString(xmlParser, index, resources));
			    if (date != null) date(date);
		    } else if (attr == R.attr.dialogDateMin) {
			    final Long date = DateParser.parse(obtainXmlAttributeString(xmlParser, index, resources));
			    if (date != null) minDate(date);
		    } else if (attr == R.attr.dialogDateMax) {
			    final Long date = DateParser.parse(obtainXmlAttributeString(xmlParser, index, resources));
			    if (date != null) {
				    maxDate(date);
				    this.date = Math.min(this.date, date);
			    }
		    } else {
			    super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
		    }
	    }
    }

    /**
     * Parser used to parse date string into date value.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static final class DateParser {

        /**
         * Template for the format used to parse date string from the Xml.
         */
        public static final String FORMAT_TEMPLATE = "yyyy-MM-dd";

        /**
         * Regular expression used to parse date string containing the <b>current</b> keyword.
         */
        private static final String KEYWORD_PATTERN = "^current([-+])(\\(\\d{1,4}-\\d{1,2}-\\d{1,2}\\))$";

        /**
         * Format used to parse date string from the Xml.
         */
        private static SimpleDateFormat format;

        /**
         * Matcher used to parse date string containing the <b>current</b> keyword.
         */
        private static Matcher keywordMatcher;

        /**
         * Calendar used to offset current date by the requested amount of years, months and days.
         */
        private static Calendar calendar;

        /**
         */
        private DateParser() {
	        // Instances of this parser are not allowed to be created.
        }

        /**
         * Parses the specified <var>dateString</var> into date value in milliseconds. The specified
         * string should be in format <b>yyyy-MM-dd</b> or one of keyword expressions specified below.
         * <p>
         * <h3>Accepted key word expressions</h3>
         * <ul>
         * <li>current</li>
         * <li>current[-+](yyyy-MM-dd)</li>
         * </ul>
         *
         * @param dateString The desired date string to parse into date value. May be {@code null}
         *                   in which case {@code null} is also returned.
         * @return Parsed date value in milliseconds or {@code null} if the specified string cannot
         * be parsed into date.
         * @see SimpleDateFormat
         */
        @Nullable public static Long parse(@Nullable final String dateString) {
	        if (TextUtils.isEmpty(dateString)) {
		        return null;
	        }
            if (dateString.contentEquals("current")) {
                return System.currentTimeMillis();
            }
            ensureInitialized();
            if (keywordMatcher.reset(dateString).matches()) {
                String groupDate = keywordMatcher.group(2);
                // Get rid of the brackets at the start and at the end of the date string.
                groupDate = groupDate.substring(1, groupDate.length() - 1);
                final String[] groupDateItems = groupDate.split("-");
                final String groupSign = keywordMatcher.group(1);
                final int offsetDirection = groupSign.contentEquals("+") ? 1 : -1;
                final int offsetYears = Integer.parseInt(groupDateItems[0]) * offsetDirection;
                final int offsetMonths = Integer.parseInt(groupDateItems[1]) * offsetDirection;
                final int offsetDays = Integer.parseInt(groupDateItems[2]) * offsetDirection;
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + offsetYears);
                calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + offsetMonths);
                calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + offsetDays);
                return calendar.getTimeInMillis();
            }
            return parseInner(dateString);
        }

        /**
         * Ensures that this parser is initialized.
         */
        private static void ensureInitialized() {
            if (calendar == null) {
                DateParser.calendar = Calendar.getInstance();
                DateParser.format = new SimpleDateFormat(FORMAT_TEMPLATE, Locale.getDefault());
                DateParser.keywordMatcher = Pattern.compile(KEYWORD_PATTERN).matcher("");
            }
        }

        /**
         * Parses the specified <var>dateString</var> using {@link #format}.
         *
         * @param dateString The string to parse into date value.
         * @return Parsed date value in milliseconds as Long or {@code null} if the specified string
         * cannot be parsed into date value.
         */
        private static Long parseInner(final String dateString) {
            try {
                return format.parse(dateString).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}