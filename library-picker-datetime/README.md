Dialogs-Picker-DateTime
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adialogs/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adialogs/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:dialogs-picker-datetime:${DESIRED_VERSION}@aar"
