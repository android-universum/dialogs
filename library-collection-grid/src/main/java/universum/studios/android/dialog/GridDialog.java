/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * An {@link AdapterDialog} implementation that can be used to present data set of items within
 * {@link GridView}.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogGridOptions dialogGridOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class GridDialog extends AdapterDialog<GridView> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "GridDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of GridDialog with {@link GridView} to present data from {@link android.widget.Adapter}.
	 */
	public GridDialog() {
		super(R.attr.dialogGridOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of GridDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New GridDialog instance.
	 */
	@NonNull public static GridDialog newInstance(@NonNull final GridOptions options) {
		final GridDialog dialog = new GridDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new GridOptions(resources);
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.dialog_content_grid, container, false);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link BaseAdapterOptions} implementation specific for the {@link GridDialog}.
	 * <p>
	 * These options are here only for convenience and do not add any extra options to the parent's
	 * {@link BaseAdapterOptions}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class GridOptions extends BaseAdapterOptions<GridOptions> {

		/**
		 * Creator used to create an instance or array of instances of GridOptions from {@link Parcel}.
		 */
		public static final Creator<GridOptions> CREATOR = new Creator<GridOptions>() {

			/**
			 */
			@Override public GridOptions createFromParcel(@NonNull final Parcel source) { return new GridOptions(source); }

			/**
			 */
			@Override public GridOptions[] newArray(final int size) { return new GridOptions[size]; }
		};

		/**
		 * Creates a new instance of GridOptions.
		 */
		public GridOptions() {
			super();
		}

		/**
		 * Creates a new instance of GridOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public GridOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of GridOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected GridOptions(@NonNull final Parcel source) {
			super(source);
		}
	}
}