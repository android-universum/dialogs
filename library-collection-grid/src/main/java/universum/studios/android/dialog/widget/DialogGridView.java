/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.GridView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.core.view.ViewCompat;
import universum.studios.android.dialog.view.DialogView;

/**
 * A {@link GridView} simple implementation that tints its over-scroll effect using the accent color
 * presented within the current dialog theme. This extended grid view also draws a shadow at its
 * both vertical (top and bottom) edges.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogGridView extends GridView implements DialogView {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogGridView";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Flag indicating whether this class has been already initialized.
	 */
	private final boolean mInitialized;

	/**
	 * Helper used to draw vertical edge shadows for this view.
	 */
	private VerticalEdgeShadowHelper mEdgeShadowHelper;

	/*
	 * Constructors ================================================================================
	 */

    /**
     * Same as {@link #DialogGridView(Context, AttributeSet)}
     * without attributes.
     */
    public DialogGridView(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #DialogGridView(Context, AttributeSet, int)} with
     * {@link android.R.attr#gridViewStyle android:gridViewStyle} as attribute for default style.
     */
    public DialogGridView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, android.R.attr.gridViewStyle);
    }

    /**
     * Same as {@link #DialogGridView(Context, AttributeSet, int, int)} with {@code 0} as default style.
     */
    public DialogGridView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
	    this.init(context, attrs, defStyleAttr, 0);
        OverScrollTintManager.applyOverScrollTint(context);

        this.mInitialized = true;
    }

    /**
     * Creates a new instance of DialogGridView for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     * @param defStyleRes  Resource id of the default style for the new view.
     */
    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DialogGridView(
    		@NonNull final Context context,
		    @Nullable final AttributeSet attrs,
		    @AttrRes final int defStyleAttr,
		    @StyleRes final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
	    this.init(context, attrs, defStyleAttr, defStyleRes);

	    this.mInitialized = true;
    }

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.mEdgeShadowHelper = new VerticalEdgeShadowHelper(this);
	}

	/**
	 */
	@Override protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
		super.onSizeChanged(width, height, oldWidth, oldHeight);
		mEdgeShadowHelper.onViewSizeChanged(width, height);
	}

	/**
	 */
	@Override protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		final boolean shadowsVisible = ViewCompat.canScrollVertically(this, -1) || ViewCompat.canScrollVertically(this, 1);
		mEdgeShadowHelper.setTopShadowVisible(shadowsVisible);
		mEdgeShadowHelper.setBottomShadowVisible(shadowsVisible);
	}

	/**
	 */
	@Override public boolean verifyDrawable(@NonNull final Drawable drawable) {
		if (mInitialized && mEdgeShadowHelper.verifyShadowDrawable(drawable)) {
			return true;
		}

		return super.verifyDrawable(drawable);
	}

	/**
	 */
	@Override public void draw(@NonNull final Canvas canvas) {
		super.draw(canvas);
		mEdgeShadowHelper.drawShadows(canvas);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}