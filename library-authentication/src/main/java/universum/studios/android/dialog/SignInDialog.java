/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import androidx.fragment.app.Fragment;
import universum.studios.android.dialog.util.DialogUtils;
import universum.studios.android.dialog.view.InputConfig;
import universum.studios.android.dialog.view.SignInDialogView;

/**
 * A {@link SimpleDialog} implementation that can be used to allow to a user to perform the <b>Sign In</b>
 * process by inputting its <b>username</b> and <b>password</b>. This dialog uses default {@link SignInDialogView}
 * implementation as its content view which consists of two {@link universum.studios.android.ui.widget.EditLayout EditLayout}
 * for that matter.
 * <p>
 * The current input values (credentials) can be obtained via {@link #getCredentials()} or cleared
 * via {@link #clearInputs()}. If needed, errors can be specified via {@link #setUsernameError(CharSequence)}
 * and {@link #setPasswordError(CharSequence)} or theirs resource equivalents {@link #setUsernameError(int)}
 * and {@link #setPasswordError(int)} and cleared via {@link #clearErrors()} or respectively via
 * {@link #clearUsernameError()} and {@link #clearPasswordError()}.
 * <p>
 * See {@link SignInOptions} for options that can be supplied to the SignInDialog from the desired
 * context via {@link #newInstance(SignInOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Callbacks</h3>
 * The SignIn dialog uses pattern matchers to validate input values for username and password whenever
 * the {@link #BUTTON_POSITIVE} is clicked. Patterns for these matchers can be specified through
 * {@link SignInOptions} mentioned above via {@link SignInOptions#passwordRegExp(String)} and
 * {@link SignInOptions#usernameRegExp(String)}. Whenever the validation process fails (some of the
 * input values does not pass the corresponding regular expression) an error callback is fired through
 * via {@link OnSignInListener#onDialogSignInError(SignInDialog, int)}. This listener can be registered
 * as normal OnDialogListener via {@link #setOnDialogListener(OnDialogListener)}, but it is enough
 * if the context (Activity or Fragment) implements this listener and the SignInDialog will automatically
 * detects such implementation and will use it as its listener as described in {@link BaseDialog}.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogSignInOptions dialogSignInOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SignInDialog extends SimpleDialog {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "SignInDialog";

	/**
	 * Code for error due to invalid user name. User name doesn't match the reg-exp pattern.
	 */
	public static final int ERROR_INVALID_USER_NAME = -0xe01;

	/**
	 * Code for error due to invalid password. Password doesn't match the reg-exp pattern.
	 */
	public static final int ERROR_INVALID_PASSWORD = -0xe02;

	/**
	 * Flag indicating whether this dialog is enabled or not.
	 */
	private static final int PFLAG_ENABLED = 0x00000001;

	/**
	 * Flag indicating whether the authentication is running or not.
	 */
	private static final int PFLAG_AUTHENTICATION_RUNNING = 0x00000001 << 1;

	/**
	 * Pattern that can be used to check for valid e-mail address.
	 */
	private static final Pattern PATTERN_EMAIL = Pattern.compile(
			"[a-zA-Z0-9\\+\\._%\\-\\+]{1,256}@" +
					"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
					"(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"
	);

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that can receive a callback about occurred sign-in error within {@link SignInDialog}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface OnSignInListener extends OnDialogListener {

		/**
		 * Invoked whenever an error with the specified <var>error</var> code occurs within the
		 * specified <var>dialog</var>.
		 *
		 * @param dialog The dialog in which the error occurs.
		 * @param error  Code of the occurred error.
		 * @return {@code True} if this event has been processed here, {@code false} to be propagated
		 * further.
		 */
		boolean onDialogSignInError(@NonNull SignInDialog dialog, int error);
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * SignIn dialog view implementation.
	 */
	private SignInDialogView mSignInView;

	/**
	 * Reg-exp matcher used to check username input.
	 */
	private Matcher mUsernameMatcher = PATTERN_EMAIL.matcher("");

	/**
	 * Reg-exp matcher used to check password input.
	 */
	private Matcher mPasswordMatcher;

	/**
	 * Set of private flags of this dialog.
	 */
	private int mPrivateFlags = PFLAG_ENABLED;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SignInDialog with two {@link android.widget.EditText}s, first for
	 * the username input, second for the password input.
	 */
	public SignInDialog() {
		super(R.attr.dialogSignInOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of SignInDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New SignInDialog instance.
	 */
	@NonNull public static SignInDialog newInstance(@NonNull final SignInOptions options) {
		final SignInDialog dialog = new SignInDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new SignInOptions(resources);
	}

	/**
	 */
	@SuppressWarnings("ConstantConditions")
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof SignInOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_SignIn);
			final SignInOptions options = (SignInOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_SignIn_dialogUsernameInputStyle) {
					if (!options.isSet(SignInOptions.USERNAME_INPUT_CONFIG)) {
						options.usernameInputStyle(attributes.getResourceId(index, -1));
					}
				} else if (index == R.styleable.Dialog_Options_SignIn_dialogPasswordInputStyle) {
					if (!options.isSet(SignInOptions.PASSWORD_INPUT_CONFIG)) {
						options.passwordInputStyle(attributes.getResourceId(index, -1));
					}
				} else if (index == R.styleable.Dialog_Options_SignIn_dialogHintUsername) {
					if (!options.isSet(SignInOptions.USERNAME_HINT)) {
						options.usernameHint(attributes.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_SignIn_dialogHintPassword) {
					if (!options.isSet(SignInOptions.PASSWORD_HINT)) {
						options.passwordHint(attributes.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_SignIn_dialogRegExpUsername) {
					if (!options.isSet(SignInOptions.USERNAME_REG_EXP)) {
						options.usernameRegExp(attributes.getString(index));
					} else {
						this.mUsernameMatcher = Pattern.compile(attributes.getString(index)).matcher("");
					}
				} else if (index == R.styleable.Dialog_Options_SignIn_dialogRegExpPassword) {
					if (!options.isSet(SignInOptions.PASSWORD_REG_EXP)) {
						options.passwordRegExp(attributes.getString(index));
					} else {
						this.mPasswordMatcher = Pattern.compile(attributes.getString(index)).matcher("");
					}
				} else if (index == R.styleable.Dialog_Options_SignIn_dialogShowSoftKeyboard) {
					if (!options.isSet(SignInOptions.SHOW_SOFT_KEYBOARD)) {
						options.showSoftKeyboard(attributes.getBoolean(index, true));
					}
				}
			}
			attributes.recycle();
		}
	}

	/**
	 *
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_sign_in, container, false);
	}

	/**
	 */
	@Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
		if (contentView instanceof SignInDialogView && options instanceof SignInOptions) {
			final SignInOptions signInOptions = (SignInOptions) options;
			final SignInDialogView signInView = (SignInDialogView) contentView;
			if (signInOptions.userHint != null) {
				signInView.setUsernameHint(signInOptions.userHint);
			}
			if (signInOptions.passHint != null) {
				signInView.setPasswordHint(signInOptions.passHint);
			}
			if (signInOptions.credentials != null) {
				signInView.setPasswordInput(signInOptions.credentials.password);
				signInView.setUsernameInput(signInOptions.credentials.username);
			}
			if (signInOptions.userRegExp != null) {
				this.mUsernameMatcher = Pattern.compile(signInOptions.userRegExp).matcher("");
			}
			if (signInOptions.passRegExp != null) {
				this.mPasswordMatcher = Pattern.compile(signInOptions.passRegExp).matcher("");
			}
			this.ensureOptionsInputConfigs(signInOptions);
			if (signInOptions.userInputConfig != null) {
				signInView.setUsernameInputConfig(signInOptions.userInputConfig);
			}
			if (signInOptions.passInputConfig != null) {
				signInView.setPasswordInputConfig(signInOptions.passInputConfig);
			}
		}
	}

	/**
	 * Ensures that the specified <var>options</var> has its edit configuration objects initialized
	 * in case of use of styles from which should be these config objects created.
	 */
	private void ensureOptionsInputConfigs(final SignInOptions options) {
		final Context context = getActivity();
		if (options != null && context != null) options.ensureInputConfigs(context);
	}

	/**
	 */
	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		// Regular expression for password is required.
		if (mPasswordMatcher == null) {
			throw new AndroidRuntimeException("Regular expression for password is required.");
		}
		if (mOptions instanceof SignInOptions && ((SignInOptions) mOptions).showSoftKeyboard) {
			DialogUtils.showSoftKeyboard(this);
		}
		final View contentView = getContentView();
		if (contentView instanceof SignInDialogView) {
			this.mSignInView = (SignInDialogView) contentView;
		}
	}

	/**
	 * Updates the enabled state of this dialog.
	 * <p>
	 * <b>Note</b>, that while this dialog is <b>disabled</b>, the current username and password
	 * input cannot be changed by the user.
	 *
	 * @param enabled {@code True} to enable this dialog, {@code false} otherwise.
	 * @see #isEnabled()
	 */
	public void setEnabled(final boolean enabled) {
		this.updateState(enabled);
	}

	/**
	 * Updates the current state of this dialog.
	 *
	 * @param enabled {@code True} if this dialog should be enabled, {@code false} to be disabled, so
	 *                user cannot interact with it (<b>note</b>, that dialog will also be not cancelable).
	 */
	private void updateState(final boolean enabled) {
		if (enabled != hasPrivateFlag(PFLAG_ENABLED)) {
			this.updatePrivateFlags(PFLAG_ENABLED, enabled);
			setCancelable(!enabled);
			if (mSignInView != null) {
				mSignInView.setEnabled(enabled);
			}
		}
	}

	/**
	 * Returns a boolean flag indicating whether this dialog is enabled or not.
	 *
	 * @return {@code True} if this dialog is enabled, {@code false} otherwise.
	 */
	public boolean isEnabled() {
		return (mPrivateFlags & PFLAG_ENABLED) != 0;
	}

	/**
	 */
	@Override protected boolean onButtonClick(final int button) {
		DialogUtils.hideSoftKeyboard(this);
		switch (button) {
			case BUTTON_POSITIVE:
				// If data are invalid, do not dispatch to listeners.
				return !performCheckData();
		}
		return super.onButtonClick(button);
	}

	/**
	 * Performs check of the current data entered within username and password inputs.
	 *
	 * @return {@code True} if data are valid and matches required regular expressions,
	 * {@code false} otherwise.
	 */
	private boolean performCheckData() {
		if (onCheckData(getUsername(), getPassword())) {
			if (DialogsConfig.DEBUG_LOG_ENABLED) {
				Log.d(TAG, "Data passed. Performing authorization.");
			}
			this.updatePrivateFlags(PFLAG_AUTHENTICATION_RUNNING, true);
			DialogUtils.hideSoftKeyboard(this);
			return true;
		}
		return false;
	}

	/**
	 * Returns the current credential values presented within the username and password EditTexts
	 * of this sign in dialog instance.
	 *
	 * @return Current credentials.
	 */
	@NonNull public Credentials getCredentials() {
		return new Credentials(getUsername(), getPassword());
	}

	/**
	 * See {@link SignInDialogView#getUsernameEditableInput()} for info.
	 *
	 * @return Current text input from the username input view.
	 */
	private String getUsername() {
		return mSignInView != null ? mSignInView.getUsernameInput().toString() : "";
	}

	/**
	 * See {@link SignInDialogView#getPasswordEditableInput()} for info.
	 *
	 * @return Current text input from the password input view.
	 */
	private String getPassword() {
		return mSignInView != null ? mSignInView.getPasswordInput().toString() : "";
	}

	/**
	 * Invoked whenever {@link #BUTTON_POSITIVE} is clicked within this dialog.
	 * <p>
	 * If checked data are invalid, {@link #notifyError(int)} will be called with one of {@link #ERROR_INVALID_USER_NAME}
	 * or {@link #ERROR_INVALID_PASSWORD}.
	 *
	 * @param username The current input value from the username EditText.
	 * @param password The current input value from the password EditText.
	 * @return {@code True} if data are valid and matches required regular expressions,
	 * {@code false} otherwise.
	 */
	protected boolean onCheckData(final String username, final String password) {
		boolean validUser, validPass;
		if (DialogsConfig.DEBUG_LOG_ENABLED) {
			Log.d(TAG, "onCheckData() user('" + username + "'), pass('" + password + "')");
		}
		// Check user name.
		if (!(validUser = mUsernameMatcher.reset(username).matches())) {
			this.notifyError(ERROR_INVALID_USER_NAME);
		} else {
			clearUsernameError();
		}
		if (DialogsConfig.DEBUG_LOG_ENABLED) {
			Log.d(TAG, "onCheckData() userMatches(" + validUser + ") with('" + mUsernameMatcher.pattern() + "')");
		}
		// Check password.
		if (!(validPass = mPasswordMatcher.reset(password).matches())) {
			this.notifyError(ERROR_INVALID_PASSWORD);
		} else {
			clearPasswordError();
		}
		if (DialogsConfig.DEBUG_LOG_ENABLED) {
			Log.d(TAG, "onCheckData() passMatches(" + validPass + ") with('" + mPasswordMatcher.pattern() + "')");
		}
		return validUser && validPass;
	}

	/**
	 * Notifies all listeners that an error with the specified <var>errorCode</var> occurred during
	 * data validation process.
	 * <p>
	 * This will call immediately {@link #onError(int)}.
	 *
	 * @param errorCode Error to notify about.
	 */
	protected final void notifyError(final int errorCode) {
		if (!onError(errorCode)) {
			// Traverse event to all available listeners.
			boolean processed = mListener instanceof OnSignInListener && ((OnSignInListener) mListener).onDialogSignInError(this, errorCode);
			if (!processed) {
				final Fragment fragment = findContextFragment();
				processed = fragment instanceof OnSignInListener && ((OnSignInListener) fragment).onDialogSignInError(this, errorCode);
			}
			if (!processed) {
				final Activity activity = getActivity();
				if (activity instanceof OnSignInListener) {
					((OnSignInListener) activity).onDialogSignInError(this, errorCode);
				}
			}
		}
	}

	/**
	 * Invoked whenever {@link #notifyError(int)} is called with the specified <var>errorCode</var>.
	 *
	 * @return {@code True} if error was handled here, {@code false} to let available listeners
	 * (listener, fragment, activity) handle this error.
	 */
	protected boolean onError(final int errorCode) {
		return false;
	}

	/**
	 * Same as {@link #setUsernameError(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired error text.
	 * @see #clearUsernameError()
	 */
	public void setUsernameError(@StringRes final int resId) {
		setUsernameError(getText(resId));
	}

	/**
	 * Sets an error text for the username input view of this dialog.
	 *
	 * @param error The desired error text.
	 * @see #clearUsernameError()
	 */
	public void setUsernameError(@NonNull final CharSequence error) {
		if ((mPrivateFlags & PFLAG_AUTHENTICATION_RUNNING) != 0) {
			this.updatePrivateFlags(PFLAG_AUTHENTICATION_RUNNING, false);
			this.updateState(true);
		}
		if (mSignInView != null) mSignInView.setUsernameError(error);
	}

	/**
	 * Clears the current error specified via {@link #setUsernameError(CharSequence)}.
	 */
	public void clearUsernameError() {
		if (mSignInView != null) mSignInView.clearUsernameError();
	}

	/**
	 * Same as {@link #setPasswordError(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired error text.
	 * @see #clearPasswordError()
	 */
	public void setPasswordError(@StringRes final int resId) {
		setPasswordError(getText(resId));
	}

	/**
	 * Sets an error text for the password input view of this dialog.
	 *
	 * @param error The desired error text.
	 * @see #clearPasswordError()
	 */
	public void setPasswordError(@NonNull final CharSequence error) {
		if ((mPrivateFlags & PFLAG_AUTHENTICATION_RUNNING) != 0) {
			this.updatePrivateFlags(PFLAG_AUTHENTICATION_RUNNING, false);
			this.updateState(true);
		}
		if (mSignInView != null) mSignInView.setPasswordError(error);
	}

	/**
	 * Clears the current error specified via {@link #setPasswordError(CharSequence)}.
	 */
	public void clearPasswordError() {
		if (mSignInView != null) mSignInView.clearPasswordError();
	}

	/**
	 * Clears the current errors specified via {@link #setUsernameError(CharSequence)} and
	 * {@link #setPasswordError(CharSequence)} (if any).
	 */
	public void clearErrors() {
		clearUsernameError();
		clearPasswordError();
	}

	/**
	 * Clears the current input values presented within the username and password input views of
	 * this dialog.
	 */
	public void clearInputs() {
		if (mSignInView != null) mSignInView.clearInputs();
	}

	/**
	 * Clears the focus of this dialog's content view.
	 */
	private void clearFocus() {
		if (mSignInView != null) mSignInView.clearFocus();
	}

	/**
	 * Updates the current private flags.
	 *
	 * @param flag Value of the desired flag to add/remove to/from the current private flags.
	 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
	 */
	private void updatePrivateFlags(final int flag, final boolean add) {
		if (add) this.mPrivateFlags |= flag;
		else this.mPrivateFlags &= ~flag;
	}

	/**
	 * Returns a boolean flag indicating whether the specified <var>flag</var> is contained within
	 * the current private flags or not.
	 *
	 * @param flag Value of the flag to check.
	 * @return {@code True} if the requested flag is contained, {@code false} otherwise.
	 */
	private boolean hasPrivateFlag(final int flag) {
		return (mPrivateFlags & flag) != 0;
	}

	/**
	 */
	@Override @NonNull protected Parcelable onSaveInstanceState() {
		final SavedState state = new SavedState(super.onSaveInstanceState());
		state.privateFlags = mPrivateFlags;
		state.userRegExp = mUsernameMatcher != null ? mUsernameMatcher.pattern().pattern() : null;
		state.passRegExp = mPasswordMatcher.pattern().pattern();
		return state;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable savedState) {
		if (!(savedState instanceof SavedState)) {
			super.onRestoreInstanceState(savedState);
			return;
		}
		final SavedState state = (SavedState) savedState;
		super.onRestoreInstanceState(state.getSuperState());
		if ((state.privateFlags & PFLAG_AUTHENTICATION_RUNNING) != 0) {
			this.updateState(false);
		}
		this.mPrivateFlags = state.privateFlags;
		this.mUsernameMatcher = state.userRegExp != null ? Pattern.compile(state.userRegExp).matcher("") : null;
		this.mPasswordMatcher = Pattern.compile(state.passRegExp).matcher("");
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Simple class used to hold a user's authentication credentials: <b>username</b> + <b>password</b>.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class Credentials implements Parcelable {

		/**
		 * Creator used to create an instance or array of instances of Credentials from {@link Parcel}.
		 */
		public static final Creator<Credentials> CREATOR = new Creator<Credentials>() {

			/**
			 */
			@Override public Credentials createFromParcel(@NonNull final Parcel source) { return new Credentials(source); }

			/**
			 */
			@Override public Credentials[] newArray(final int size) { return new Credentials[size]; }
		};

		/**
		 * Username credential.
		 */
		public final String username;

		/**
		 * Password credential.
		 */
		public final String password;

		/**
		 * Creates a new instance of Credentials with values from the given <var>other</var>.
		 *
		 * @param other Other instance of credentials from which to copy its values.
		 */
		public Credentials(@NonNull final Credentials other) {
			this(other.username, other.password);
		}

		/**
		 * Creates a new instance of Credentials with the given values.
		 *
		 * @param user Value of username credential.
		 * @param pass Value of password credential.
		 */
		public Credentials(@NonNull final String user, @NonNull final String pass) {
			this.username = user;
			this.password = pass;
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of Credentials form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected Credentials(@NonNull final Parcel source) {
			this.username = source.readString();
			this.password = source.readString();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			dest.writeString(username);
			dest.writeString(password);
		}

		/**
		 */
		@Override public int describeContents() {
			return 0;
		}
	}

	/**
	 * A {@link DialogOptions} implementation specific for the {@link SignInDialog}.
	 * <p>
	 * Below are listed setters which may be used to supply desired data to {@link SignInDialog}
	 * through these options:
	 * <ul>
	 * <li>{@link #credentials(Credentials)}</li>
	 * <li>{@link #usernameHint(CharSequence)}</li>
	 * <li>{@link #usernameHint(int)}</li>
	 * <li>{@link #passwordHint(CharSequence)}</li>
	 * <li>{@link #passwordHint(int)}</li>
	 * <li>{@link #usernameRegExp(String)}</li>
	 * <li>{@link #usernameRegExp(int)}</li>
	 * <li>{@link #passwordRegExp(String)}</li>
	 * <li>{@link #passwordRegExp(int)}</li>
	 * <li>{@link #usernameInputConfig(InputConfig)}</li>
	 * <li>{@link #passwordInputConfig(InputConfig)}</li>
	 * <li>{@link #showSoftKeyboard(boolean)}</li>
	 * </ul>
	 * <p>
	 * <h3>Xml attributes</h3>
	 * See {@link R.styleable#Dialog_Options_SignIn SignInOptions Attributes},
	 * {@link R.styleable#Dialog_Options DialogOptions Attributes}
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SignInOptions extends DialogOptions<SignInOptions> {

		/**
		 * Creator used to create an instance or array of instances of SignInOptions from {@link Parcel}.
		 */
		public static final Creator<SignInOptions> CREATOR = new Creator<SignInOptions>() {

			/**
			 */
			@Override public SignInOptions createFromParcel(@NonNull final Parcel source) { return new SignInOptions(source); }

			/**
			 */
			@Override public SignInOptions[] newArray(final int size) { return new SignInOptions[size]; }
		};

		/**
		 * Flag for <b>username hint</b> parameter.
		 */
		static final int USERNAME_HINT = 0x00000001;

		/**
		 * Flag for <b>password hint</b> parameter.
		 */
		static final int PASSWORD_HINT = 0x00000001 << 1;

		/**
		 * Flag for <b>username regular expression</b> parameter.
		 */
		static final int USERNAME_REG_EXP = 0x00000001 << 2;

		/**
		 * Flag for <b>password regular expression</b> parameter.
		 */
		static final int PASSWORD_REG_EXP = 0x00000001 << 3;

		/**
		 * Flag for <b>credentials</b> parameter.
		 */
		static final int CREDENTIALS = 0x00000001 << 4;

		/**
		 * Flag for <b>show soft keyboard</b> parameter.
		 */
		static final int SHOW_SOFT_KEYBOARD = 0x00000001 << 5;

		/**
		 * Flag for <b>username input config</b> parameter.
		 */
		static final int USERNAME_INPUT_CONFIG = 0x00000001 << 6;

		/**
		 * Flag for <b>password input config</b> parameter.
		 */
		static final int PASSWORD_INPUT_CONFIG = 0x00000001 << 7;

		/**
		 * Hint for edit text view.
		 */
		CharSequence userHint, passHint;

		/**
		 * Regular expression used to check value of edit text view.
		 */
		String userRegExp, passRegExp;

		/**
		 * Initial credentials.
		 */
		Credentials credentials;

		/**
		 * Flag indicating whether a soft keyboard should be showed or not.
		 */
		boolean showSoftKeyboard = true;

		/**
		 * Configuration options for username input.
		 */
		InputConfig userInputConfig;

		/**
		 * Resource id of the style holding configuration options for username edit input.
		 */
		int userInputConfigStyle = -1;

		/**
		 * Configuration options for password input.
		 */
		InputConfig passInputConfig;

		/**
		 * Resource id of the style holding configuration options for password edit input.
		 */
		int passInputConfigStyle = -1;

		/**
		 * Creates a new instance of SignInOptions.
		 */
		public SignInOptions() {
			super();
		}

		/**
		 * Creates a new instance of SignInOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public SignInOptions(@NonNull final Resources resources) {
			super(resources);
			remain(true);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of SignInOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SignInOptions(@NonNull final Parcel source) {
			super(source);
			this.userHint = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.passHint = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.userRegExp = source.readString();
			this.passRegExp = source.readString();
			this.showSoftKeyboard = source.readInt() == 1;
			this.credentials = source.readParcelable(DialogsConfig.class.getClassLoader());
			this.userInputConfig = source.readParcelable(DialogsConfig.class.getClassLoader());
			this.passInputConfig = source.readParcelable(DialogsConfig.class.getClassLoader());
			this.userInputConfigStyle = source.readInt();
			this.passInputConfigStyle = source.readInt();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			TextUtils.writeToParcel(userHint, dest, flags);
			TextUtils.writeToParcel(passHint, dest, flags);
			dest.writeString(userRegExp);
			dest.writeString(passRegExp);
			dest.writeInt(showSoftKeyboard ? 1 : 0);
			dest.writeParcelable(credentials, flags);
			dest.writeParcelable(userInputConfig, flags);
			dest.writeParcelable(passInputConfig, flags);
			dest.writeInt(userInputConfigStyle);
			dest.writeInt(passInputConfigStyle);
		}

		/**
		 */
		@Override public SignInOptions merge(@NonNull final DialogOptions options) {
			if (!(options instanceof SignInOptions)) {
				return super.merge(options);
			}
			final SignInOptions signInOptions = (SignInOptions) options;
			if (signInOptions.isSet(USERNAME_HINT)) {
				this.userHint = copyCharSequence(signInOptions.userHint);
				updateIsSet(USERNAME_HINT);
			}
			if (signInOptions.isSet(PASSWORD_HINT)) {
				this.passHint = copyCharSequence(signInOptions.passHint);
				updateIsSet(PASSWORD_HINT);
			}
			if (signInOptions.isSet(USERNAME_REG_EXP)) {
				this.userRegExp = signInOptions.userRegExp;
				updateIsSet(USERNAME_REG_EXP);
			}
			if (signInOptions.isSet(PASSWORD_REG_EXP)) {
				this.passRegExp = signInOptions.passRegExp;
				updateIsSet(PASSWORD_REG_EXP);
			}
			if (signInOptions.isSet(CREDENTIALS)) {
				if (signInOptions.credentials != null) {
					this.credentials = new Credentials(signInOptions.credentials);
				} else {
					this.credentials = null;
				}
				updateIsSet(CREDENTIALS);
			}
			if (signInOptions.isSet(SHOW_SOFT_KEYBOARD)) {
				this.showSoftKeyboard = signInOptions.showSoftKeyboard;
				updateIsSet(SHOW_SOFT_KEYBOARD);
			}
			if (signInOptions.isSet(USERNAME_INPUT_CONFIG)) {
				this.userInputConfigStyle = signInOptions.userInputConfigStyle;
				if (signInOptions.userInputConfig != null) {
					this.userInputConfig = new InputConfig(signInOptions.userInputConfig);
				} else {
					this.userInputConfig = null;
				}
				updateIsSet(USERNAME_INPUT_CONFIG);
			}
			if (signInOptions.isSet(PASSWORD_INPUT_CONFIG)) {
				this.passInputConfigStyle = signInOptions.passInputConfigStyle;
				if (signInOptions.passInputConfig != null) {
					this.passInputConfig = new InputConfig(signInOptions.passInputConfig);
				} else {
					this.passInputConfig = null;
				}
				updateIsSet(PASSWORD_INPUT_CONFIG);
			}
			return super.merge(options);
		}

		/**
		 * Same as {@link #usernameHint(CharSequence)} but with resource id.
		 *
		 * @param resId Resource id of the desired hint text.
		 * @see R.attr#dialogHintUsername dialog:dialogHintUsername
		 * @see #usernameHint()
		 */
		public SignInOptions usernameHint(@StringRes final int resId) {
			return usernameHint(string(resId));
		}

		/**
		 * Sets the hint text for username input of a dialog associated with these options.
		 *
		 * @param hint The desired hint text.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogHintUsername dialog:dialogHintUsername
		 * @see #usernameHint()
		 */
		public SignInOptions usernameHint(@Nullable final CharSequence hint) {
			updateIsSet(USERNAME_HINT);
			if (userHint == null || !userHint.equals(hint)) {
				this.userHint = hint;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the hint text for username input, set to these options.
		 *
		 * @return Username hint.
		 * @see #usernameHint(int)
		 * @see #usernameHint(CharSequence)
		 */
		@Nullable public CharSequence usernameHint() {
			return userHint;
		}

		/**
		 * Same as {@link #usernameRegExp(String)} but with resource id.
		 *
		 * @param resId Resource id of the desired regular expression.
		 * @see R.attr#dialogRegExpUsername dialog:dialogRegExpUsername
		 * @see #usernameRegExp()
		 */
		public SignInOptions usernameRegExp(@StringRes final int resId) {
			return usernameRegExp(string(resId));
		}

		/**
		 * Sets the regular expression used to check username input of a dialog associated with this
		 * options.
		 *
		 * @param regExp The desired regular expression for username input.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogRegExpUsername dialog:dialogRegExpUsername
		 * @see #usernameRegExp()
		 */
		public SignInOptions usernameRegExp(@Nullable final String regExp) {
			updateIsSet(USERNAME_REG_EXP);
			if (userRegExp == null || !userRegExp.equals(regExp)) {
				this.userRegExp = regExp;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the regular expressions for username input, set to these options.
		 *
		 * @return Username regular expression.
		 * @see #usernameRegExp(int)
		 * @see #usernameRegExp(String)
		 */
		@Nullable public String usernameRegExp() {
			return userRegExp;
		}

		/**
		 * Same as {@link #passwordHint(CharSequence)} but with resource id.
		 *
		 * @param resId Resource id of the desired hint text.
		 * @see R.attr#dialogHintPassword dialog:dialogHintPassword
		 * @see #passwordHint()
		 */
		public SignInOptions passwordHint(@StringRes final int resId) {
			return passwordHint(string(resId));
		}

		/**
		 * Sets the hint text for password input of a dialog associated with these options.
		 *
		 * @param hint The desired hint text.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogHintPassword dialog:dialogHintPassword
		 * @see #passwordHint()
		 */
		public SignInOptions passwordHint(@Nullable final CharSequence hint) {
			updateIsSet(PASSWORD_HINT);
			if (passHint == null || !passHint.equals(hint)) {
				this.passHint = hint;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the hint text for password input, set to these options.
		 *
		 * @return Password hint.
		 * @see #passwordHint(int)
		 * @see #passwordHint(CharSequence)
		 */
		@Nullable public CharSequence passwordHint() {
			return passHint;
		}

		/**
		 * Same as {@link #passwordRegExp(String)} but with resource id.
		 *
		 * @param resId Resource id of the desired regular expression.
		 * @see R.attr#dialogRegExpPassword dialog:dialogRegExpPassword
		 * @see #passwordRegExp()
		 */
		public SignInOptions passwordRegExp(@StringRes final int resId) {
			return passwordRegExp(string(resId));
		}

		/**
		 * Sets the regular expression used to check password input of a dialog associated with this
		 * options.
		 *
		 * @param regExp The desired regular expression for password input.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogRegExpPassword dialog:dialogRegExpPassword
		 * @see #passwordRegExp()
		 */
		public SignInOptions passwordRegExp(@NonNull final String regExp) {
			updateIsSet(PASSWORD_REG_EXP);
			if (passRegExp == null || !passRegExp.equals(regExp)) {
				this.passRegExp = regExp;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the regular expression for password input, set to these options.
		 *
		 * @return Password regular expression.
		 * @see #passwordRegExp(int)
		 * @see #passwordRegExp(String)
		 */
		@Nullable public String passwordRegExp() {
			return passRegExp;
		}

		/**
		 * Sets the initial credentials to be set to username and password edit texts of a dialog
		 * associated with these options.
		 *
		 * @param credentials The desired credentials.
		 * @return These options to allow methods chaining.
		 * @see #credentials()
		 */
		public SignInOptions credentials(@Nullable final Credentials credentials) {
			this.credentials = credentials;
			updateIsSet(CREDENTIALS);
			notifyChanged();
			return this;
		}

		/**
		 * Returns the initial credentials, set to these options.
		 *
		 * @return Initial credentials.
		 * @see #credentials(Credentials)
		 */
		@Nullable public Credentials credentials() {
			return credentials;
		}

		/**
		 * Sets a flag indicating whether to show soft keyboard for dialog dialog associated with
		 * these options or not.
		 *
		 * @param show {@code True} to show, {@code false} otherwise.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogShowSoftKeyboard dialog:dialogShowSoftKeyboard
		 * @see #shouldShowSoftKeyboard()
		 */
		public SignInOptions showSoftKeyboard(final boolean show) {
			updateIsSet(SHOW_SOFT_KEYBOARD);
			if (showSoftKeyboard != show) {
				this.showSoftKeyboard = show;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns a flag indicating whether to show soft keyboard for dialog or not.
		 * <p>
		 * Default value: <b>true</b>
		 *
		 * @return {@code True} to show, {@code false} otherwise.
		 * @see #showSoftKeyboard(boolean)
		 */
		public boolean shouldShowSoftKeyboard() {
			return showSoftKeyboard;
		}

		/**
		 * Specifies the configuration options for username input of a dialog associated with these
		 * options.
		 *
		 * @param inputConfig The desired username configuration options.
		 * @return These options to allow methods chaining.
		 * @see #usernameInputConfig()
		 */
		public SignInOptions usernameInputConfig(@NonNull final InputConfig inputConfig) {
			updateIsSet(USERNAME_INPUT_CONFIG);
			if (this.userInputConfig != inputConfig) {
				this.userInputConfig = inputConfig;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Sets a resource id of the style containing attributes for configuration options for username
		 * input of a dialog associated with these options.
		 *
		 * @param resId Resource id of the desired style.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogUsernameInputStyle dialog:dialogUsernameInputStyle
		 * @see #usernameInputConfig()
		 */
		final SignInOptions usernameInputStyle(@StyleRes final int resId) {
			updateIsSet(USERNAME_INPUT_CONFIG);
			if (userInputConfigStyle != resId) {
				this.userInputConfigStyle = resId;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the username input configuration options, set to these dialog options.
		 *
		 * @return Instance of InputConfig or {@code null} if no configuration has been specified.
		 * @see #usernameInputConfig(InputConfig)
		 * @see #usernameInputStyle(int)
		 */
		@Nullable public InputConfig usernameInputConfig() {
			return userInputConfig;
		}

		/**
		 * Specifies the configuration options for password input of a dialog associated with these
		 * options.
		 *
		 * @param inputConfig The desired password configuration options.
		 * @return These options to allow methods chaining.
		 * @see #passwordInputConfig()
		 */
		public SignInOptions passwordInputConfig(@NonNull final InputConfig inputConfig) {
			updateIsSet(PASSWORD_INPUT_CONFIG);
			if (this.passInputConfig != inputConfig) {
				this.passInputConfig = inputConfig;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Sets a resource id of the style containing attributes for configuration options for password
		 * input of a dialog associated with these options.
		 *
		 * @param resId Resource id of the desired style.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogPasswordInputStyle dialog:dialogPasswordInputStyle
		 * @see #passwordInputConfig()
		 */
		final SignInOptions passwordInputStyle(@StyleRes final int resId) {
			updateIsSet(PASSWORD_INPUT_CONFIG);
			if (passInputConfigStyle != resId) {
				this.passInputConfigStyle = resId;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the password input configuration options, set to these dialog options.
		 *
		 * @return Instance of InputConfig or {@code null} if no configuration has been specified.
		 * @see #passwordInputConfig(InputConfig)
		 * @see #passwordInputStyle(int)
		 */
		@Nullable public InputConfig passwordInputConfig() {
			return userInputConfig;
		}

		/**
		 * Ensures that edit configuration objects are initialized in case of use of styles from
		 * which should be these config objects created.
		 *
		 * @param context Context used to create edit config objects from the current styles (if any).
		 */
		final void ensureInputConfigs(@NonNull final Context context) {
			if (userInputConfigStyle != -1 && userInputConfig == null) {
				this.userInputConfig = InputConfig.fromStyle(context, userInputConfigStyle);
			}
			if (passInputConfigStyle != -1 && passInputConfig == null) {
				this.passInputConfig = InputConfig.fromStyle(context, passInputConfigStyle);
			}
		}

		/**
		 */
		@SuppressWarnings("ConstantConditions")
		@Override protected void onParseXmlAttribute(
				@NonNull final XmlResourceParser xmlParser,
				final int index,
				@AttrRes final int attr,
				@NonNull final Resources resources,
				@Nullable final Resources.Theme theme
		) {
			if (attr == R.attr.dialogHintUsername) {
				usernameHint(obtainXmlAttributeText(xmlParser, index, resources));
			} else if (attr == R.attr.dialogHintPassword) {
				passwordHint(obtainXmlAttributeText(xmlParser, index, resources));
			} else if (attr == R.attr.dialogRegExpUsername) {
				usernameRegExp(obtainXmlAttributeString(xmlParser, index, resources));
			} else if (attr == R.attr.dialogRegExpPassword) {
				passwordRegExp(obtainXmlAttributeString(xmlParser, index, resources));
			} else if (attr == R.attr.dialogUsernameInputStyle) {
				usernameInputStyle(xmlParser.getAttributeResourceValue(index, userInputConfigStyle));
			} else if (attr == R.attr.dialogPasswordInputStyle) {
				passwordInputStyle(xmlParser.getAttributeResourceValue(index, passInputConfigStyle));
			} else if (attr == R.attr.dialogShowSoftKeyboard) {
				showSoftKeyboard(xmlParser.getAttributeBooleanValue(index, showSoftKeyboard));
			} else {
				super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
			}
		}
	}

	/**
	 * A {@link BaseSavedState} implementation used to ensure that the state of {@link SignInDialog}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends BaseSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		int privateFlags;

		/**
		 */
		String userRegExp, passRegExp;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.privateFlags = source.readInt();
			this.userRegExp = source.readString();
			this.passRegExp = source.readString();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(privateFlags);
			dest.writeString(userRegExp);
			dest.writeString(passRegExp);
		}
	}
}