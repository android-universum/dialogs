/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.DialogsConfig;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.SignInDialog;
import universum.studios.android.dialog.view.InputConfig;
import universum.studios.android.dialog.view.SignInDialogView;
import universum.studios.android.ui.widget.EditLayout;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link SignInDialogView} implementation used by {@link SignInDialog SignInDialog} as content view.
 * <p>
 * This class represents a container for two EditTexts, one for username input, the another one for
 * password input. Both text fields can be configured via {@link #setUsernameInputConfig(InputConfig)}
 * and {@link #setPasswordInputConfig(InputConfig)}. Following methods allow to set hints and initial
 * values for credentials:
 * <ul>
 * <li>{@link #setUsernameHint(CharSequence)}</li>
 * <li>{@link #setUsernameHint(int)}</li>
 * <li>{@link #setPasswordHint(CharSequence)}</li>
 * <li>{@link #setPasswordHint(int)}</li>
 * <li>{@link #setUsernameInput(CharSequence)}</li>
 * <li>{@link #setPasswordInput(CharSequence)}</li>
 * </ul>
 * Current values from username and password text fields can be obtained via {@link #getUsernameEditableInput()}
 * and {@link #getPasswordEditableInput()}. To clear the current values within the text fields, invoke
 * {@link #clearInputs()} or independently {@link #clearUsernameInput()} and {@link #clearPasswordInput()}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Errors handling</h3>
 * Errors for the text fields can be specified via {@link #setUsernameError(CharSequence)} or
 * {@link #setUsernameError(int)} and {@link #setPasswordError(CharSequence)} or {@link #setPasswordError(int)}.
 * To clear the current errors, invoke {@link #clearErrors()} or independently {@link #clearUsernameError()}
 * and {@link #clearPasswordError()}.
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutSignInContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_sign_in} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SignInDialogContentView extends LinearLayout implements SignInDialogView {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SignInDialogContentView";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * EditLayout with input view for username input.
	 */
	private EditLayout mUsernameLayout;

	/**
	 * EditLayout with input view for password input.
	 */
	private EditLayout mPasswordLayout;

	/**
	 * Configuration options for the username input.
	 */
	private InputConfig mUsernameInputConfig;

	/**
	 * Configuration options for the password input.
	 */
	private InputConfig mPasswordInputConfig;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SignInDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public SignInDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SignInDialogContentView(Context, AttributeSet, int)} with {@code 0} as
	 * attribute for default style.
	 */
	public SignInDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, 0, 0);
	}

	/**
	 * Same as {@link #SignInDialogContentView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SignInDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of SignInDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SignInDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		int layoutResource = R.layout.dialog_layout_sign_in;
		if (theme.resolveAttribute(R.attr.dialogLayoutSignInContent, typedValue, true)) {
			layoutResource = typedValue.resourceId;
		}
		this.inflateHierarchy(context, layoutResource);
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mUsernameLayout = (EditLayout) findViewById(R.id.dialog_edit_layout_username);
			this.mPasswordLayout = (EditLayout) findViewById(R.id.dialog_edit_layout_password);
			onFinishInflate();
		}
	}

	/**
	 */
	@Override public void setEnabled(final boolean enabled) {
		super.setEnabled(enabled);
		for (int i = 0; i < getChildCount(); i++) {
			getChildAt(i).setEnabled(enabled);
		}
	}

	/**
	 */
	@Override public void setUsernameInputConfig(@NonNull final InputConfig inputConfig) {
		this.updateInputConfig(mUsernameLayout, mUsernameInputConfig = inputConfig);
	}

	/**
	 */
	@Override public void setPasswordInputConfig(@NonNull final InputConfig inputConfig) {
		this.updateInputConfig(mPasswordLayout, mPasswordInputConfig = inputConfig);
	}

	/**
	 * Updates configuration of the specified <var>editLayout</var> depends on the options specified
	 * by <var>config</var>.
	 *
	 * @param editLayout The edit layout of which configuration to update.
	 * @param config     The desired configuration options.
	 */
	private void updateInputConfig(final EditLayout editLayout, final InputConfig config) {
		if (editLayout != null) {
			editLayout.setNote(config.note);
			if (config.lengthConstraint >= 0) {
				editLayout.setLengthConstraint(config.lengthConstraint);
			}
			if (config.maxLength >= 0) {
				editLayout.setMaxLength(config.maxLength);
			}
			if (config.inputFeatures >= 0) {
				editLayout.requestInputFeatures(config.inputFeatures);
			}
			if (config.inputType > 0) {
				editLayout.setInputType(config.inputType);
			}
		}
	}

	/**
	 */
	@Override public void setUsernameHint(@StringRes final int resId) {
		setUsernameHint(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setUsernameHint(@NonNull final CharSequence hint) {
		if (mUsernameLayout != null) mUsernameLayout.setHint(hint);
	}

	/**
	 */
	@Override @NonNull public CharSequence getUsernameHint() {
		return mUsernameLayout != null ? mUsernameLayout.getHint() : "";
	}

	/**
	 */
	@Override public void setUsernameInput(@Nullable final CharSequence input) {
		if (mUsernameLayout != null) mUsernameLayout.setText(input);
	}

	/**
	 */
	@Override @NonNull public CharSequence getUsernameInput() {
		final Editable editable = getUsernameEditableInput();
		return editable != null ? editable.toString() : "";
	}

	/**
	 */
	@Override @Nullable public Editable getUsernameEditableInput() {
		return mUsernameLayout != null ? mUsernameLayout.getEditableText() : null;
	}

	/**
	 */
	@Override public void clearUsernameInput() {
		if (mUsernameLayout != null) mUsernameLayout.setText("");
	}

	/**
	 */
	@Override public void setUsernameError(@StringRes final int resId) {
		setUsernameError(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setUsernameError(@NonNull final CharSequence error) {
		if (mUsernameLayout != null) mUsernameLayout.setError(error);
	}

	/**
	 */
	@Override @NonNull public CharSequence getUsernameError() {
		return mUsernameLayout != null ? mUsernameLayout.getError() : "";
	}

	/**
	 */
	@Override public void clearUsernameError() {
		if (mUsernameLayout != null) mUsernameLayout.clearError();
	}

	/**
	 */
	@Override public void setPasswordHint(@StringRes final int resId) {
		setPasswordHint(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setPasswordHint(@NonNull final CharSequence hint) {
		if (mPasswordLayout != null) mPasswordLayout.setHint(hint);
	}

	/**
	 */
	@Override @NonNull public CharSequence getPasswordHint() {
		return mPasswordLayout != null ? mPasswordLayout.getHint() : "";
	}

	/**
	 */
	@Override public void setPasswordInput(@Nullable final CharSequence input) {
		if (mPasswordLayout != null) mPasswordLayout.setText(input);
	}

	/**
	 */
	@Override @NonNull public CharSequence getPasswordInput() {
		final Editable editable = getPasswordEditableInput();
		return editable != null ? editable.toString() : "";
	}

	/**
	 */
	@Override @Nullable public Editable getPasswordEditableInput() {
		return mPasswordLayout != null ? mPasswordLayout.getEditableText() : null;
	}

	/**
	 */
	@Override public void clearPasswordInput() {
		if (mPasswordLayout != null) mPasswordLayout.setText("");
	}

	/**
	 */
	@Override public void setPasswordError(@StringRes int resId) {
		setPasswordError(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setPasswordError(@NonNull final CharSequence error) {
		if (mPasswordLayout != null) mPasswordLayout.setError(error);
	}

	/**
	 */
	@Override @NonNull public CharSequence getPasswordError() {
		return mPasswordLayout != null ? mPasswordLayout.getError() : "";
	}

	/**
	 */
	@Override public void clearPasswordError() {
		if (mPasswordLayout != null) mPasswordLayout.clearError();
	}

	/**
	 */
	@Override public void clearInputs() {
		clearUsernameInput();
		clearPasswordInput();
	}

	/**
	 */
	@Override public void clearErrors() {
		clearUsernameError();
		clearPasswordError();
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.inputConfigUser = mUsernameInputConfig;
		savedState.inputConfigPass = mPasswordInputConfig;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		if (savedState.inputConfigUser != null) {
			setUsernameInputConfig(savedState.inputConfigUser);
		}
		if (savedState.inputConfigPass != null) {
			setPasswordInputConfig(savedState.inputConfigPass);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link SignInDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		InputConfig inputConfigUser, inputConfigPass;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.inputConfigUser = source.readParcelable(DialogsConfig.class.getClassLoader());
			this.inputConfigPass = source.readParcelable(DialogsConfig.class.getClassLoader());
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeParcelable(inputConfigUser, flags);
			dest.writeParcelable(inputConfigPass, flags);
		}
	}
}