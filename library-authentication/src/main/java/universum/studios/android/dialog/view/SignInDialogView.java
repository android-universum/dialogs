/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.text.Editable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.SignInDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link SignInDialog SignInDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.SignInDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface SignInDialogView extends DialogView {

	/**
	 * Updates the enabled state of this dialog view.
	 * <p>
	 * <b>Note</b> that while this view is <b>disabled</b>, the current inputs within username and
	 * password EditTexts cannot be updated by a user.
	 *
	 * @param enabled {@code True} to enable input, {@code false} otherwise.
	 * @see #isEnabled()
	 */
	void setEnabled(boolean enabled);

	/**
	 * Returns a boolean flag indicating whether this dialog view is enabled or not.
	 *
	 * @return {@code True} if it is enabled, {@code false} otherwise.
	 * @see #setEnabled(boolean)
	 */
	boolean isEnabled();

	/**
	 * Sets a configuration for the username input view of this dialog view.
	 *
	 * @param inputConfig The desired configuration.
	 */
	void setUsernameInputConfig(@NonNull InputConfig inputConfig);

	/**
	 * Same as {@link #setUsernameHint(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired hint text.
	 * @see #getUsernameHint()
	 * @see #setUsernameInput(CharSequence)
	 */
	void setUsernameHint(@StringRes int resId);

	/**
	 * Sets a hint text for the username input view of this dialog view.
	 *
	 * @param hint The desired hint text. May be {@code null} to clear the current one.
	 * @see #getUsernameHint()
	 * @see #setUsernameInput(CharSequence)
	 */
	void setUsernameHint(@NonNull CharSequence hint);

	/**
	 * Returns the username hint text specified for this dialog view.
	 *
	 * @return Hint for username input. May be empty if no hint has been specified yet.
	 * @see #setUsernameHint(int)
	 * @see #setUsernameHint(CharSequence)
	 */
	@NonNull CharSequence getUsernameHint();

	/**
	 * Sets an input text value for the username input view of this dialog view.
	 *
	 * @param input The desired username input. May be {@code null} to clear the current one.
	 * @see #getUsernameInput()
	 * @see #setUsernameHint(CharSequence)
	 */
	void setUsernameInput(@Nullable CharSequence input);

	/**
	 * Returns the current username input text of this dialog view.
	 *
	 * @return Input text. May be empty if no input has been specified yet.
	 * @see #setUsernameInput(CharSequence)
	 * @see #getUsernameEditableInput()
	 */
	@NonNull CharSequence getUsernameInput();

	/**
	 * Returns the current username editable input value of this dialog view.
	 *
	 * @return Editable input value or {@code null} if no input has been specified yet.
	 * @see #getUsernameInput()
	 * @see #clearUsernameInput()
	 */
	@Nullable Editable getUsernameEditableInput();

	/**
	 * Clears the current username editable input value of this dialog view.
	 *
	 * @see #getUsernameEditableInput()
	 */
	void clearUsernameInput();

	/**
	 * Same as {@link #setUsernameError(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired error text.
	 * @see #getUsernameError()
	 * @see #clearUsernameError()
	 */
	void setUsernameError(@StringRes int resId);

	/**
	 * Sets an error text for the username input view of this dialog view.
	 *
	 * @param error The desired error text.
	 * @see #getUsernameError()
	 * @see #clearUsernameError()
	 */
	void setUsernameError(@NonNull CharSequence error);

	/**
	 * Returns the current username error specified for this dialog view.
	 *
	 * @return Error text. May be empty if no error has been specified yet.
	 * @see #setUsernameError(int)
	 * @see #setUsernameError(CharSequence)
	 */
	@NonNull CharSequence getUsernameError();

	/**
	 * Clears the current username error specified for this dialog view (if any).
	 *
	 * @see #setUsernameError(CharSequence)
	 */
	void clearUsernameError();

	/**
	 * Sets a configuration for the password input view of this dialog view.
	 *
	 * @param inputConfig The desired configuration.
	 */
	void setPasswordInputConfig(@NonNull InputConfig inputConfig);

	/**
	 * Same as {@link #setPasswordHint(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired hint text.
	 * @see #getPasswordHint()
	 * @see #setPasswordInput(CharSequence)
	 */
	void setPasswordHint(@StringRes int resId);

	/**
	 * Sets a hint text for the password input view of this dialog view.
	 *
	 * @param hint The desired hint text. May be {@code null} to clear the current one.
	 * @see #getPasswordHint()
	 * @see #setPasswordInput(CharSequence)
	 */
	void setPasswordHint(@NonNull CharSequence hint);

	/**
	 * Returns the password hint text specified for this dialog view.
	 *
	 * @return Hint for password input. May be empty if no hint has been specified yet.
	 * @see #setPasswordHint(int)
	 * @see #setPasswordHint(CharSequence)
	 */
	@NonNull CharSequence getPasswordHint();

	/**
	 * Sets an input text value for the password input view of this dialog view.
	 *
	 * @param input The desired password input. May be {@code null} to clear the current one.
	 * @see #getPasswordInput()
	 * @see #setPasswordHint(CharSequence)
	 */
	void setPasswordInput(@Nullable CharSequence input);

	/**
	 * Returns the current password input text of this dialog view.
	 *
	 * @return Input text. May be empty if no input has been specified yet.
	 * @see #setPasswordInput(CharSequence)
	 * @see #getPasswordEditableInput()
	 */
	@NonNull CharSequence getPasswordInput();

	/**
	 * Returns the current password editable input value of this dialog view.
	 *
	 * @return Editable input value or {@code null} if no input has been specified yet.
	 * @see #getPasswordInput()
	 * @see #clearPasswordInput()
	 */
	@Nullable Editable getPasswordEditableInput();

	/**
	 * Clears the current password editable input value of this dialog view.
	 *
	 * @see #getPasswordEditableInput()
	 */
	void clearPasswordInput();

	/**
	 * Same as {@link #setPasswordError(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired error text.
	 * @see #getPasswordError()
	 * @see #clearPasswordError()
	 */
	void setPasswordError(@StringRes int resId);

	/**
	 * Sets an error text for the password input view of this dialog view.
	 *
	 * @param error The desired error text.
	 * @see #getPasswordError()
	 * @see #clearPasswordError()
	 */
	void setPasswordError(@NonNull CharSequence error);

	/**
	 * Returns the current password error specified for this dialog view.
	 *
	 * @return Error text. May be empty if no error has been specified yet.
	 * @see #setPasswordError(int)
	 * @see #setPasswordError(CharSequence)
	 */
	@NonNull CharSequence getPasswordError();

	/**
	 * Clears the current password error specified for this dialog view (if any).
	 *
	 * @see #setPasswordError(CharSequence)
	 */
	void clearPasswordError();

	/**
	 * Clears the current username and password editable input values this dialog view.
	 *
	 * @see #clearUsernameInput()
	 * @see #clearPasswordInput()
	 */
	void clearInputs();

	/**
	 * Clears the current username and password errors specified for this dialog view (if any).
	 *
	 * @see #clearUsernameError()
	 * @see #clearPasswordError()
	 */
	void clearErrors();

	/**
	 * Clears the focus of this dialog view.
	 */
	void clearFocus();
}