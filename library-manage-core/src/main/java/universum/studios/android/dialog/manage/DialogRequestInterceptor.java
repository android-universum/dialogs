/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * Interface that may be used to intercept a specific {@link DialogRequest} when it is being executed
 * via its associated {@link DialogController}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see DialogController#setRequestInterceptor(DialogRequestInterceptor)
 */
public interface DialogRequestInterceptor {

	/**
	 * Called to allow this request interceptor to intercept execution of the given dialog <var>request</var>.
	 * <p>
	 * Interceptor may also just change configuration of the request and return {@code null} to indicate
	 * that the associated dialog controller should handle the execution.
	 *
	 * @param request The request to be executed.
	 * @return Dialog fragment associated with the request as result of the handled execution,
	 * {@code null} to let the dialog controller handle the execution.
	 */
	@Nullable DialogFragment interceptDialogRequest(@NonNull DialogRequest request);
}