/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.DialogOptions;

/**
 * DialogFactory specifies an interface for factories that may be attached to {@link DialogController}
 * in order to provide dialog fragment instances with theirs corresponding TAGs for that controller.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface DialogFactory {

	/**
	 * Checks whether this dialog factory provides an instance of dialog fragment for the specified
	 * <var>dialogId</var>.
	 *
	 * @param dialogId Id of the desired dialog to check.
	 * @return {@code True} if dialog is provided, so {@link #createDialog(int, DialogOptions)} may
	 * be called to create an instance of such dialog fragment, {@code false} otherwise.
	 */
	boolean isDialogProvided(int dialogId);

	/**
	 * Creates a new instance of the dialog fragment associated with the specified <var>dialogId</var>.
	 *
	 * @param dialogId Id of the desired dialog fragment to create a new instance of.
	 * @param options  Options that should be attached to the requested dialog. May be {@code null}.
	 * @return Instance of dialog fragment associated with the <var>dialogId</var> or {@code null}
	 * if this dialog factory does not provide dialog fragment for the requested id.
	 * @see #createDialogTag(int)
	 * @see #isDialogProvided(int)
	 */
	@Nullable DialogFragment createDialog(int dialogId, @Nullable DialogOptions options);

	/**
	 * Creates a tag for the dialog fragment associated with the specified <var>dialogId</var>.
	 *
	 * @param dialogId Id of the desired dialog fragment for which to create its TAG.
	 * @return Tag for dialog fragment associated with the <var>dialogId</var> or {@code null} if this
	 * dialog factory does not provide dialog fragment for the requested id.
	 * @see #isDialogProvided(int)
	 */
	@Nullable String createDialogTag(int dialogId);
}