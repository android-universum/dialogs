/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import universum.studios.android.dialog.DialogOptions;

/**
 * A request that may be used to show|dismiss a desired {@link DialogFragment}. New instance of request
 * may be created via {@link DialogController#newRequest(DialogFragment)} or via
 * {@link DialogController#newRequest(int)} for dialog fragments provided by {@link DialogFactory}.
 * When request is created it may be configured via methods listed below and then executed via {@link #execute()}.
 *
 * <h3>Configuration</h3>
 * <ul>
 * <li>{@link #dialogId(int)}</li>
 * <li>{@link #outgoingDialogId(int)}</li>
 * <li>{@link #options(DialogOptions)}</li>
 * <li>{@link #intent(int)}</li>
 * <li>{@link #tag(String)}</li>
 * <li>{@link #replaceSame(boolean)}</li>
 * <li>{@link #allowStateLoss(boolean)}</li>
 * <li>{@link #immediate(boolean)}</li>
 * </ul>
 * <p>
 * <b>Note, that each dialog request may be executed only once.</b>
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see DialogController
 * @see DialogRequestInterceptor
 */
public final class DialogRequest {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogRequest";

	/**
	 * Constant used to determine that no dialog fragment id has been specified.
	 */
	public static final int NO_ID = -1;

	/**
	 * Intent type used to indicate that the associated dialog fragment should be <b>shown</b>.
	 *
	 * @see DialogFragment#show(FragmentTransaction, String)
	 */
	public static final int SHOW = 0x00;

	/**
	 * Intent type used to indicate that the associated dialog fragment should be <b>dismissed</b>.
	 *
	 * @see DialogFragment#dismiss()
	 */
	public static final int DISMISS = 0x01;

	/**
	 * Defines an annotation for determining available intent types for {@link #intent(int)} method.
	 *
	 * <h3>Available types:</h3>
	 * <ul>
	 * <li>{@link #SHOW}</li>
	 * <li>{@link #DISMISS}</li>
	 * </ul>
	 *
	 * @see #intent(int)
	 */
	@IntDef({SHOW, DISMISS})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Intent {}

	/**
	 * Flag indicating that a same fragment (currently displayed) can be replaced by the associated fragment.
	 *
	 * @see FragmentTransaction#replace(int, Fragment, String)
	 */
	static final int REPLACE_SAME = 0x00000001;

	/**
	 * Flag indicating that the associated {@link FragmentTransaction} should be committed allowing
	 * state loss.
	 *
	 * @see FragmentTransaction#commitAllowingStateLoss()
	 */
	static final int ALLOW_STATE_LOSS = 0x00000001 << 1;

	/**
	 * Flag indicating that the associated {@link FragmentTransaction} should be executed immediately.
	 *
	 * @see FragmentManager#executePendingTransactions()
	 */
	static final int IMMEDIATE = 0x00000001 << 2;

	/**
	 * Flag indicating that execution of dialog request should be performed regardless of the current
	 * {@link Lifecycle}'s state.
	 */
	static final int IGNORE_LIFECYCLE_STATE = 0x00000001 << 4;

	/**
	 * Defines an annotation for determining available boolean flags for DialogRequest.
	 */
	@IntDef(flag = true, value = {
			REPLACE_SAME,
			ALLOW_STATE_LOSS,
			IMMEDIATE,
			IGNORE_LIFECYCLE_STATE
	})
	@Retention(RetentionPolicy.SOURCE)
	private @interface Flag {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Controller that has been used to create this request and also is responsible for execution
	 * of this request.
	 *
	 * @see #execute()
	 */
	private final DialogController mController;

	/**
	 * Dialog fragment instance associated with this request.
	 */
	DialogFragment mDialogFragment;

	/**
	 * Id of the associated dialog fragment.
	 */
	int mDialogId = NO_ID;

	/**
	 * Id of the outgoing dialog fragment that will be replaced by the associated dialog fragment.
	 */
	private int mOutgoingDialogId = NO_ID;

	/**
	 * Dialog options that should be attached to the associated dialog fragment.
	 */
	DialogOptions mOptions;

	/**
	 * Intent type determining what operation to perform with the associated dialog fragment.
	 */
	@Intent int mIntent = SHOW;

	/**
	 * Tag for the associated dialog fragment.
	 */
	String mTag;

	/**
	 * Flags specified for this request.
	 *
	 * @see Flag @Flag
	 */
	@Flag private int mFlags;

	/**
	 * Boolean flag indicating whether this request has been already executed via {@link #execute()}
	 * or not.
	 */
	private boolean mExecuted;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DialogRequest for the given <var>controller</var>.
	 *
	 * @param controller Dialog controller that creates the new request and will be also responsible
	 *                   for its execution.
	 */
	DialogRequest(final DialogController controller) {
		this(controller, null);
	}

	/**
	 * Creates a new instance of DialogRequest for the given <var>controller</var> and <var>dialogFragment</var>.
	 *
	 * @param controller     Dialog controller that creates the new request and will be also responsible
	 *                       for its execution.
	 * @param dialogFragment The dialog fragment to associate with the new request.
	 */
	DialogRequest(final DialogController controller, final DialogFragment dialogFragment) {
		this.mController = controller;
		this.mDialogFragment = dialogFragment;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@SuppressWarnings("StringBufferReplaceableByString")
	@Override public String toString() {
		final StringBuilder builder = new StringBuilder(128);
		builder.append("DialogRequest{dialogId: ");
		builder.append(mDialogId);
		builder.append(", outgoingDialogId: ");
		builder.append(mOutgoingDialogId);
		builder.append(", options: ");
		builder.append(mOptions);
		builder.append(", intent: ");
		builder.append(mIntent);
		builder.append(", tag: ");
		builder.append(mTag);
		builder.append(", replaceSame: ");
		builder.append(hasFlag(REPLACE_SAME));
		builder.append(", ignoreLifecycleState: ");
		builder.append(hasFlag(IGNORE_LIFECYCLE_STATE));
		builder.append(", allowStateLoss: ");
		builder.append(hasFlag(ALLOW_STATE_LOSS));
		builder.append(", immediate: ");
		builder.append(hasFlag(IMMEDIATE));
		builder.append(", executed: ");
		builder.append(mExecuted);
		return builder.append("}").toString();
	}

	/**
	 * Returns the fragment instance associated with this request.
	 *
	 * @return This request's fragment.
	 * @see DialogController#newRequest(DialogFragment)
	 */
	@Nullable public DialogFragment dialogFragment() {
		return mDialogFragment;
	}

	/**
	 * Sets an id of the dialog fragment associated with this request.
	 * <p>
	 * This id along with {@link #outgoingDialogId()} may be used to determine exact change between
	 * two dialog fragments and configure this request accordingly when using {@link DialogRequestInterceptor}.
	 *
	 * @param dialogId The desired dialog fragment id.
	 * @return This request to allow methods chaining.
	 * @see #dialogId()
	 * @see #outgoingDialogId(int)
	 */
	public DialogRequest dialogId(final int dialogId) {
		this.mDialogId = dialogId;
		return this;
	}

	/**
	 * Returns the id of the associated fragment.
	 * <p>
	 * Default value: <b>{@link #NO_ID}</b>
	 *
	 * @return Fragment id or {@link #NO_ID} if no id has been specified.
	 * @see #dialogId(int)
	 */
	public int dialogId() {
		return mDialogId;
	}

	/**
	 * Sets an id of the outgoing dialog fragment that is to be replaced by the dialog fragment
	 * associated with this request.
	 * <p>
	 * This id along with {@link #dialogId()} may be used to determine exact change between two
	 * fragments and configure this request accordingly when using {@link DialogRequestInterceptor}.
	 *
	 * @param dialogId The desired dialog fragment id.
	 * @return This request to allow methods chaining.
	 * @see #outgoingDialogId()
	 * @see #dialogId(int)
	 */
	public DialogRequest outgoingDialogId(final int dialogId) {
		this.mOutgoingDialogId = dialogId;
		return this;
	}

	/**
	 * Returns the id of the outgoing dialog fragment.
	 * <p>
	 * Default value: <b>{@link #NO_ID}</b>
	 *
	 * @return Dialog fragment id or {@link #NO_ID} if no id has been specified.
	 * @see #outgoingDialogId(int)
	 */
	public int outgoingDialogId() {
		return mOutgoingDialogId;
	}

	/**
	 * Sets an options to be attached to the dialog fragment associated with this request.
	 *
	 * @param options The desired dialog options. May be {@code null} to not associated any options
	 *                with the dialog fragment.
	 * @return This request to allow methods chaining.
	 * @see #options()
	 */
	public DialogRequest options(@Nullable final DialogOptions options) {
		this.mOptions = options;
		return this;
	}

	/**
	 * Returns the dialog options to be attached to the associated dialog fragment.
	 *
	 * @return Dialog options specified for this request or {@code null} if no options have been specified.
	 * @see #options(DialogOptions)
	 */
	@Nullable public DialogOptions options() {
		return mOptions;
	}

	/**
	 * Sets an intent type determining what operation to perform with the associated dialog fragment.
	 *
	 * @param type The desired intent type. One of types defined by {@link Intent @intent} annotation.
	 * @return This request to allow methods chaining.
	 * @see #intent()
	 */
	public DialogRequest intent(@Intent final int type) {
		this.mIntent = type;
		return this;
	}

	/**
	 * Returns the intent type of this request.
	 * <p>
	 * Default value: <b>{@link #SHOW}</b>
	 *
	 * @return One of intent types defined by {@link Intent @intent} annotation.
	 * @see #intent(int)
	 */
	@Intent public int intent() {
		return mIntent;
	}

	/**
	 * Sets a tag for the associated dialog fragment.
	 *
	 * @param dialogTag The desired dialog fragment tag. May be {@code null}.
	 * @return This request to allow methods chaining.
	 * @see #tag()
	 * @see DialogFragment#getTag()
	 */
	public final DialogRequest tag(@Nullable final String dialogTag) {
		this.mTag = dialogTag;
		return this;
	}

	/**
	 * Returns the tag by which should be the associated dialog fragment identified.
	 * <p>
	 * Default value: <b>{@code null}</b>
	 *
	 * @return Tag for the associated dialog fragment to be shown using these options.
	 * @see #tag(String)
	 */
	@Nullable public final String tag() {
		return mTag;
	}

	/**
	 * Sets a boolean flag indicating whether the already displayed dialog fragment with the same TAG
	 * as that specified for this request may be replaced by the associated dialog fragment or not.
	 *
	 * @param replace {@code True} to replace an existing dialog fragment with the same TAG as specified
	 *                via {@link #tag(String)} with the associated one, {@code false} otherwise.
	 * @return This request to allow methods chaining.
	 * @see #replaceSame()
	 */
	public final DialogRequest replaceSame(final boolean replace) {
		return setHasFlag(REPLACE_SAME, replace);
	}

	/**
	 * Returns boolean flag indicating whether already displayed dialog fragment with the same TAG
	 * may be replaced by a new one.
	 * <p>
	 * Default value: <b>{@code false}</b>
	 *
	 * @return {@code True} if already displayed dialog fragment with the same TAG may be replaced,
	 * {@code false} otherwise.
	 * @see #replaceSame(boolean)
	 */
	public final boolean replaceSame() {
		return hasFlag(REPLACE_SAME);
	}

	/**
	 * Sets a boolean flag indicating whether {@link DialogController} should ignore {@link Lifecycle}'s
	 * current state when executing this request or not.
	 * <p>
	 * Default value: <b>{@code false}</b>
	 *
	 * @param ignore {@code True} to ignore lifecycle's current state when executing this request,
	 *               {@code false} otherwise.
	 * @return This request to allow methods chaining.
	 *
	 * @see #ignoreLifecycleState()
	 * @see #allowStateLoss(boolean)
	 */
	public DialogRequest ignoreLifecycleState(final boolean ignore) {
		return setHasFlag(IGNORE_LIFECYCLE_STATE, ignore);
	}

	/**
	 * Returns boolean flag indicating whether current {@link Lifecycle}'s state should be ignored
	 * when executing this request.
	 *
	 * @return {@code True} if lifecycle's current state should be ignored, {@code false} otherwise.
	 *
	 * @see #ignoreLifecycleState(boolean)
	 */
	public boolean ignoreLifecycleState() {
		return hasFlag(IGNORE_LIFECYCLE_STATE);
	}

	/**
	 * Sets a boolean flag indicating whether the intended operation for the associated dialog fragment
	 * may be executed allowing state loss or not.
	 *
	 * @param allow {@code True} to allow state loss when committing the associated fragment transaction,
	 *              {@code false} otherwise.
	 * @return This request to allow methods chaining.
	 * @see DialogFragment#dismissAllowingStateLoss()
	 * @see FragmentTransaction#commitAllowingStateLoss()
	 * @see #allowStateLoss()
	 */
	public final DialogRequest allowStateLoss(final boolean allow) {
		return setHasFlag(ALLOW_STATE_LOSS, allow);
	}

	/**
	 * Returns boolean flag indicating whether to commit fragment transaction allowing state loss.
	 * <p>
	 * Default value: <b>{@code false}</b>
	 *
	 * @return {@code True} if transaction may be committed allowing state loss, {@code false} otherwise.
	 * @see #allowStateLoss(boolean)
	 */
	public final boolean allowStateLoss() {
		return hasFlag(ALLOW_STATE_LOSS);
	}

	/**
	 * Sets a boolean flag indicating whether the intended operation for the associated dialog fragment
	 * should be executed immediately or not.
	 *
	 * @param immediate {@code True} to execute immediately (synchronously), {@code false} otherwise
	 *                  (asynchronously).
	 * @return This request to allow methods chaining.
	 * @see FragmentManager#executePendingTransactions()
	 * @see #immediate()
	 */
	public DialogRequest immediate(final boolean immediate) {
		return setHasFlag(IMMEDIATE, immediate);
	}

	/**
	 * Returns boolean indicating whether to execute fragment transaction immediately.
	 * <p>
	 * Default value: <b>{@code false}</b>
	 *
	 * @return {@code True} if fragment transaction should be executed immediately (synchronously),
	 * {@code false} otherwise (asynchronously).
	 * @see #immediate(boolean)
	 */
	public boolean immediate() {
		return hasFlag(IMMEDIATE);
	}

	/**
	 * Sets whether this request has the specified <var>flag</var> registered or not.
	 *
	 * @param flag One of flags defined by {@link Flag @Flag} annotation.
	 * @param has  {@code True} to determine that this request has this flag, {@code false} that it
	 *             has not.
	 * @return This request to allow methods chaining.
	 */
	private DialogRequest setHasFlag(@Flag final int flag, final boolean has) {
		if (has) this.mFlags |= flag;
		else this.mFlags &= ~flag;
		return this;
	}

	/**
	 * Checks whether this request has the specified <var>flag</var> registered or not.
	 *
	 * @param flag One of flags defined by {@link Flag @Flag} annotation.
	 * @return {@code True} if flag is registered, {@code false} otherwise.
	 */
	boolean hasFlag(@Flag final int flag) {
		return (mFlags & flag) != 0;
	}

	/**
	 * Executes this request via the associated {@link DialogController} that was used to create
	 * this request instance.
	 * <p>
	 * <b>Note</b>, that each request may be executed only once and any subsequent calls to this
	 * method will throw an exception.
	 *
	 * @return The dialog fragment that has been associated with this request either during its
	 * initialization or as result of execution process. May be {@code null} if the execution has failed.
	 * @throws IllegalStateException If this request has been already executed.
	 */
	@Nullable public DialogFragment execute() {
		this.assertNotExecuted();
		final DialogFragment dialogFragment = mController.executeRequest(this);
		this.mExecuted = true;
		return dialogFragment;
	}

	/**
	 * Asserts that this request has not been executed yet. If it has been executed, an exception is
	 * thrown.
	 */
	private void assertNotExecuted() {
		if (mExecuted) throw new IllegalStateException("Already executed!");
	}

	/**
	 * Returns boolean flag indicating whether this request has been executed.
	 *
	 * @return {@code True} if {@link #execute()} has been called for this request, {@code false}
	 * otherwise.
	 */
	public boolean executed() {
		return mExecuted;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}