/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import universum.studios.android.dialog.ContextDialog;
import universum.studios.android.dialog.DialogsConfig;

/**
 * DialogController class is designed primarily to simplify {@link DialogFragment DialogFragments}
 * management within an Android application.
 * <p>
 * Whether it is desired to show|replace|dismiss a specific {@link DialogFragment}, this may be
 * requested via {@link DialogRequest}. A new request may be created via {@link #newRequest(DialogFragment)}.
 * Each fragment request is associated with the controller trough which it has been created and that
 * controller is responsible for execution of dialog request when {@link DialogRequest#execute()}
 * is called.
 *
 * <h3>Factory</h3>
 * The best advantage of the DialogController and globally of this library may be accomplished by
 * using of {@link DialogFactory} attached to the desired dialog controller. Basically in an
 * Android application you will use directly instances of the DialogController to show|dismiss...
 * dialog fragments and for each of that application screens (Activities) a single DialogFactory may
 * be defined which will provide dialog fragment instances for that activity screen. For example, one
 * dialog factory for fragment with sign in logic of authentication activity and another one for fragment
 * with forgot password logic.
 * <p>
 * The desired factory for DialogController may be specified via {@link #setFactory(DialogFactory)}.
 * Dialog requests for dialog fragments provided by the attached factory may be than created via
 * {@link #newRequest(int)}. <b>Note, that it is required that factory is attached to
 * the controller before call to this method, otherwise an exception will be thrown.</b>
 * <p>
 * Dialog fragments that are provided by factory attached to the dialog controller may be found via
 * {@link #findDialogFragmentByFactoryId(int)} (when already displayed) by theirs corresponding id
 * defined in the related factory.
 *
 * <h3>Callbacks</h3>
 * If you need to handle restored dialog instances after orientation change, you can do so by
 * attaching restore listener via {@link #registerOnRestoreListener(OnRestoreListener)} and invoking
 * one of {@link #checkRestoredDialogs(int[])} or {@link #checkRestoredDialogs(String[])} from
 * {@link Activity#onCreate(android.os.Bundle) Activity.onCreate(android.os.Bundle)} or from
 * {@link Fragment#onActivityCreated(android.os.Bundle)} to check whether some dialogs has been
 * restored or not.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogController {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "DialogController";

	/**
	 * Default TAG used for dialog fragments.
	 */
	public static final String DIALOG_TAG = DialogsConfig.class.getPackage().getName() + ".TAG.Dialog";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that may be used to receive callback about executed {@link DialogRequest}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 *
	 * @see #registerOnRequestListener(OnRequestListener)
	 */
	public interface OnRequestListener {

		/**
		 * Invoked whenever the specified <var>request</var> has been executed.
		 *
		 * @param request The executed dialog request.
		 * @see DialogRequest#execute()
		 */
		void onRequestExecuted(@NonNull DialogRequest request);
	}

	/**
	 * Listener that may be used to receive callback about restored dialog.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface OnRestoreListener {

		/**
		 * Invoked whenever {@link DialogController#checkRestoredDialogs(String[])} is called for
		 * the associated controller and there some dialogs already displayed.
		 * <p>
		 * This callback will be invoked for each of the displayed dialogs.
		 *
		 * @param dialog Instance of the restored dialog.
		 */
		void onDialogRestored(@NonNull DialogFragment dialog);
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Fragment manager used to perform dialog fragments related operations.
	 */
	private final FragmentManager mManager;

	/**
	 * Lifecycle of the context for which is this controller created. If attached, its current state
	 * is checked with {@link #lifecycleRequiredState} whenever a {@link DialogRequest} is requested
	 * to be executed via {@link #executeRequest(DialogRequest)}. May be null.
	 */
	@VisibleForTesting Lifecycle lifecycle;

	/**
	 * State of the attached lifecycle which is required in order to be any {@link DialogRequest}
	 * passed to {@link #executeRequest(DialogRequest)} executed, unless specific dialog request
	 * has specified {@link DialogRequest#IGNORE_LIFECYCLE_STATE} flag.
	 */
	private Lifecycle.State lifecycleRequiredState = Lifecycle.State.STARTED;

	/**
	 * Factory that provides dialog fragment instances for this controller.
	 */
	private DialogFactory mFactory;

	/**
	 * Interceptor that may be used to intercept an execution of a particular {@link DialogRequest}
	 * when its execution has been requested via {@link #executeRequest(DialogRequest)}.
	 */
	private DialogRequestInterceptor mRequestInterceptor;

	/**
	 * List of listener callbacks that may be notified about executed dialog requests.
	 */
	private List<OnRequestListener> mRequestListeners;

	/**
	 * List of listener callbacks that may be notified about restored dialogs.
	 */
	private List<OnRestoreListener> mRestoreListeners;

	/**
	 * Tag of the parent fragment in case, when this manager is used in a fragment context.
	 */
	private String mParentFragmentTag;

	/**
	 * Id of the parent fragment in case, when this manager is used in a fragment context.
	 */
	private int mParentFragmentId;

	/**
	 * Boolean flag indicating whether this controller has been destroyed or not.
	 */
	private boolean mDestroyed;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DialogController for the given <var>parentActivity</var>.
	 * <p>
	 * Passed activity will be used to obtain an instance of {@link FragmentManager} for the new
	 * controller.
	 * <p>
	 * This constructor attaches the given activity to the new controller as one of interfaces
	 * listed below if the activity implements listed interfaces respectively:
	 * <ul>
	 * <li>{@link DialogRequestInterceptor} -&gt; {@link #setRequestInterceptor(DialogRequestInterceptor)}</li>
	 * <li>{@link OnRequestListener} -&gt; {@link #registerOnRequestListener(OnRequestListener)}</li>
	 * <li>{@link OnRestoreListener} -&gt; {@link #registerOnRestoreListener(OnRestoreListener)}</li>
	 * </ul>
	 *
	 * @param parentActivity The activity that wants to use the new fragment controller.
	 * @see #DialogController(Fragment)
	 */
	private DialogController(@NonNull final FragmentActivity parentActivity) {
		this(parentActivity.getSupportFragmentManager());
		setLifecycle(parentActivity.getLifecycle());
		if (parentActivity instanceof DialogRequestInterceptor) {
			setRequestInterceptor((DialogRequestInterceptor) parentActivity);
		}
		if (parentActivity instanceof OnRequestListener) {
			registerOnRequestListener((OnRequestListener) parentActivity);
		}
		if (parentActivity instanceof OnRestoreListener) {
			registerOnRestoreListener((OnRestoreListener) parentActivity);
		}
	}

	/**
	 * Creates a new instance of DialogController for the given <var>parentFragment</var>.
	 * <p>
	 * Passed fragment will be used to obtain an instance of {@link FragmentManager} for the new
	 * controller.
	 * <p>
	 * This constructor attaches the given fragment to the new controller as one of interfaces
	 * listed below if the fragment implements listed interfaces respectively:
	 * <ul>
	 * <li>{@link DialogRequestInterceptor} -&gt; {@link #setRequestInterceptor(DialogRequestInterceptor)}</li>
	 * <li>{@link OnRequestListener} -&gt; {@link #registerOnRequestListener(OnRequestListener)}</li>
	 * <li>{@link OnRestoreListener} -&gt; {@link #registerOnRestoreListener(OnRestoreListener)}</li>
	 * </ul>
	 *
	 * @param parentFragment The fragment that wants to use the new fragment controller.
	 * @see #DialogController(FragmentActivity)
	 */
	private DialogController(@NonNull final Fragment parentFragment) {
		this(parentFragment.getParentFragmentManager());
		setLifecycle(parentFragment.getLifecycle());
		if (parentFragment instanceof DialogRequestInterceptor) {
			setRequestInterceptor((DialogRequestInterceptor) parentFragment);
		}
		if (parentFragment instanceof OnRequestListener) {
			registerOnRequestListener((OnRequestListener) parentFragment);
		}
		if (parentFragment instanceof OnRestoreListener) {
			registerOnRestoreListener((OnRestoreListener) parentFragment);
		}
		this.mParentFragmentTag = parentFragment.getTag();
		this.mParentFragmentId = parentFragment.getId();
	}

	/**
	 * Creates a new instance of DialogController with the given <var>fragmentManager</var>.
	 *
	 * @param fragmentManager Fragment manager that will be used to perform dialog fragments related
	 *                        operations.
	 * @see #DialogController(FragmentActivity)
	 * @see #DialogController(Fragment)
	 */
	public DialogController(@NonNull final FragmentManager fragmentManager) {
		this.mManager = fragmentManager;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of DialogController for the given <var>activity</var>.
	 * <p>
	 * Passed activity will be used to obtain its {@link Lifecycle} along with {@link FragmentManager}
	 * for the new controller.
	 * <p>
	 * New controller will also have the activity attached as one of listeners/interceptors listed
	 * below if that activity implements these interfaces respectively:
	 * <ul>
	 * <li>{@link DialogRequestInterceptor} -&gt; {@link #setRequestInterceptor(DialogRequestInterceptor)}</li>
	 * <li>{@link OnRequestListener} -&gt; {@link #registerOnRequestListener(OnRequestListener)}</li>
	 * <li>{@link OnRestoreListener} -&gt; {@link #registerOnRestoreListener(OnRestoreListener)}</li>
	 * </ul>
	 * <p>
	 * <b>Do not forget to destroy the new controller via {@link #destroy()} when the activity is
	 * also destroyed.</b>
	 *
	 * @param activity The activity in which context will be the new dialog controller used.
	 *
	 * @see #DialogController(FragmentManager)
	 * @see #setLifecycle(Lifecycle)
	 * @see #setLifecycleRequiredState(Lifecycle.State)
	 */
	public static DialogController create(@NonNull final FragmentActivity activity) {
		return new DialogController(activity);
	}

	/**
	 * Creates a new instance of DialogController for the given <var>fragment</var>.
	 * <p>
	 * Passed fragment will be used to obtain its {@link Lifecycle} along with {@link FragmentManager}
	 * for the new controller.
	 * <p>
	 * New controller will also have the fragment attached as one of listeners/interceptors listed
	 * below if that fragment implements these interfaces respectively:
	 * <ul>
	 * <li>{@link DialogRequestInterceptor} -&gt; {@link #setRequestInterceptor(DialogRequestInterceptor)}</li>
	 * <li>{@link OnRequestListener} -&gt; {@link #registerOnRequestListener(OnRequestListener)}</li>
	 * <li>{@link OnRestoreListener} -&gt; {@link #registerOnRestoreListener(OnRestoreListener)}</li>
	 * </ul>
	 * <p>
	 * <b>Do not forget to destroy the new controller via {@link #destroy()} when the fragment is
	 * also destroyed.</b>
	 *
	 * @param fragment The fragment in which context will be the new dialog controller used.
	 *
	 * @see #DialogController(FragmentManager)
	 * @see #setLifecycle(Lifecycle)
	 * @see #setLifecycleRequiredState(Lifecycle.State)
	 */
	public static DialogController create(@NonNull final Fragment fragment) {
		return new DialogController(fragment);
	}

	/**
	 * Returns the fragment manager specified for this controller during its initialization.
	 *
	 * @return FragmentManager instance.
	 * @see #DialogController(FragmentManager)
	 */
	@NonNull public final FragmentManager getFragmentManager() {
		return mManager;
	}

	/**
	 * Sets a Lifecycle of the parent context in which is this controller created. This lifecycle is
	 * used by this controller to ensure safe execution of {@link DialogRequest DialogRequests}
	 * according to required lifecycle state specified via {@link #setLifecycleRequiredState(Lifecycle.State)}.
	 * <p>
	 * If no lifecycle is attached, dialog requests are executed without current lifecycle's state check.
	 *
	 * @param lifecycle The lifecycle of the parent context (activity or fragment). May be {@code null}
	 *
	 * @see #setLifecycleRequiredState(Lifecycle.State)
	 */
	public void setLifecycle(@Nullable final Lifecycle lifecycle) {
		this.lifecycle = lifecycle;
	}

	/**
	 * Sets a state of Lifecycle which is required for execution of {@link DialogRequest DialogRequests}
	 * performed using this controller. The specified state will be checked for all dialog requests
	 * if this controller has {@link Lifecycle} attached via {@link #setLifecycle(Lifecycle)}. If the
	 * current state of the attached lifecycle is not <b>at least</b> the required one, requests
	 * requested for execution at that particular time will be ignored.
	 * <p>
	 * This state check may be ignored for a single request via {@link DialogRequest#ignoreLifecycleState(boolean)}.
	 * <p>
	 * Default value: <b>{@link Lifecycle.State#STARTED}</b>
	 *
	 * @param requiredState The desired required lifecycle state.
	 *
	 * @see #getLifecycleRequiredState()
	 * @see #setLifecycle(Lifecycle)
	 */
	public void setLifecycleRequiredState(@NonNull final Lifecycle.State requiredState) {
		this.lifecycleRequiredState = requiredState;
	}

	/**
	 * Returns the required state for lifecycle checked when executing {@link DialogRequest DialogRequests}
	 * via this controller.
	 *
	 * @return The required lifecycle state.
	 *
	 * @see #setLifecycleRequiredState(Lifecycle.State)
	 */
	@NonNull public Lifecycle.State getLifecycleRequiredState() {
		return lifecycleRequiredState;
	}

	/**
	 * Sets a dialog factory that should provide dialog fragment instances for {@link DialogRequest DialogRequests}
	 * created via {@link #newRequest(int)}.
	 *
	 * @param factory The desired factory. May {@code null} to clear the current one.
	 * @see #getFactory()
	 * @see #hasFactory()
	 */
	public void setFactory(@Nullable final DialogFactory factory) {
		this.mFactory = factory;
	}

	/**
	 * Checks whether this controller has dialog factory attached or not.
	 *
	 * @return {@code True} if factory is attached, {@code false} otherwise.
	 * @see #setFactory(DialogFactory)
	 * @see #getFactory()
	 */
	public boolean hasFactory() {
		return mFactory != null;
	}

	/**
	 * Asserts that the factory has been attached to this controller. If no factory is attached,
	 * an exception is thrown.
	 */
	private void assertHasFactory() {
		if (mFactory == null) throw new IllegalStateException("No factory attached!");
	}

	/**
	 * Returns the current dialog factory attached to this controller.
	 *
	 * @return This controller's factory or {@code null} if there is no factory attached yet.
	 * @see #setFactory(DialogFactory)
	 * @see #hasFactory()
	 */
	@Nullable public DialogFactory getFactory() {
		return mFactory;
	}

	/**
	 * Sets an interceptor that may be used to intercept an execution of a {@link DialogRequest}
	 * created via {@link #newRequest(DialogFragment)} when execution of that request has been requested
	 * via {@link DialogRequest#execute()}.
	 *
	 * @param interceptor The desired interceptor. May be {@code null} to clear the current one.
	 */
	public void setRequestInterceptor(@Nullable final DialogRequestInterceptor interceptor) {
		this.mRequestInterceptor = interceptor;
	}

	/**
	 * Registers a callback to be invoked when a {@link DialogRequest} is executed via this controller.
	 * <p>
	 * Dialog request created via {@link #newRequest(DialogFragment)} is executed whenever its
	 * {@link DialogRequest#execute()} is called and the associated controller does not have
	 * {@link DialogRequestInterceptor} attached or the attached interceptor did not intercept
	 * execution of that particular request.
	 *
	 * @param listener The desired listener callback to be registered.
	 * @see #unregisterOnRequestListener(OnRequestListener)
	 */
	public void registerOnRequestListener(@NonNull final OnRequestListener listener) {
		if (mRequestListeners == null) this.mRequestListeners = new ArrayList<>(1);
		if (!mRequestListeners.contains(listener)) mRequestListeners.add(listener);
	}

	/**
	 * Un-registers the given callback from the registered request listeners.
	 *
	 * @param listener The desired listener callback to be un-registered.
	 * @see #registerOnRequestListener(OnRequestListener)
	 */
	public void unregisterOnRequestListener(@NonNull final OnRequestListener listener) {
		if (mRequestListeners != null) mRequestListeners.remove(listener);
	}

	/**
	 * Notifies all registered {@link OnRequestListener OnRequestListeners} that the given
	 * <var>request</var> has been executed.
	 *
	 * @param request The request that has been just executed via {@link #executeRequest(DialogRequest)}.
	 */
	private void notifyRequestExecuted(final DialogRequest request) {
		if (mRequestListeners != null && !mRequestListeners.isEmpty()) {
			for (OnRequestListener listener : mRequestListeners) {
				listener.onRequestExecuted(request);
			}
		}
	}

	/**
	 * Registers a callback to be invoked whenever {@link #checkRestoredDialogs(String[])} is called
	 * for this controller and there are some dialogs already displayed.
	 *
	 * @param listener The desired listener callback to be registered.
	 */
	public void registerOnRestoreListener(@NonNull final OnRestoreListener listener) {
		if (mRestoreListeners == null) this.mRestoreListeners = new ArrayList<>(1);
		if (!mRestoreListeners.contains(listener)) mRestoreListeners.add(listener);
	}

	/**
	 * Un-registers the given callback from the registered restore listeners.
	 *
	 * @param listener The desired listener callback to be un-registered.
	 * @see #registerOnRestoreListener(OnRestoreListener)
	 */
	public void unregisterOnRestoreListener(@NonNull final OnRestoreListener listener) {
		if (mRestoreListeners != null) mRestoreListeners.remove(listener);
	}

	/**
	 * Same as {@link #checkRestoredDialogs(String[])} where dialog tags will be requested from
	 * the current factory for the specified <var>dialogIds</var>.
	 *
	 * @param dialogIds Set of ids of the dialogs to check if were restored.
	 * @see #registerOnRestoreListener(OnRestoreListener)
	 */
	public boolean checkRestoredDialogs(@NonNull final int... dialogIds) {
		if (dialogIds.length > 0) {
			assertHasFactory();
			final String[] tags = new String[dialogIds.length];
			for (int i = 0; i < tags.length; i++) {
				tags[i] = mFactory.createDialogTag(dialogIds[i]);
			}
			return checkRestoredDialogs(tags);
		}
		return false;
	}

	/**
	 * Performs check for restored dialogs. Call to this method will fire {@link OnRestoreListener#onDialogRestored(DialogFragment)}
	 * for each of registered listeners and for each of found dialog fragments that are at this time
	 * displayed.
	 * <p>
	 * <b>Note, that this method should be called from {@link Activity#onCreate(Bundle)} of the
	 * parent activity or from {@link Fragment#onActivityCreated(Bundle)} of the parent fragment in
	 * order to really check which dialogs are restored, for example after orientation change.</b>
	 *
	 * @param dialogTags Set of tags of the dialogs to check if were restored.
	 * @return {@code True} if at least one dialog was restored, {@code false} otherwise.
	 * @see #registerOnRestoreListener(OnRestoreListener)
	 */
	public boolean checkRestoredDialogs(@NonNull final String... dialogTags) {
		boolean restored = false;
		if (dialogTags.length > 0) {
			for (final String dialogTag : dialogTags) {
				final Fragment fragment = mManager.findFragmentByTag(dialogTag);
				if (fragment instanceof DialogFragment) {
					this.notifyDialogRestored((DialogFragment) fragment);
					restored = true;
				}
			}
		}
		return restored;
	}

	/**
	 * Notifies all registered {@link OnRestoreListener OnRestoreListener} that the given
	 * <var>dialog</var> has been restored.
	 *
	 * @param dialog The dialog that has been just restored due to call to {@link #checkRestoredDialogs(String[])}.
	 */
	private void notifyDialogRestored(final DialogFragment dialog) {
		if (mRestoreListeners != null && !mRestoreListeners.isEmpty()) {
			for (OnRestoreListener listener : mRestoreListeners) {
				listener.onDialogRestored(dialog);
			}
		}
	}

	/**
	 * Creates a new instance of DialogRequest for the given <var>dialogId</var>. The new request
	 * will have the given dialog id attached along with this controller which will be responsible
	 * for execution of the new request when its {@link DialogRequest#execute()} is called.
	 * <p>
	 * <b>Note</b>, that execution of the created request assumes that there is factory attached and
	 * that factory provides dialog fragment that is associated with the specified <var>dialogId</var>
	 * otherwise an exception will be thrown.
	 *
	 * @param dialogId Id of the desired factory dialog for which to crate the new request.
	 * @return New dialog request with the specified dialog id attached.
	 */
	@NonNull public final DialogRequest newRequest(final int dialogId) {
		this.assertNotDestroyed("NEW REQUEST");
		return new DialogRequest(this).dialogId(dialogId);
	}

	/**
	 * Creates a new DialogRequest for the given <var>dialogFragment</var>. The new request will have
	 * the given dialog fragment attached along with this controller which will be responsible for
	 * execution of the new request when its {@link DialogRequest#execute()} is called.
	 * <p>
	 * The returned request will have default {@link #DIALOG_TAG} attached via {@link DialogRequest#tag(String)}.
	 *
	 * @param dialogFragment The dialog fragment for which to create the new request.
	 * @return New dialog fragment request with default {@link #DIALOG_TAG}.
	 * @see DialogRequest#tag(String)
	 */
	@NonNull public final DialogRequest newRequest(@NonNull final DialogFragment dialogFragment) {
		this.assertNotDestroyed("NEW REQUEST");
		return new DialogRequest(this, dialogFragment).tag(DIALOG_TAG);
	}

	/**
	 * Performs execution of the given dialog <var>request</var>.
	 * <p>
	 * This method also notifies all registered {@link OnRequestListener OnRequestListeners} about
	 * the request execution.
	 * <p>
	 * <b>Note</b>, that this method does not check if the request has been already executed or not.
	 *
	 * @param request The dialog request to be executed.
	 * @return The dialog fragment that has been associated with the request either during its
	 * initialization or as result of this execution. May be {@code null} if the execution has failed.
	 * @throws IllegalStateException    If there is no factory attached.
	 * @throws IllegalArgumentException If the attached factory does not provide dialog fragment for
	 *                                  the dialog id specified for the request.
	 * @see DialogRequestInterceptor#interceptDialogRequest(DialogRequest)
	 */
	@Nullable final DialogFragment executeRequest(final DialogRequest request) {
		this.assertNotDestroyed("EXECUTE REQUEST");

		final Lifecycle.State lifecycleCurrentState = lifecycle == null ? null : lifecycle.getCurrentState();
		if (!request.hasFlag(DialogRequest.IGNORE_LIFECYCLE_STATE) && lifecycleCurrentState != null && !lifecycleCurrentState.isAtLeast(lifecycleRequiredState)) {
			Log.w(TAG, "Current Lifecycle's state(" + lifecycleCurrentState + ") is not at least(" + lifecycleRequiredState + "). Ignoring request!");
			return null;
		}

		DialogFragment dialogFragment = request.mDialogFragment;
		if (dialogFragment == null) {
			this.assertHasFactory();
			final int dialogId = request.mDialogId;
			if (!mFactory.isDialogProvided(dialogId)) {
				throw new IllegalArgumentException(
						"Cannot execute request for factory dialog. Current factory(" + mFactory.getClass() + ") " +
								"does not provide dialog for the requested id(" + dialogId + ")!");
			}
			switch (request.mIntent) {
				case DialogRequest.DISMISS:
					dialogFragment = findDialogFragmentByFactoryId(dialogId);
					break;
				case DialogRequest.SHOW:
				default:
					dialogFragment = mFactory.createDialog(dialogId, request.mOptions);
					if (dialogFragment == null) {
						throw new IllegalArgumentException(
								"Cannot execute request for factory dialog. Current factory(" + mFactory.getClass() + ") is cheating. " +
										"DialogFactory.isDialogProvided(...) returned true, but DialogFactory.createDialog(...) returned null!"
						);
					}
					break;
			}
			if (dialogFragment == null) {
				return null;
			}
			request.mDialogFragment = dialogFragment;
			if (request.mTag == null) {
				request.tag(mFactory.createDialogTag(dialogId));
			}
		}
		dialogFragment = mRequestInterceptor != null ? mRequestInterceptor.interceptDialogRequest(request) : null;
		if (dialogFragment == null) {
			dialogFragment = onExecuteRequest(request);
		}
		this.notifyRequestExecuted(request);
		return dialogFragment;
	}

	/**
	 * Called to perform execution of the given dialog <var>request</var>.
	 * <p>
	 * This implementation creates a new {@link FragmentTransaction} via {@link #createTransaction(DialogRequest)}
	 * for the request and commits it via appropriate commit method and returns the associated dialog
	 * fragment.
	 *
	 * @param request The dialog request of which execution has been requested via call to
	 *                {@link DialogRequest#execute()}
	 * @return The dialog fragment associated with the request.
	 */
	@SuppressWarnings("ConstantConditions")
	@NonNull protected DialogFragment onExecuteRequest(@NonNull final DialogRequest request) {
		if (mManager.isDestroyed()) {
			throw new IllegalStateException("Cannot execute dialog request in context of activity that has been already destroyed!");
		}
		// Crate transaction for the dialog request.
		final DialogFragment dialogFragment = request.mDialogFragment;
		// Resolve operation type. todo: move this resolving to create transaction method ...
		switch (request.mIntent) {
			case DialogRequest.DISMISS:
				try {
					if (request.hasFlag(DialogRequest.ALLOW_STATE_LOSS)) {
						dialogFragment.dismissAllowingStateLoss();
					} else {
						dialogFragment.dismiss();
					}
				} catch (IllegalStateException e) {
					Log.e(TAG, "Error occurred when trying to dismiss dialog fragment(" + dialogFragment + ").", e);
				}
				break;
			case DialogRequest.SHOW:
			default:
				final String tag = request.mTag;
				if (!request.hasFlag(DialogRequest.REPLACE_SAME)) {
					// Do not replace same dialog fragment if there is already displayed
					// dialog fragment with the same tag.
					final Fragment existingFragment = mManager.findFragmentByTag(tag);
					if (existingFragment != null) {
						if (!(existingFragment instanceof DialogFragment)) {
							throw new IllegalArgumentException("Found fragment with tag(" + tag + ") but it is not a DialogFragment!");
						}
						if (DialogsConfig.LOG_ENABLED) {
							Log.v(TAG, "Dialog fragment with tag(" + tag + ") is already displayed.");
						}
						return (DialogFragment) existingFragment;
					}
				}
				if (dialogFragment instanceof ContextDialog) {
					final ContextDialog contextDialog = (ContextDialog) dialogFragment;
					contextDialog.setContextFragmentTag(mParentFragmentTag);
					contextDialog.setContextFragmentId(mParentFragmentId);
				}
				dialogFragment.show(createTransaction(request), tag);
				break;
		}
		if (request.hasFlag(DialogRequest.IMMEDIATE)) {
			mManager.executePendingTransactions();
		}
		return dialogFragment;
	}

	/**
	 * Begins a new FragmentTransaction for the given dialog <var>request</var>.
	 *
	 * @param request The request specifying configuration parameters for the transaction to be created.
	 * @return The desired fragment transaction that may be used to show a dialog fragment.
	 */
	@SuppressLint("CommitTransaction")
	@NonNull public FragmentTransaction createTransaction(@NonNull final DialogRequest request) {
		return mManager.beginTransaction();
	}

	/**
	 * Delegates to {@link FragmentManager#findFragmentByTag(String)} with the TAG obtained via
	 * {@link DialogFactory#createDialogTag(int)} from the current factory.
	 * <p>
	 * <b>Note</b>, that this method assumes that there is factory attached and that factory provides
	 * dialog fragment that is associated with the specified <var>factoryDialogId</var> otherwise an
	 * exception is thrown.
	 *
	 * @param factoryDialogId Id of the desired factory dialog fragment to find.
	 * @return The requested dialog fragment if found, {@code null} otherwise.
	 * @throws IllegalStateException    If there is no factory attached.
	 * @throws IllegalArgumentException If the attached factory does not provide dialog fragment for
	 *                                  the specified id.
	 */
	@Nullable public DialogFragment findDialogFragmentByFactoryId(final int factoryDialogId) {
		this.assertNotDestroyed("FIND DIALOG FRAGMENT BY FACTORY ID");
		this.assertHasFactory();
		if (!mFactory.isDialogProvided(factoryDialogId)) {
			throw new IllegalArgumentException(
					"Cannot find dialog fragment by factory id. Current factory(" + mFactory.getClass() + ") " +
							"does not provide dialog fragment for the requested id(" + factoryDialogId + ")!");
		}
		final Fragment fragment = mManager.findFragmentByTag(mFactory.createDialogTag(factoryDialogId));
		if (fragment == null) {
			return null;
		}
		if (fragment instanceof DialogFragment) {
			return (DialogFragment) fragment;
		}
		throw new IllegalStateException("Found fragment(" + fragment + ") for factory id(" + factoryDialogId + ") is not a DialogFragment.");
	}

	/**
	 * Destroys this dialog controller instance.
	 * <p>
	 * Fragment controller should be destroyed whenever it is used in application component that has
	 * 'shorter' lifecycle (like fragment) as its parent application component (activity).
	 * <p>
	 * <b>Note</b>, that already destroyed controller should not be used further as such usage will
	 * result in an exception to be thrown.
	 */
	public final void destroy() {
		this.mDestroyed = true;
	}

	/**
	 * Asserts that this controller is not destroyed yet. If it is already destroyed, an exception
	 * is thrown.
	 *
	 * @param forAction Action for which the check should be performed. The action will be placed
	 *                  into exception if it will be thrown.
	 */
	private void assertNotDestroyed(final String forAction) {
		if (mDestroyed) {
			throw new IllegalStateException("Cannot perform " + forAction + " action. Controller is already destroyed!");
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}