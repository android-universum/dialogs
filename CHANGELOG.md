Change-Log
===============
> Regular configuration update: _14.01.2022_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 0.x ##

### 0.12.0 ###
> 14.01.2022

- New **group id**.

### 0.11.2 ###
> 27.08.2020

- Restore properly state of `DatePickerDialog` and `TimePickerDialog` after orientation change.

### 0.11.1 ###
> 25.06.2020

- Allow to specify **time zone** for _TimePickerDialog_.

### 0.11.0 ###
> 09.03.2020

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).
     - Updated `androidx.fragment:fragment` dependency to **1.2.2** version.
- Removed elements that has been **deprecated** in previous versions.

### 0.10.9 ###
> 07.01.2020

- Resolved [Issue #26](https://bitbucket.org/android-universum/dialogs/issues/26).

### 0.10.8 ###
> 28.12.2019

- Resolved [Issue #24](https://bitbucket.org/android-universum/dialogs/issues/24).
- Resolved [Issue #25](https://bitbucket.org/android-universum/dialogs/issues/25).

### 0.10.7 ###
> 08.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### 0.10.6 ###
> 06.10.2019

- Fixed `BaseDialog` leaking system `Dialog.OnShowListener`.
- Resolved [Issue #23](https://bitbucket.org/android-universum/dialogs/issues/23).

### 0.10.5 ###
> 15.02.2019

- Stability improvements.

### 0.10.4 ###
> 02.02.2019

- Stability improvements.

### 0.10.3 ###
> 23.01.2019

- Small updates and improvements.

### 0.10.2 ###
> 09.12.2018

- Allowed to specify a desired **TimeZone** for `DatePickerDialog` via its options.

### 0.10.1 ###
> 02.12.2018

- Resolved [Issue #11](https://bitbucket.org/android-universum/dialogs/issues/11).

### 0.10.0 ###
> 20.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### 0.9.5 ###
> 06.08.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_ for both versions of the library.

### 0.9.4 ###
> 15.06.2017

- Fixed `NullPointerException` in `SimpleDialogButtonsView`.

### 0.9.3 ###
> 26.04.2017

- Fixed `findDialogFragmentByFactoryId(...)` of `DialogController`.

### 0.9.2 ###
> 17.03.2017

- Stability improvements.
- Removed deprecated methods from the previous release.

### 0.9.1 ###
> 02.03.2017

- Pre-release patch.

### 0.9.0 ###
> 23.01.2017

- First pre-release.