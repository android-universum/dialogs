/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.UniqueDialog;
import universum.studios.android.dialog.annotation.DialogAnnotations;
import universum.studios.android.dialog.annotation.FactoryDialog;
import universum.studios.android.dialog.annotation.FactoryDialogs;
import universum.studios.android.dialog.annotation.handler.BaseManagementAnnotationHandlers;
import universum.studios.android.dialog.annotation.handler.DialogFactoryAnnotationHandler;

/**
 * A {@link DialogFactory} base implementation that may be used for dialog factories used
 * across Android application project.
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link FactoryDialogs @FactoryDialogs} <b>[class - inherited]</b>
 * <p>
 * If this annotation is presented, all ids of dialogs specified via this annotation will be
 * attached to an instance of annotated BaseDialogFactory subclass, so {@link #isDialogProvided(int)}
 * will return always {@code true} for each of these ids.
 * <p>
 * Also, there will be automatically created default tags for all such ids, so they may be obtained
 * via {@link #createDialogTag(int)} with the corresponding dialog id.
 * </li>
 * <li>
 * {@link FactoryDialog @FactoryDialog} <b>[member - inherited]</b>
 * <p>
 * This annotation provides same result as {@link FactoryDialog @FactoryDialog} annotation, but this
 * annotation is meant to be used to mark directly constant fields that specify dialog ids and also
 * provides more configuration options like the type of dialog fragment that should be instantiated
 * for the specified id.
 * <p>
 * <b>Note</b>, that tagged name for dialog with the specified id will be automatically created
 * using its id but may be also specified via {@link FactoryDialog#taggedName()} attribute.
 * </li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class BaseDialogFactory implements DialogFactory {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseDialogFactory";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Enums =======================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	private final DialogFactoryAnnotationHandler mAnnotationHandler;

	/**
	 * Context in which will be this factory used.
	 */
	protected final Context mContext;

	/**
	 * An application resources.
	 */
	protected final Resources mResources;

	/**
	 * Set of dialog item holders created from annotated fields ({@link FactoryDialog @FactoryDialog})
	 * of this factory instance.
	 */
	private SparseArray<DialogItem> mItems;

	/**
	 * Id of the dialog which was last checked by {@link #isDialogProvided(int)}.
	 */
	private int mLastCheckedDialogId = -1;

	/**
	 * Flag indicating whether an instance of dialog for {@link #mLastCheckedDialogId} can be provided
	 * by this factory or not.
	 */
	private boolean mDialogProvided;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseDialogFactory with the given context. If
	 * {@link universum.studios.android.dialog.annotation.FactoryDialogs @FactoryDialogs}
	 * annotations are presented above a sub-class of this BaseDialogFactory, they will be processed
	 * here.
	 *
	 * @param context Context in which will be this factory used.
	 */
	public BaseDialogFactory(@NonNull final Context context) {
		this.mContext = context;
		this.mResources = context.getResources();
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mItems = mAnnotationHandler != null ? mAnnotationHandler.getDialogItems() : null;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a tag for dialog in the required format depends on a package name of the passed
	 * <var>classOfFactory</var> and <var>dialogName</var>.
	 * <p>
	 * Example format: <u>com.android.app.dialog.SignInDialogs.TAG.ConfirmDialog</u>
	 * <p>
	 * - where <b>com.android.app.dialog</b> is name of the package where is the specified
	 * <var>classOfFactory</var> placed, <b>SignInDialogs</b> is factory class name, <b>ConfirmDialog</b>
	 * is <var>dialogName</var> and <b>TAG</b> is tag identifier.
	 *
	 * @param classOfFactory Class of factory which provides dialog with the given name.
	 * @param dialogName     Dialog name (can be dialog class name) for which tag should be created.
	 * @return Dialog tag in required format, or {@code ""} if <var>dialogName</var> is
	 * {@code null} or empty.
	 */
	@Nullable public static String createDialogTag(@NonNull final Class<?> classOfFactory, @NonNull final String dialogName) {
		if (TextUtils.isEmpty(dialogName)) {
			return null;
		}
		return classOfFactory.getPackage().getName() + "." + classOfFactory.getSimpleName() + ".TAG." + dialogName;
	}

	/**
	 * Invoked to create annotations handler for this instance.
	 *
	 * @return Annotations handler specific for this class.
	 */
	private DialogFactoryAnnotationHandler onCreateAnnotationHandler() {
		return BaseManagementAnnotationHandlers.obtainFactoryHandler(getClass());
	}

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws IllegalStateException If annotations processing is not enabled for the Dialogs library.
	 */
	@NonNull protected DialogFactoryAnnotationHandler getAnnotationHandler() {
		DialogAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 */
	@Override public boolean isDialogProvided(final int dialogId) {
		if (dialogId == mLastCheckedDialogId) {
			return mDialogProvided;
		}
		this.mLastCheckedDialogId = dialogId;
		return mDialogProvided = providesDialog(dialogId);
	}

	/**
	 * Invoked whenever {@link #isDialogProvided(int)} is called and the specified <var>dialogId</var>
	 * differs from the one last time called.
	 * <p>
	 * This implementation returns {@code true} if there is {@link FactoryDialogs @FactoryDialogs}
	 * or {@link FactoryDialog @FactoryDialog} annotation presented for the specified <var>dialogId</var>,
	 * {@code false} otherwise.
	 */
	protected boolean providesDialog(final int dialogId) {
		return (mItems != null) && mItems.indexOfKey(dialogId) >= 0;
	}

	/**
	 */
	@Override @Nullable public DialogFragment createDialog(final int dialogId, @Nullable final DialogOptions options) {
		return isDialogProvided(dialogId) ? attachDialogId(onCreateDialog(dialogId, options), dialogId) : null;
	}

	/**
	 * Invoked whenever {@link #createDialog(int, DialogOptions)} is called and none of
	 * the current joined factories provides dialog for the specified <var>dialogId</var>.
	 * <p>
	 * This implementation returns the requested fragment instance if there is {@link FactoryDialog @FactoryDialog}
	 * annotation presented for the specified <var>dialogId</var> with valid dialog fragment class
	 * type ({@link FactoryDialog#value() @FactoryDialog.value()}), {@code null} otherwise.
	 */
	@Nullable protected DialogFragment onCreateDialog(final int dialogId, @Nullable final DialogOptions options) {
		return providesDialog(dialogId) ? mItems.get(dialogId).newDialogInstance(options) : null;
	}

	/**
	 * Attaches the specified <var>dialogId</var> to the specified <var>dialog</var> if it is instance
	 * of {@link UniqueDialog}.
	 *
	 * @param dialog   Dialog to set up.
	 * @param dialogId The id which will be assigned to the passed dialog.
	 */
	DialogFragment attachDialogId(final DialogFragment dialog, final int dialogId) {
		if (dialog instanceof UniqueDialog) ((UniqueDialog) dialog).setDialogId(dialogId);
		return dialog;
	}

	/**
	 */
	@Override @Nullable public String createDialogTag(final int dialogId) {
		return isDialogProvided(dialogId) ? onCreateDialogTag(dialogId) : null;
	}

	/**
	 * Invoked whenever {@link #createDialogTag(int)} is called and this factory provides dialog
	 * instance for the specified <var>dialogId</var>.
	 * <p>
	 * This implementation returns the requested dialog TAG created from dialog ids specified via
	 * {@link FactoryDialogs @FactoryDialogs} or for a single id marked by {@link FactoryDialog @FactoryDialog}.
	 * If neither of these annotations is presented, the TAG is created via {@link #createDialogTag(Class, String)}
	 * with the dialog id as <var>dialogName</var>.
	 */
	@Nullable protected String onCreateDialogTag(final int dialogId) {
		return mItems.indexOfKey(dialogId) >= 0 ? mItems.get(dialogId).tag : createDialogTag(getClass(), Integer.toString(dialogId));
	}

	/*
	 * Inner classes ===============================================================================
	 */
}