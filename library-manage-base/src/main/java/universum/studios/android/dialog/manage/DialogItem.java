/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.OptionsDialog;
import universum.studios.android.dialog.annotation.FactoryDialog;

/**
 * Item holder that is used by {@link BaseDialogFactory} to instantiate new dialogs via reflection
 * that has been specified via {@link FactoryDialog @FactoryDialog} annotation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class DialogItem {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    private static final String TAG = "DialogItem";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Dialog id specified for this item.
     */
    public final int id;

    /**
     * Dialog tag specified for this item.
     */
    public final String tag;

    /**
     * Dialog type specified for this item.
     */
    public final Class<? extends DialogFragment> type;

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #DialogItem(int, Class, String)} with {@code null} <var>tag</var>.
     */
    public DialogItem(final int id, @NonNull final Class<? extends DialogFragment> type) {
        this(id, type, null);
    }

    /**
     * Creates a new instance of DialogItem with the given parameters.
     */
    public DialogItem(final int id, @NonNull final Class<? extends DialogFragment> type, @Nullable final String tag) {
        this.id = id;
        this.tag = tag;
        this.type = type;
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Creates a new instance of DialogFragment specified for this item.
     *
     * @param options Options for dialog. May be {@code null}.
     * @return New dialog fragment instance or {@code null} if type of this item is
     * {@link DialogFragment DialogFragment.class} which is default and can not be instantiated
     * or instantiation error occur.
     */
    @Nullable public DialogFragment newDialogInstance(@Nullable final DialogOptions options) {
        if (type.equals(DialogFragment.class)) {
            return null;
        }
        try {
            final DialogFragment dialogFragment = type.newInstance();
            if (options != null && dialogFragment instanceof OptionsDialog) {
                ((OptionsDialog) dialogFragment).setOptions(options);
            }
            return dialogFragment;
        } catch (Exception e) {
            final String dialogName = type.getSimpleName();
            Log.e(TAG, "Failed to instantiate a new dialog fragment instance class of(" + dialogName + "). " +
                    "Make sure that this dialog fragment class is accessible and has public empty constructor.", e);
        }
        return null;
    }

    /*
     * Inner classes ===============================================================================
     */
}