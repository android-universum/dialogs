/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.annotation.handler;

import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.annotation.DialogAnnotations;
import universum.studios.android.dialog.annotation.FactoryDialog;
import universum.studios.android.dialog.annotation.FactoryDialogs;
import universum.studios.android.dialog.manage.BaseDialogFactory;
import universum.studios.android.dialog.manage.DialogItem;

/**
 * An {@link AnnotationHandlers} implementation providing {@link AnnotationHandler} instances for
 * <b>base management</b> associated dialogs and classes.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class BaseManagementAnnotationHandlers extends AnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private BaseManagementAnnotationHandlers() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a {@link DialogFactoryAnnotationHandler} implementation for the given <var>classOfFactory</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable public static DialogFactoryAnnotationHandler obtainFactoryHandler(@NonNull final Class<?> classOfFactory) {
		return obtainHandler(DialogFactoryHandler.class, classOfFactory);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link DialogFactoryAnnotationHandler} implementation for {@link BaseDialogFactory} class.
	 */
	@SuppressWarnings("WeakerAccess") static final class DialogFactoryHandler extends BaseAnnotationHandler implements DialogFactoryAnnotationHandler {

		/**
		 * Array of fragment items populated from the {@link FactoryDialogs @FactoryDialogs} or
		 * {@link FactoryDialog @FactoryDialog} annotations if presented.
		 */
		final SparseArray<DialogItem> items;

		/**
		 * Same as {@link BaseAnnotationHandler#BaseAnnotationHandler(Class, Class)} with
		 * {@link BaseDialogFactory} as <var>maxSuperClass</var>.
		 */
		public DialogFactoryHandler(@NonNull final Class<?> annotatedClass) {
			super(annotatedClass, BaseDialogFactory.class);
			final SparseArray<DialogItem> items = new SparseArray<>();
			final FactoryDialogs dialogs = findAnnotationRecursive(FactoryDialogs.class);
			if (dialogs != null) {
				final int[] ids = dialogs.value();
				if (ids.length > 0) {
					for (int id : ids) {
						items.put(id, new DialogItem(
								id,
								DialogFragment.class,
								BaseDialogFactory.createDialogTag(mAnnotatedClass, Integer.toString(id))
						));
					}
				}
			}
			DialogAnnotations.iterateFields(new DialogAnnotations.FieldProcessor() {

				/**
				 */
				@Override public void onProcessField(@NonNull final Field field, @NonNull final String name) {
					if (field.isAnnotationPresent(FactoryDialog.class) && int.class.equals(field.getType())) {
						final FactoryDialog factoryDialog = field.getAnnotation(FactoryDialog.class);
						try {
							field.setAccessible(true);
							final int id = (int) field.get(null);
							items.put(id, new DialogItem(
									id,
									factoryDialog.value(),
									BaseDialogFactory.createDialogTag(
											mAnnotatedClass,
											TextUtils.isEmpty(factoryDialog.taggedName()) ?
													Integer.toString(id) :
													factoryDialog.taggedName()
									)
							));
						} catch (IllegalAccessException e) {
							Log.e(
									DialogFactoryHandler.class.getSimpleName(),
									"Failed to obtain id value from @FactoryDialog " + name + "!",
									e
							);
						}
					}
				}
			}, mAnnotatedClass, mMaxSuperClass);
			this.items = items.size() > 0 ? items : null;
		}

		/**
		 */
		@Override @Nullable public SparseArray<DialogItem> getDialogItems() {
			return items;
		}
	}
}