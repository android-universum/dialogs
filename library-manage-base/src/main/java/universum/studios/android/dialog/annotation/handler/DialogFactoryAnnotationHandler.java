/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.annotation.handler;

import android.util.SparseArray;

import androidx.annotation.Nullable;
import universum.studios.android.dialog.annotation.FactoryDialog;
import universum.studios.android.dialog.annotation.FactoryDialogs;
import universum.studios.android.dialog.manage.BaseDialogFactory;
import universum.studios.android.dialog.manage.DialogItem;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Dialogs library
 * that are used to handle processing of annotations attached to classes derived from {@link BaseDialogFactory}
 * classes provided by this library.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface DialogFactoryAnnotationHandler extends AnnotationHandler {

	/**
	 * Returns an array with DialogItems mapped to theirs ids that has been created from
	 * {@link FactoryDialog @FactoryDialog} or {@link FactoryDialogs @FactoryDialogs} (if presented).
	 *
	 * @return Array with dialog items created from the processed annotations or {@code null} if
	 * there were no annotations specified.
	 */
	@Nullable SparseArray<DialogItem> getDialogItems();
}