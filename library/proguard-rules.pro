# Keep all dialog annotations.
-keep public @interface universum.studios.android.dialog.annotation.** { *; }
-keep @interface universum.studios.android.dialog.**$** { *; }
# Keep classes of all dialogs that can be inflated from an Xml file.
-keep class * implements universum.studios.android.dialog.XmlDialog
# Keep DialogFactory implementation details:
# - public empty constructor for proper working of instantiation process using reflection when joining
#   factories via annotation
-keepclassmembers class * implements universum.studios.android.dialog.manage.DialogFactory {
    public <init>();
}
# Keep members with @FactoryDialog annotation within dialog factories.
-keepclassmembers class * extends universum.studios.android.dialog.manage.BaseDialogFactory {
    @universum.studios.android.dialog.annotation.FactoryDialog *;
}
# Keep annotation handlers implementation details:
# - constructor taking Class parameter [always]
-keepclassmembers class * extends universum.studios.android.dialog.annotation.handler.BaseAnnotationHandler {
    public <init>(java.lang.Class);
}