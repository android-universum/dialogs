@Dialogs-Picker
===============

This module groups the following modules into one **single group**:

- [Picker-Color](https://bitbucket.org/android-universum/dialogs/src/main/library-picker-color)
- [Picker-DateTime](https://bitbucket.org/android-universum/dialogs/src/main/library-picker-datetime)
- [Picker-Locale](https://bitbucket.org/android-universum/dialogs/src/main/library-picker-locale)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adialogs/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adialogs/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:dialogs-picker:${DESIRED_VERSION}@aar"
