/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.R;

/**
 * A {@link DialogBaseAdapter} implementation which specifies API and logic allowing to select items
 * from the adapter's data set. Whether the adapter should allow single or multiple items selection
 * can be specified via {@link #setSelectionMode(int)} passing one of {@link #SINGLE} or {@link #MULTIPLE}.
 * <b>Note</b> that based on the specified mode, the adapter will inflate different types of selectable
 * item views (single = RadioButton, multiple = CheckBox) in {@link #onCreateView(ViewGroup, int)} method.
 * <p>
 * Changing of current selection state for a specific item can be done via {@link #toggleItemSelection(long)},
 * and to check whenever an item at the specific position is selected or not can be done via
 * {@link #isItemSelected(long)} or {@link #isItemSelected(long)} for the item's id. If you need to clear the
 * current selection, call {@link #clearSelection()}. You can also specify bulk selection (after
 * orientation change to restore the previous selection state) via {@link #setSelection(long[])}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class DialogBaseSelectionAdapter<I extends DialogSelectionAdapter.Item>
		extends DialogBaseAdapter<I, DialogBaseSelectionAdapter.ItemHolder>
		implements DialogSelectionAdapter {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogBaseSelectionAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===============================================================================
	 */

	/**
	 * Defines the count of view types provided by this adapter by {@link #getView(int, View, ViewGroup)}.
	 */
	public static final int VIEW_TYPE_COUNT = 3;

	/**
	 * View type indicating view for simple item used when no selection mode was requested.
	 */
	public static final int VIEW_TYPE_ITEM_SIMPLE = NONE;

	/**
	 * View type indicating view for single mode selection.
	 */
	public static final int VIEW_TYPE_ITEM_SINGLE = SINGLE;

	/**
	 * View type indicating view for multiple mode selection.
	 */
	public static final int VIEW_TYPE_ITEM_MULTIPLE = MULTIPLE;

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

    /**
     * List containing all currently selected ids.
     */
    private final List<Long> selection = new ArrayList<>(10);

	/**
	 * Selection mode of this adapter. This mode will determine how calling of {@link #toggleItemSelection(long)}
	 * should be interpreted.
	 */
	private int selectionMode = SINGLE;

	/**
	 * Flag indicating whether this adapter allows empty selection or not, so if {@link #selection}
	 * can be empty or not.
	 */
	private boolean emptySelectionAllowed = true;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * See {@link DialogBaseAdapter#DialogBaseAdapter(Context, LayoutInflater)}.
	 */
	public DialogBaseSelectionAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater) {
		super(context, inflater);
	}

	/**
	 * See {@link DialogBaseAdapter#DialogBaseAdapter(Context, LayoutInflater, List)}.
	 */
	public DialogBaseSelectionAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @NonNull final List<I> items) {
		super(context, inflater, items);
	}

	/**
	 * See {@link DialogBaseAdapter#DialogBaseAdapter(Context, LayoutInflater, Object[])}.
	 */
	public DialogBaseSelectionAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @NonNull final I[] items) {
		super(context, inflater, items);
	}

	/*
	 * Methods =====================================================================================
	 */

    /**
     */
    @Override public void setSelectionMode(@SelectionMode final int mode) {
        switch (mode) {
            case NONE:
            case SINGLE:
            case MULTIPLE:
                this.selectionMode = mode;
                break;
        }
    }

    /**
     */
    @Override @SelectionMode public int getSelectionMode() {
        return selectionMode;
    }

    /**
     */
    @Override public void setEmptySelectionAllowed(final boolean allowed) {
        this.emptySelectionAllowed = allowed;
    }

    /**
     */
    @Override public boolean isEmptySelectionAllowed() {
        return emptySelectionAllowed;
    }

	/**
	 */
	@Override public int toggleItemSelection(final long id) {
        boolean selectionChanged = false;
		switch (selectionMode) {
			case SINGLE:
                if (!selection.contains(id)) {
                    selection.clear();
                    selection.add(id);
                } else if (emptySelectionAllowed) {
                    selection.remove(id);
                }
                selectionChanged = true;
				break;
			case MULTIPLE:
                if (!selection.contains(id)) {
                    selection.add(id);
                } else if (selection.size() > 1 || emptySelectionAllowed) {
                    selection.remove(id);
                }
                selectionChanged = true;
				break;
		}
		if (selectionChanged) {
            notifyDataSetChanged();
        }
		return selection.size();
	}

	/**
	 */
	@Override public boolean isItemSelected(final long id) {
        return selection.contains(id);
	}

	/**
	 */
	@Override public void clearSelection() {
        this.selection.clear();
	}

	/**
	 */
	@Override public int getViewTypeCount() {
		return VIEW_TYPE_COUNT;
	}

	/**
	 */
	@Override public int getItemViewType(final int position) {
		return selectionMode;
	}

	/**
	 */
	@Override public long getItemId(final int position) {
        if (position < 0 || position >= getCount()) return -1;
		else return getItem(position).getId();
	}

    /**
     */
    @Override @Nullable public Item getSelectedItem() {
        if (selectionMode != SINGLE) {
            throw new IllegalStateException("Not in SINGLE selection mode!");
        }
        final int n = getCount();
        for (int i = 0; i < n; i++) {
            final Item item = getItem(i);
            if (selection.contains(item.getId())) {
                return item;
            }
        }
        return null;
    }

    /**
	 */
	@Override @NonNull public List<Item> getSelectedItems() {
        if (selectionMode != MULTIPLE) {
            throw new IllegalStateException("Not in MULTIPLE selection mode!");
        }
        final int n = getCount();
        final List<Item> items = new ArrayList<>(selection.size());
        for (int i = 0; i < n; i++) {
            final Item item = getItem(i);
            if (selection.contains(item.getId())) {
                items.add(item);
            }
        }
        return items;
	}

	/**
	 */
	@Override public void setSelection(@NonNull final long[] ids) {
		if (mItems == null || mItems.isEmpty()) {
			throw new IllegalStateException("Cannot set selection to adapter with empty data set.");
		}
        this.selection.clear();
		if (ids.length == 0) {
			return;
		}
		for (long id : ids) {
			this.selection.add(id);
		}
	}

	/**
	 */
	@Override @NonNull public long[] getSelection() {
        final int n = selection.size();
        final long[] selectionArray = new long[n];
        for (int i = 0; i < n; i++) {
            selectionArray[i] = selection.get(i);
        }
        return selectionArray;
	}

	/**
	 */
	@Override @NonNull protected View onCreateView(@NonNull final ViewGroup parent, final int position) {
		switch (mCurrentViewType) {
			case VIEW_TYPE_ITEM_SINGLE:
				return inflate(R.layout.dialog_item_selection_single, parent);
			case VIEW_TYPE_ITEM_MULTIPLE:
				return inflate(R.layout.dialog_item_selection_multiple, parent);
			case VIEW_TYPE_ITEM_SIMPLE:
			default:
				return inflate(R.layout.dialog_item_selection_simple, parent);
		}
	}

	/**
	 */
	@Override @Nullable protected ItemHolder onCreateViewHolder(@NonNull final View view, final int position) {
		return new ItemHolder(view);
	}

	/**
	 */
	@Override protected void onBindViewHolder(@NonNull final ItemHolder viewHolder, final int position) {
		onBindViewHolder(viewHolder, isItemSelected(getItemId(position)), position);
	}

	/**
	 * Called whenever {@link #onBindViewHolder(Object, int)} is invoked for the specified <var>position</var>.
	 * This is only helper implementation to supply <b>selection</b> state for this position.
	 *
	 * @param selected {@code True} if item at the position is selected, {@code false} otherwise.
	 */
	protected abstract void onBindViewHolder(@NonNull ItemHolder viewHolder, boolean selected, int position);

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link DialogViewHolder} implementation for item views of {@link DialogBaseSelectionAdapter}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class ItemHolder extends DialogViewHolder {

		/**
		 * Text view of the selectable item (may be a {@link CompoundButton}).
		 */
		@Nullable public final TextView textView;

		/**
		 * Creates a new instance of ViewHolder for selectable item view.
		 *
		 * @param itemView View of the item.
		 */
		public ItemHolder(@NonNull final View itemView) {
            super(itemView);
			this.textView = itemView.findViewById(R.id.dialog_item_text);
		}
	}
}