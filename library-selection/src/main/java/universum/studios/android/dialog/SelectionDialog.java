/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.ArrayRes;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.adapter.DialogBaseSelectionAdapter;
import universum.studios.android.dialog.adapter.DialogSelectionAdapter;

/**
 * An {@link AdapterDialog} implementation that can be used to allow to a user to pick from a set of
 * selectable items one or more items. This dialog uses {@link universum.studios.android.dialog.widget.ListDialogContentView DialogListContent}
 * as its content view that uses {@link ListView ListView} as AdapterView so items will be presented in the list.
 * <p>
 * Whether to allow single or multiple items selection can be specified through {@link SelectionOptions}
 * via {@link SelectionOptions#selectionMode(int)}. <b>Note</b> that as described in {@link DialogBaseSelectionAdapter},
 * this adapter will create, based on the specified selection mode, a different types of item views
 * (single = RadioButton, multiple = CheckBox).
 * <p>
 * See {@link SelectionOptions} for options that can be supplied to the SelectionDialog from the
 * desired context via {@link #newInstance(SelectionOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogSelectionOptions dialogSelectionOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SelectionDialog extends AdapterDialog<ListView> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SelectionDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SelectionDialog(int)} with {@link R.attr#dialogSelectionOptions dialogSelectionOptions}
	 * options style attribute.
	 */
	public SelectionDialog() {
		super(R.attr.dialogSelectionOptions);
	}

	/**
	 * Creates a new instance of SelectionDialog with {@link ListView} the present data set of
	 * selectable items.
	 *
	 * @param optionsStyleAttr An attribute which contains a reference to the options (style) specific
	 *                         for this type of dialog within the current dialogs theme.
	 */
	@SuppressLint("ValidFragment")
	protected SelectionDialog(@AttrRes final int optionsStyleAttr) {
		super(optionsStyleAttr);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of SelectionDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New SelectionDialog instance.
	 */
	@NonNull public static SelectionDialog newInstance(@NonNull final SelectionOptions options) {
		final SelectionDialog dialog = new SelectionDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new SelectionOptions(resources);
	}

	/**
	 */
	@SuppressWarnings("ResourceType")
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof SelectionOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Selection);
			final SelectionOptions options = (SelectionOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Selection_dialogSelectionMode) {
					if (!options.isSet(SelectionOptions.SELECTION_MODE)) {
						options.selectionMode(attributes.getInt(index, options.selectionMode));
					}
				} else if (index == R.styleable.Dialog_Options_Selection_dialogEmptySelectionAllowed) {
					if (!options.isSet(SelectionOptions.EMPTY_SELECTION_ALLOWED)) {
						options.emptySelectionAllowed(attributes.getBoolean(index, options.emptySelectionAllowed));
					}
				}
			}
			attributes.recycle();
		}
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_list, container, false);
	}

	/**
	 */
	@Override protected void onBindView(
			@NonNull final View dialogView,
			@NonNull final DialogOptions options,
			@Nullable final Bundle savedInstanceState
	) {
		super.onBindView(dialogView, options, savedInstanceState);
		if (options instanceof SelectionOptions && getAdapter() == null) {
			final SelectionOptions selectionOptions = (SelectionOptions) options;
			if (selectionOptions.items != null) {
				final ItemsAdapter adapter = new ItemsAdapter<>(getContext(), mLayoutInflater, selectionOptions.items);
				adapter.setSelectionMode(selectionOptions.selectionMode);
				adapter.setEmptySelectionAllowed(selectionOptions.emptySelectionAllowed);
				if (selectionOptions.selection != null) {
					adapter.setSelection(selectionOptions.selection);
				}
				setAdapter(adapter);
			}
		}
	}

	/**
	 */
	@Override protected boolean onItemClick(
			@NonNull final AdapterView<?> parent,
			@NonNull final View view,
			final int position,
			final long id
	) {
		if (mAdapter instanceof DialogSelectionAdapter) {
			final DialogSelectionAdapter selectionAdapter = (DialogSelectionAdapter) mAdapter;
			if (selectionAdapter.getSelectionMode() == DialogSelectionAdapter.NONE) {
				return false;
			}
			selectionAdapter.toggleItemSelection(id);
			return mOptions.hasButtons();
		}
		return super.onItemClick(parent, view, position, id);
	}

	/**
	 * Returns the current selected item within this dialog.
	 * <p>
	 * The requested item will be obtained from the current adapter (if instance of {@link DialogSelectionAdapter})
	 * via {@link DialogSelectionAdapter#getSelectedItem()}.
	 * <p>
	 * <b>Note</b>, that the adapter must be in {@link DialogSelectionAdapter#SINGLE} selection mode,
	 * otherwise {@code null} will be returned.
	 *
	 * @return Selected item or {@code null} if no item has been selected yet.
	 * @see #getSelectedItems()
	 * @see DialogSelectionAdapter#getSelectionMode()
	 */
	@Nullable public DialogSelectionAdapter.Item getSelectedItem() {
		if (mAdapter instanceof DialogSelectionAdapter) {
			final DialogSelectionAdapter selectionAdapter = (DialogSelectionAdapter) mAdapter;
			if (selectionAdapter.getSelectionMode() == DialogSelectionAdapter.SINGLE) {
				return selectionAdapter.getSelectedItem();
			}
		}
		return null;
	}

	/**
	 * Returns a list containing current selected items within this dialog.
	 * <p>
	 * The requested items will be obtained from the current adapter (if instance of {@link DialogSelectionAdapter})
	 * via {@link DialogSelectionAdapter#getSelectedItems()}.
	 * <p>
	 * <b>Note</b>, that the adapter must be in {@link DialogSelectionAdapter#MULTIPLE} selection mode,
	 * otherwise {@code null} will be returned.
	 *
	 * @return List with selected items. Will be empty if no items are selected yet.
	 * @see #getSelectedItem()
	 * @see DialogSelectionAdapter#getSelectionMode()
	 */
	@Nullable public List<DialogSelectionAdapter.Item> getSelectedItems() {
		if (mAdapter instanceof DialogSelectionAdapter) {
			final DialogSelectionAdapter selectionAdapter = (DialogSelectionAdapter) mAdapter;
			if (selectionAdapter.getSelectionMode() == DialogSelectionAdapter.MULTIPLE) {
				return selectionAdapter.getSelectedItems();
			}
		}
		return null;
	}

	/**
	 * Returns an array containing ids for the current selected items within this dialog.
	 * <p>
	 * The requested ids will be obtained from the current adapter (if instance of {@link DialogSelectionAdapter})
	 * via {@link DialogSelectionAdapter#getSelection()}.
	 *
	 * @return Array with selected ids. Will be empty if no items are selected yet or the adapter
	 * is not available.
	 * @see #getSelectionSize()
	 */
	@NonNull public long[] getSelection() {
		return mAdapter instanceof DialogSelectionAdapter ? ((DialogSelectionAdapter) mAdapter).getSelection() : new long[0];
	}

	/**
	 * Returns the size of the current selection of this dialog.
	 *
	 * @return The count of the selected items.
	 * @see #getSelection()
	 */
	public int getSelectionSize() {
		return getSelection().length;
	}

	/**
	 */
	@Override @NonNull protected Parcelable onSaveInstanceState() {
		final SavedState state = new SavedState(super.onSaveInstanceState());
		state.selection = mAdapter instanceof DialogSelectionAdapter ? ((DialogSelectionAdapter) mAdapter).getSelection() : null;
		return state;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@NonNull final Parcelable savedState) {
		if (!(savedState instanceof SavedState)) {
			super.onRestoreInstanceState(savedState);
			return;
		}
		final SavedState state = (SavedState) savedState;
		super.onRestoreInstanceState(state.getSuperState());
		if (state.selection != null && mOptions instanceof SelectionOptions) {
			((SelectionOptions) mOptions).selection(state.selection);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link BaseAdapterOptions} implementation specific for the {@link SelectionDialog}.
	 * <p>
	 * Below are listed setters which may be used to supply desired data to {@link SelectionDialog}
	 * through these options:
	 * <ul>
	 * <li>{@link #items(DialogSelectionAdapter.Item[])}</li>
	 * <li>{@link #items(List)}</li>
	 * <li>{@link #items(int)}</li>
	 * <li>{@link #selection(long[])}</li>
	 * <li>{@link #selectionMode(int)}</li>
	 * <li>{@link #emptySelectionAllowed(boolean)}</li>
	 * </ul>
	 *
	 * <h3>Xml attributes</h3>
	 * See {@link R.styleable#Dialog_Options_Selection SelectionOptions Attributes},
	 * {@link R.styleable#Dialog_Options DialogOptions Attributes}
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SelectionOptions extends BaseAdapterOptions<SelectionOptions> {

		/**
		 * Creator used to create an instance or array of instances of SelectionOptions from {@link Parcel}.
		 */
		public static final Creator<SelectionOptions> CREATOR = new Creator<SelectionOptions>() {

			/**
			 */
			@Override public SelectionOptions createFromParcel(@NonNull final Parcel source) { return new SelectionOptions(source); }

			/**
			 */
			@Override public SelectionOptions[] newArray(final int size) { return new SelectionOptions[size]; }
		};

		/**
		 * Flag for <b>selection mode</b> parameter.
		 */
		static final int SELECTION_MODE = 0x00000001;

		/**
		 * Flag for <b>empty selection allowed</b> parameter.
		 */
		static final int EMPTY_SELECTION_ALLOWED = 0x00000001 << 1;

		/**
		 * Flag for <b>items</b> parameter.
		 */
		static final int ITEMS = 0x00000001 << 2;

		/**
		 * Flag for <b>selection</b> parameter.
		 */
		static final int SELECTION = 0x00000001 << 3;

		/**
		 * Selection mode for {@link DialogSelectionAdapter} adapter.
		 */
		int selectionMode = DialogSelectionAdapter.SINGLE;

		/**
		 * Flag indicating whether there can be zero items selected within adapter or not.
		 */
		boolean emptySelectionAllowed;

		/**
		 * Data set of items to be provided by selection adapter.
		 */
		List<DialogSelectionAdapter.Item> items;

		/**
		 * Set of ids for initial selection.
		 */
		long[] selection;

		/**
		 * Creates a new instance of SelectionOptions.
		 */
		public SelectionOptions() {
			super();
		}

		/**
		 * Creates a new instance of SelectionOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public SelectionOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of SelectionOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SelectionOptions(@NonNull final Parcel source) {
			super(source);
			this.selectionMode = source.readInt();
			this.emptySelectionAllowed = source.readInt() == 1;
			//noinspection unchecked
			this.items = source.readArrayList(DialogsConfig.class.getClassLoader());
			if (source.readInt() >= 0) {
				selection = new long[source.readInt()];
				source.readLongArray(selection);
			}
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(selectionMode);
			dest.writeInt(emptySelectionAllowed ? 1 : 0);
			dest.writeList(items);
			if (selection != null) {
				dest.writeInt(selection.length);
				dest.writeLongArray(selection);
			} else {
				dest.writeInt(-1);
			}
		}

		/**
		 */
		@Override public SelectionOptions merge(@NonNull final DialogOptions options) {
			if (!(options instanceof SelectionOptions)) {
				return super.merge(options);
			}
			final SelectionOptions selectionOptions = (SelectionOptions) options;
			if (selectionOptions.isSet(SELECTION_MODE)) {
				this.selectionMode = selectionOptions.selectionMode;
				updateIsSet(SELECTION_MODE);
			}
			if (selectionOptions.isSet(EMPTY_SELECTION_ALLOWED)) {
				this.emptySelectionAllowed = selectionOptions.emptySelectionAllowed;
				updateIsSet(EMPTY_SELECTION_ALLOWED);
			}
			if (selectionOptions.isSet(ITEMS)) {
				if (selectionOptions.items != null) {
					this.items = new ArrayList<>(selectionOptions.items);
				} else {
					this.items = null;
				}
				updateIsSet(ITEMS);
			}
			if (selectionOptions.isSet(SELECTION)) {
				if (selectionOptions.selection != null) {
					this.selection = new long[selectionOptions.selection.length];
					System.arraycopy(selectionOptions.selection, 0, selection, 0, selectionOptions.selection.length);
				} else {
					this.selection = null;
				}
				updateIsSet(SELECTION);
			}
			return super.merge(options);
		}

		/**
		 * Sets the selection mode for {@link DialogSelectionAdapter} implementation of a dialog
		 * associated with these options.
		 *
		 * @param mode The desired selection mode. One of {@link DialogSelectionAdapter#SINGLE SINGLE}
		 *             or {@link DialogSelectionAdapter#MULTIPLE MULTIPLE}.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogSelectionMode dialog:dialogSelectionMode
		 * @see #selectionMode()
		 */
		public SelectionOptions selectionMode(@DialogSelectionAdapter.SelectionMode final int mode) {
			updateIsSet(SELECTION_MODE);
			if (selectionMode != mode) {
				this.selectionMode = mode;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the selection mode, set to these option.
		 * <p>
		 * Default value: <b>{@link DialogSelectionAdapter#SINGLE SINGLE}</b>
		 *
		 * @return One of {@link DialogSelectionAdapter#SINGLE SINGLE}
		 * or {@link DialogSelectionAdapter#MULTIPLE MULTIPLE}.
		 * @see #selectionMode(int)
		 */
		@DialogSelectionAdapter.SelectionMode public int selectionMode() {
			return selectionMode;
		}

		/**
		 * Sets a flag indicating whether there can be none item selected within {@link DialogSelectionAdapter}
		 * implementation of a dialog associated with these options.
		 *
		 * @param allowed {@code True} if none item can be selected, {@code false} otherwise.
		 * @return These options to allow methods chaining.
		 * @see DialogSelectionAdapter#setEmptySelectionAllowed(boolean)
		 * @see R.attr#dialogEmptySelectionAllowed dialog:dialogEmptySelectionAllowed
		 * @see #shouldAllowEmptySelection()
		 */
		public SelectionOptions emptySelectionAllowed(final boolean allowed) {
			updateIsSet(EMPTY_SELECTION_ALLOWED);
			if (emptySelectionAllowed != allowed) {
				this.emptySelectionAllowed = allowed;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns a flag indicating whether empty selection is allowed for {@link DialogSelectionAdapter}
		 * implementation, set to these options.
		 * <p>
		 * Default value: <b>false</b>
		 *
		 * @return {@code True} if it is allowed, {@code false} otherwise.
		 * @see #emptySelectionAllowed(boolean)
		 */
		public boolean shouldAllowEmptySelection() {
			return emptySelectionAllowed;
		}

		/**
		 * Same as {@link #items(List)}, so string array obtained by the specified <var>resId</var>
		 * will be converted into list of {@link DialogSelectionAdapter.Item} items, where each of
		 * these items will have its id set according to its position within the requested string array.
		 *
		 * @param resId Resource id of the desired string array with items data set.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogItems dialog:dialogItems
		 * @see #items()
		 */
		public SelectionOptions items(@ArrayRes final int resId) {
			checkRequiredResources();
			return items(itemsFromStringArray(resId));
		}

		/**
		 * Same as {@link #items(List)} for array.
		 *
		 * @see #items()
		 */
		public SelectionOptions items(@NonNull final DialogSelectionAdapter.Item[] items) {
			return items(Arrays.asList(items));
		}

		/**
		 * Sets the data set of items for {@link DialogSelectionAdapter} implementation of a dialog
		 * associated with these options.
		 *
		 * @param items The desired list of items.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogItems dialog:dialogItems
		 * @see #items()
		 */
		public SelectionOptions items(@NonNull final List<DialogSelectionAdapter.Item> items) {
			updateIsSet(ITEMS);
			this.items = items;
			notifyChanged();
			return this;
		}

		/**
		 * Returns the items, set to these options.
		 *
		 * @return List of items.
		 * @see #items(int)
		 * @see #items(List)
		 * @see #items(DialogSelectionAdapter.Item[])
		 */
		@Nullable public List<DialogSelectionAdapter.Item> items() {
			return items;
		}

		/**
		 * Sets the initial selection for {@link DialogSelectionAdapter} implementation of a dialog
		 * associated with these options.
		 *
		 * @param ids Array with ids of initially selected items.
		 * @return These options to allow methods chaining.
		 * @see #selection()
		 */
		public SelectionOptions selection(@NonNull final long[] ids) {
			updateIsSet(SELECTION);
			this.selection = ids;
			notifyChanged();
			return this;
		}

		/**
		 * Returns the initial selection, set to these options.
		 *
		 * @return Array with initially selected ids.
		 * @see #selection(long[])
		 */
		@Nullable public long[] selection() {
			return selection;
		}

		/**
		 */
		@SuppressWarnings("ResourceType")
		@Override protected void onParseXmlAttribute(
				@NonNull final XmlResourceParser xmlParser,
				final int index,
				@AttrRes final int attr,
				@NonNull final Resources resources,
				@Nullable final Resources.Theme theme
		) {
			if (attr == R.attr.dialogSelectionMode) {
				selectionMode(xmlParser.getAttributeIntValue(index, selectionMode));
			} else if (attr == R.attr.dialogEmptySelectionAllowed) {
				emptySelectionAllowed(xmlParser.getAttributeBooleanValue(index, emptySelectionAllowed));
			} else if (attr == R.attr.dialogItems) {
				final int arrayResId = xmlParser.getAttributeResourceValue(index, -1);
				if (arrayResId != -1) items(arrayResId);
			} else {
				super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
			}
		}

		/**
		 * Transforms a string array obtained by the specified <var>resId</var> into list of
		 * {@link DialogSelectionAdapter.Item} implementations.
		 *
		 * @param resId Resource id of the desired string array with items data set.
		 * @return List with string items wrapped into {@link DialogSelectionAdapter.Item} implementation.
		 */
		List<DialogSelectionAdapter.Item> itemsFromStringArray(final int resId) {
			final CharSequence[] itemsArray = mResources.getTextArray(resId);
			if (itemsArray.length > 0) {
				final List<DialogSelectionAdapter.Item> items = new ArrayList<>(itemsArray.length);
				for (int i = 0; i < itemsArray.length; i++) {
					items.add(new TextItem(i + 1, itemsArray[i]));
				}
				return items;
			}
			return null;
		}
	}

	/**
	 * A {@link DialogBaseSelectionAdapter} implementation used by {@link SelectionDialog} as adapter
	 * that provides data set of selectable items.
	 *
	 * @param <I> Type of the selection item provided by this adapter.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class ItemsAdapter<I extends DialogSelectionAdapter.Item> extends DialogBaseSelectionAdapter<I> {

		/**
		 * See {@link DialogBaseSelectionAdapter#DialogBaseSelectionAdapter(Context, LayoutInflater)}.
		 */
		public ItemsAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater) {
			super(context, inflater);
		}

		/**
		 * See {@link DialogBaseSelectionAdapter#DialogBaseSelectionAdapter(Context, LayoutInflater, List)}.
		 */
		public ItemsAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @NonNull final List<I> items) {
			super(context, inflater, items);
		}

		/**
		 * See {@link DialogBaseSelectionAdapter#DialogBaseSelectionAdapter(Context, LayoutInflater, Item[])}.
		 */
		public ItemsAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @NonNull final I[] items) {
			super(context, inflater, items);
		}

		/**
		 */
		@Override protected void onBindViewHolder(@NonNull final ItemHolder viewHolder, final boolean selected, final int position) {
			viewHolder.textView.setText(getItem(position).getText());
			if (viewHolder.textView instanceof CompoundButton) {
				((CompoundButton) viewHolder.textView).setChecked(selected);
			}
		}
	}

	/**
	 * Simple implementation of {@link DialogSelectionAdapter.Item} to wrap string into selectable
	 * item.
	 */
	public static final class TextItem implements DialogSelectionAdapter.Item {

		/**
		 * Creator used to create an instance or array of instances of TextItem from {@link Parcel}.
		 */
		public static final Creator<TextItem> CREATOR = new Creator<TextItem>() {

			/**
			 */
			@Override public TextItem createFromParcel(@NonNull final Parcel source) { return new TextItem(source); }

			/**
			 */
			@Override public TextItem[] newArray(final int size) { return new TextItem[size]; }
		};

		/**
		 * Id of this item.
		 */
		final int id;

		/**
		 * Text of this item.
		 */
		final CharSequence text;

		/**
		 * Creates a new instance of TextItem with the specified <var>id</var> and <var>text</var>.
		 *
		 * @param id   Id for the new item.
		 * @param text Text for the new item.
		 */
		public TextItem(final int id, @NonNull final CharSequence text) {
			this.id = id;
			this.text = text;
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of TextItem form the given parcel <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		private TextItem(@NonNull final Parcel source) {
			this.id = source.readInt();
            this.text = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			dest.writeInt(id);
			TextUtils.writeToParcel(text, dest, flags);
		}

		/**
		 */
		@Override public String toString() {
			return text.toString();
		}

		/**
		 */
		@Override public long getId() {
			return id;
		}

		/**
		 */
		@Override @NonNull public CharSequence getText() {
			return text;
		}

		/**
		 */
		@Override public int describeContents() {
			return 0;
		}
	}

	/**
	 * A {@link BaseSavedState} implementation used to ensure that the state of {@link SelectionDialog}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends BaseSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		long[] selection;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.selection = new long[source.readInt()];
			source.readLongArray(selection);
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(selection != null ? selection.length : 0);
			dest.writeLongArray(selection);
		}
	}
}