/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.adapter;

import android.os.Parcelable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.SelectionDialog;

/**
 * Required interface for adapters tend to be used within {@link SelectionDialog SelectionDialog}
 * to provide data set of selectable items for such a dialog.
 * <p>
 * The SelectionDialog will use {@link #toggleItemSelection(long)} to update current selection state
 * for the selected item. The adapter implementation will be also used to access current selected
 * items via {@link #getSelectedItems()} or current selection via {@link #getSelection()}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface DialogSelectionAdapter {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 */
	int NONE = 0;

	/**
	 * Mode that allows only a single item to be selected.
	 */
	int SINGLE = 1;

	/**
	 * Mode that allows multiple items to be selected.
	 */
	int MULTIPLE = 2;

	/**
	 * Defines an annotation for determining set of allowed modes for {@link #setSelectionMode(int)}
	 * method.
	 */
	@IntDef({NONE, SINGLE, MULTIPLE})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface SelectionMode {}

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Base interface for selectable items used within data set of {@link DialogSelectionAdapter}
	 * implementations.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Item extends Parcelable {

		/**
		 * Returns the id of this item.
		 *
		 * @return This item's id.
		 */
		long getId();

		/**
		 * Returns the display text of this item.
		 *
		 * @return This item's text.
		 */
		@NonNull CharSequence getText();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets the selection mode for this adapter. This mode determines whether multiple items can be
	 * selected within this adapter or only single one.
	 *
	 * @param mode One of {@link #SINGLE} or {@link #MULTIPLE}.
	 * @see #getSelectionMode()
	 * @see #setEmptySelectionAllowed(boolean)
	 */
	void setSelectionMode(@SelectionMode int mode);

	/**
	 * Returns the current selection mode of this adapter.
	 *
	 * @return One of {@link #SINGLE} or {@link #MULTIPLE}.
	 * @see #setSelectionMode(int)
	 */
	@SelectionMode int getSelectionMode();

	/**
	 * Sets a flag indicating whether this adapter allows empty selection or not. That's it, if none
	 * item can be selected within this adapter or not.
	 *
	 * @param allowed {@code True} to allow empty selection, {@code false} to be always at least one
	 *                item selected, depends on the current selection mode.
	 * @see #setSelectionMode(int)
	 * @see #isEmptySelectionAllowed()
	 */
	void setEmptySelectionAllowed(boolean allowed);

	/**
	 * Returns a flag indicating whether this adapter allows empty selection or not.
	 *
	 * @return {@code True} if empty selection is allowed, {@code false} otherwise.
	 * @see #setEmptySelectionAllowed(boolean)
	 */
	boolean isEmptySelectionAllowed();

	/**
	 * Changes selection state of an item with the specified <var>id</var> to the opposite one
	 * ({@code selected -> unselected; unselected -> selected}).
	 * <p>
	 * <b>Note</b>, that if selection mode of this adapter is {@link #SINGLE} and
	 * {@link #setEmptySelectionAllowed(boolean)} has been requested with {@code false} and this is
	 * called for the already selected position, nothing will change.
	 *
	 * @param id The id of an item of which selection state to toggle.
	 * @return Size of the current selection.
	 * @see #isItemSelected(long)
	 * @see #getSelection()
	 */
	int toggleItemSelection(long id);

	/**
	 * Checks whether an item with the specified <var>id</var> is selected or not.
	 *
	 * @param id The id of the desired item of which selection to check.
	 * @return {@code True} if item with the id is selected, {@code false} otherwise.
	 * @see #toggleItemSelection(long)
	 * @see #getSelection()
	 */
	boolean isItemSelected(long id);

	/**
	 * @see #getSelectedItems()
	 */
	@Nullable Item getItem(int position);

	/**
	 * Returns the current selected item within this adapter.
	 * <p>
	 * <b>Note</b>, that this method should be called only when selection mode is set to {@link #SINGLE}
	 * otherwise an exception will be thrown.
	 *
	 * @return Selected item or {@code null} if no item has been selected yet.
	 * @see #toggleItemSelection(long)
	 * @see #getSelectedItems()
	 * @see #getSelectionMode()
	 */
	@Nullable Item getSelectedItem();

	/**
	 * Returns a set of the current selected items within this adapter.
	 * <p>
	 * <b>Note</b>, that this method should be called only when selection mode is set to {@link #MULTIPLE}
	 * otherwise an exception will be thrown.
	 *
	 * @return List with selected items. Will be empty if no items are selected yet.
	 * @see #toggleItemSelection(long)
	 * @see #getSelection()
	 * @see #getSelectionMode()
	 */
	@NonNull List<Item> getSelectedItems();

	/**
	 * Sets the initial selection for this adapter. <b>Note</b>, that this should be called only to
	 * supply initial selection not to change current selection of this adapter.
	 * <p>
	 * <b>Also note</b>, that this can be requested only upon none empty adapter to properly resolve
	 * also positions of the specified ids within the current data set.
	 *
	 * @param ids Array of ids of items which should be selected.
	 * @see #getSelection()
	 * @see #clearSelection()
	 * @see #toggleItemSelection(long)
	 */
	void setSelection(@NonNull long[] ids);

	/**
	 * Returns a set of ids of the current selected items within this adapter.
	 *
	 * @return Array with selected ids. Will be empty if no items are selected yet.
	 * @see #setSelection(long[])
	 */
	@NonNull long[] getSelection();

	/**
	 * Clears the current selection of this adapter. This will clear all current selected ids and
	 * will cause {@code notifyDataSetChanged()}.
	 *
	 * @see #setSelection(long[])
	 */
	void clearSelection();
}