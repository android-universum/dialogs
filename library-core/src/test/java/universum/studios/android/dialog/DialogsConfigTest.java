/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import org.junit.Test;

import universum.studios.android.testing.TestCase;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Martin Albedinsky
 */
public class DialogsConfigTest implements TestCase {

	@Test public void configuration() {
		// Assert:
		assertTrue(DialogsConfig.LOG_ENABLED);
		assertFalse(DialogsConfig.DEBUG_LOG_ENABLED);
		assertTrue(DialogsConfig.ANNOTATIONS_PROCESSING_ENABLED);
	}
}