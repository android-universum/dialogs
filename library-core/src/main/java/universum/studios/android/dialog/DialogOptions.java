/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.ArrayRes;
import androidx.annotation.AttrRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntDef;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.DialogButtonsView;

/**
 * DialogOptions class is used to supply options to a specific {@link Dialog} implementation.
 * Data within these options are then used to bind such a dialog with content related to that type
 * of dialog (like title, buttons, content message, icon, ...).
 * <p>
 * For simple dialog implementations these options class provides following setters that can be
 * used to supply the desired data for the dialog:
 * <ul>
 * <li>{@link #theme(int)}</li>
 * <li>{@link #icon(int)}</li>
 * <li>{@link #vectorIcon(int)}</li>
 * <li>{@link #title(CharSequence)}</li>
 * <li>{@link #title(int)}</li>
 * <li>{@link #content(CharSequence)}</li>
 * <li>{@link #content(int)}</li>
 * <li>{@link #contentFlag(int)}</li>
 * <li>{@link #contentFlags(int)}</li>
 * <li>{@link #positiveButton(CharSequence)}</li>
 * <li>{@link #positiveButton(int)}</li>
 * <li>{@link #negativeButton(CharSequence)}</li>
 * <li>{@link #negativeButton(int)}</li>
 * <li>{@link #neutralButton(CharSequence)}</li>
 * <li>{@link #neutralButton(int)}</li>
 * <li>{@link #infoButton(CharSequence)}</li>
 * <li>{@link #infoButton(int)}</li>
 * <li>{@link #button(DialogOptions.Button)}</li>
 * <li>{@link #buttonsWidthMode(int)}</li>
 * <li>{@link #cancelable(boolean)}</li>
 * <li>{@link #remain(boolean)}</li>
 * <li>{@link #dismissOnRestore(boolean)}</li>
 * <li>{@link #bundle(android.os.Bundle)}</li>
 * </ul>
 *
 * <h3>String array</h3>
 * A simple options with title, content and buttons can be created from a simple string array placed
 * within an application resources via {@link #fromStringArray(int)}. Below are listed some example
 * string arrays that will be accepted:
 * <p>
 * <pre>
 *  &lt;string-array name="dialog_error"&gt;
 *      &lt;!-- Content message --&gt;
 *      &lt;item&gt;Server error occurred. Please try again later.&lt;/item&gt;
 *      &lt;!-- Title (only supplied so the next item can be parsed as neutral button) --&gt;
 *      &lt;item&gt;&#64;null&lt;/item&gt;
 *      &lt;!-- Neutral button --&gt;
 *      &lt;item&gt;OK&lt;/item&gt;
 *  &lt;/string-array&gt;
 *
 *  &lt;string-array name="dialog_sing_in_success"&gt;
 *      &lt;!-- Content message --&gt;
 *      &lt;item&gt;You have been successfully signed up. Would you like to update your profile?&lt;/item&gt;
 *      &lt;!-- Title --&gt;
 *      &lt;item&gt;Sign Up&lt;/item&gt;
 *      &lt;!-- Negative button --&gt;
 *      &lt;item&gt;No&lt;/item&gt;
 *      &lt;!-- Positive button --&gt;
 *      &lt;item&gt;Update profile&lt;/item&gt;
 *  &lt;/string-array&gt;
 * </pre>
 * See {@link #fromStringArray(int)} description for detailed info about parsing logic.
 *
 * <h3>Inflating</h3>
 * An instance of DialogOptions can be inflated via {@link #inflate(XmlResourceParser, Resources, Resources.Theme)}
 * where the specified XmlResourceParser should contain data for the new options. Inheritance hierarchies
 * of DialogOptions that want to parse theirs related attributes from Xml should override
 * {@link #onParseXmlAttribute(XmlResourceParser, int, int, Resources, Resources.Theme)} and do the
 * parsing logic there. The next section lists attributes that are supported via Xml by DialogOptions
 * class.
 *
 * <h3>Xml attributes</h3>
 * See {@link R.styleable#Dialog_Options DialogOptions Attributes}
 *
 * @param <O> Type of the DialogOptions implementation to allow proper methods chaining.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("unchecked")
public class DialogOptions<O extends DialogOptions> implements Parcelable {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogOptions";

	/**
	 * Flag indicating that a content specified via {@link #content(CharSequence)} should be trimmed
	 * before it is set to the view that will present it.
	 */
	public static final int CONTENT_TRIM = 0x00000001;

	/**
	 * Flag indicating that a content specified via {@link #content(CharSequence)} should be presented
	 * within a view as <b>HTML</b> content.
	 * <p>
	 * For this matter will be used {@link android.text.Html#fromHtml(String) Html.fromHtml(String)}
	 * so the specified content text will be converted into string.
	 */
	public static final int CONTENT_AS_HTML = 0x00000001 << 1;

	/**
	 * Defines an annotation for determining set of allowed content flags.
	 */
	@IntDef(flag = true, value = {
			CONTENT_TRIM,
			CONTENT_AS_HTML
	})
	@Retention(RetentionPolicy.SOURCE)
	public @interface ContentFlag {}

	/**
	 * Flag for <b>theme</b> parameter.
	 */
	static final int THEME = 0x00000001;

	/**
	 * Flag for <b>icon</b> parameter.
	 */
	static final int ICON = 0x00000001 << 1;

	/**
	 * Flag for <b>vector icon</b> parameter.
	 */
	static final int VECTOR_ICON = 0x00000001 << 2;

	/**
	 * Flag for <b>title</b> parameter.
	 */
	static final int TITLE = 0x00000001 << 3;

	/**
	 * Flag for <b>content</b> parameter.
	 */
	static final int CONTENT = 0x00000001 << 4;

	/**
	 * Flag for <b>content flags</b> parameter.
	 */
	static final int CONTENT_FLAGS = 0x00000001 << 5;

	/**
	 * Flag for <b>buttons</b> parameter.
	 */
	static final int BUTTONS = 0x00000001 << 6;

	/**
	 * Flag for <b>buttons width mode</b> parameter.
	 */
	static final int BUTTONS_WIDTH_MODE = 0x00000001 << 7;

	/**
	 * Flag for <b>bundle</b> parameter.
	 */
	static final int BUNDLE = 0x00000001 << 8;

	/**
	 * Flag for <b>cancelable</b> parameter.
	 */
	static final int CANCELABLE = 0x00000001 << 9;

	/**
	 * Flag for <b>cancelable</b> parameter.
	 */
	static final int DISMISS_ON_RESTORE = 0x00000001 << 10;

	/**
	 * Flag for <b>cancelable</b> parameter.
	 */
	static final int REMAIN = 0x00000001 << 11;

	/**
	 * Flag indicating whether a dialog is cancelable or not.
	 */
	private static final int FLAG_CANCELABLE = 0x00000001;

	/**
	 * Flag indicating whether to dismiss a dialog after it was restored.
	 */
	private static final int FLAG_DISMISS_ON_RESTORE = 0x00000001 << 1;

	/**
	 * Flag indicating whether a dialog should remain visible on the screen after click on any button
	 * except {@link Dialog#BUTTON_NEGATIVE BUTTON_NEGATIVE}.
	 */
	private static final int FLAG_REMAIN = 0x00000001 << 2;

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Callback that can be used to listen for changes within a particular DialogOptions.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface Callback {

		/**
		 * Invoked whenever a value within the given <var>options</var> has been changed.
		 *
		 * @param options Changed options.
		 */
		void onOptionsChanged(@NonNull DialogOptions options);
	}

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Creator used to create an instance or array of instances of DialogOptions from {@link android.os.Parcel}.
	 */
	public static final Parcelable.Creator<DialogOptions> CREATOR = new Parcelable.Creator<DialogOptions>() {

		/**
		 */
		@Override public DialogOptions createFromParcel(@NonNull final Parcel source) { return new DialogOptions(source); }

		/**
		 */
		@Override public DialogOptions[] newArray(final int size) { return new DialogOptions[size]; }
	};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Application resources.
	 */
	Resources mResources;

	/**
	 * Theme for the dialog. This theme can be used to specify a theme attributes specific for a single
	 * dialog.
	 */
	int mTheme;

	/**
	 * Icon resource id.
	 */
	int mIcon = -1;

	/**
	 * Vector icon resource id.
	 */
	int mVectorIcon = -1;

	/**
	 * Title text.
	 */
	CharSequence mTitle;

	/**
	 * Content (message) text.
	 */
	CharSequence mContent;

	/**
	 * Set of content flags determining how to treat a content specified via {@link #content(CharSequence)}.
	 */
	int mContentFlags = CONTENT_TRIM;

	/**
	 * Resource for dialog's content layout.
	 */
	int mContentLayout;

	/**
	 * List of buttons for these options.
	 */
	List<Button> mButtons;

	/**
	 * Width mode for buttons.
	 */
	int mButtonsWidthMode = DialogButtonsView.WIDTH_MODE_WRAP_CONTENT;

	/**
	 * Bundle with custom data.
	 */
	Bundle mBundle;

	/**
	 * Set of flags determining which basic option values has been specified for these options.
	 */
	int mSetBasicOptions;

	/**
	 * Set of flags determining which option values has been specified for these options.
	 */
	int mSetOptions;

	/**
	 * Callback used to notify listener that some of values of these options has been changed.
	 */
	Callback mCallback;

	/**
	 * Set of the flags specified for these options.
	 */
	private int mFlags;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DialogOptions.
	 *
	 * @see #DialogOptions(android.content.res.Resources)
	 */
	public DialogOptions() {
		this(FLAG_CANCELABLE);
	}

	/**
	 * Creates a new instance of DialogOptions with the given resources.
	 *
	 * @param resources Application resources used to obtain values for these options when they
	 *                  are specified by theirs resource ids.
	 */
	public DialogOptions(@NonNull final Resources resources) {
		this(FLAG_CANCELABLE);
		this.mResources = resources;
	}

	/**
	 * Called form {@link #CREATOR} to create an instance of DialogOptions form the given parcel
	 * <var>source</var>.
	 *
	 * @param source Parcel with data for the new instance.
	 */
	protected DialogOptions(@NonNull final Parcel source) {
		this(0);
		this.mTheme = source.readInt();
		this.mIcon = source.readInt();
		this.mVectorIcon = source.readInt();
		this.mTitle = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
		this.mContent = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
		this.mContentFlags = source.readInt();
		this.mContentLayout = source.readInt();
		final int n = source.readInt();
		if (n > 0) {
			this.ensureButtons();
			for (int i = 0; i < n; i++) {
				mButtons.add(new Button(source));
			}
		}
		this.mButtonsWidthMode = source.readInt();
		this.mBundle = source.readBundle(DialogsConfig.class.getClassLoader());
		this.mSetBasicOptions = source.readInt();
		this.mSetOptions = source.readInt();
		this.mFlags = source.readInt();
	}

	/**
	 * Creates a new instance of DialogOptions with the specified flags.
	 *
	 * @param flags Initial flags.
	 */
	private DialogOptions(final int flags) {
		this.mFlags = flags;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
		dest.writeInt(mTheme);
		dest.writeInt(mIcon);
		dest.writeInt(mVectorIcon);
		TextUtils.writeToParcel(mTitle, dest, flags);
		TextUtils.writeToParcel(mContent, dest, flags);
		dest.writeInt(mContentFlags);
		dest.writeInt(mContentLayout);
		if (mButtons != null) {
			dest.writeInt(mButtons.size());
			for (Button button : mButtons) {
				button.writeToParcel(dest, flags);
			}
		} else {
			dest.writeInt(0);
		}
		dest.writeInt(mButtonsWidthMode);
		dest.writeBundle(mBundle);
		dest.writeInt(mSetBasicOptions);
		dest.writeInt(mSetOptions);
		dest.writeInt(mFlags);
	}

	/**
	 * Inflates data for these options from the given Xml <var>parser</var>.
	 *
	 * @param parser    Xml parser with data from which to inflate these options.
	 * @param resources Application resources used to resolve attribute values.
	 * @param theme     Theme to apply to the attribute values. May be {@code null}.
	 * @return These options to allow methods chaining.
	 */
	@SuppressWarnings("unchecked")
	public O inflate(@NonNull final XmlResourceParser parser, @NonNull final Resources resources, @Nullable final Resources.Theme theme) {
		final int attrCount = parser.getAttributeCount();
		for (int i = 0; i < attrCount; i++) {
			onParseXmlAttribute(parser, i, parser.getAttributeNameResource(i), resources, theme);
		}
		return (O) this;
	}

	/**
	 * Invoked for each index of the currently being processed {@code XmlResourceParser} passed to
	 * {@link #inflate(XmlResourceParser, Resources, Resources.Theme)} method.
	 *
	 * @param xmlParser Xml parser with data for these options.
	 * @param index     The index of attribute to parse within the passed <var>xmlParser</var>.
	 * @param attr      The id of attribute to parse, like {@link android.R.attr#textSize}.
	 * @param resources Application resources to be used to resolve attribute values.
	 * @param theme     Theme to apply to the attribute values. May be {@code null}.
	 */
	@SuppressWarnings({"ResourceType", "ConstantConditions"})
	protected void onParseXmlAttribute(
			@NonNull final XmlResourceParser xmlParser,
			final int index,
			@AttrRes final int attr,
			@NonNull final Resources resources,
			@Nullable final Resources.Theme theme
	) {
		if (attr == R.attr.dialogStringOptions) {
			final int arrayRes = xmlParser.getAttributeResourceValue(index, -1);
			if (arrayRes != -1) fromStringArray(arrayRes);
		} else if (attr == R.attr.dialogIcon) {
			icon(xmlParser.getAttributeResourceValue(index, 0));
		} else if (attr == R.attr.dialogVectorIcon) {
			vectorIcon(xmlParser.getAttributeResourceValue(index, 0));
		} else if (attr == R.attr.dialogTitle) {
			title(obtainXmlAttributeText(xmlParser, index, resources));
		} else if (attr == R.attr.dialogContent) {
			content(obtainXmlAttributeText(xmlParser, index, resources));
		} else if (attr == R.attr.dialogContentFlags) {
			contentFlags(xmlParser.getAttributeIntValue(index, mContentFlags));
		} else if (attr == R.attr.dialogContentLayout) {
			contentLayout(xmlParser.getAttributeResourceValue(index, mContentLayout));
		} else if (attr == R.attr.dialogNegativeButton) {
			negativeButton(obtainXmlAttributeText(xmlParser, index, resources));
		} else if (attr == R.attr.dialogNeutralButton) {
			neutralButton(obtainXmlAttributeText(xmlParser, index, resources));
		} else if (attr == R.attr.dialogPositiveButton) {
			positiveButton(obtainXmlAttributeText(xmlParser, index, resources));
		} else if (attr == R.attr.dialogInfoButton) {
			infoButton(obtainXmlAttributeText(xmlParser, index, resources));
		} else if (attr == R.attr.dialogButtonsWidthMode) {
			buttonsWidthMode(xmlParser.getAttributeIntValue(index, mButtonsWidthMode));
		} else if (attr == R.attr.dialogCancelable) {
			cancelable(xmlParser.getAttributeBooleanValue(index, true));
		} else if (attr == R.attr.dialogDismissOnRestore) {
			dismissOnRestore(xmlParser.getAttributeBooleanValue(index, false));
		} else if (attr == R.attr.dialogRemain) {
			remain(xmlParser.getAttributeBooleanValue(index, false));
		} else if (attr == R.attr.dialogTheme) {
			theme(xmlParser.getAttributeResourceValue(index, mTheme));
		}
	}

	/**
	 * Obtains a {@link String} value from the given <var>parser</var> for the specified index.
	 *
	 * @param parser    Xml parser with data for these options.
	 * @param index     The index of attribute which holds the string to obtain from the passed <var>xmlParser</var>.
	 * @param resources Application resources.
	 * @return String value.
	 * @see #obtainXmlAttributeText(XmlResourceParser, int, Resources)
	 */
	@Nullable protected static String obtainXmlAttributeString(@NonNull final XmlResourceParser parser, final int index, @NonNull final Resources resources) {
		String string;
		final int stringRes = parser.getAttributeResourceValue(index, -1);
		switch (stringRes) {
			case -1:
				string = parser.getAttributeValue(index);
				break;
			case 0:
				string = null;
				break;
			default:
				string = resources.getString(stringRes);
				break;
		}
		return string;
	}

	/**
	 * Obtains a {@link CharSequence} value from the given <var>parser</var> for the specified index.
	 *
	 * @param parser    Xml parser with data for these options.
	 * @param index     The index of attribute which holds the text to obtain from the passed <var>xmlParser</var>.
	 * @param resources Application resources.
	 * @return CharSequence value.
	 * @see #obtainXmlAttributeString(XmlResourceParser, int, Resources)
	 */
	@Nullable protected static CharSequence obtainXmlAttributeText(@NonNull final XmlResourceParser parser, final int index, @NonNull final Resources resources) {
		CharSequence text;
		final int stringRes = parser.getAttributeResourceValue(index, -1);
		switch (stringRes) {
			case -1:
				text = parser.getAttributeValue(index);
				break;
			case 0:
				text = null;
				break;
			default:
				text = resources.getText(stringRes);
				break;
		}
		return text;
	}

	/**
	 * Fills this instance of dialog options with data parsed from string array obtained by the
	 * specified <var>resId</var> from the current resources.
	 * <h3>Data parsing:</h3>
	 * Options are filled from string array depends on its length as described below:
	 * <pre>
	 * switch (array.length) {
	 *      case 1:
	 *          // Only content[0] for the dialog is presented.
	 *          break;
	 *      case 2:
	 *          // Content[0] and title[1] for the dialog are presented.
	 *          break;
	 *      case 3:
	 *          // Content[0], title[1] and also neutral[2] button for dialog
	 *          // are presented.
	 *          break;
	 *      case 4:
	 *          // Content[0], title[1] and also negative[2] and positive[3] button
	 *          // for dialog are presented.
	 *          break;
	 *      default:
	 *          // All text elements for dialog are presented. Options
	 *          // will contains content[0], title[1], negative[2], neutral[3]
	 *          // and positive[4] button
	 *          break;
	 * }
	 * </pre>
	 * <p>
	 * <b>Note</b>, that if you want to create dialog without title but with content and also some
	 * buttons, you should place at the title position {@code @null}.
	 *
	 * @param resId Resource id of the desired string array with data for these options.
	 * @return These options with parsed data.
	 */
	public O fromStringArray(@ArrayRes final int resId) {
		this.checkRequiredResources();
		final String[] stringArray = mResources.getStringArray(resId);
		/*
		 * <pre>
		 * Resolve options from the array depends on the array length:
		 * ==1 => There is only value for content presented in the array.
		 * ==2 => There is presented value for content and title in the array.
		 * >=3 => There are presented all base values in the array to build base dialog options.
		 * </pre>
		 */
		final int length = stringArray.length;
		if (length > 0) {
			content(stringArray[0]);
		}
		if (length > 1) {
			title(stringArray[1]);
		}
		if (length > 2) {
			switch (length) {
				case 3:
					neutralButton(stringArray[2]);
					break;
				case 4:
					negativeButton(stringArray[2]);
					positiveButton(stringArray[3]);
					break;
				case 5:
					negativeButton(stringArray[2]);
					neutralButton(stringArray[3]);
					positiveButton(stringArray[4]);
					break;
			}
		}
		return (O) this;
	}

	/**
	 * Merges these options instance with the given <var>options</var>. This performs only simple
	 * merging logic in that way, that value of each parameter hold by the given options will be set
	 * to the same parameter of these options only if it was already set to the given options.
	 * <p>
	 * In other words, after merge, these options will at least have specified values for same parameters
	 * as specified for the given options.
	 *
	 * @param options The desired options to merge with.
	 * @return These options to allow methods chaining.
	 */
	public O merge(@NonNull final DialogOptions options) {
		if (options.isBasicSet(ICON)) {
			this.mIcon = options.mIcon;
			updateBasicIsSet(ICON);
		}
		if (options.isBasicSet(VECTOR_ICON)) {
			this.mVectorIcon = options.mVectorIcon;
			updateBasicIsSet(VECTOR_ICON);
		}
		if (options.isBasicSet(TITLE)) {
			if (options.mTitle != null) {
				this.mTitle = new SpannableStringBuilder(options.mTitle);
			} else {
				this.mTitle = null;
			}
			updateBasicIsSet(TITLE);
		}
		if (options.isBasicSet(CONTENT)) {
			if (options.mContent != null) {
				this.mContent = new SpannableStringBuilder(options.mContent);
			} else {
				this.mContent = null;
			}
			updateBasicIsSet(CONTENT);
		}
		if (options.isBasicSet(BUTTONS)) {
			this.ensureButtons();
			for (int i = 0; i < options.mButtons.size(); i++) {
				final Button button = (Button) options.mButtons.get(i);
				mButtons.add(new Button(button.id, button.text));
			}
		}
		if (options.isBasicSet(BUTTONS_WIDTH_MODE)) {
			this.mButtonsWidthMode = options.mButtonsWidthMode;
			updateBasicIsSet(BUTTONS_WIDTH_MODE);
		}
		if (options.isBasicSet(CANCELABLE)) {
			this.updatePrivateFlags(FLAG_CANCELABLE, options.shouldBeCancelable());
			updateBasicIsSet(CANCELABLE);
		}
		if (options.isBasicSet(DISMISS_ON_RESTORE)) {
			this.updatePrivateFlags(FLAG_DISMISS_ON_RESTORE, options.shouldDismissOnRestore());
			updateBasicIsSet(DISMISS_ON_RESTORE);
		}
		if (options.isBasicSet(REMAIN)) {
			this.updatePrivateFlags(FLAG_REMAIN, options.shouldRemain());
			updateBasicIsSet(REMAIN);
		}
		if (options.isBasicSet(BUNDLE)) {
			if (options.mBundle != null) {
				this.mBundle = new Bundle(options.mBundle);
			} else {
				this.mBundle = null;
			}
			updateBasicIsSet(BUNDLE);
		}
		if (options.isBasicSet(CONTENT_FLAGS)) {
			this.mContentFlags = options.mContentFlags;
			updateBasicIsSet(CONTENT_FLAGS);
		}
		if (options.isBasicSet(THEME)) {
			this.mTheme = options.mTheme;
			updateBasicIsSet(THEME);
		}
		return (O) this;
	}

	/**
	 * Updates the current "is set" basic options.
	 *
	 * @param option Flag of the desired option parameter to add to the current "is set" options.
	 * @see #isBasicSet(int)
	 */
	private void updateBasicIsSet(final int option) {
		this.mSetBasicOptions |= option;
	}

	/**
	 * Returns a boolean flag indicating whether a value for the specified <var>option</var> has been
	 * set to these options or not.
	 *
	 * @param option Flag of the option to check.
	 * @return {@code True} if the requested option flag is set, {@code false} otherwise.
	 * @see #updateBasicIsSet(int)
	 */
	boolean isBasicSet(final int option) {
		return (mSetBasicOptions & option) != 0;
	}

	/**
	 * Updates the current flags for set options.
	 * <p>
	 * This method may be used by inheritance hierarchies to store set option flags for theirs
	 * related/additional options.
	 *
	 * @param option Flag of the desired option parameter to add to the current set options.
	 * @see #isSet(int)
	 */
	final void updateIsSet(final int option) {
		this.mSetOptions |= option;
	}

	/**
	 * Returns a boolean flag indicating whether a value for the specified <var>option</var> has been
	 * set to these options or not.
	 * <p>
	 * This method may be used by inheritance hierarchies to check if flag for theirs related/additional
	 * option is set.
	 *
	 * @param option Flag of the option to check.
	 * @return {@code True} if the requested option flag is set, {@code false} otherwise.
	 * @see #updateIsSet(int)
	 */
	final boolean isSet(final int option) {
		return (mSetOptions & option) != 0;
	}

	/**
	 * Creates a new copy of the specified <var>charSequence</var>.
	 *
	 * @param charSequence The char sequence of which copy to create.
	 * @return The new copy or {@code null} if the specified char sequence is <b>empty or null</b>.
	 */
	@Nullable protected final CharSequence copyCharSequence(@Nullable final CharSequence charSequence) {
		return !TextUtils.isEmpty(charSequence) ? new SpannableStringBuilder(charSequence) : null;
	}

	/**
	 */
	@Override public int describeContents() {
		return 0;
	}

	/**
	 * Sets a theme for a dialog associated with these options. This theme can be used to specify a
	 * theme attributes specific for a single dialog, like a customized text color for negative button
	 * or a title.
	 *
	 * @param theme The desired dialog theme.
	 * @return These options to allow methods chaining.
	 * @see #theme()
	 * @see R.attr#dialogTheme dialog:dialogTheme
	 */
	public O theme(@StyleRes final int theme) {
		updateBasicIsSet(THEME);
		this.mTheme = theme;
		// Do not notify this change, because as soon as dialog is created, we do not allow its
		// theme to be changed.
		return (O) this;
	}

	/**
	 * Returns the theme for dialog, set to these options.
	 *
	 * @return Dialog theme or {@code 0} by default.
	 * @see #theme(int)
	 */
	@StyleRes public int theme() {
		return mTheme;
	}

	/**
	 * Sets a resource id for the icon of a dialog associated with these options.
	 *
	 * @param resId Resource id of the desired icon.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogIcon dialog:dialogIcon
	 * @see #icon()
	 * @see #hasIcon()
	 * @see #vectorIcon(int)
	 */
	public O icon(@DrawableRes final int resId) {
		updateBasicIsSet(ICON);
		if (mIcon != resId) {
			this.mIcon = resId;
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns a flag indicating whether these options has some resource id for icon or not.
	 *
	 * @return {@code True} if these options has valid resource id for icon set, {@code false} otherwise.
	 * @see #icon(int)
	 */
	public boolean hasIcon() {
		return mIcon > 0;
	}

	/**
	 * Returns the icon resource id, set to these options.
	 *
	 * @return Icon resource id or {@code -1} by default.
	 * @see #icon(int)
	 * @see #hasIcon()
	 */
	@DrawableRes public int icon() {
		return mIcon;
	}

	/**
	 * Sets a vector resource id for the icon of a dialog associated with these options.
	 *
	 * @param resId Resource id of the desired vector icon.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogVectorIcon dialog:dialogVectorIcon
	 * @see #vectorIcon()
	 * @see #hasVectorIcon()
	 * @see #icon(int)
	 */
	public O vectorIcon(@DrawableRes final int resId) {
		updateBasicIsSet(VECTOR_ICON);
		if (mVectorIcon != resId) {
			this.mVectorIcon = resId;
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns a flag indicating whether these options has some resource id for vector icon or not.
	 *
	 * @return {@code True} if these options has valid resource id for vector icon set, {@code false}
	 * otherwise.
	 * @see #vectorIcon(int)
	 */
	public boolean hasVectorIcon() {
		return mVectorIcon > 0;
	}

	/**
	 * Returns the vector icon resource id, set to these options.
	 *
	 * @return Vector icon resource id or {@code -1} by default.
	 * @see #vectorIcon(int)
	 * @see #hasVectorIcon()
	 */
	@DrawableRes public int vectorIcon() {
		return mVectorIcon;
	}

	/**
	 * Same as {@link #title(CharSequence)}, where text will be obtained from the resources passed
	 * to these options during initialization.
	 *
	 * @param resId Resource id of the desired text.
	 * @throws java.lang.NullPointerException If these options was not created with valid resources.
	 * @see R.attr#dialogTitle dialog:dialogTitle
	 * @see #title()
	 */
	public O title(@StringRes final int resId) {
		return title(text(resId));
	}

	/**
	 * Sets a text for the <b>title</b> of a dialog associated with these options.
	 *
	 * @param text The desired text for title.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogTitle dialog:dialogTitle
	 * @see #title()
	 */
	public O title(@Nullable final CharSequence text) {
		updateBasicIsSet(TITLE);
		if (mTitle == null || !mTitle.equals(text)) {
			this.mTitle = text;
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns a flag indicating whether these options has some title text or not.
	 *
	 * @return {@code True} if these options has <b>none empty</b> title text set, {@code false} otherwise.
	 * @see #title()
	 */
	public boolean hasTitle() {
		return !TextUtils.isEmpty(mTitle);
	}

	/**
	 * Returns the <b>title</b> text, set to these options.
	 *
	 * @return Title text or {@code null} by default.
	 * @see #title(int)
	 * @see #title(CharSequence)
	 */
	@Nullable public CharSequence title() {
		return mTitle;
	}

	/**
	 * Same as {@link #content(CharSequence)}, where text will be obtained from the resources passed
	 * to these options during initialization.
	 *
	 * @param resId Resource id of the desired content text.
	 * @throws java.lang.NullPointerException If these options was not created with valid resources.
	 * @see R.attr#dialogContent dialog:dialogContent
	 * @see #content()
	 */
	public O content(@StringRes final int resId) {
		return content(text(resId));
	}

	/**
	 * Sets a text for the <b>content</b> of a dialog associated with these options.
	 *
	 * @param text The desired text for content.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogContent dialog:dialogContent
	 * @see #content()
	 */
	public O content(@Nullable final CharSequence text) {
		updateBasicIsSet(CONTENT);
		if (mContent == null || !mContent.equals(text)) {
			this.mContent = text;
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns the <b>content</b> text, set to these options.
	 *
	 * @return Content text or {@code null} by default.
	 * @see #content(int)
	 * @see #content(CharSequence)
	 * @see #contentFlags(int)
	 */
	@Nullable public CharSequence content() {
		return mContent;
	}

	/**
	 * Appends the specified content <var>flag</var> to the current ones.
	 *
	 * @param flag The desired flag. One of {@link #CONTENT_TRIM}, {@link #CONTENT_AS_HTML}.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogContentFlags dialog:dialogContentFlags
	 * @see #contentFlags(int)
	 * @see #contentFlags()
	 */
	public O contentFlag(@ContentFlag final int flag) {
		return contentFlags(mContentFlags |= flag);
	}

	/**
	 * Specifies a set of content flags that are used to determine how the content specified via
	 * {@link #content(CharSequence)} should be treated and processed.
	 *
	 * @param flags The desired content flags. One of {@link #CONTENT_TRIM}, {@link #CONTENT_AS_HTML}
	 *              or theirs combination.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogContentFlags dialog:dialogContentFlags
	 * @see #contentFlag(int)
	 * @see #contentFlags()
	 */
	public O contentFlags(@ContentFlag final int flags) {
		updateBasicIsSet(CONTENT_FLAGS);
		if (mContentFlags != flags) {
			this.mContentFlags = flags;
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns the content flags, set to these options.
	 * <p>
	 * Default value: <b>{@link #CONTENT_TRIM}</b>
	 *
	 * @return Set of content flags. One of {@link #CONTENT_TRIM}, {@link #CONTENT_AS_HTML} or theirs
	 * combination.
	 * @see #contentFlags(int)
	 * @see #contentFlag(int)
	 */
	@ContentFlag public int contentFlags() {
		return mContentFlags;
	}

	/**
	 * Sets a resource id of layout that should be inflated as content layout for a dialog associated
	 * with these options.
	 *
	 * @param resource The desired content layout resource.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogContentLayout dialog:dialogContentLayout
	 * @see #contentLayout()
	 */
	public O contentLayout(@LayoutRes final int resource) {
		this.mContentLayout = resource;
		return (O) this;
	}

	/**
	 * Returns the content layout resource, set to these options.
	 *
	 * @return Layout resource or {@code 0} if no resource has been specified.
	 * @see #contentLayout(int)
	 */
	@LayoutRes public int contentLayout() {
		return mContentLayout;
	}

	/**
	 * Same as {@link #positiveButton(CharSequence)}, where text will be obtained from the resources
	 * passed to these options during initialization.
	 *
	 * @param resId Resource id of the desired text.
	 * @see R.attr#dialogPositiveButton dialog:dialogPositiveButton
	 * @see #neutralButton(int)
	 * @see #negativeButton(int)
	 * @see #infoButton(int)
	 * @see #buttons()
	 */
	public O positiveButton(@StringRes final int resId) {
		return positiveButton(text(resId));
	}

	/**
	 * Sets a text for the <b>positive</b> button of a dialog associated with these options.
	 *
	 * @param text The desired text for positive button.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogPositiveButton dialog:dialogPositiveButton
	 * @see #neutralButton(CharSequence)
	 * @see #negativeButton(CharSequence)
	 * @see #infoButton(CharSequence)
	 * @see #buttons()
	 */
	public O positiveButton(@NonNull final CharSequence text) {
		return button(new Button(Dialog.BUTTON_POSITIVE, text));
	}

	/**
	 * Same as {@link #neutralButton(CharSequence)}, where text will be obtained from the resources
	 * passed to these options during initialization.
	 *
	 * @param resId Resource id of the desired text.
	 * @see R.attr#dialogNegativeButton dialog:dialogNegativeButton
	 * @see #positiveButton(int)
	 * @see #neutralButton(int)
	 * @see #infoButton(int)
	 * @see #buttons()
	 */
	public O negativeButton(@StringRes final int resId) {
		return negativeButton(text(resId));
	}

	/**
	 * Sets a text for the <b>negative</b> button of a dialog associated with these options.
	 *
	 * @param text The desired text for negative button.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogNegativeButton dialog:dialogNegativeButton
	 * @see #positiveButton(CharSequence)
	 * @see #neutralButton(CharSequence)
	 * @see #infoButton(CharSequence)
	 * @see #buttons()
	 */
	public O negativeButton(@NonNull final CharSequence text) {
		return button(new Button(Dialog.BUTTON_NEGATIVE, text));
	}

	/**
	 * Same as {@link #neutralButton(CharSequence)}, where text will be obtained from the resources
	 * passed to these options during initialization.
	 *
	 * @param resId Resource id of the desired text.
	 * @see R.attr#dialogNeutralButton dialog:dialogNeutralButton
	 * @see #positiveButton(int)
	 * @see #negativeButton(int)
	 * @see #infoButton(int)
	 * @see #buttons()
	 */
	public O neutralButton(@StringRes final int resId) {
		return neutralButton(text(resId));
	}

	/**
	 * Sets a text for the <b>neutral</b> button of a dialog associated with these options.
	 *
	 * @param text The desired text for neutral button.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogNeutralButton dialog:dialogNeutralButton
	 * @see #positiveButton(CharSequence)
	 * @see #negativeButton(CharSequence)
	 * @see #infoButton(CharSequence)
	 * @see #buttons()
	 */
	public O neutralButton(@NonNull final CharSequence text) {
		return button(new Button(Dialog.BUTTON_NEUTRAL, text));
	}

	/**
	 * Same as {@link #infoButton(CharSequence)}, where text will be obtained from the resources
	 * passed to these options during initialization.
	 *
	 * @param resId Resource id of the desired text.
	 * @see R.attr#dialogInfoButton dialog:dialogInfoButton
	 * @see #positiveButton(int)
	 * @see #neutralButton(int)
	 * @see #negativeButton(int)
	 * @see #buttons()
	 */
	public O infoButton(@StringRes final int resId) {
		return infoButton(text(resId));
	}

	/**
	 * Sets a text for the <b>info</b> button of a dialog associated with these options.
	 *
	 * @param text The desired text for info button.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogInfoButton dialog:dialogInfoButton
	 * @see #positiveButton(CharSequence)
	 * @see #neutralButton(CharSequence)
	 * @see #negativeButton(CharSequence)
	 * @see #buttons()
	 */
	public O infoButton(@NonNull final CharSequence text) {
		return button(new Button(Dialog.BUTTON_INFO, DialogButtonsView.CATEGORY_INFO, text));
	}

	/**
	 * Adds the given <var>button</var> to these options.
	 *
	 * @param button The desired button.
	 * @return These options to allow methods chaining.
	 * @see #buttons()
	 */
	public O button(@NonNull final Button button) {
		this.ensureButtons();
		mButtons.add(button);
		updateBasicIsSet(BUTTONS);
		notifyChanged();
		return (O) this;
	}

	/**
	 * Ensures that the list for buttons is initialized.
	 */
	private void ensureButtons() {
		if (mButtons == null) this.mButtons = new ArrayList<>();
	}

	/**
	 * Returns a flag indicating whether these options has some buttons or not.
	 *
	 * @return {@code True} if these options has some buttons, {@code false} otherwise.
	 * @see #buttons()
	 */
	public boolean hasButtons() {
		return mButtons != null && !mButtons.isEmpty();
	}

	/**
	 * Returns the current buttons added to these options.
	 *
	 * @return Current buttons list or {@code null} if no button was added yet.
	 * @see #button(Button)
	 * @see #positiveButton(CharSequence)
	 * @see #neutralButton(CharSequence)
	 * @see #negativeButton(CharSequence)
	 * @see #infoButton(CharSequence)
	 */
	@Nullable public List<Button> buttons() {
		return mButtons;
	}

	/**
	 * Sets the mode used when measuring width of each button of a dialog associated with these options.
	 *
	 * @param mode The desired width mode. One of {@link DialogButtonsView#WIDTH_MODE_WRAP_CONTENT WIDTH_MODE_WRAP_CONTENT}
	 *             or {@link DialogButtonsView#WIDTH_MODE_MATCH_PARENT WIDTH_MODE_MATCH_PARENT}.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogButtonsWidthMode dialog:dialogButtonsWidthMode
	 * @see #buttonsWidthMode()
	 */
	public O buttonsWidthMode(@DialogButtonsView.WidthMode final int mode) {
		updateBasicIsSet(BUTTONS_WIDTH_MODE);
		if (mButtonsWidthMode != mode) {
			this.mButtonsWidthMode = mode;
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns the buttons width mode, set to these options.
	 * <p>
	 * Default value: <b>{@link DialogButtonsView#WIDTH_MODE_WRAP_CONTENT WIDTH_MODE_WRAP_CONTENT}</b>
	 *
	 * @return One of {@link DialogButtonsView#WIDTH_MODE_WRAP_CONTENT} or {@link DialogButtonsView#WIDTH_MODE_MATCH_PARENT}.
	 * @see #buttonsWidthMode(int)
	 */
	@DialogButtonsView.WidthMode public int buttonsWidthMode() {
		return mButtonsWidthMode;
	}

	/**
	 * Sets the bundle for a dialog associated with these options.
	 *
	 * @param bundle The desired bundle to deliver to associated dialog.
	 * @return These options to allow methods chaining.
	 * @see #bundle()
	 */
	public O bundle(@Nullable final Bundle bundle) {
		this.mBundle = bundle;
		updateBasicIsSet(BUNDLE);
		notifyChanged();
		return (O) this;
	}

	/**
	 * Returns the bundle, set to these options.
	 *
	 * @return Bundle or {@code null} by default.
	 * @see #bundle(android.os.Bundle)
	 */
	@Nullable public Bundle bundle() {
		return mBundle;
	}

	/**
	 * Sets a flag indicating whether a dialog associated with these options should be cancelable or
	 * not.
	 *
	 * @param cancelable {@code True} to be cancelable, {@code false} otherwise.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogCancelable dialog:dialogCancelable
	 * @see #shouldBeCancelable()
	 */
	public O cancelable(final boolean cancelable) {
		updateBasicIsSet(CANCELABLE);
		if (hasPrivateFlag(FLAG_CANCELABLE) != cancelable) {
			this.updatePrivateFlags(FLAG_CANCELABLE, cancelable);
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns a flag indicating whether a dialog should be cancelable or not.
	 * <p>
	 * Default value: <b>true</b>
	 *
	 * @return {@code True} if dialog should be cancelable, {@code false} otherwise.
	 * @see #cancelable(boolean)
	 */
	public boolean shouldBeCancelable() {
		return (mFlags & FLAG_CANCELABLE) != 0;
	}

	/**
	 * Sets a flag indicating whether a dialog associated with these options should be dismissed after
	 * it is restored or not.
	 *
	 * @param dismiss {@code True} to be dialog dismissed, {@code false} otherwise.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogDismissOnRestore dialog:dialogDismissOnRestore
	 * @see #shouldDismissOnRestore()
	 */
	public O dismissOnRestore(final boolean dismiss) {
		updateBasicIsSet(DISMISS_ON_RESTORE);
		if (hasPrivateFlag(FLAG_DISMISS_ON_RESTORE) != dismiss) {
			this.updatePrivateFlags(FLAG_DISMISS_ON_RESTORE, dismiss);
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns a flag indicating whether a dialog should be dismissed after it is restored or not.
	 * <p>
	 * Default value: <b>false</b>
	 *
	 * @return {@code True} if dialog should be dismissed, {@code false} otherwise.
	 * @see #dismissOnRestore(boolean)
	 */
	public boolean shouldDismissOnRestore() {
		return (mFlags & FLAG_DISMISS_ON_RESTORE) != 0;
	}

	/**
	 * Sets a flag indicating whether a dialog associated with these options should remain visible,
	 * after a button (except {@link Dialog#BUTTON_NEGATIVE})
	 * has been clicked or not.
	 *
	 * @param remain {@code True} to remain showing, {@code false} to be dismissed.
	 * @return These options to allow methods chaining.
	 * @see R.attr#dialogRemain dialog:dialogRemain
	 * @see #shouldRemain()
	 */
	public O remain(final boolean remain) {
		updateBasicIsSet(REMAIN);
		if (hasPrivateFlag(FLAG_REMAIN) != remain) {
			this.updatePrivateFlags(FLAG_REMAIN, remain);
			notifyChanged();
		}
		return (O) this;
	}

	/**
	 * Returns a flag indicating whether a dialog should remain showing, after a button (except
	 * {@link Dialog#BUTTON_NEGATIVE}) click or not.
	 * <p>
	 * Default value: <b>false</b>
	 *
	 * @return {@code True} to remain, {@code false} otherwise.
	 * @see #remain(boolean)
	 */
	public boolean shouldRemain() {
		return (mFlags & FLAG_REMAIN) != 0;
	}

	/**
	 * Returns the resources with which was these options created.
	 *
	 * @return Application resources or {@code null} if these options was not created with resources
	 * object.
	 * @see #DialogOptions(Resources)
	 */
	@Nullable protected final Resources resources() {
		return mResources;
	}

	/**
	 * Returns a {@code String} value obtained from application resources.
	 *
	 * @param resId Resource id of the desired string.
	 * @return String value from application resource.
	 * @throws java.lang.NullPointerException If these options were not created with valid {@link Resources}
	 *                                        object.
	 * @see #text(int)
	 */
	@NonNull protected final String string(@StringRes final int resId) {
		this.checkRequiredResources();
		return mResources.getString(resId);
	}

	/**
	 * Returns a {@code CharSequence} value obtained from application resources.
	 *
	 * @param resId Resource id of the desired text.
	 * @return Text value from application resources.
	 * @throws java.lang.NullPointerException If these options were not created with valid {@link Resources}
	 *                                        object.
	 * @see #string(int)
	 */
	@NonNull protected final CharSequence text(@StringRes final int resId) {
		this.checkRequiredResources();
		return mResources.getText(resId);
	}

	/**
	 * Registers a callback to be invoked whenever some value hold by these options changes.
	 *
	 * @param callback The desired callback.
	 */
	public void setCallback(@Nullable final Callback callback) {
		this.mCallback = callback;
	}

	/**
	 * Notifies the current callback that some of values hold by these options has been changed.
	 */
	protected void notifyChanged() {
		if (mCallback != null) mCallback.onOptionsChanged(this);
	}

	/**
	 * Checks whether the current resources are available or not.
	 *
	 * @throws java.lang.NullPointerException If there are not resources available.
	 */
	final void checkRequiredResources() {
		if (mResources == null) throw new NullPointerException(
				"No Resources object available to obtain requested resource value."
		);
	}

	/**
	 * Updates the current private flags.
	 *
	 * @param flag Value of the desired flag to add/remove to/from the current private flags.
	 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
	 */
	private void updatePrivateFlags(final int flag, final boolean add) {
		if (add) this.mFlags |= flag;
		else this.mFlags &= ~flag;
	}

	/**
	 * Returns a boolean flag indicating whether the specified <var>flag</var> is contained within
	 * the current private flags or not.
	 *
	 * @param flag Value of the flag to check.
	 * @return {@code True} if the requested flag is contained, {@code false} otherwise.
	 */
	private boolean hasPrivateFlag(final int flag) {
		return (mFlags & flag) != 0;
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Helper class used to transfer data necessary to create buttons for a dialog associated with
	 * a specific {@link DialogOptions} caring set of these buttons. Use one of {@link #Button(int, CharSequence)}
	 * or {@link #Button(int, int, CharSequence)} to initialize a new instance of Button and supply
	 * it to your desired options via {@link #button(DialogOptions.Button)}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class Button implements Parcelable {

		/**
		 * Creator used to create an instance or array of instances of Button from {@link android.os.Parcel}.
		 */
		public static final Parcelable.Creator<Button> CREATOR = new Parcelable.Creator<Button>() {
			/**
			 */
			@Override public Button createFromParcel(@NonNull final Parcel source) { return new Button(source); }

			/**
			 */
			@Override public Button[] newArray(final int size) { return new Button[size]; }
		};

		/**
		 * Id of this button.
		 */
		int id;

		/**
		 * Category of this button.
		 */
		int category;

		/**
		 * Text for this button.
		 */
		CharSequence text;

		/**
		 * Creates a new instance of Button.
		 *
		 * @param id   An id for the new button.
		 * @param text A text for the new button.
		 */
		public Button(final int id, @NonNull final CharSequence text) {
			this(id, DialogButtonsView.CATEGORY_ACTION, text);
		}

		/**
		 * Creates a new instance of Button.
		 *
		 * @param id       An id for the new button.
		 * @param category A category for the new button.
		 * @param text     A text for the new button.
		 */
		public Button(final int id, final int category, @NonNull final CharSequence text) {
			this.id = id;
			this.category = category;
			this.text = text;
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of Button form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected Button(@NonNull final Parcel source) {
			this.id = source.readInt();
			this.category = source.readInt();
			this.text = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			dest.writeInt(id);
			dest.writeInt(category);
			TextUtils.writeToParcel(text, dest, flags);
		}

		/**
		 * Returns the id of this button.
		 *
		 * @return This button's id.
		 */
		public int getId() {
			return id;
		}

		/**
		 * Returns the category of this button.
		 *
		 * @return This button's category.
		 */
		public int getCategory() {
			return category;
		}

		/**
		 * Returns the text for this button.
		 *
		 * @return This button's text.
		 */
		@NonNull public CharSequence getText() {
			return text;
		}

		/**
		 */
		@Override public int describeContents() {
			return 0;
		}
	}
}