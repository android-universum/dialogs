/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.graphics.Typeface;
import android.os.Parcelable;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.Dialog;

/**
 * Interface specifying API layer for a view that can be used as buttons view for implementations
 * of {@link Dialog Dialog} interface to ensure proper working
 * of binding process of the buttons view including overall management of such a view within the
 * associated dialog.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see DialogTitleView
 */
public interface DialogButtonsView extends DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Category flag for action buttons situated at the right side of the buttons view.
	 */
	int CATEGORY_ACTION = 0x01;

	/**
	 * Category flag for info buttons situated at the left side of the buttons view.
	 */
	int CATEGORY_INFO = 0x02;

	/**
	 * Mode used to measure buttons, so theirs size will be determined by size of theirs content.
	 */
	int WIDTH_MODE_WRAP_CONTENT = 0x01;

	/**
	 * Mode used to measure buttons, so theirs size will be determined by size of theirs parent, and
	 * equally divided depends on theirs count.
	 */
	int WIDTH_MODE_MATCH_PARENT = 0x02;

	/**
	 * Defines an annotation for determining set of allowed modes for {@link #setButtonsWidthMode(int)}
	 * method.
	 */
	@IntDef({WIDTH_MODE_WRAP_CONTENT, WIDTH_MODE_MATCH_PARENT})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface WidthMode {}

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that can receive callback about clicked button within a {@link DialogButtonsView}
	 * implementation.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	// todo: rename to OnButtonClickListener ...
	interface OnButtonsClickListener {

		/**
		 * Invoked whenever the given <var>button</var> has been clicked within the specified
		 * dialog <var>buttons</var> view.
		 *
		 * @param buttons The buttons view within which has been button clicked.
		 * @param button  The clicked button.
		 */
		void onButtonClick(@NonNull DialogButtonsView buttons, @NonNull DialogButton button);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Registers a callback to be invoked whenever a specific button is clicked within this buttons
	 * view.
	 *
	 * @param listener Listener callback to register.
	 * @see #unregisterOnButtonsClickListener(OnButtonsClickListener)
	 */
	void registerOnButtonsClickListener(@NonNull OnButtonsClickListener listener);

	/**
	 * Un-registers the specified <var>listener</var>, so it will no longer receive button click
	 * callbacks.
	 *
	 * @param listener Listener callback to unregister.
	 * @see #registerOnButtonsClickListener(OnButtonsClickListener)
	 */
	void unregisterOnButtonsClickListener(@NonNull OnButtonsClickListener listener);

	/**
	 * Sets the typeface for all buttons presented within this view.
	 *
	 * @param typeface The desired typeface to set.
	 */
	void setButtonsTypeface(@Nullable Typeface typeface);

	/**
	 * Same as {@link #newButton(int, int)} with {@link #CATEGORY_ACTION} as category for the new
	 * button.
	 */
	@NonNull DialogButton newButton(int id);

	/**
	 * Creates a new instance of DialogButton implementation.
	 *
	 * @param id       An id for the new button.
	 * @param category A category for the new button. One of {@link #CATEGORY_ACTION} or {@link #CATEGORY_INFO}.
	 * @return New DialogButton.
	 * @see #addButton(DialogButton)
	 */
	@NonNull DialogButton newButton(int id, int category);

	/**
	 * Adds the given <var>button</var> into this buttons view.
	 *
	 * @param button The desired button to add.
	 * @see #newButton(int)
	 */
	void addButton(@NonNull DialogButton button);

	/**
	 * Returns the button at the specified <var>index</var> from the current action buttons.
	 * <p>
	 * <b>Note</b>, that action buttons are positioned from the start to the end or from the top
	 * to the bottom.
	 *
	 * @param index The index of the desired action button to obtain.
	 * @return Dialog button or {@code null} if there is no action button at the specified index.
	 * @see #getInfoButtonAt(int)
	 * @see #findButtonById(int)
	 */
	@Nullable DialogButton getActionButtonAt(int index);

	/**
	 * Returns a button at the specified <var>index</var> from the current info buttons.
	 * <p>
	 * <b>Note</b>, that info buttons are positioned from the start to the end or from the top
	 * to the bottom.
	 *
	 * @param index The index of the desired info button to obtain.
	 * @return Dialog button or {@code null} if there is no info button at the specified index.
	 * @see #getActionButtonAt(int)
	 * @see #findButtonById(int)
	 */
	@Nullable DialogButton getInfoButtonAt(int index);

	/**
	 * Searches for button with the specified <var>id</var> among the current buttons.
	 *
	 * @param id The id of the desired button to find.
	 * @return Dialog button or {@code null} if there is no button with the specified id.
	 * @see #getActionButtonAt(int)
	 * @see #getInfoButtonAt(int)
	 */
	@Nullable DialogButton findButtonById(int id);

	/**
	 * Changes enabled state of a button with the specified <var>id</var>.
	 *
	 * @param id      Id of the desired button to enable/disable.
	 * @param enabled {@code True} to enable button, {@code false} to disable it.
	 */
	void setButtonEnabled(int id, boolean enabled);

	/**
	 * Removes the specified <var>button</var> and its corresponding view.
	 *
	 * @param button The button to be removed.
	 * @see #removeActionButtonAt(int)
	 * @see #removeInfoButtonAt(int)
	 * @see #removeAllButtons()
	 */
	void removeButton(@NonNull DialogButton button);

	/**
	 * Removes action button and its corresponding view at the specified <var>index</var>.
	 *
	 * @param index Index of the desired button to be removed.
	 * @see #removeActionButtonAt(int)
	 * @see #removeAllButtons()
	 */
	void removeActionButtonAt(int index);

	/**
	 * Removes info button and its corresponding view at the specified <var>index</var>.
	 *
	 * @param index Index of the desired button to be removed.
	 * @see #removeActionButtonAt(int)
	 * @see #removeAllButtons()
	 */
	void removeInfoButtonAt(int index);

	/**
	 * Removes all buttons and theirs corresponding views.
	 *
	 * @see #removeActionButtonAt(int)
	 * @see #removeInfoButtonAt(int)
	 */
	void removeAllButtons();

	/**
	 * Sets the width mode used when measuring buttons of this view.
	 *
	 * @param mode The desired mode. One of {@link #WIDTH_MODE_WRAP_CONTENT} or {@link #WIDTH_MODE_MATCH_PARENT}.
	 * @see #getButtonsWidthMode()
	 */
	void setButtonsWidthMode(@WidthMode int mode);

	/**
	 * Returns the current width mode used for buttons measurement.
	 *
	 * @return Current with mode for buttons.
	 * @see #setButtonsWidthMode(int)
	 */
	@WidthMode int getButtonsWidthMode();

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * The DialogButton class specifies API that allows directly manage a single button within an
	 * implementation of {@link DialogButtonsView} where it is implementation of this button used.
	 * <p>
	 * A single button can be identified via {@link #getId()} and also through its category that
	 * can be obtained via {@link #getCategory()}. If you want to change the current text of button
	 * this can be done via {@link #setText(CharSequence)} or its resource equivalent
	 * {@link #setText(int)}.
	 *
	 * <h3>States</h3>
	 * Methods listed below describe API that can be used to change current state of a particular
	 * button or to check whenever the button is in the desired state or not:
	 * <ul>
	 * <li>{@link #setVisible(boolean)}</li>
	 * <li>{@link #isVisible()}</li>
	 * <li>{@link #setEnabled(boolean)}</li>
	 * <li>{@link #isEnabled()}</li>
	 * <li>{@link #setSelected(boolean)}</li>
	 * <li>{@link #isSelected()}</li>
	 * </ul>
	 *
	 * <h3>View</h3>
	 * Current view of the button (if it is attached, {@link #hasViewAttached()}) can be obtained
	 * via {@link #getView()}, but remember that any changes related to the view's state (enabled,
	 * selected, ...) applied directly to the view will be not applied to the button itself. If you
	 * want to supply custom view to the button, you can do it via {@link #attachView(View)},
	 * but also remember that such a view will not be restored after orientation change.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	abstract class DialogButton implements Parcelable {

		/**
		 * Returns the id of this button.
		 *
		 * @return This button's id.
		 */
		public abstract int getId();

		/**
		 * Returns the category of this button.
		 *
		 * @return This button's category.
		 */
		public abstract int getCategory();

		/**
		 * Same as {@link #setText(CharSequence)} for resource id.
		 *
		 * @param resId Resource id of the desired text.
		 * @see #getText()
		 */
		public abstract void setText(@StringRes int resId);

		/**
		 * Sets the text for this button.
		 *
		 * @param text The desired text.
		 * @see #getText()
		 */
		public abstract void setText(@NonNull CharSequence text);

		/**
		 * Returns the current text of this button.
		 *
		 * @return This button's text.
		 * @see #setText(int)
		 * @see #setText(CharSequence)
		 */
		@NonNull public abstract CharSequence getText();

		/**
		 * Updates the enabled state of this button.
		 *
		 * @param enabled {@code True} to enabled button, {@code false} to disable it, so it will be
		 *                no clickable.
		 * @see #isEnabled()
		 */
		public abstract void setEnabled(boolean enabled);

		/**
		 * Returns the current enabled state of this button.
		 *
		 * @return {@code True} if this button is enabled (clickable), {@code false} otherwise.
		 * @see #setEnabled(boolean)
		 */
		public abstract boolean isEnabled();

		/**
		 * Updates the selection state of this button.
		 *
		 * @param selected {@code True} to select button, {@code false} otherwise.
		 * @see #isSelected()
		 */
		public abstract void setSelected(boolean selected);

		/**
		 * Returns the current selected state of this button.
		 *
		 * @return {@code True} if this button is selected, {@code false} otherwise.
		 * @see #setSelected(boolean)
		 */
		public abstract boolean isSelected();

		/**
		 * Updates the visibility state of this button.
		 *
		 * @param visible {@code True} to show button, {@code false} to hide it.
		 * @see #isVisible()
		 */
		public abstract void setVisible(boolean visible);

		/**
		 * Returns the current visibility state of this button.
		 *
		 * @return {@code True} if this button is visible, {@code false} otherwise.
		 * @see #setVisible(boolean)
		 */
		public abstract boolean isVisible();

		/**
		 * Attaches the given <var>view</var> to this button.
		 *
		 * @param view The view of this button used to present current states of this button.
		 * @see #detachView()
		 * @see #hasViewAttached()
		 */
		public abstract void attachView(@NonNull View view);

		/**
		 * Detaches the current view of this button, so it should be no longer used to present the
		 * current states of this button.
		 *
		 * @see #detachView()
		 * @see #hasViewAttached()
		 */
		public abstract void detachView();

		/**
		 * Returns a flag indicating whether this button has its view attached by {@link #attachView(View)}
		 * or not.
		 *
		 * @return {@code True} if this button has attached view, {@code false} otherwise.
		 */
		public abstract boolean hasViewAttached();

		/**
		 * Returns the current attached view of this button.
		 *
		 * @return This button's view or {@code null} if this button does not have its view attached.
		 * @see #hasViewAttached()
		 */
		@Nullable public abstract View getView();

		/**
		 */
		@Override public int describeContents() {
			return 0;
		}
	}
}