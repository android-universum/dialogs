/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Interface specifying API for dialogs that can be inflated from Xml file.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface XmlDialog {

	/**
	 * Inflates data for this XmlDialog from the given Xml <var>parser</var>.
	 *
	 * @param parser    Xml parser with data from which to inflate this dialog.
	 * @param resources Application resources used to resolve attribute values.
	 * @param theme     Theme to apply to the attribute values. May be {@code null}.
	 * @param options   Dialog options to be merged with the default options of this dialog.
	 *                  May be {@code null}.
	 */
	void inflate(@NonNull XmlResourceParser parser, @NonNull Resources resources, @Nullable Resources.Theme theme, @Nullable DialogOptions options);
}