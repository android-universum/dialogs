/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.Dialog;

/**
 * Interface specifying API layer for a layout that can be used as root layout for for implementations
 * of {@link Dialog Dialog} interface to ensure proper working
 * of building process of the whole dialog's layout containing title, content and buttons view.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see DialogTitleView
 * @see DialogButtonsView
 */
public interface DialogLayout extends DialogView {

	/**
	 * Inserts the given <var>titleView</var> at the first position of this dialog view implementation.
	 *
	 * @param titleView View which represents dialog title.
	 * @see #getTitleView()
	 * @see #removeTitleView()
	 */
	void addTitleView(@NonNull View titleView);

	/**
	 * Removes the current title view from the layout of this dialog view implementation, if it's presented.
	 *
	 * @see #getTitleView()
	 */
	void removeTitleView();

	/**
	 * Returns the title view of this dialog view implementation.
	 *
	 * @return Current title view, if it's presented, {@code null} otherwise.
	 * @see #addTitleView(View)
	 */
	@Nullable View getTitleView();

	/**
	 * Inserts the given <var>contentView</var> at the middle position (below the title view) of this
	 * dialog view implementation.
	 *
	 * @param contentView View which represents dialog content.
	 * @see #getContentView()
	 * @see #removeContentView()
	 */
	void addContentView(@NonNull View contentView);

	/**
	 * Removes the current content view from the layout of this dialog view implementation, if it's
	 * presented.
	 *
	 * @see #getContentView()
	 */
	void removeContentView();

	/**
	 * Returns the content view of this dialog view implementation.
	 *
	 * @return Current content view, if it's presented, {@code null} otherwise.
	 * @see #addContentView(View)
	 */
	@Nullable View getContentView();

	/**
	 * Inserts the given <var>buttonsView</var> at the last position (below the body view) of this
	 * dialog view implementation.
	 *
	 * @param buttonsView View which represents dialog buttons.
	 * @see #getButtonsView()
	 * @see #removeButtonsView()
	 */
	void addButtonsView(@NonNull View buttonsView);

	/**
	 * Removes the current buttons view from the layout of this dialog view implementation, if it's presented.
	 *
	 * @see #getButtonsView()
	 */
	void removeButtonsView();

	/**
	 * Returns the buttons view of this dialog view implementation.
	 *
	 * @return Current buttons view, if it's presented, {@code null} otherwise.
	 * @see #addButtonsView(View)
	 */
	@Nullable View getButtonsView();
}