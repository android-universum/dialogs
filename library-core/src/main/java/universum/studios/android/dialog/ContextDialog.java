/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import androidx.annotation.Nullable;

/**
 * Interface specifying API required for dialogs that are to be used also within context of
 * {@link android.app.Fragment Fragments} to ensure proper dispatching of dialog
 * callbacks to its context fragment.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see UniqueDialog
 */
public interface ContextDialog {

	/**
	 * Sets a tag of the fragment within which context will be this dialog displayed.
	 * <p>
	 * If {@code none null}, such a fragment should be used as listener for this dialog, so it can
	 * receive all callbacks from this dialog instance.
	 *
	 * @param fragmentTag Context fragment tag.
	 * @see #setContextFragmentId(int)
	 */
	void setContextFragmentTag(@Nullable String fragmentTag);

	/**
	 * Sets an id of the fragment within which context will be this dialog displayed.
	 * <p>
	 * If {@code none negative}, such a fragment should be used as listener for this dialog, so it
	 * can receive all callbacks from this dialog instance.
	 *
	 * @param fragmentId Context fragment id.
	 * @see #setContextFragmentTag(String)
	 */
	void setContextFragmentId(int fragmentId);
}