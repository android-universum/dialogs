/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.view.LayoutInflater;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Interface specifying API and global listeners for dialogs.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Dialog.OnDialogListener
 * @see Dialog.OnShowListener
 * @see Dialog.OnCancelListener
 * @see Dialog.OnDismissListener
 */
public interface Dialog extends UniqueDialog {

    /*
     * Constants ===================================================================================
     */

    /**
     * The identifier for the dialog single <b>info</b> button.
     */
    int BUTTON_INFO = 0x00;

    /**
     * The identifier for the dialog <b>positive</b> button.
     */
    int BUTTON_POSITIVE = 0x01;

    /**
     * The identifier for the dialog <b>neutral</b> button.
     */
    int BUTTON_NEUTRAL = 0x02;

    /**
     * The identifier for the dialog <b>negative</b> button.
     */
    int BUTTON_NEGATIVE = 0x03;

    /**
     * Defines an annotation for determining set of available dialog buttons.
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({BUTTON_INFO, BUTTON_POSITIVE, BUTTON_NEUTRAL, BUTTON_NEGATIVE})
    @SuppressWarnings("UnnecessaryInterfaceModifier")
    public @interface Button {}

    /*
     * Interface ===================================================================================
     */

    /**
     * Listener that can receive callback about clicked button within a specific dialog.
     * <p>
     * <b>Note</b>, that all listeners for a particular dialogs that want to extend callbacks of
     * this listener should inherit from this listener.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface OnDialogListener {

        /**
         * Invoked whenever a button with the specified <var>button</var> identifier has been clicked
         * within the specified <var>dialog</var>.
         *
         * @param dialog The dialog in which was the specified button clicked.
         * @param button Button identifier.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         */
        boolean onDialogButtonClick(@NonNull Dialog dialog, @Button int button);
    }

    /**
     * Listener that can receive a callback that a specific dialog has been showed.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface OnShowListener {

        /**
         * Invoked when the specified <var>dialog</var> has been showed.
         *
         * @param dialog The dialog which was just now showed.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         */
        boolean onDialogShown(@NonNull Dialog dialog);
    }

    /**
     * Listener that can receive a callback that a specific dialog has been canceled.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface OnCancelListener {

        /**
         * Invoked when the specified <var>dialog</var> has been cancelled.
         * <p>
         * <b>Note, that this is only informative callback. The specified dialog is no longer visible.</b>
         *
         * @param dialog The dialog which was just now cancelled.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         */
        boolean onDialogCancelled(@NonNull Dialog dialog);
    }

    /**
     * Listener that can receive a callback that a specific dialog has been dismissed.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface OnDismissListener {

        /**
         * Invoked when the specified <var>dialog</var> has been dismissed.
         * <p>
         * <b>Note, that this is only informative callback. The specified dialog is no longer visible.</b>
         *
         * @param dialog The dialog which was just now dismissed.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         */
        boolean onDialogDismissed(@NonNull Dialog dialog);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the layout inflater associated with this dialog.
     * <p>
     * The returned inflater should be used to inflate only views that will be presented in the dialog
     * context but can be provided from outside, for example item views provided by {@link android.widget.Adapter Adapters}.
     *
     * @return Layout inflater of this dialog or {@code null} if this dialog does not have its view
     * created yet.
     */
    @Nullable
    LayoutInflater getDialogLayoutInflater();

    /**
     * Updates a visibility of the indeterminate {@code ProgressBar} within DialogTitleView.
     *
     * @param visible {@code True} to show indeterminate progress bar, {@code false} to hide it.
     * @see universum.studios.android.dialog.view.DialogTitleView#setIndeterminateProgressBarVisible(boolean)
     */
    void setIndeterminateProgressBarVisible(boolean visible);

    /**
     * Updates enabled state of a button with the specified <var>buttonId</var>.
     *
     * @param buttonId Id of the desired dialog button to enable/disable.
     * @param enabled  {@code True} to enabled button, {@code false} to disable it.
     * @see universum.studios.android.dialog.view.DialogButtonsView#setButtonEnabled(int, boolean)
     */
    void setButtonEnabled(int buttonId, boolean enabled);

    /**
     * Returns a flag indicating whether this dialog instance is visible (currently showing) or not.
     *
     * @return {@code True} if this dialog is visible, {@code false} otherwise.
     */
    boolean isVisible();

    /**
     * Dismisses this dialog instance.
     * <p>
     * <b>Note</b>, that after dialog is dismissed, its state cannot be restored.
     */
    void dismiss();
}