/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

/**
 * Interface specifying API allowing to set a unique id to a concrete dialog by which can be that
 * dialog later identified.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see ContextDialog
 */
public interface UniqueDialog {

	/**
	 * Sets the unique id for this dialog instance.
	 *
	 * @param dialogId The desired id by which can be this dialog instance identified.
	 * @see #getDialogId()
	 */
	void setDialogId(int dialogId);

	/**
	 * Returns the id of this dialog instance.
	 *
	 * @return Dialog id.
	 * @see #setDialogId(int)
	 */
	int getDialogId();
}