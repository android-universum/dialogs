/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;

/**
 * Base interface for views used for dialogs from within this library to specify (delegate) API of
 * the views from within Android SDK for better usability.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface DialogView {

	/**
	 * Delegates to {@link View#setVisibility(int)}.
	 */
	void setVisibility(int visibility);

	/**
	 * Delegates to {@link View#setEnabled(boolean)}.
	 */
	void setEnabled(boolean enabled);

	/**
	 * Delegates to {@link View#findViewById(int)}.
	 */
	@Nullable <T extends View> T findViewById(@IdRes int viewId);
}