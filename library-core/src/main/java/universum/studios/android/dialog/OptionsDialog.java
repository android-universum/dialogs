/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Interface specifying API and global listeners for dialogs.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface OptionsDialog {

	/**
	 * Sets a new options for this dialog instance.
	 *
	 * @param options The desired options used to bind dialog view with data.
	 * @see #getOptions()
	 */
	void setOptions(@NonNull DialogOptions options);

	/**
	 * Returns the current options of this dialog instance.
	 *
	 * @return Either dialog options passed to {@link #setOptions(DialogOptions)} or default options.
	 * May be {@code null} if this dialog instance is not created yet.
	 */
	@Nullable DialogOptions getOptions();
}