/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.annotation.handler;

import java.lang.annotation.Annotation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.dialog.annotation.DialogAnnotations;

/**
 * An {@link AnnotationHandler} base implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class BaseAnnotationHandler implements AnnotationHandler {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseAnnotationHandler";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Class for which has been this handler created.
	 */
	final Class<?> mAnnotatedClass;

	/**
	 * Class that is used when obtaining annotations from {@link #mAnnotatedClass} recursively via
	 * {@link DialogAnnotations#obtainAnnotationFrom(Class, Class, Class)}.
	 */
	final Class<?> mMaxSuperClass;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseAnnotationHandler for the specified <var>annotatedClass</var>.
	 *
	 * @param annotatedClass The class of which annotations processing should the new handler handle.
	 * @param maxSuperClass  Max super class of the annotated class up to which to search for annotations
	 *                       recursively when searching for requested annotation via {@link #findAnnotationRecursive(Class)}.
	 */
	BaseAnnotationHandler(@NonNull final Class<?> annotatedClass, @Nullable final Class<?> maxSuperClass) {
		this.mAnnotatedClass = annotatedClass;
		this.mMaxSuperClass = maxSuperClass;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override @NonNull public final Class<?> getAnnotatedClass() {
		return mAnnotatedClass;
	}

	/**
	 * Like {@link #findAnnotationRecursive(Class)} but this method tries to find the requested
	 * annotation only for the class attached to this handler.
	 *
	 * @param classOfAnnotation Class of the annotation to find.
	 * @param <A>               Type of the annotation to find.
	 * @return Found annotation or {@code null} if there is no such annotation presented.
	 */
	final <A extends Annotation> A findAnnotation(final Class<A> classOfAnnotation) {
		return DialogAnnotations.obtainAnnotationFrom(classOfAnnotation, mAnnotatedClass, null);
	}

	/**
	 * Tries to find annotation with the requested <var>classOfAnnotation</var> for the class attached
	 * to this handler recursively using also max super class specified for this handler (if any).
	 *
	 * @param classOfAnnotation Class of the annotation to find.
	 * @param <A>               Type of the annotation to find.
	 * @return Found annotation or {@code null} if there is no such annotation presented.
	 */
	final <A extends Annotation> A findAnnotationRecursive(final Class<A> classOfAnnotation) {
		return DialogAnnotations.obtainAnnotationFrom(classOfAnnotation, mAnnotatedClass, mMaxSuperClass);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}