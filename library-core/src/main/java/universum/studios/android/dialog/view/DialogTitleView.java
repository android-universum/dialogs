/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import universum.studios.android.dialog.Dialog;

/**
 * Interface specifying API layer for a view that can be used as title view for implementations
 * of {@link Dialog Dialog} interface to ensure proper working
 * of binding process of the title view including overall management of such a view within the
 * associated dialog.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see DialogButtonsView
 */
public interface DialogTitleView extends DialogView {

	/**
	 * Like {@link #setIconResource(int)} but this method will use simple utility method to obtain
	 * the desired vector drawable and set it as icon {@link Drawable} via {@link #setIconDrawable(Drawable)}
	 * method.
	 *
	 * @param resId Resource id of the desired <b>vector</b> drawable. Can be {@code 0} to clear the
	 *              current icon drawable.
	 */
	void setVectorIconResource(@DrawableRes int resId);

	/**
	 * Same as {@link #setIconDrawable(Drawable)} for resource id.
	 *
	 * @param resId Resource id of the desired icon. Can be {@code 0} to clear the current icon drawable.
	 * @see #getIconDrawable()
	 * @see #setIconVisible(boolean)
	 */
	void setIconResource(@DrawableRes int resId);

	/**
	 * Sets a drawable for the icon view of this dialog title view.
	 *
	 * @param drawable The desired icon drawable. May be {@code null} to clear the current one.
	 * @see #getIconDrawable()
	 * @see #setIconVisible(boolean)
	 */
	void setIconDrawable(@Nullable Drawable drawable);

	/**
	 * Returns the current icon drawable presented by this dialog title view.
	 *
	 * @return Icon drawable.
	 * @see #setIconResource(int)
	 * @see #setIconDrawable(Drawable)
	 */
	@Nullable Drawable getIconDrawable();

	/**
	 * Updates the current visibility of the icon view.
	 *
	 * @param visible {@code True} to set icon visible, {@code false} otherwise.
	 * @see #isIconVisible()
	 * @see #setIconDrawable(Drawable)
	 */
	void setIconVisible(boolean visible);

	/**
	 * Returns a boolean flag indicating whether the icon view is visible or not.
	 *
	 * @return {@code True} if icon is visible, {@code false} otherwise.
	 * @see #setIconVisible(boolean)
	 */
	boolean isIconVisible();

	/**
	 * Same as {@link #setTitle(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired text.
	 * @see #getTitle()
	 * @see #setTitleVisible(boolean)
	 */
	void setTitle(@StringRes int resId);

	/**
	 * Sets a text for the title text view of this dialog title view.
	 *
	 * @param title The desired title text. May be {@code null} to clear the current one.
	 * @see #getTitle()
	 * @see #setTitleVisible(boolean)
	 */
	void setTitle(@Nullable CharSequence title);

	/**
	 * Returns the title text presented by this dialog title view.
	 *
	 * @return Current title text.
	 * @see #setTitle(int)
	 * @see #setTitle(CharSequence)
	 */
	@NonNull CharSequence getTitle();

	/**
	 * Updates the current visibility of the title text view.
	 *
	 * @param visible {@code True} to set title visible, {@code false} otherwise.
	 * @see #isTitleVisible()
	 */
	void setTitleVisible(boolean visible);

	/**
	 * Returns a boolean flag indicating whether the title text view is visible or not.
	 *
	 * @return {@code True} if title is visible, {@code false} otherwise.
	 * @see #setTitleVisible(boolean)
	 */
	boolean isTitleVisible();

	/**
	 * Sets the type face for view which presents title text.
	 *
	 * @param typeface The desired type face.
	 */
	void setTitleTypeface(@Nullable Typeface typeface);

	/**
	 * Updates the current visibility of the circular indeterminate progress bar.
	 *
	 * @param visible {@code True} to set progress bar visible, {@code false} otherwise.
	 * @see #isIndeterminateProgressBarVisible()
	 */
	void setIndeterminateProgressBarVisible(boolean visible);

	/**
	 * Returns a boolean flag indicating whether the indeterminate progress bar is visible or not.
	 *
	 * @return {@code True} if progress bar is visible, {@code false} otherwise.
	 * @see #setIndeterminateProgressBarVisible(boolean)
	 */
	boolean isIndeterminateProgressBarVisible();
}