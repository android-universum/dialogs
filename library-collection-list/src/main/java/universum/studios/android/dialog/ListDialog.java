/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * An {@link AdapterDialog} implementation that can be used to present data set of items within
 * {@link ListView}.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogListOptions dialogListOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ListDialog extends AdapterDialog<ListView> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ListDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ListDialog with {@link ListView} to present data from {@link android.widget.Adapter}.
	 */
	public ListDialog() {
		super(R.attr.dialogListOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of ListDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New ListDialog instance.
	 */
	@NonNull public static ListDialog newInstance(@NonNull final ListOptions options) {
		final ListDialog dialog = new ListDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new ListOptions(resources);
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_list, container, false);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link BaseAdapterOptions} implementation specific for the {@link ListDialog}.
	 * <p>
	 * These options are here only for convenience and do not add any extra options to the parent's
	 * {@link BaseAdapterOptions}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class ListOptions extends BaseAdapterOptions<ListOptions> {

		/**
		 * Creator used to create an instance or array of instances of ListOptions from {@link Parcel}.
		 */
		public static final Parcelable.Creator<ListOptions> CREATOR = new Parcelable.Creator<ListOptions>() {

			/**
			 */
			@Override public ListOptions createFromParcel(@NonNull final Parcel source) { return new ListOptions(source); }

			/**
			 */
			@Override public ListOptions[] newArray(final int size) { return new ListOptions[size]; }
		};

		/**
		 * Creates a new instance of ListOptions.
		 */
		public ListOptions() {
			super();
		}

		/**
		 * Creates a new instance of ListOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public ListOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of ListOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected ListOptions(@NonNull final Parcel source) {
			super(source);
		}
	}
}