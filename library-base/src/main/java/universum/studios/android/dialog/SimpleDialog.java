/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.DialogButtonsView;
import universum.studios.android.dialog.view.DialogTitleView;

/**
 * A {@link BaseDialog} implementation that can be used to present a simple dialog with <b>title</b>,
 * <b>content</b> and <b>buttons</b>. The SimpleDialog will handle default binding of data from its
 * options to its views like setting title, content message and creation of the buttons and also
 * dispatching button click events to the attached listener.
 * <p>
 * This dialog uses following default implementations for its layout core components:
 * <ul>
 * <li>{@link universum.studios.android.dialog.widget.SimpleDialogTitleView DialogTitle}</li>
 * <li>{@link universum.studios.android.dialog.widget.SimpleDialogContentView DialogContentView}</li>
 * <li>{@link universum.studios.android.dialog.widget.SimpleDialogButtonsView DialogButtons}</li>
 * </ul>
 * <p>
 * This dialog provides only fixed API for accessing its view components like {@link #setIndeterminateProgressBarVisible(boolean)}
 * or {@link #setButtonEnabled(int, boolean)} and if you need to access more, you need to expose/implement
 * such API by yourself within your SimpleDialog implementation.
 * <p>
 * See {@link DialogOptions} for options that can be supplied to the SimpleDialog from the desired
 * context via {@link #newInstance(DialogOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Inflating</h3>
 * An instance of SimpleDialog can be inflated via {@link #inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)},
 * where the specified XmlParser should contain all necessary data for the inflation process. During
 * this process the dialog's default options are inflated via {@link DialogOptions#inflate(XmlResourceParser, Resources, Resources.Theme)}
 * and merged with the ones passed to the dialog's inflation method (if any) via {@link DialogOptions#merge(DialogOptions)}.
 * <p>
 * After the options are inflated, a particular dialog can inflate all its necessary data within
 * {@link #onParseXmlAttribute(XmlResourceParser, int, int, Resources, Resources.Theme)} that is
 * called while iterating the XmlParser's data.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogOptions dialogOptions} attribute.
 * <p>
 * Attributes for the options are parsed within {@link #onProcessOptionsStyle(Context, int)} that is
 * called while iterating {@link TypedArray} obtained for a style resource passed to
 * {@link #onProcessOptionsStyle(Context, int)}.
 *
 * <h3>Custom dialog</h3>
 * Basically, if you need to implement a custom type of dialog, you can do so by inheriting the
 * {@link SimpleDialog} class and overriding/implementing the following methods/classes:
 * <p>
 * <pre>
 * public class GalleryDialog extends SimpleDialog {
 *
 *      &#64;NonNull
 *      public static GalleryDialog newInstance(&#64;NonNull GalleryOptions options) {
 *          final GalleryDialog dialog = new GalleryDialog();
 *          dialog.setOptions(options);
 *          return dialog;
 *      }
 *
 *      &#64;NonNull
 *      &#64;Override
 *      protected DialogOptions onCreateDefaultOptions(@NonNull Resources resources) {
 *          return new GalleryOptions(resources);
 *      }
 *
 *      &#64;Override
 *      protected View onCreateContentView(&#64;NonNull LayoutInflater inflater, &#64;NonNull ViewGroup container, &#64;Nullable Bundle savedInstanceState) {
 *          // Return here a content view specific for this type of dialog.
 *          return inflater.inflate(R.layout.gallery_view, container, false);
 *      }
 *
 *      &#64;Override
 *      protected void onBindView(&#64;NonNull View dialogView, &#64;NonNull DialogOptions options, &#64;Nullable Bundle savedInstanceState) {
 *          super.onBindView(dialogView, options, savedInstanceState);
 *          // This method is called from within onViewCreated(...) to bind the dialogView with data
 *          // form the options.
 *
 *          final GalleryView galleryView = (GalleryView) dialogView.findViewById(R.id.gallery_view);
 *          if (galleryView != null) {
 *              galleryView.setAdapter(new GalleryAdapter());
 *              galleryView.setVisibleItemsCount(((GalleryOptions) options).visibleItemsCount);
 *              // ... other binding stuff
 *          }
 *
 *          // If your content view can save its current state after orientation change you do not
 *          // need to bind it after the state of your dialog has been restored.
 *          if (savedInstanceState == null) {
 *              // ... perform binding only if the dialog is first time created
 *          }
 *      }
 *
 *      // That's pretty much everything you need to implement, of course you can override much more
 *      // like:
 *      // onCreateTitleView(...) - to supply custom title view implementation
 *      // onCreateButtonsView(...) - to supply custom buttons view implementation
 *      // onParseOptionsStyleAttribute(...) - to parse attributes from the options style
 *      // onParseXmlAttribute(...) - to parse Xml attributes during inflation process
 *      // ...
 *
 *      // but remember, more you change the default behaviour of the dialog implementation than more
 *      // you will need to re-implement, like if you will need really different structure for buttons
 *      // view, you can implement what-ever view you like but you will need to handle binding of that
 *      // buttons view by self within onBindView(...) method.
 *
 *      public static class GalleryOptions extends DialogOptions&lt;GalleryOptions&gt; {
 *
 *          // Each options implementation should have its own parcelable creator.
 *          public static final Parcelable.Creator&lt;GalleryOptions&gt; CREATOR = new Parcelable.Creator&lt;GalleryOptions&gt;() {
 *              // ... creator implementation
 *          };
 *
 *          int visibleItemsCount = 3;
 *
 *          public GalleryOptions() {
 *          }
 *
 *          public GalleryOptions(&#64;NonNull Resources resources) {
 *              super(resources);
 *              // Constructor used when the options should have access to resources due to using
 *              // of string, text, ... resource ids when supplying data to options.
 *          }
 *
 *          protected GalleryOptions(&#64;NonNull Parcel source) {
 *              super(source);
 *              // Constructor called from the parcelable Creator.
 *              this.visibleItemsCount = source.readInt();
 *              // ... read other options values
 *          }
 *
 *          &#64;Override
 *          public void writeToParcel(&#64;NonNull Parcel dest, int flags) {
 *              super.writeToParcel(dest, flags);
 *              dest.writeInt(visibleItemsCount);
 *              // ... write other options values
 *          }
 *
 *          &#64;Override
 *          public GalleryOptions merge(&#64;NonNull DialogOptions options) {
 *              // Basically implementation of this method is required only for dialogs that are
 *              // intended to be inflated from an Xml file while also an instance of options is
 *              // supplied to the inflating dialog from the outside context so data from the
 *              // options inflated from the Xml file need to be merged with those from the outside
 *              // context.
 *              if (!(options instanceof GalleryOptions)) {
 *                  return super.merge(options);
 *              }
 *              final GalleryOptions galleryOptions = (GalleryOptions) options;
 *              // ... implement merging logic
 *              return super.merge(options);
 *          }
 *      }
 * }
 * </pre>
 * <p>
 * If you do not need any of the logic implemented within the SimpleDialog class you can also inherit
 * from the {@link BaseDialog} class and implement just methods to create core component views
 * (title, content and buttons), supply default options via {@link #onCreateDefaultOptions(Resources)}
 * and implement simple binding logic within {@link #onBindView(View, DialogOptions, Bundle)}
 * method.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SimpleDialog extends BaseDialog implements XmlDialog, DialogButtonsView.OnButtonsClickListener {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SimpleDialog(int)} with {@link R.attr#dialogOptions dialogOptions} as options style
	 * attribute.
	 */
	public SimpleDialog() {
		this(R.attr.dialogOptions);
	}

	/**
	 * Creates a new instance of simple Dialog with default options (title, icon, buttons, ...)
	 * specified by an options style within the current dialogs theme by the specified <var>optionsStyleAttr</var>.
	 *
	 * @param optionsStyleAttr An attribute which contains a reference to the options (style) specific
	 *                         for this type of dialog within the current dialogs theme.
	 */
	@SuppressLint("ValidFragment")
	protected SimpleDialog(@AttrRes final int optionsStyleAttr) {
		super(optionsStyleAttr);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of Dialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New Dialog instance.
	 */
	@NonNull public static SimpleDialog newInstance(@NonNull final DialogOptions options) {
		final SimpleDialog dialog = new SimpleDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override public void inflate(
			@NonNull final XmlResourceParser parser,
			@NonNull final Resources resources,
			@Nullable final Resources.Theme theme,
			@Nullable DialogOptions options
	) {
		if (options == null) {
			options = onCreateDefaultOptions(resources);
			options.inflate(parser, resources, theme);
		} else {
			final DialogOptions xmlOptions = onCreateDefaultOptions(resources);
			xmlOptions.inflate(parser, resources, theme);
			xmlOptions.merge(options);
			options = xmlOptions;
		}
		final int attrCount = parser.getAttributeCount();
		for (int i = 0; i < attrCount; i++) {
			onParseXmlAttribute(parser, i, parser.getAttributeNameResource(i), resources, theme);
		}
		setOptions(options);
	}

	/**
	 * Invoked for each index of the currently being processed {@code XmlResourceParser} passed to
	 * {@link #inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)} method.
	 *
	 * @param xmlParser Xml parser with data for this dialog.
	 * @param index     The index of attribute to parse within the passed <var>xmlParser</var>.
	 * @param attr      The id of attribute to parse, like {@link R.attr#dialogCancelable}.
	 * @param resources Application resources to be used to resolve attribute values.
	 * @param theme     Theme to apply to the attribute values. May be {@code null}.
	 */
	protected void onParseXmlAttribute(
			@NonNull XmlResourceParser xmlParser,
			int index,
			@AttrRes int attr,
			@NonNull Resources resources,
			@Nullable Resources.Theme theme
	) {}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new DialogOptions(resources);
	}

	/**
	 */
	@SuppressWarnings("ResourceType")
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		final TypedArray optionsArray = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options);
		if (optionsArray != null) {
			final int n = optionsArray.getIndexCount();
			for (int i = 0; i < n; i++) {
				int index = optionsArray.getIndex(i);
				if (index == R.styleable.Dialog_Options_dialogIcon) {
					if (!mOptions.isBasicSet(DialogOptions.ICON)) {
						mOptions.icon(optionsArray.getResourceId(index, 0));
					}
				} else if (index == R.styleable.Dialog_Options_dialogVectorIcon) {
					if (!mOptions.isBasicSet(DialogOptions.VECTOR_ICON)) {
						mOptions.vectorIcon(optionsArray.getResourceId(index, 0));
					}
				} else if (index == R.styleable.Dialog_Options_dialogTitle) {
					if (!mOptions.isBasicSet(DialogOptions.ICON)) {
						mOptions.title(optionsArray.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_dialogContent) {
					if (!mOptions.isBasicSet(DialogOptions.CONTENT)) {
						mOptions.content(optionsArray.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_dialogPositiveButton) {
					if (!mOptions.isBasicSet(DialogOptions.BUTTONS)) {
						mOptions.positiveButton(optionsArray.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_dialogNeutralButton) {
					if (!mOptions.isBasicSet(DialogOptions.BUTTONS)) {
						mOptions.neutralButton(optionsArray.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_dialogNegativeButton) {
					if (!mOptions.isBasicSet(DialogOptions.BUTTONS)) {
						mOptions.negativeButton(optionsArray.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_dialogButtonsWidthMode) {
					if (!mOptions.isBasicSet(DialogOptions.BUTTONS_WIDTH_MODE)) {
						mOptions.buttonsWidthMode(optionsArray.getInt(index, mOptions.buttonsWidthMode()));
					}
				} else if (index == R.styleable.Dialog_Options_dialogCancelable) {
					if (!mOptions.isBasicSet(DialogOptions.CANCELABLE)) {
						setCancelable(optionsArray.getBoolean(index, mOptions.shouldBeCancelable()));
					}
				} else if (index == R.styleable.Dialog_Options_dialogRemain) {
					if (!mOptions.isBasicSet(DialogOptions.REMAIN)) {
						mOptions.remain(optionsArray.getBoolean(index, mOptions.shouldRemain()));
					}
				} else if (index == R.styleable.Dialog_Options_dialogDismissOnRestore) {
					if (!mOptions.isBasicSet(DialogOptions.DISMISS_ON_RESTORE)) {
						mOptions.dismissOnRestore(optionsArray.getBoolean(index, mOptions.shouldDismissOnRestore()));
					}
				}
			}
			optionsArray.recycle();
		}
	}

	/**
	 */
	@Override @NonNull protected View onCreateTitleView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_title, container, false);
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(
				mOptions != null && mOptions.mContentLayout > 0 ? mOptions.mContentLayout : R.layout.dialog_content,
				container,
				false
		);
	}

	/**
	 */
	@Override @NonNull protected View onCreateButtonsView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_buttons, container, false);
	}

	/**
	 */
	@Override protected void onBindView(
			@NonNull final View dialogView,
			@NonNull final DialogOptions options,
			@Nullable final Bundle savedInstanceState
	) {
		// By default, implementations of dialog views saves its state so perform binding only if
		// this saved state is not available an also this is invoked whenever the current options
		// has been changed with null savedInstanceState.
		if (savedInstanceState == null) {
			onBindTitleView(getTitleView(), options);
			onBindContentView(getContentView(), options);
			onBindButtonsView(getButtonsView(), options);
		}
	}

	/**
	 * Invoked whenever {@link #onBindView(View, DialogOptions, Bundle)}
	 * is called with {@code null} <var>savedInstanceState</var>.
	 *
	 * @param titleView Current title view presented within dialog layout.
	 * @param options   Current dialog options.
	 */
	void onBindTitleView(final View titleView, final DialogOptions options) {
		if (titleView instanceof DialogTitleView) {
			final DialogTitleView view = (DialogTitleView) titleView;
			boolean showTitle = false;
			// Resolve title icon.
			if (options.hasVectorIcon()) {
				view.setVectorIconResource(options.mVectorIcon);
				showTitle = true;
			} else if (options.hasIcon()) {
				view.setIconResource(options.mIcon);
				showTitle = true;
			} else {
				view.setIconVisible(false);
			}
			// Resolve title text.
			if (options.hasTitle()) {
				view.setTitle(options.mTitle);
				showTitle = true;
			} else {
				view.setTitleVisible(false);
			}
			// Resolve title view visibility.
			view.setVisibility(showTitle ? View.VISIBLE : View.GONE);
		}
	}

	/**
	 * Invoked whenever {@link #onBindView(View, DialogOptions, Bundle)}
	 * is called with {@code null} <var>savedInstanceState</var>.
	 *
	 * @param contentView Current body view presented within dialog layout.
	 * @param options     Current dialog options.
	 */
	void onBindContentView(final View contentView, final DialogOptions options) {
		if (contentView == null) return;
		final boolean hasContent = !TextUtils.isEmpty(options.mContent);
		if (contentView instanceof TextView) {
			this.bindContentView((TextView) contentView, options);
			contentView.setVisibility(hasContent ? View.VISIBLE : View.GONE);
		} else if (contentView instanceof ViewGroup) {
			final TextView textViewContent = (TextView) contentView.findViewById(R.id.dialog_text_view_content);
			if (textViewContent != null) {
				this.bindContentView(textViewContent, options);
				textViewContent.setVisibility(hasContent ? View.VISIBLE : View.GONE);
			}
		}
	}

	/**
	 * Binds the specified <var>contentView</var> with the content text provided by the given <var>options</var>.
	 *
	 * @param contentView The content view to bind with text.
	 * @param options     The dialog options of this dialog.
	 */
	private void bindContentView(final TextView contentView, final DialogOptions options) {
		if (!TextUtils.isEmpty(options.mContent)) {
			final int contentFlags = options.mContentFlags;
			final boolean trim = (contentFlags & DialogOptions.CONTENT_TRIM) != 0;
			final boolean asHtml = (contentFlags & DialogOptions.CONTENT_AS_HTML) != 0;

			final CharSequence content = trim ? trimText(options.mContent) : options.mContent;

			if (asHtml) contentView.setText(Html.fromHtml(content.toString()));
			else contentView.setText(content);
		} else {
			contentView.setText("");
		}
	}

	/**
	 * Removes whitespaces from the start and end of the specified <var>text</var>.
	 *
	 * @param text The text to be trimmed.
	 * @return Trimmed text.
	 */
	private CharSequence trimText(final CharSequence text) {
		int length = text.length();
		final int trimmedLength = TextUtils.getTrimmedLength(text);
		if (length > trimmedLength) {
			final SpannableStringBuilder builder = new SpannableStringBuilder(text);

			// Remove white spaces from the start.
			int start = 0;
			while (start < length && builder.charAt(start) <= ' ') {
				start++;
			}
			builder.delete(0, start);
			length -= start;

			// Remove white spaces from the end.
			int end = length;
			while (end >= 0 && builder.charAt(end - 1) <= ' ') {
				end--;
			}
			builder.delete(end, length);
			return builder;
		}
		return text;
	}

	/**
	 * Invoked whenever {@link #onBindView(View, DialogOptions, Bundle)}
	 * is called with {@code null} <var>savedInstanceState</var>.
	 *
	 * @param buttonsView Current buttons view presented within dialog layout.
	 * @param options     Current dialog options.
	 */
	void onBindButtonsView(final View buttonsView, final DialogOptions options) {
		if (buttonsView instanceof DialogButtonsView) {
			final DialogButtonsView view = (DialogButtonsView) buttonsView;
			if (!options.hasButtons()) {
				view.setVisibility(View.GONE);
				return;
			}

			view.setButtonsWidthMode(options.mButtonsWidthMode);
			for (int i = 0; i < options.mButtons.size(); i++) {
				final DialogOptions.Button button = (DialogOptions.Button) options.mButtons.get(i);
				final DialogButtonsView.DialogButton dialogButton = view.newButton(button.getId(), button.getCategory());
				dialogButton.setText(button.getText());
				view.addButton(dialogButton);
			}
		}
	}

	/**
	 */
	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final View buttonsView = getButtonsView();
		if (buttonsView instanceof DialogButtonsView) {
			((DialogButtonsView) buttonsView).registerOnButtonsClickListener(this);
		}
		if (savedInstanceState == null) {
			onUpdateContentViewPadding();
		}
	}

	/**
	 * Invoked to update the padding of the content view according to the current visibility of other
	 * dialog view hierarchy components.
	 * <p>
	 * Inheritance hierarchies may override this method to implement custom correction of content
	 * view's padding or to not perform any correction at all.
	 */
	protected void onUpdateContentViewPadding() {
		final View contentView = getContentView();
		if (contentView == null) {
			return;
		}
		int dialogPrimarySpacing = 0;
		final Resources.Theme theme = mLayoutInflater.getContext().getTheme();
		if (theme != null) {
			final TypedValue typedValue = new TypedValue();
			if (theme.resolveAttribute(R.attr.dialogSpacingPrimary, typedValue, true)) {
				dialogPrimarySpacing = Math.round(typedValue.getDimension(getResources().getDisplayMetrics()));
			}
		}
		final View titleView = getTitleView();
		final View buttonsView = getButtonsView();
		int paddingTop = contentView.getPaddingTop();
		int paddingBottom = contentView.getPaddingBottom();
		boolean updatePadding = false;
		if ((titleView == null || titleView.getVisibility() == View.GONE) && paddingTop == 0) {
			paddingTop = dialogPrimarySpacing;
			updatePadding = true;
		}
		if ((buttonsView == null || buttonsView.getVisibility() == View.GONE) && paddingBottom == 0) {
			paddingBottom = dialogPrimarySpacing;
			updatePadding = true;
		}
		if (updatePadding) {
			contentView.setPadding(
					contentView.getPaddingLeft(),
					paddingTop,
					contentView.getPaddingRight(),
					paddingBottom
			);
		}
	}

	/**
	 */
	@Override public void onViewStateRestored(@Nullable final Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		// Update padding of the content view depends on the title/buttons view visibility.
		onUpdateContentViewPadding();
	}

	/**
	 */
	@Override public void setButtonEnabled(final int buttonId, final boolean enabled) {
		final View buttonsView = getButtonsView();
		if (buttonsView instanceof DialogButtonsView) {
			((DialogButtonsView) buttonsView).setButtonEnabled(buttonId, enabled);
		}
	}

	/**
	 */
	@Override public void onButtonClick(@NonNull final DialogButtonsView buttons, @NonNull final DialogButtonsView.DialogButton button) {
		notifyButtonClick(button.getId());
	}

	/**
	 */
	public void setIndeterminateProgressBarVisible(final boolean visible) {
		final View titleView = getTitleView();
		if (titleView instanceof DialogTitleView) {
			((DialogTitleView) titleView).setIndeterminateProgressBarVisible(visible);
		}
	}

	/**
	 * Returns a flag indicating whether the indeterminate {@code ProgressBar} within DialogTitleView
	 * is visible or not.
	 *
	 * @return {@code True} if it is visible, {@code false} otherwise.
	 * @see DialogTitleView#isIndeterminateProgressBarVisible()
	 */
	public boolean isIndeterminateProgressBarVisible() {
		final View titleView = getTitleView();
		return titleView instanceof DialogTitleView && ((DialogTitleView) titleView).isIndeterminateProgressBarVisible();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}