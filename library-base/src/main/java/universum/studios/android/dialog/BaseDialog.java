/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.AttrRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.dialog.view.DialogLayout;
import universum.studios.android.dialog.widget.DialogBasicLayout;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link DialogFragment} implementation as base used by dialogs from the Dialogs library. This
 * dialog fragment class specifies base structure to create a view hierarchy for its dialog and to
 * bind it with data provided by {@link DialogOptions} specified
 * via {@link #setOptions(DialogOptions)}.
 *
 * <h3>Dialog options</h3>
 * As mentioned above, the dialog options are used to specify a set of data that should be presented
 * within the dialog's view hierarchy or to determine a specific dialog's behavior relevant for that
 * type of dialog. BaseDialog requires a default options to be always available so the derived
 * classes must implement {@link #onCreateDefaultOptions(Resources)} method as
 * it can be invoked whenever the BaseDialog does not have valid options available.
 * BaseDialog uses these options as described below:
 * <ul>
 * <li>
 * {@link DialogOptions#shouldBeCancelable()}
 * - to determine whether the dialog can be canceled or not ({@link #setCancelable(boolean)}),
 * </li>
 * <li>
 * {@link DialogOptions#shouldDismissOnRestore()}
 * - to determine whether the dialog should be dismissed after it was restored (after orientation change),
 * </li>
 * <li>
 * {@link DialogOptions#shouldRemain()}
 * - to determine whether the dialog should remain showing after the {@link Dialog#BUTTON_POSITIVE}
 * or {@link Dialog#BUTTON_NEUTRAL} button click.
 * </li>
 * </ul>
 * Also the options are passed along the dialog's hierarchy calls where they are appropriate.
 * <p>
 * Dialog's current options can be obtained via {@link #getOptions()}.
 *
 * <h3>View hierarchy</h3>
 * By default the BaseDialog builds its view hierarchy using implementation of {@link DialogLayout}
 * that places supplied views (title, content, buttons) the following way:
 * <pre>
 *  _______________________
 * |         TITLE         |
 * |_______________________|
 * |                       |
 * |        CONTENT        |
 * |_______________________|
 * |        BUTTONS        |
 * |_______________________|
 * </pre>
 * The custom layout can be supplied via {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)},
 * but doing so will require implementation of custom layout building logic (if the custom layout is
 * not implementing {@link DialogLayout} interface).
 * <p>
 * When the BaseDialog is building its layout it calls following methods in the exact order as listed
 * below and supplies them to the DialogLayout implementation:
 * <ul>
 * <li>{@link #onCreateTitleView(LayoutInflater, ViewGroup, Bundle)}</li>
 * <li>{@link #onCreateContentView(LayoutInflater, ViewGroup, Bundle)}</li>
 * <li>{@link #onCreateButtonsView(LayoutInflater, ViewGroup, Bundle)}</li>
 * </ul>
 * <b>All the methods listed above are required to be implemented.</b>
 * <p>
 * If the default building process of the view hierarchy is untouched by the derived dialog class the
 * following methods can be used to access core view components:
 * <ul>
 * <li>{@link #getLayout()}</li>
 * <li>{@link #getTitleView()}</li>
 * <li>{@link #getContentView()}</li>
 * <li>{@link #getButtonsView()}</li>
 * </ul>
 * <p>
 * After the dialog's view is created the data binding process takes its place and
 * {@link #onBindView(View, DialogOptions, Bundle)} is invoked from
 * within {@link #onViewCreated(View, Bundle)}. <b>Note</b>, that the binding
 * process is also re-invoked whenever the dialog's options has been changed, so {@link #onOptionsChanged(DialogOptions)}
 * has been invoked.
 *
 * <h3>Data binding</h3>
 * BaseDialog does not handle any data binding to the view hierarchy from the dialog options what
 * so ever. This is responsibility of the concrete implementation of the BaseDialog class that should
 * be implemented within {@link #onBindView(View, DialogOptions, Bundle)}.
 * <p>
 * Also note, that if the concrete implementation of the BaseDialog need to save its current state
 * that can be restored after orientation change, it should do so within {@link #onSaveInstanceState()}
 * and within {@link #onRestoreInstanceState(Parcelable)} as described below:
 * <p>
 * <pre>
 * public class MyDialog extends BaseDialog {
 *
 *     // ... implementation logic
 *
 *     &#64;Override
 *     protected Parcelable onSaveInstanceState() {
 *          final SavedState state = new SavedState(super.onSaveInstanceState);
 *          // ... attach here data to be saved to the state
 *          return state;
 *     }
 *
 *     &#64;Override
 *     protected void onRestoreInstanceState(&#64;NonNull Parcelable savedState) {
 *          if (!(savedState instanceof SavedState)) {
 *              return super.onRestoreInstanceState(state);
 *          }
 *
 *          final SavedState state = (SavedState) savedState;
 *          super.onRestoreInstanceState(state.getSuperState());
 *          // ... restore here saved state
 *     }
 *
 *     // ... implementation logic, again
 *
 *     static final class SavedState extends BaseDialog.BaseSavedState {
 *
 *         // Each saved state implementation should have its own parcelable creator.
 *         public static final Creator&lt;SavedState&gt; CREATOR = new Creator&lt;SavedState&gt;() {
 *              // ... creator implementation
 *         }
 *
 *         SavedState(&#64;NonNull Parcelable superState) {
 *             super(superState);
 *         }
 *
 *         SavedState(&#64;NonNull Parcel source) {
 *             super(source);
 *             // ... restore here saved data
 *         }
 *
 *         &#64;Override
 *         public void writeToParcel(&#64;NonNull Parcel dest, int flags) {
 *             super.writeToParcel(dest, flags);
 *             // ... write here data to be saved
 *         }
 *     }
 * }
 * </pre>
 * <b>Note, that saving/restoring of the dialog's state is meant for preserving of data that are
 * "private" for the dialog's context/use only and not for preserving of data presented within
 * its view hierarchy. Such a saving/restoring should be handled within the view hierarchy and by
 * its views itself.</b> This is because of the restoring of the dialog's state is invoked from
 * {@link #onCreate(Bundle)} method when at that time the dialog's view is not created
 * yet, thus cannot be restored.
 * <p>
 * If you still insists to restore the dialog's view state within dialog's context you can do so
 * via standard {@link #onSaveInstanceState(Bundle)} and {@link #onViewCreated(View, Bundle)}
 * methods.
 *
 * <h3>Callbacks</h3>
 * All listeners listed below can be attached to a specific instance (implementation) of BaseDialog
 * class:
 * <ul>
 * <li>{@link Dialog.OnDialogListener}</li>
 * <li>{@link Dialog.OnCancelListener}</li>
 * <li>{@link Dialog.OnDismissListener}</li>
 * <li>{@link Dialog.OnShowListener}</li>
 * </ul>
 * <p>
 * <b>Note, that dialog can automatically detect these listeners if they are implemented by the
 * context (Activity or Fragment) in which is that particular dialog shown, so you do not need to
 * "manually" set any of the listeners described above.</b>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class BaseDialog extends DialogFragment
        implements
        Dialog,
        OptionsDialog,
        ContextDialog,
        DialogOptions.Callback {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    private static final String TAG = "BaseDialog";

    /**
     * Bundle key for dialog options.
     */
    private static final String BUNDLE_OPTIONS = "universum.studios.android.dialog.BaseDialog.BUNDLE.DialogOptions";

    /**
     * Bundle key for dialog saved state.
     */
    private static final String BUNDLE_SAVED_STATE = "universum.studios.android.dialog.BaseDialog.BUNDLE.SavedState";

	/*
	 * Interface ===================================================================================
	 */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Options specific for this type of dialog.
     */
    DialogOptions mOptions;

    /**
     * Dialog listener to receive callbacks specific for the current type of this dialog.
     */
    OnDialogListener mListener;

    /**
     * Dialog listener to receive callback that this dialog has been showed.
     */
    OnShowListener mShowListener;

    /**
     * Dialog listener to receive callback that this dialog has been canceled.
     */
    OnCancelListener mCancelListener;

    /**
     * Dialog listener to receive callback that this dialog has been dismissed.
     */
    OnDismissListener mDismissListener;

    /**
     * This dialog's id. Should be unique in the same context.
     */
    int mDialogId;

    /**
     * Layout inflater which can be used to inflate views for this dialog's context.
     */
    LayoutInflater mLayoutInflater;

    /**
     * Attribute for options for this dialog presented within the current theme.
     */
    private int mOptionsStyleAttr;

    /**
     * Id of this main dialog view.
     */
    private int mDialogLayoutId = R.id.dialog_layout;

    /**
     * Tag of the fragment within which is this dialog displayed, {@code null} by default.
     */
    private String mContextFragmentTag;

    /**
     * Id of the fragment within which is this dialog displayed, {@code -1} by default.
     */
    private int mContextFragmentId = -1;

    /**
     * Main layout of this dialog instance.
     */
    private View mDialogLayout;

    /**
     * Flag indicating whether a view of this dialog is created or not.
     */
    private boolean mViewCreated;

    /**
     * Flag indicating whether a super method call has been performed or not.
     */
    private boolean mCalled;

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #BaseDialog(int)} with {@code 0} as <var>styleAttr</var>.
     */
    public BaseDialog() {
        this(0);
    }

    /**
     * Creates a new instance of BaseDialog to provide base structure for an Android Dialog component.
     *
     * @param optionsStyleAttr An attribute which contains a reference to the options (style) specific
     *                         for this type of dialog within the current dialogs theme.
     */
    protected BaseDialog(@AttrRes final int optionsStyleAttr) {
        this.mOptionsStyleAttr = optionsStyleAttr;
    }

    /*
     * Methods =====================================================================================
     */

    /**
     */
    @Override public final void setDialogId(final int dialogId) {
        this.mDialogId = dialogId;
    }

    /**
     */
    @Override public final int getDialogId() {
        return mDialogId;
    }

    /**
     * If this dialog has already its view created, this will cause the view of this dialog to be
     * updated with data from the specified options.
     */
    @Override public void setOptions(@NonNull final DialogOptions options) {
        if (mOptions != null) {
            mOptions.setCallback(null);
        }
        this.mOptions = options;
        mOptions.setCallback(this);
        setCancelable(options.shouldBeCancelable());
        onOptionsChanged(options);
    }

    /**
     */
    @Override @Nullable public DialogOptions getOptions() {
        return mOptions;
    }

    /**
     * This implementation will re-bind the current view of this dialog by {@link #onBindView(View, DialogOptions, Bundle)}.
     */
    @Override public void onOptionsChanged(@NonNull final DialogOptions options) {
        if (mOptions == options && mViewCreated) this.bindView(null);
    }

    /**
     * Registers a callback to be invoked when a specific action for this type of dialog occurs.
     * In most cases, this is basically button click action.
     *
     * @param listener Listener callback. May be {@code null} to clear the current one.
     */
    public void setOnDialogListener(@Nullable final OnDialogListener listener) {
        this.mListener = listener;
    }

    /**
     * Registers a callback to be invoked after this dialog has been showed.
     *
     * @param listener Listener callback. May be {@code null} to clear the current one.
     */
    public void setOnShowListener(@Nullable final OnShowListener listener) {
        this.mShowListener = listener;
    }

    /**
     * Registers a callback to be invoked after this dialog has been canceled.
     *
     * @param listener Listener callback. May be {@code null} to clear the current one.
     */
    public void setOnCancelListener(@Nullable final OnCancelListener listener) {
        this.mCancelListener = listener;
    }

    /**
     * Registers a callback to be invoked after this dialog has been dismissed.
     *
     * @param listener Listener callback. May be {@code null} to clear the current one.
     */
    public void setOnDismissListener(@Nullable final OnDismissListener listener) {
        this.mDismissListener = listener;
    }

    /**
     */
    @Override public final void setContextFragmentTag(@Nullable final String fragmentTag) {
        this.mContextFragmentTag = fragmentTag;
    }

    /**
     * Returns a tag of the fragment (<i>if this dialog instance was displayed within Fragment context</i>)
     * within which context is this dialog showing.
     *
     * @return Fragment tag or {@code null} by default.
     * @see #setContextFragmentTag(String)
     */
    @Nullable public final String getContextFragmentTag() {
        return mContextFragmentTag;
    }

	/**
	 */
	@Override public final void setContextFragmentId(final int fragmentId) {
		this.mContextFragmentId = fragmentId;
	}

    /**
     * Returns an id of the fragment (<i>if this dialog instance was displayed within Fragment context</i>)
     * within which context is this dialog showing.
     *
     * @return Fragment id or {@code -1} by default.
     * @see #setContextFragmentId(int)
     */
    public final int getContextFragmentId() {
        return mContextFragmentId;
    }

    /**
     * Returns an instance of Fragment within which context is this dialog showing.
     *
     * @return Context fragment or {@code null} if this dialog was showed within Activity context.
     */
    @Nullable protected final Fragment findContextFragment() {
        if (mContextFragmentTag == null && mContextFragmentId < 0) {
            return null;
        }
        final FragmentManager manager = getFragmentManager();
        if (manager == null) {
            return null;
        }
        // First check by tag, then by id.
        final Fragment fragment = manager.findFragmentByTag(mContextFragmentTag);
        return fragment != null ? fragment : manager.findFragmentById(mContextFragmentId);
    }

    /**
     */
    @Override public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context = getActivity();
        // Check if restore this dialog instance state.
        if (savedInstanceState != null) {
            // User class loader from this library, so unmarshaling of custom parcelable object will
            // not cause exceptions.
            savedInstanceState.setClassLoader(DialogOptions.class.getClassLoader());
            this.mOptions = savedInstanceState.getParcelable(BUNDLE_OPTIONS);
            if (mOptions != null && mOptions.shouldDismissOnRestore()) {
                // Dialog shouldn't be visible after restore, so dismiss it.
                dismiss();
            }
            // Dispatch to restore instance state.
            restoreState(savedInstanceState.getParcelable(BUNDLE_SAVED_STATE));
        } else {
            // Create default options if we don't have any yet.
            if (mOptions == null) {
                this.mOptions = onCreateDefaultOptions(getResources());
            }
        }
        int dialogTheme = android.R.style.Theme_Dialog;
        final TypedValue typedValue = new TypedValue();
        final Resources.Theme theme = context.getTheme();
        // Get the dialog theme from the current application theme.
        if (theme.resolveAttribute(R.attr.dialogTheme, typedValue, true)) {
            dialogTheme = typedValue.resourceId;
            // Process options from the theme if this is the first time when this dialog is
            // being created.
            if (savedInstanceState == null) {
                this.processDialogOptionsStyle(dialogTheme);
            }
        }
        // If the options specify dialog theme, such theme will override the one presented within the
        // current application theme.
        if (mOptions.mTheme > 0) {
            dialogTheme = mOptions.mTheme;
        }
        super.setStyle(STYLE_NO_FRAME, dialogTheme);
    }

    /**
     * Invoked to create default options for this dialog fragment instance. This is called whenever
     * the current options (when needed) are {@code null}.
     *
     * @param resources An application resources. Can be used to create options which needs to access
     *                  values from resources, like strings, ....
     * @return Default options for this dialog.
     */
    @NonNull protected abstract DialogOptions onCreateDefaultOptions(@NonNull Resources resources);

    /**
     * Performs processing of the options specific fro this type of dialog from the current dialogs
     * theme.
     * <p>
     * Will call {@link #onProcessOptionsStyle(Context, int)}.
     *
     * @param dialogTheme Dialog theme.
     */
    private void processDialogOptionsStyle(final int dialogTheme) {
        if (mOptionsStyleAttr > 0) {
            final Context context = getActivity();
            final TypedArray themeArray = context.obtainStyledAttributes(dialogTheme, new int[]{mOptionsStyleAttr});
            if (themeArray != null) {
                final int optionsStyleRes = themeArray.getResourceId(0, -1);
                if (optionsStyleRes != -1) {
                    onProcessOptionsStyle(context, optionsStyleRes);
                }
                themeArray.recycle();
            }
        }
    }

    /**
     * Invoked from {@link #onCreate(Bundle)}, when this dialog is being first time created
     * to obtain data for options of this dialog from the values of the specified <var>optionsStyle</var>.
     *
     * @param context      Current context.
     * @param optionsStyle The style resource with values for dialog options.
     */
    protected void onProcessOptionsStyle(@NonNull Context context, @StyleRes int optionsStyle) {}

    /**
     * @see #onCreateLayout(LayoutInflater, ViewGroup, Bundle)
     */
    @Override public View onCreateView(
    		@NonNull final LayoutInflater inflater,
		    @Nullable final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        final View dialogLayout = onCreateLayout(mLayoutInflater = inflater, container, savedInstanceState);
        // Build dialog layout.
        this.buildLayout(inflater, dialogLayout, savedInstanceState);
        // Mark created layout.
        if (dialogLayout.getId() != View.NO_ID) {
            this.mDialogLayoutId = dialogLayout.getId();
        } else {
            dialogLayout.setId(mDialogLayoutId);
        }
        return dialogLayout;
    }

    /**
     */
    @Override @Nullable public final LayoutInflater getDialogLayoutInflater() {
        return mLayoutInflater;
    }

    /**
     * Invoked from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * to create main layout for this dialog.
     *
     * @param inflater           Layout inflater which can be used to inflate custom layouts.
     * @param container          Parent container for dialog view.
     * @param savedInstanceState Saved state passed to {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @return By default returns an implementation of {@link DialogLayout}.
     * @see #onCreateTitleView(LayoutInflater, ViewGroup, Bundle)
     * @see #onCreateContentView(LayoutInflater, ViewGroup, Bundle)
     * @see #onCreateButtonsView(LayoutInflater, ViewGroup, Bundle)
     */
    @NonNull protected ViewGroup onCreateLayout(
    		@NonNull final LayoutInflater inflater,
		    @Nullable final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        return new DialogBasicLayout(inflater.getContext());
    }

    /**
     * Performs building of layout for this dialog fragment.
     *
     * @param inflater           Layout inflater which can be used to inflate custom layouts.
     * @param dialogView         Dialog view created by {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState Saved state passed to {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     */
    private void buildLayout(final LayoutInflater inflater, final View dialogView, final Bundle savedInstanceState) {
        if (dialogView instanceof ViewGroup) {
            if (dialogView instanceof DialogLayout) {
                final DialogLayout view = (DialogLayout) dialogView;
                view.addTitleView(onCreateTitleView(inflater, (ViewGroup) dialogView, savedInstanceState));
                view.addContentView(onCreateContentView(inflater, (ViewGroup) dialogView, savedInstanceState));
                view.addButtonsView(onCreateButtonsView(inflater, (ViewGroup) dialogView, savedInstanceState));
            } else {
                final ViewGroup viewGroup = (ViewGroup) dialogView;
                viewGroup.addView(onCreateTitleView(inflater, viewGroup, savedInstanceState));
                viewGroup.addView(onCreateContentView(inflater, viewGroup, savedInstanceState));
                viewGroup.addView(onCreateButtonsView(inflater, viewGroup, savedInstanceState));
            }
        }
    }

    /**
     * Invoked from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * to create title view for this dialog.
     *
     * @param inflater           Layout inflater which can be used to inflate custom layouts.
     * @param container          Parent container for dialog title view. This is the same view as
     *                           created by {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState Saved state passed to {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @return Implementation should return view which can support <b>icon + title</b> presentation.
     * @see #onCreateContentView(LayoutInflater, ViewGroup, Bundle)
     * @see #onCreateButtonsView(LayoutInflater, ViewGroup, Bundle)
     */
    @NonNull protected abstract View onCreateTitleView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * Invoked from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * to create content view for this dialog.
     *
     * @param inflater           Layout inflater which can be used to inflate custom layouts.
     * @param container          Parent container for dialog content view. This is the same view as
     *                           created by {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState Saved state passed to {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @return Implementation should return view which can support content presentation specific
     * for this type of dialog.
     * @see #onCreateTitleView(LayoutInflater, ViewGroup, Bundle)
     * @see #onCreateButtonsView(LayoutInflater, ViewGroup, Bundle)
     */
    @NonNull protected abstract View onCreateContentView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * Invoked from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * to create buttons view for this dialog.
     *
     * @param inflater           Layout inflater which can be used to inflate custom layouts.
     * @param container          Parent container for dialog buttons view. This is the same view as
     *                           created by {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState Saved state passed to {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @return Implementation should return view which can support multiple buttons presentation.
     * @see #onCreateTitleView(LayoutInflater, ViewGroup, Bundle)
     * @see #onCreateButtonsView(LayoutInflater, ViewGroup, Bundle)
     */
    @NonNull protected abstract View onCreateButtonsView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * @see #onBindView(View, DialogOptions, Bundle)
     */
    @Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mViewCreated = true;
        if ((mDialogLayout = view.findViewById(mDialogLayoutId)) != null) {
            // Dispatch to set up dialog view.
            this.bindView(savedInstanceState);
        }
        final android.app.Dialog dialog = getDialog();
        if (dialog != null) {
            this.hideFrameworkTitleDivider(dialog);
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                /**
                 */
                @Override public void onShow(@NonNull final DialogInterface dialogInterface) {
                    dialog.setOnShowListener(null);
                    BaseDialog.this.onShow(dialogInterface);
                }
            });
        }
    }

    /**
     * Performs binding of this dialog fragment instance.
     * <p>
     * Will call {@link #onBindView(View, DialogOptions, Bundle)} if
     * the current dialog view is valid.
     *
     * @param savedInstanceState Saved dialog state or {@code null} if this dialog instance does
     *                           not saves its state or this is called due to change in dialog options.
     */
    private void bindView(final Bundle savedInstanceState) {
        if (mDialogLayout != null) onBindView(mDialogLayout, mOptions, savedInstanceState);
    }

    /**
     * Invoked from {@link #onViewCreated(View, Bundle)}.
     * <p>
     * This is a good place to set all data from the passed <var>options</var> to view of this dialog
     * fragment before its dialog will be showed to a user.
     *
     * @param dialogView         Dialog view created by {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)}
     * @param options            Current dialog options.
     * @param savedInstanceState Saved state of this dialog instance or {@code null} if this is
     *                           called due to change in the current <var>options</var> or this dialog
     *                           fragment does not saves its state.
     */
    protected abstract void onBindView(@NonNull View dialogView, @NonNull DialogOptions options, @Nullable Bundle savedInstanceState);

    /**
     * Hides the divider visible within the framework's dialog title.
     *
     * @param dialog Dialog of this dialog fragment.
     */
    private void hideFrameworkTitleDivider(final android.app.Dialog dialog) {
        try {
            final int dividerId = getResources().getIdentifier("android:id/titleDivider", null, null);
            final View divider = dialog.findViewById(dividerId);
            if (divider != null) {
                divider.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to hide framework's title divider view.", e);
        }
    }

	/**
	 * Returns the main layout of this dialog created via {@link #onCreateLayout(LayoutInflater, ViewGroup, Bundle)}.
	 *
	 * @return This dialog's main layout or {@code null} if this dialog instance does not have its
	 * view created yet.
	 * @see #getTitleView()
	 * @see #getContentView()
	 * @see #getButtonsView()
	 */
	@Nullable protected final View getLayout() {
		return mDialogLayout;
	}

    /**
     * Same as calling {@link View#findViewById(int)} upon the root view of this dialog fragment instance.
     *
     * @return The desired view with the specified id or {@code null} if there is no such a view
     * or this dialog fragment does not have its view created yet.
     */
    @Nullable protected final View findViewById(@IdRes final int id) {
        final View view = getView();
        return (view != null) ? view.findViewById(id) : null;
    }

    /**
     * Returns the current title view presented within this dialog's layout with the
     * {@link R.id#dialog_title} id.
     * <p>
     * If this library is used properly, this will be the same view as created by
     * {@link #onCreateTitleView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @return Title view or {@code null} if this dialog does not have created its layout yet.
     * @see #getContentView()
     * @see #getButtonsView()
     * @see #getLayout()
     */
    @Nullable protected View getTitleView() {
        return mDialogLayout != null ? mDialogLayout.findViewById(R.id.dialog_title) : null;
    }

    /**
     * Returns the current content view presented within this dialog's layout with the
     * {@link R.id#dialog_content} id.
     * <p>
     * If this library is used properly, this will be the same view as created by
     * {@link #onCreateContentView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @return Body view or {@code null} if this dialog does not have created its layout yet.
     * @see #getTitleView()
     * @see #getButtonsView()
     * @see #getLayout()
     */
    @Nullable protected View getContentView() {
        return mDialogLayout != null ? mDialogLayout.findViewById(R.id.dialog_content) : null;
    }

    /**
     * Returns the current buttons view presented within this dialog's layout with the
     * {@link R.id#dialog_buttons} id.
     * <p>
     * If this library is used properly, this will be the same view as created by
     * {@link #onCreateButtonsView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @return Buttons view or {@code null} if this dialog does not have created its layout yet.
     * @see #getTitleView()
     * @see #getContentView()
     * @see #getLayout()
     */
    @Nullable protected View getButtonsView() {
        return mDialogLayout != null ? mDialogLayout.findViewById(R.id.dialog_buttons) : null;
    }

    /**
     * Notifies that a button with the specified id has been clicked within this dialog.
     * <p>
     * If this dialog does not handle this event within {@link #onButtonClick(int)}, available listeners
     * (listener, fragment, activity) will be notified instead.
     *
     * @param buttonId The clicked button identifier.
     */
    protected void notifyButtonClick(final int buttonId) {
        if (!onButtonClick(buttonId)) {
            // Traverse event to all available listeners.
            boolean processed = mListener != null && mListener.onDialogButtonClick(this, buttonId);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnDialogListener && ((OnDialogListener) fragment).onDialogButtonClick(this, buttonId);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnDialogListener) {
                    ((OnDialogListener) activity).onDialogButtonClick(this, buttonId);
                }
            }
        }
    }

    /**
     * Invoked whenever a button with the specified <var>button</var> id has been clicked within this
     * dialog.
     * <p>
     * This implementation always returns {@code false}.
     *
     * @param button The clicked button identifier.
     * @return {@code True} if button click event was handled here, {@code false} to let available
     * listeners (listener, fragment, activity) handle this event.
     */
    protected boolean onButtonClick(final int button) {
        switch (button) {
            case BUTTON_INFO:
            case BUTTON_NEGATIVE:
                dismiss();
                break;
            case BUTTON_POSITIVE:
            case BUTTON_NEUTRAL:
                if (!mOptions.shouldRemain()) {
                    dismiss();
                }
        }
        return false;
    }

    /**
     * See {@link DialogInterface.OnShowListener#onShow(DialogInterface)}.
     *
     * @see #setOnShowListener(OnShowListener)
     */
    public void onShow(final DialogInterface dialog) {
        if (!onShowed()) {
            // Traverse event to all available listeners.
            boolean processed = mShowListener != null && mShowListener.onDialogShown(this);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnShowListener && ((OnShowListener) fragment).onDialogShown(this);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnShowListener) {
                    ((OnShowListener) activity).onDialogShown(this);
                }
            }
        }
    }

    /**
     * Invoked whenever {@link #onShow(DialogInterface)} is called for this dialog
     * instance.
     *
     * @return {@code True} if this event was handled here, {@code false} to let available listeners
     * (listener, fragment, activity) handle this event.
     */
    protected boolean onShowed() {
        return false;
    }

    /**
     * @see #setOnCancelListener(OnCancelListener)
     */
    @Override public void onCancel(@NonNull final DialogInterface dialog) {
        super.onCancel(dialog);
        if (!onCancelled()) {
            // Traverse event to all available listeners.
            boolean processed = mCancelListener != null && mCancelListener.onDialogCancelled(this);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnCancelListener && ((OnCancelListener) fragment).onDialogCancelled(this);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnCancelListener) {
                    ((OnCancelListener) activity).onDialogCancelled(this);
                }
            }
        }
    }

    /**
     * Invoked whenever {@link #onCancel(DialogInterface)} is called for this dialog
     * instance.
     *
     * @return {@code True} if this event was handled here, {@code false} to let available listeners
     * (listener, fragment, activity) handle this event.
     */
    protected boolean onCancelled() {
        return false;
    }

    /**
     * @see #setOnDismissListener(OnDismissListener)
     */
    @Override public void onDismiss(@NonNull final DialogInterface dialog) {
        super.onDismiss(dialog);
        if (!onDismissed()) {
            // Traverse event to all available listeners.
            boolean processed = mDismissListener != null && mDismissListener.onDialogDismissed(this);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnDismissListener && ((OnDismissListener) fragment).onDialogDismissed(this);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnDismissListener) {
                    ((OnDismissListener) activity).onDialogDismissed(this);
                }
            }
        }
    }

    /**
     * Invoked whenever {@link #onDismiss(DialogInterface)} is called for this dialog
     * instance.
     *
     * @return {@code True} if this event was handled here, {@code false} to let available listeners
     * (listener, fragment, activity) handle this event.
     */
    protected boolean onDismissed() {
        return false;
    }

    /**
     */
    @Override public void onDestroyView() {
        final android.app.Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setOnShowListener(null);
        }
        super.onDestroyView();
        this.mViewCreated = false;
    }

    /**
     */
    @Override public void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_SAVED_STATE, this.saveState());
        outState.putParcelable(BUNDLE_OPTIONS, mOptions);
    }

    /**
     * Performs state saving of this dialog fragment instance.
     *
     * @return This dialog fragment saved state.
     */
    private Parcelable saveState() {
        this.mCalled = false;
        final Parcelable savedState = onSaveInstanceState();
        this.throwIfNotCalled("onSaveInstanceState");
        return savedState;
    }

    /**
     * Invoked to save the current state of this dialog.
     * <p>
     * <em>Derived classes must call through to the super class's implementation of this method.
     * If they do not, an exception will be thrown.</em>
     *
     * @return Saved state of this dialog or an <b>empty</b> state if this dialog does not need to save
     * its state.
     */
    @NonNull protected Parcelable onSaveInstanceState() {
        this.mCalled = true;
        final BaseSavedState state = new BaseSavedState(BaseSavedState.EMPTY_STATE);
        state.dialogId = mDialogId;
        state.parentFragmentTag = mContextFragmentTag;
        state.parentFragmentId = mContextFragmentId;
        return state;
    }

    /**
     * Performs state restoring of this dialog fragment instance.
     *
     * @param state Previously saved state by {@link #saveState()} for this dialog fragment.
     */
    private void restoreState(final Parcelable state) {
        this.mCalled = false;
        onRestoreInstanceState(state);
        this.throwIfNotCalled("onRestoreInstanceState");
    }

    /**
     * Invoked to restore a previous state, saved by {@link #onSaveInstanceState()}, of this dialog.
     * <p>
     * <em>Derived classes must call through to the super class's implementation of this method.
     * If they do not, an exception will be thrown.</em>
     *
     * @param savedState Same state as returned by {@link #onSaveInstanceState()}.
     */
    protected void onRestoreInstanceState(@NonNull final Parcelable savedState) {
        this.mCalled = true;
        if (savedState instanceof BaseSavedState) {
            final BaseSavedState state = (BaseSavedState) savedState;
            this.mDialogId = state.dialogId;
            this.mContextFragmentTag = state.parentFragmentTag;
            this.mContextFragmentId = state.parentFragmentId;
        }
    }

    /**
     * Throws {@code IllegalStateException} if the current {@link #mCalled} flag is set to {@code false}.
     *
     * @param methodName Name of the super method that was not properly called.
     */
    final void throwIfNotCalled(final String methodName) {
        if (!mCalled)
            throw new IllegalStateException("(" + this + ") did not call trough to super." + methodName + "()");
    }

	/**
	 */
	@Override public void onDestroy() {
		super.onDestroy();
		this.mListener = null;
		this.mShowListener = null;
		this.mCancelListener = null;
		this.mDismissListener = null;
		this.mLayoutInflater = null;
	}

	/*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link WidgetSavedState} implementation that should be used by inheritance hierarchies of
     * {@link BaseDialog} to ensure the state of all instances along the chain is saved.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class BaseSavedState extends WidgetSavedState {

        /**
         * Creator used to create an instance or array of instances of BaseSavedState from {@link Parcel}.
         */
        public static final Parcelable.Creator<BaseSavedState> CREATOR = new Parcelable.Creator<BaseSavedState>() {

            /**
             */
            @Override public BaseSavedState createFromParcel(@NonNull final Parcel source) { return new BaseSavedState(source); }

            /**
             */
            @Override public BaseSavedState[] newArray(final int size) { return new BaseSavedState[size]; }
        };

        /**
         */
        private int dialogId, parentFragmentId;

        /**
         */
        private String parentFragmentTag;

        /**
         * Creates a new instance BaseSavedState with the given <var>superState</var> to allow
         * chaining of saved states in {@link #onSaveInstanceState()} and also in
         * {@link #onRestoreInstanceState(Parcelable)}.
         *
         * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
         *                   {@code onSaveInstanceState()}.
         */
        protected BaseSavedState(@NonNull final Parcelable superState) {
            super(superState);
        }

        /**
         * Called from {@link #CREATOR} to create an instance of BaseSavedState form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected BaseSavedState(@NonNull final Parcel source) {
            super(source);
            this.dialogId = source.readInt();
            this.parentFragmentId = source.readInt();
            this.parentFragmentTag = source.readString();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(dialogId);
            dest.writeInt(parentFragmentId);
            dest.writeString(parentFragmentTag);
        }
    }
}