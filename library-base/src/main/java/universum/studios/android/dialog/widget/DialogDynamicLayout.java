/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.DialogLayout;

/**
 * Like {@link DialogBasicLayout}, this layout is used as main layout for dialogs from this library.
 * The difference is that this layout implementation should be used for dialogs of which content view
 * may exceed available window's space where such a dialog is displayed. Such <b>dynamic</b> content
 * views are all {@link android.widget.AdapterView AdapterView} implementations and also scrollable
 * containers like {@link android.widget.ScrollView ScrollView} or {@link android.webkit.WebView WebView}.
 * <p>
 * This layout inherits from {@link FrameLayout} and adds some extended implementation details to
 * {@link #onMeasure(int, int)} and {@link #onLayout(boolean, int, int, int, int)} to properly
 * measure and layout such a dynamic content so the title and buttons view are always visible
 * to a user and the content view, if necessary, scrolls.
 * <p>
 * <b>Note</b>, that this layout checks for max count of its child views and allows only 3 (title,
 * content, buttons).
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogDynamicLayout extends FrameLayout implements DialogLayout {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogDynamicLayout";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Dialog body view.
	 */
	private View mContentView;

	/**
	 * Dialog title view.
	 */
	private View mTitleView;

	/**
	 * Dialog buttons view.
	 */
	private View mButtonsView;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #DialogDynamicLayout(Context, AttributeSet)} without attributes.
	 */
	public DialogDynamicLayout(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #DialogDynamicLayout(Context, AttributeSet, int)} with {@code 0} as attribute
	 * for default style.
	 */
	public DialogDynamicLayout(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Same as {@link #DialogDynamicLayout(Context, AttributeSet, int, int)} with {@code 0} as default
	 * style.
	 */
	public DialogDynamicLayout(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of DialogDynamicLayout for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public DialogDynamicLayout(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void addTitleView(@NonNull final View titleView) {
		if (mTitleView != null) {
			removeTitleView();
		}
		addView(mTitleView = titleView);
	}

	/**
	 */
	@Override public void removeTitleView() {
		if (mTitleView != null) {
			removeView(mTitleView);
			this.mTitleView = null;
		}
	}

	/**
	 */
	@Override public void addContentView(@NonNull final View contentView) {
		if (mContentView != null) {
			removeContentView();
		}
		addView(mContentView = contentView);
	}

	/**
	 */
	@Override public void removeContentView() {
		if (mContentView != null) {
			removeView(mContentView);
			this.mContentView = null;
		}
	}

	/**
	 */
	@Override public void addButtonsView(@NonNull final View buttonsView) {
		if (mButtonsView != null) {
			removeButtonsView();
		}
		final ViewGroup.LayoutParams params = buttonsView.getLayoutParams();
		if (params instanceof LayoutParams) {
			((LayoutParams) params).gravity = Gravity.BOTTOM;
			addView(mButtonsView = buttonsView, params);
		} else {
			addView(mButtonsView = buttonsView, createButtonsViewParams());
		}
	}

	/**
	 */
	@Override public void removeButtonsView() {
		if (mButtonsView != null) {
			removeView(mButtonsView);
			this.mButtonsView = null;
		}
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child) {
		this.checkCurrentViewsCount();
		super.addView(child);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, final int index) {
		this.checkCurrentViewsCount();
		super.addView(child, index);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, @NonNull final ViewGroup.LayoutParams params) {
		this.checkCurrentViewsCount();
		super.addView(child, params);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, final int index, @NonNull final ViewGroup.LayoutParams params) {
		this.checkCurrentViewsCount();
		super.addView(child, index, params);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, final int width, final int height) {
		this.checkCurrentViewsCount();
		super.addView(child, width, height);
	}

	/**
	 */
	@Override @Nullable public View getButtonsView() {
		return mButtonsView;
	}

	/**
	 */
	@Override @Nullable public View getContentView() {
		return mContentView;
	}

	/**
	 */
	@Override @Nullable public View getTitleView() {
		return mTitleView;
	}

	/**
	 */
	@Override public final int getChildCount() {
		return super.getChildCount();
	}

	/**
	 */
	@Override protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mContentView != null) {
			final MarginLayoutParams contentParams = (MarginLayoutParams) mContentView.getLayoutParams();
			boolean paramsUpdated = false;
			if (contentParams != null) {
				if (mContentView.getMeasuredHeight() == 0) {
					if (mTitleView != null && mTitleView.getVisibility() != GONE && mButtonsView != null && mButtonsView.getVisibility() != GONE) {
						final MarginLayoutParams buttonsParams = (MarginLayoutParams) mButtonsView.getLayoutParams();
						if (buttonsParams != null) {
							buttonsParams.topMargin = mTitleView.getMeasuredHeight();
						}
					}
				} else {
					if (mTitleView != null && mTitleView.getVisibility() != GONE) {
						final int titleMeasuredHeight = mTitleView.getMeasuredHeight();
						if (contentParams.topMargin != titleMeasuredHeight) {
							contentParams.topMargin = titleMeasuredHeight;
							paramsUpdated = true;
						}
					}

					if (mButtonsView != null && mButtonsView.getVisibility() != GONE) {
						final int buttonsMeasuredHeight = mButtonsView.getMeasuredHeight();
						if (contentParams.bottomMargin != buttonsMeasuredHeight) {
							contentParams.bottomMargin = buttonsMeasuredHeight;
							paramsUpdated = true;
						}
					}
				}
			}
			if (paramsUpdated) {
				// Re-measure with updated layout params.
				super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			}
		}
	}

	/**
	 */
	@Override protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
			// On pre HONEYCOMB, the margins of child views are not properly handled, so we need to
			// check those margins and properly layout our children. For now, only content view "suffers",
			// from this not correct behaviour so layout only it.
			final boolean titleViewVisible = mTitleView != null && mTitleView.getVisibility() == VISIBLE;
			final boolean contentViewVisible = mContentView != null && mContentView.getVisibility() == VISIBLE;
			if (titleViewVisible && contentViewVisible && mContentView.getTop() < mTitleView.getBottom()) {
				final LayoutParams contentParams = (LayoutParams) mContentView.getLayoutParams();
				if (contentParams != null && contentParams.topMargin > 0) {
					final int topBottomOffset = contentParams.topMargin;
					mContentView.layout(
							mContentView.getLeft(),
							mContentView.getTop() + topBottomOffset,
							mContentView.getRight(),
							mContentView.getBottom() + topBottomOffset
					);
				}
			}
		}
	}

	/**
	 * Checks the current count of child views within this layout.
	 *
	 * @throws IllegalStateException If there are already 3 views in the layout.
	 */
	private void checkCurrentViewsCount() {
		if (getChildCount() == 3) throw new IllegalStateException(
				"Dialog layout can contain max 3 top child views (title, body and buttons)."
		);
	}

	/**
	 * Creates new layout parameters for buttons view.
	 * @return New instance of LayoutParams.
	 */
	private LayoutParams createButtonsViewParams() {
		final LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.BOTTOM;
		return params;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}