/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.DialogLayout;

/**
 * A {@link DialogLayout} implementation used as default main layout for almost all dialogs from within
 * this library of which content is relatively static. As this layout inherits from {@link LinearLayout}
 * with {@link #VERTICAL} orientation, title, content and buttons view will be laid out vertically.
 * Due to constrained children layout process of LinearLayout (exceeded content will not scroll),
 * this layout implementation should be used only for dialogs of which content, including title and
 * buttons, will always fit into window bounds where such a dialog is displayed. For content that may
 * exceed such a space use {@link DialogDynamicLayout} implementation that can handle such layout
 * behaviour.
 * <p>
 * <b>Note</b>, that this layout checks for max count of its child views and allows only 3 (title,
 * content, buttons).
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see DialogDynamicLayout
 */
public class DialogBasicLayout extends LinearLayout implements DialogLayout {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogBasicLayout";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Dialog body view added into this dialog layout.
	 */
	private View mContentView;

	/**
	 * Dialog title view added into this dialog layout.
	 */
	private View mTitleView;

	/**
	 * Dialog buttons view added into this dialog layout.
	 */
	private View mButtonsView;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #DialogBasicLayout(Context, AttributeSet)} without attributes.
	 */
	public DialogBasicLayout(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #DialogBasicLayout(Context, AttributeSet, int)} with {@code 0} as attribute
	 * for default style.
	 */
	public DialogBasicLayout(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, 0, 0);
	}

	/**
	 * Same as {@link #DialogBasicLayout(Context, AttributeSet, int, int)} with {@code 0} as default
	 * style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public DialogBasicLayout(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of DialogBasicLayout for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public DialogBasicLayout(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		setOrientation(VERTICAL);
	}

	/**
	 */
	@Override public void addTitleView(@NonNull final View titleView) {
		if (mTitleView != null) {
			removeTitleView();
		}
		addView(mTitleView = titleView);
	}

	/**
	 */
	@Override public void removeTitleView() {
		if (mTitleView != null) {
			removeView(mTitleView);
			this.mTitleView = null;
		}
	}

	/**
	 */
	@Override public void addContentView(@NonNull final View contentView) {
		if (mContentView != null) {
			removeContentView();
		}
		addView(mContentView = contentView);
	}

	/**
	 */
	@Override public void removeContentView() {
		if (mContentView != null) {
			removeView(mContentView);
		}
	}

	/**
	 */
	@Override public void addButtonsView(@NonNull final View buttonsView) {
		if (mButtonsView != null) {
			removeButtonsView();
		}
		addView(mButtonsView = buttonsView);
	}

	/**
	 */
	@Override public void removeButtonsView() {
		if (mButtonsView != null) {
			removeView(mButtonsView);
			this.mButtonsView = null;
		}
	}


	/**
	 */
	@Override public final void addView(@NonNull final View child) {
		this.checkCurrentViewsCount();
		super.addView(child);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, final int index) {
		this.checkCurrentViewsCount();
		super.addView(child, index);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, @NonNull final ViewGroup.LayoutParams params) {
		this.checkCurrentViewsCount();
		super.addView(child, params);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, final int index, final ViewGroup.LayoutParams params) {
		this.checkCurrentViewsCount();
		super.addView(child, index, params);
	}

	/**
	 */
	@Override public final void addView(@NonNull final View child, final int width, final int height) {
		this.checkCurrentViewsCount();
		super.addView(child, width, height);
	}

	/**
	 */
	@Override @Nullable public View getButtonsView() {
		return mButtonsView;
	}

	/**
	 */
	@Override @Nullable public View getContentView() {
		return mContentView;
	}

	/**
	 */
	@Override @Nullable public View getTitleView() {
		return mTitleView;
	}

	/**
	 * Checks the current count of child views within this layout.
	 *
	 * @throws IllegalStateException If there are already 3 views in the layout.
	 */
	private void checkCurrentViewsCount() {
		if (getChildCount() == 3) {
			throw new IllegalStateException("Dialog layout can contain max 3 top child views (title, body and buttons).");
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}