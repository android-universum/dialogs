/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.DialogsConfig;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.view.DialogButtonsView;
import universum.studios.android.graphics.Colors;
import universum.studios.android.ui.widget.TintManager;
import universum.studios.android.ui.widget.WidgetSavedState;
import universum.studios.android.ui.widget.WidgetStateSet;

/**
 * A {@link DialogButtonsView} implementation used by {@link SimpleDialog SimpleDialog} as buttons view.
 * <p>
 * This class represents a container for all dialog buttons (info + action). A new button can be
 * created via {@link #newButton(int)} or {@link #newButton(int, int)} and added into buttons view
 * via {@link #addButton(DialogButtonsView.DialogButton)}. Each of the buttons holds its own state
 * that can be changed at any time to match your specific needs. See {@link DialogButtonsView.DialogButton}
 * for additional info.
 * <p>
 * A specific button can be find by its id via {@link #findButtonById(int)} or obtained by its current
 * position via {@link #getActionButtonAt(int)} or {@link #getInfoButtonAt(int)}. All buttons can be
 * obtained via {@link #getButtons()} or respectively via {@link #getActionButtons()} and
 * {@link #getInfoButtons()}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Buttons layout</h3>
 * This buttons view implementation supports two categories of buttons, {@link #CATEGORY_ACTION} and
 * {@link #CATEGORY_INFO}. The action buttons are laid out from the right to the left, and info buttons
 * from the left to the right. <b>Note</b>, that by default are all buttons layout side by side
 * <b>horizontally</b> if there is enough space, if not buttons will be layout stacked in which case
 * the info buttons will be positioned below the action buttons.
 * <p>
 * What you can change from this perspective is how wide should buttons be (how much space should they
 * take) via specifying a width mode flag via {@link #setButtonsWidthMode(int)}. You can use either
 * {@link #WIDTH_MODE_MATCH_PARENT} or {@link #WIDTH_MODE_WRAP_CONTENT}.
 *
 * <h3>Callbacks</h3>
 * You can listen for button click events via {@link OnButtonsClickListener} that
 * can be registered via {@link #registerOnButtonsClickListener(OnButtonsClickListener)}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
// todo: rename to DialogBasicButtonsView ...
public class SimpleDialogButtonsView extends LinearLayout implements DialogButtonsView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "SimpleDialogButtonsView";

	/**
	 * Buttons layout mode which indicates that buttons should be layout side by side, and so
	 * horizontally.
	 */
	private static final int LAYOUT_MODE_SIDE_BY_SIDE = 0x01;

	/**
	 * Buttons layout mode which indicates that buttons should be layout stacked, and so vertically.
	 */
	private static final int LAYOUT_MODE_STACKED = 0x02;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Comparator used for sorting of action buttons by theirs id.
	 */
	private static final Comparator<DialogButton> BUTTONS_COMPARATOR = new Comparator<DialogButton>() {

		/**
		 */
		@Override public int compare(@NonNull final DialogButton first, @NonNull final DialogButton second) {
			final int lhsId = first.getId();
			final int rhsId = second.getId();
			if (lhsId < rhsId) {
				return -1;
			} else if (lhsId > rhsId) {
				return 1;
			}
			return 0;
		}
	};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * On click listener for buttons.
	 */
	private final View.OnClickListener BUTTONS_CLICK_LISTENER = new View.OnClickListener() {

		/**
		 */
		@Override public void onClick(@NonNull final View view) {
			handleButtonClick(((DialogButton) view.getTag()).getId());
		}
	};

	/**
	 * Inflater used to inflate button view.
	 */
	private LayoutInflater mInflater;

	/**
	 * Set of view listeners.
	 */
	private List<OnButtonsClickListener> mListeners;

	/**
	 * Set of action buttons added into this buttons view.
	 */
	private List<DialogButton> mActionButtons;

	/**
	 * Set of action buttons added into this buttons view.
	 */
	private List<DialogButton> mInfoButtons;

	/**
	 * Typeface instance for each of buttons view.
	 */
	private Typeface mTypeface;

	/**
	 * Layout mode which determines how should be buttons of this view layout.
	 */
	private int mButtonsLayoutMode = LAYOUT_MODE_SIDE_BY_SIDE;

	/**
	 * Width mode for buttons which determines how should be buttons of this view measured.
	 */
	private int mButtonsWidthMode = WIDTH_MODE_WRAP_CONTENT;

	/**
	 * Accent color obtained from the current dialog theme.
	 */
	private int mAccentColor;

	/**
	 * Flag indicating whether the current theme is dark one or not.
	 */
	private boolean mDarkTheme;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SimpleDialogButtonsView(Context, AttributeSet)} without attributes.
	 */
	public SimpleDialogButtonsView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SimpleDialogButtonsView(Context, AttributeSet, int)} with
	 * {@link R.attr#dialogButtonsStyle dialogButtonsStyle} as attribute for default style.
	 */
	public SimpleDialogButtonsView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, R.attr.dialogButtonsStyle, 0);
	}

	/**
	 * Same as {@link #SimpleDialogButtonsView(Context, AttributeSet, int, int)} with {@code 0} as default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SimpleDialogButtonsView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of DialogButtons for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SimpleDialogButtonsView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.mInflater = LayoutInflater.from(context);
		// Process accent attribute from the current dialog theme.
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		if (theme.resolveAttribute(R.attr.colorAccent, typedValue, true)) {
			this.mAccentColor = typedValue.data;
		}
		if (theme.resolveAttribute(R.attr.isLightTheme, typedValue, true)) {
			this.mDarkTheme = typedValue.data == 0;
		}
	}

	/**
	 * Creates a new instance of ColorStateList for the text of buttons presented within
	 * {@link SimpleDialogButtonsView} widget.
	 *
	 * @param accentColor Color used to apply accent to the new color state list.
	 * @param darkTheme   {@code True} to create colors list specific for dark theme, {@code false}
	 *                    for light theme.
	 * @return New ColorStateList instance.
	 */
	@NonNull public static ColorStateList createButtonTextColorStateList(final int accentColor, final boolean darkTheme) {
		return new ColorStateList(
				new int[][]{
						WidgetStateSet.ENABLED_PRESSED,
						WidgetStateSet.ENABLED_FOCUSED,
						WidgetStateSet.ENABLED,
						WidgetStateSet.DISABLED
				},
				new int[]{
						Colors.darker(accentColor, 0.2f),
						Colors.brighter(accentColor, 0.8f),
						accentColor,
						darkTheme ? TintManager.COLOR_DISABLED : TintManager.COLOR_DISABLED_LIGHT
				}
		);
	}

	/**
	 */
	@Override public void registerOnButtonsClickListener(@NonNull final OnButtonsClickListener listener) {
		if (mListeners == null) this.mListeners = new ArrayList<>(1);
		if (!mListeners.contains(listener)) mListeners.add(listener);
	}

	/**
	 */
	@Override public void unregisterOnButtonsClickListener(@NonNull final OnButtonsClickListener listener) {
		if (mListeners != null) mListeners.remove(listener);
	}

	/**
	 */
	@Override @NonNull public DialogButton newButton(final int id) {
		return newButton(id, CATEGORY_ACTION);
	}

	/**
	 */
	@Override @NonNull public DialogButton newButton(final int id, final int category) {
		return new ButtonImpl(id, category);
	}

	/**
	 */
	@Override public void addButton(@NonNull final DialogButton button) {
		final int category = button.getCategory();
		this.ensureButtons(category);
		if (category == CATEGORY_INFO) {
			mInfoButtons.add(button);
		} else {
			mActionButtons.add(button);
			// Re-sort action buttons.
			Collections.sort(mActionButtons, BUTTONS_COMPARATOR);
		}
		this.addButtonView(button);
	}

	/**
	 * Ensures that the array for buttons is initialized.
	 *
	 * @param category Category flag of buttons list to ensure.
	 */
	private void ensureButtons(final int category) {
		if (category == CATEGORY_ACTION && mActionButtons == null) {
			this.mActionButtons = new ArrayList<>();
		} else if (category == CATEGORY_INFO && mInfoButtons == null) {
			this.mInfoButtons = new ArrayList<>();
		}
	}

	/**
	 * Adds a view of the given <var>button</var> into the current view hierarchy. If the given button
	 * does not have its view attached yet, the default one will be created.
	 *
	 * @param button The button of which view to add.
	 */
	@SuppressWarnings("ConstantConditions")
	private void addButtonView(final DialogButton button) {
		View buttonView;
		if (!button.hasViewAttached()) {
			if ((buttonView = onCreateButtonView(button)) == null) {
				return;
			}
			button.attachView(buttonView);
			addView(buttonView);
		} else {
			buttonView = button.getView();
			addView(buttonView);
		}
		if (mTypeface != null && buttonView instanceof TextView) {
			((TextView) buttonView).setTypeface(mTypeface);
		}
		buttonView.setOnClickListener(BUTTONS_CLICK_LISTENER);
	}

	/**
	 * Invoked to create a view for the given <var>button</var>.
	 *
	 * @param button The button for which to crate the new view.
	 * @return Button view.
	 */
	private View onCreateButtonView(final DialogButton button) {
		Button buttonView = null;
		switch (button.getCategory()) {
			case CATEGORY_ACTION:
				switch (button.getId()) {
					case Dialog.BUTTON_POSITIVE:
						buttonView = inflateButtonView(R.layout.dialog_button_positive);
						break;
					case Dialog.BUTTON_NEGATIVE:
						buttonView = inflateButtonView(R.layout.dialog_button_negative);
						break;
					case Dialog.BUTTON_NEUTRAL:
						buttonView = inflateButtonView(R.layout.dialog_button_neutral);
						break;
					default:
						buttonView = inflateButtonView(R.layout.dialog_button);
						break;
				}
				if (buttonView.getCurrentTextColor() == 0 && mAccentColor != 0) {
					buttonView.setTextColor(createButtonTextColorStateList(mAccentColor, mDarkTheme));
				}
				break;
			case CATEGORY_INFO:
				buttonView = inflateButtonView(R.layout.dialog_button_info);
				break;
		}
		return buttonView;
	}

	/**
	 * Inflates the specified <var>resource</var> as button view.
	 *
	 * @param resource The desired layout resource to be inflated.
	 * @return Inflated Button from the specified resource.
	 */
	private Button inflateButtonView(final int resource) {
		return (Button) mInflater.inflate(resource, this, false);
	}

	/**
	 * Invoked whenever a button with the specified <var>buttonId</var> has been clicked.
	 *
	 * @param buttonId The id of clicked button.
	 */
	final void handleButtonClick(final int buttonId) {
		final DialogButton button = findButtonById(buttonId);
		if (button != null) {
			if (mListeners != null && !mListeners.isEmpty()) {
				for (OnButtonsClickListener listener : mListeners) {
					listener.onButtonClick(this, button);
				}
			}
		} else {
			Log.w(TAG, "Clicked button with unknown id(" + buttonId + ").");
		}
	}

	/**
	 */
	@Override @Nullable public DialogButton findButtonById(final int id) {
		if (!hasButtons()) {
			return null;
		}
		if (mActionButtons != null) {
			for (DialogButton button : mActionButtons) {
				if (button.getId() == id) {
					return button;
				}
			}
		}
		if (mInfoButtons != null) {
			for (DialogButton button : mInfoButtons) {
				if (button.getId() == id) {
					return button;
				}
			}
		}
		return null;
	}

	/**
	 */
	@Override @Nullable public DialogButton getActionButtonAt(final int index) {
		return mActionButtons != null && index < mActionButtons.size() ? mActionButtons.get(index) : null;
	}

	/**
	 */
	@Override @Nullable public DialogButton getInfoButtonAt(final int index) {
		return mInfoButtons != null && index < mInfoButtons.size() ? mInfoButtons.get(index) : null;
	}

	/**
	 * Returns all buttons presented within this buttons view.
	 *
	 * @return List with all action + info buttons.
	 */
	@NonNull public List<DialogButton> getButtons() {
		final List<DialogButton> buttons = new ArrayList<>();
		if (mActionButtons != null) buttons.addAll(mActionButtons);
		if (mInfoButtons != null) buttons.addAll(mInfoButtons);
		return buttons;
	}

	/**
	 * Returns all action buttons presented within this buttons view.
	 *
	 * @return List with all action buttons.
	 */
	@Nullable public List<DialogButton> getActionButtons() {
		return mActionButtons;
	}

	/**
	 * Returns all info buttons presented within this buttons view.
	 *
	 * @return List with all info buttons.
	 */
	@Nullable public List<DialogButton> getInfoButtons() {
		return mInfoButtons;
	}

	/**
	 * Checks whether there are some action/info buttons currently available or not.
	 *
	 * @return {@code True} if we have some buttons to manage, {@code false otherwise}.
	 */
	private boolean hasButtons() {
		return (mActionButtons != null && !mActionButtons.isEmpty()) || (mInfoButtons != null && !mInfoButtons.isEmpty());
	}

	/**
	 */
	@Override public void setButtonEnabled(final int id, final boolean enabled) {
		final DialogButton button = findButtonById(id);
		if (button != null) button.setEnabled(enabled);
	}

	/**
	 */
	@Override public void removeButton(@NonNull final DialogButton button) {
		if (mActionButtons != null && mActionButtons.contains(button)) {
			mActionButtons.remove(button);
			if (button.hasViewAttached()) {
				removeView(button.getView());
			}
		} else if (mInfoButtons != null && mInfoButtons.contains(button)) {
			mInfoButtons.remove(button);
			if (button.hasViewAttached()) {
				removeView(button.getView());
			}
		}
	}

	/**
	 */
	@SuppressWarnings("ConstantConditions")
	@Override public void removeActionButtonAt(final int index) {
		final int buttonsCount = mActionButtons != null ? mActionButtons.size() : 0;
		if (index >= 0 && index < buttonsCount) {
			final DialogButton button = mActionButtons.remove(index);
			if (button.hasViewAttached()) {
				removeView(button.getView());
			}
		}
	}

	/**
	 */
	@SuppressWarnings("ConstantConditions")
	@Override public void removeInfoButtonAt(final int index) {
		final int buttonsCount = mInfoButtons != null ? mInfoButtons.size() : 0;
		if (index >= 0 && index < buttonsCount) {
			final DialogButton button = mInfoButtons.remove(index);
			if (button.hasViewAttached()) {
				removeView(button.getView());
			}
		}
	}

	/**
	 */
	@Override public void removeAllButtons() {
		if (mActionButtons != null && !mActionButtons.isEmpty()) {
			for (DialogButton button : mActionButtons) {
				if (button.hasViewAttached()) removeView(button.getView());
			}
			mActionButtons.clear();
		}
		if (mInfoButtons != null && !mInfoButtons.isEmpty()) {
			for (DialogButton button : mInfoButtons) {
				if (button.hasViewAttached()) removeView(button.getView());
			}
			mInfoButtons.clear();
		}
	}

	/**
	 */
	@Override public void setButtonsWidthMode(@WidthMode final int mode) {
		if (mButtonsWidthMode != mode) {
			this.mButtonsWidthMode = mode;
			switch (mode) {
				case WIDTH_MODE_WRAP_CONTENT:
					break;
				case WIDTH_MODE_MATCH_PARENT:
					final int paddingLeft = getPaddingLeft();
					final int paddingRight = getPaddingRight();
					if (paddingLeft != paddingRight) {
						setPadding(
								Math.min(paddingLeft, paddingRight),
								getPaddingTop(),
								Math.min(paddingLeft, paddingRight),
								getPaddingBottom()
						);
					}
					break;
			}
			requestLayout();
		}
	}

	/**
	 */
	@Override @WidthMode public int getButtonsWidthMode() {
		return mButtonsWidthMode;
	}

	/**
	 */
	@Override public void setButtonsTypeface(@Nullable final Typeface typeface) {
		this.mTypeface = typeface;
		if (mActionButtons != null && !mActionButtons.isEmpty()) {
			for (DialogButton button : mActionButtons) {
				if (button.hasViewAttached()) {
					final View view = button.getView();
					if (view instanceof TextView) {
						((TextView) view).setTypeface(typeface);
					}
				}
			}
		}
	}

	/**
	 */
	@Override protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		if (!hasButtons()) {
			setMeasuredDimension(0, 0);
			return;
		}
		final int paddingLeft = getPaddingLeft();
		final int paddingRight = getPaddingRight();
		int orientation = HORIZONTAL;
		this.mButtonsLayoutMode = LAYOUT_MODE_SIDE_BY_SIDE;
		int measureSpecWidth = View.MeasureSpec.getSize(widthMeasureSpec);
		int actionButtonsWidth = 0;
		if (mActionButtons != null) {
			final int actionButtonsCount = mActionButtons.size();
			for (DialogButton button : mActionButtons) {
				final View buttonView = button.getView();
				if (buttonView != null && buttonView.getVisibility() != View.GONE) {
					measureChild(buttonView, widthMeasureSpec, heightMeasureSpec);
					final LinearLayout.LayoutParams btnParams = (LinearLayout.LayoutParams) buttonView.getLayoutParams();
					actionButtonsWidth += buttonView.getMeasuredWidth() + btnParams.leftMargin + btnParams.rightMargin;
				}
			}

			if (actionButtonsWidth > (measureSpecWidth - paddingLeft - paddingRight)) {
				this.mButtonsLayoutMode = LAYOUT_MODE_STACKED;
				orientation = VERTICAL;
			}

			// Change orientation and let the super measure our buttons and our total size depends on the
			// current orientation.
			setOrientation(orientation);
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);

			// If we have side-by-side buttons which should match-parent size measure them there.
			if (mButtonsLayoutMode == LAYOUT_MODE_SIDE_BY_SIDE && mButtonsWidthMode == WIDTH_MODE_MATCH_PARENT) {
				final int btnWidth = (measureSpecWidth - paddingLeft - paddingRight) / actionButtonsCount;
				// Check if the max computed width fits for all buttons, if not do not measure them
				// in the match_parent mode.
				boolean exceeds = false;
				for (DialogButton button : mActionButtons) {
					final View buttonView = button.getView();
					if (buttonView != null && buttonView.getVisibility() != View.GONE) {
						final LinearLayout.LayoutParams btnParams = (LinearLayout.LayoutParams) buttonView.getLayoutParams();
						if ((btnWidth - btnParams.leftMargin - btnParams.rightMargin) < buttonView.getMeasuredWidth()) {
							exceeds = true;
							break;
						}
					}
				}

				if (!exceeds) {
					for (DialogButton button : mActionButtons) {
						final View buttonView = button.getView();
						if (buttonView != null && buttonView.getVisibility() != View.GONE) {
							final LinearLayout.LayoutParams btnParams = (LinearLayout.LayoutParams) buttonView.getLayoutParams();
							measureViewToExactSize(
									buttonView,
									btnWidth - btnParams.leftMargin - btnParams.rightMargin,
									buttonView.getMeasuredHeight()
							);
						}
					}
				}
			}
		} else {
			setOrientation(orientation);
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
		// There is no need to measure info buttons, because they do not need some extra measurement
		// process like action buttons. We will layout them in the HORIZONTAL orientation if there is
		// enough space from the left, and in the VERTICAL orientation just below the action buttons.
	}

	/**
	 * Measures the given view to the requested <var>width</var> and <var>height</var>.
	 *
	 * @param view   The view to be measured.
	 * @param width  The width to which should be view measured.
	 * @param height The height to which should be view measured.
	 */
	private void measureViewToExactSize(final View view, final int width, final int height) {
		view.measure(
				View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
				View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY)
		);
	}

	/**
	 */
	@Override protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {
		if (hasButtons()) {
			switch (mButtonsLayoutMode) {
				// Layout buttons horizontally.
				case LAYOUT_MODE_SIDE_BY_SIDE:
					this.layoutSideBySide(left, top, right, bottom);
					break;
				// Layout buttons vertically.
				case LAYOUT_MODE_STACKED:
					this.layoutStacked(left, top, right, bottom);
					break;
			}
		}
	}

	/**
	 * Layouts current buttons <b>side-by-side</b> (horizontally) starting with action buttons from
	 * the right and then info buttons from the left.
	 */
	private void layoutSideBySide(final int left, final int top, final int right, final int bottom) {
		// todo: layout based on the LTR orientation
		int btnTop, btnRight, btnLeft, btnWidth, btnHeight;
		final int paddingTop = getPaddingTop();
		btnRight = right - getPaddingRight();
		btnLeft = left + getPaddingLeft();
		int actionButtonsLeft = right;
		if (mActionButtons != null) {
			// Start layout the action buttons from the right side to the left.
			for (DialogButton button : mActionButtons) {
				final View buttonView = button.getView();
				if (buttonView != null && buttonView.getVisibility() != View.GONE) {
					btnWidth = buttonView.getMeasuredWidth();
					btnHeight = buttonView.getMeasuredHeight();

					final LinearLayout.LayoutParams buttonParams = (LinearLayout.LayoutParams) buttonView.getLayoutParams();
					btnTop = paddingTop + buttonParams.topMargin;
					btnRight -= buttonParams.rightMargin;
					buttonView.layout(actionButtonsLeft = btnRight - btnWidth, btnTop, btnRight, btnTop + btnHeight);
					btnRight = btnRight - btnWidth - buttonParams.leftMargin;
				}
			}
		}
		if (mInfoButtons != null) {
			if (actionButtonsLeft > btnLeft) {
				// Start layout the info buttons from the left side to the right.
				for (DialogButton button : mInfoButtons) {
					final View buttonView = button.getView();
					if (buttonView != null && buttonView.getVisibility() != View.GONE) {
						btnWidth = buttonView.getMeasuredWidth();
						btnHeight = buttonView.getMeasuredHeight();

						final LinearLayout.LayoutParams buttonParams = (LinearLayout.LayoutParams) buttonView.getLayoutParams();
						btnTop = paddingTop + buttonParams.topMargin;
						btnLeft += buttonParams.leftMargin;

						if (btnLeft + btnWidth > actionButtonsLeft) {
							// No enough space left for info buttons.
							break;
						}

						buttonView.layout(btnLeft, btnTop, btnLeft + btnWidth, btnTop + btnHeight);
						btnLeft = btnLeft + btnWidth + buttonParams.rightMargin;
					}
				}
			}
		}
	}

	/**
	 * Layouts current buttons <b>stacked</b> (vertically) starting with action buttons and then
	 * info buttons.
	 * <p>
	 * <b>Note, that this layout method does not layout info buttons.</b>
	 */
	private void layoutStacked(final int left, final int top, final int right, final int bottom) {
		// todo: layout based on the LTR orientation
		int btnTop = getPaddingTop();
		int btnRight = right - getPaddingRight();
		// Layout all buttons vertically.
		if (mActionButtons != null) {
			for (DialogButton button : mActionButtons) {
				btnTop = this.layoutButtonStacked(button, btnTop, btnRight);
			}
		}
		if (mInfoButtons != null) {
			for (DialogButton button : mInfoButtons) {
				btnTop = this.layoutButtonStacked(button, btnTop, btnRight);
			}
		}
	}

	/**
	 * Layouts a view of the specified <var>button</var> at the specified <var>top</var> and <var>right</var>
	 * position.
	 *
	 * @param button The button of which view to layout.
	 * @param top    The top position where to layout the button view.
	 * @param right  The right position where to layout the button view.
	 * @return The top position for the next button view.
	 */
	private int layoutButtonStacked(final DialogButton button, int top, final int right) {
		final View buttonView = button.getView();
		if (buttonView != null && buttonView.getVisibility() != View.GONE) {
			final int btnWidth = buttonView.getMeasuredWidth();
			final int btnHeight = buttonView.getMeasuredHeight();
			final LinearLayout.LayoutParams btnParams = (LinearLayout.LayoutParams) buttonView.getLayoutParams();
			top += btnParams.topMargin;
			buttonView.layout(
					right - btnParams.rightMargin - btnWidth,
					top,
					right - btnParams.rightMargin,
					top + btnHeight
			);
			top += btnHeight + btnParams.bottomMargin;
		}
		return top;
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.visible = getVisibility() == View.VISIBLE;
		savedState.buttonsWidthMode = mButtonsWidthMode;
		if (mActionButtons != null && !mActionButtons.isEmpty()) {
			savedState.actionButtons = new ArrayList<>(mActionButtons.size());
			savedState.actionButtons.addAll(mActionButtons);
		}
		if (mInfoButtons != null && !mInfoButtons.isEmpty()) {
			savedState.infoButtons = new ArrayList<>(mInfoButtons.size());
			savedState.infoButtons.addAll(mInfoButtons);
		}
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		switch (mButtonsWidthMode = savedState.buttonsWidthMode) {
			case WIDTH_MODE_WRAP_CONTENT:
				break;
			case WIDTH_MODE_MATCH_PARENT:
				final int paddingLeft = getPaddingLeft();
				final int paddingRight = getPaddingRight();
				if (paddingLeft != paddingRight) {
					setPadding(
							Math.min(paddingLeft, paddingRight),
							getPaddingTop(),
							Math.min(paddingLeft, paddingRight),
							getPaddingBottom()
					);
				}
				break;
		}
		if (savedState.actionButtons != null) {
			this.ensureButtons(CATEGORY_ACTION);
			mActionButtons.addAll(savedState.actionButtons);
			savedState.actionButtons.clear();
		}
		if (savedState.infoButtons != null) {
			this.ensureButtons(CATEGORY_INFO);
			mInfoButtons.addAll(savedState.infoButtons);
			savedState.infoButtons.clear();
		}

		// Build layout with buttons.
		removeAllViews();
		if (mActionButtons != null && !mActionButtons.isEmpty()) {
			// Sort action buttons.
			Collections.sort(mActionButtons, BUTTONS_COMPARATOR);
			for (DialogButton button : mActionButtons) {
				button.detachView();
				this.addButtonView(button);
			}
		}
		if (mInfoButtons != null && !mInfoButtons.isEmpty()) {
			for (DialogButton button : mInfoButtons) {
				button.detachView();
				this.addButtonView(button);
			}
		}

		if (!savedState.visible) {
			setVisibility(View.GONE);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link SimpleDialogButtonsView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		List<DialogButton> actionButtons, infoButtons;

		/**
		 */
		int buttonsWidthMode;

		/**
		 */
		boolean visible;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.visible = source.readInt() == 1;
			this.buttonsWidthMode = source.readInt();
			final int actionN = source.readInt();
			if (actionN > 0) {
				this.actionButtons = new ArrayList<>(actionN);
				for (int i = 0; i < actionN; i++) {
					actionButtons.add((DialogButton) source.readParcelable(DialogsConfig.class.getClassLoader()));
				}
			}
			final int infoN = source.readInt();
			if (infoN > 0) {
				this.infoButtons = new ArrayList<>(infoN);
				for (int i = 0; i < infoN; i++) {
					infoButtons.add((DialogButton) source.readParcelable(DialogsConfig.class.getClassLoader()));
				}
			}
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(visible ? 1 : 0);
			dest.writeInt(buttonsWidthMode);
			if (actionButtons != null && actionButtons.isEmpty()) {
				dest.writeInt(actionButtons.size());
				for (DialogButton button : actionButtons) {
					button.writeToParcel(dest, flags);
				}
			} else {
				dest.writeInt(0);
			}
			if (infoButtons != null && infoButtons.isEmpty()) {
				dest.writeInt(infoButtons.size());
				for (DialogButton button : infoButtons) {
					button.writeToParcel(dest, flags);
				}
			} else {
				dest.writeInt(0);
			}
		}
	}

	/**
	 * Default implementation of DialogButton.
	 */
	static final class ButtonImpl extends DialogButton {

		/**
		 * Flag indicating whether a view is enabled or not.
		 */
		static final int PFLAG_ENALBED = 0x00000001;

		/**
		 * Flag indicating whether a view is selected or not.
		 */
		static final int PFLAG_SELECTED = 0x00000001 << 1;

		/**
		 * Flag indicating whether a view is visible or not.
		 */
		static final int PFLAG_VISIBLE = 0x00000001 << 2;

		/**
		 * Creator used to create an instance or array of instances of ButtonImpl from {@link Parcel}.
		 */
		public static final Creator<ButtonImpl> CREATOR = new Creator<ButtonImpl>() {

			/**
			 */
			@Override public ButtonImpl createFromParcel(@NonNull final Parcel source) {
				return new ButtonImpl(source);
			}

			/**
			 */
			@Override public ButtonImpl[] newArray(final int size) {
				return new ButtonImpl[size];
			}
		};

		/**
		 * Id of this button implementation.
		 */
		final int id;

		/**
		 * Category of this button implementation.
		 */
		int category = CATEGORY_ACTION;

		/**
		 * Text of this button implementation.
		 */
		CharSequence text;

		/**
		 * Resource id of the text of this button implementation.
		 */
		int textRes;

		/**
		 * Currently attached view of this button implementation.
		 */
		View view;

		/**
		 * Set of private flags for this button implementation.
		 */
		int privateFlags = PFLAG_ENALBED | PFLAG_VISIBLE;

		/**
		 * Creates a new instance of ButtonImpl.
		 *
		 * @param id       An id for the new button.
		 * @param category A category for the new button.
		 */
		ButtonImpl(final int id, final int category) {
			this.id = id;
			this.category = category;
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of ButtonImpl form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		ButtonImpl(@NonNull final Parcel source) {
			this.id = source.readInt();
			this.category = source.readInt();
			this.textRes = source.readInt();
			this.text = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.privateFlags = source.readInt();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			dest.writeInt(id);
			dest.writeInt(category);
			dest.writeInt(textRes);
			TextUtils.writeToParcel(text, dest, flags);
			dest.writeInt(privateFlags);
		}

		/**
		 */
		@Override public int getId() {
			return id;
		}

		/**
		 */
		@Override public int getCategory() {
			return category;
		}

		/**
		 */
		@Override public void setText(@StringRes final int resId) {
			if (textRes != resId) {
				this.textRes = resId;
				if (view instanceof TextView) {
					final TextView textView = (TextView) view;
					textView.setText(textRes);
					this.text = textView.getText();
				}
			}
		}

		/**
		 */
		@Override public void setText(@NonNull final CharSequence text) {
			this.text = text;
			if (view instanceof TextView) ((TextView) view).setText(text);
		}

		/**
		 */
		@Override @NonNull public CharSequence getText() {
			return text != null ? text : "";
		}

		/**
		 */
		@Override public void setEnabled(final boolean enabled) {
			this.updatePrivateFlags(PFLAG_ENALBED, enabled);
			if (view != null) view.setEnabled(enabled);
		}

		/**
		 */
		@Override public boolean isEnabled() {
			return (privateFlags & PFLAG_ENALBED) != 0;
		}

		/**
		 */
		@Override public void setSelected(final boolean selected) {
			this.updatePrivateFlags(PFLAG_SELECTED, selected);
			if (view != null) view.setSelected(selected);
		}

		/**
		 */
		@Override public boolean isSelected() {
			return (privateFlags & PFLAG_SELECTED) != 0;
		}

		/**
		 */
		@Override public void setVisible(final boolean visible) {
			this.updatePrivateFlags(PFLAG_VISIBLE, visible);
			if (view != null) view.setVisibility(visible ? VISIBLE : GONE);
		}

		/**
		 */
		@Override public boolean isVisible() {
			return (privateFlags & PFLAG_VISIBLE) != 0;
		}

		/**
		 */
		@Override public void attachView(@NonNull final View view) {
			this.view = view;
			this.view.setTag(this);
			if (view instanceof TextView) {
				final TextView textView = (TextView) view;
				if (textRes > 0) {
					textView.setText(textRes);
					this.text = textView.getText();
				} else {
					textView.setText(text);
				}
			}
			view.setEnabled(isEnabled());
			view.setSelected(isSelected());
			view.setVisibility(isVisible() ? View.VISIBLE : View.GONE);
		}

		/**
		 */
		@Override public void detachView() {
			this.view.setTag(null);
			this.view = null;
		}

		/**
		 */
		@Override public boolean hasViewAttached() {
			return view != null;
		}

		/**
		 */
		@Override @Nullable public View getView() {
			return view;
		}

		/**
		 * Updates the current private flags.
		 *
		 * @param flag Value of the desired flag to add/remove to/from the current private flags.
		 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
		 */
		private void updatePrivateFlags(final int flag, final boolean add) {
			if (add) this.privateFlags |= flag;
			else this.privateFlags &= ~flag;
		}
	}
}