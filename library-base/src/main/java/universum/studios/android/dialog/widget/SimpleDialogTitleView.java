/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.view.DialogTitleView;
import universum.studios.android.ui.util.ResourceUtils;
import universum.studios.android.ui.widget.ImageViewWidget;
import universum.studios.android.ui.widget.LinearLayoutWidget;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link DialogTitleView} implementation used by {@link SimpleDialog SimpleDialog} as title view.
 * <p>
 * This class represents a container for dialog title icon, title text and title loading bar.
 * Icon may be specified via {@link #setIconDrawable(Drawable)} or its resource equivalent. Title
 * text may be specified via {@link #setTitle(CharSequence)} or its resource equivalent {@link #setTitle(int)}.
 * <p>
 * To show/hide indeterminate progress bar, that is situated on the right side of this view, invoke
 * {@link #setIndeterminateProgressBarVisible(boolean)} with the appropriate boolean flag.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Xml attributes</h3>
 * {@link R.attr#dialogTitleStyle dialogTitleStyle}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
// todo: rename to DialogBasicTitleView
public class SimpleDialogTitleView extends LinearLayoutWidget implements DialogTitleView {

	/*
	* Constants ===================================================================================
	*/

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleDialogTitleView";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Image view for dialog icon.
	 */
	private ImageView mIconView;

	/**
	 * Text view for dialog title text.
	 */
	private TextView mTitleView;

	/**
	 * Resource id of the icon drawable.
	 */
	private int mIconRes;

	/**
	 * Boolean flag indicating whether the {@link #mIconRes} has been specified via {@link #setVectorIconResource(int)}
	 * or via {@link #setIconResource(int)}.
	 */
	private boolean mHasVectorIconRes;

	/**
	 * Circular progress bar.
	 */
	private View mProgressBar;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SimpleDialogTitleView(Context, AttributeSet)} without attributes.
	 */
	public SimpleDialogTitleView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #SimpleDialogTitleView(Context, AttributeSet, int)} with
	 * {@link R.attr#dialogTitleStyle dialogTitleStyle} as attribute for default style.
	 */
	public SimpleDialogTitleView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, R.attr.dialogTitleStyle, 0);
	}

	/**
	 * Same as {@link #SimpleDialogTitleView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SimpleDialogTitleView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of SimpleDialogTitleView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SimpleDialogTitleView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		setOrientation(LinearLayout.HORIZONTAL);
		setGravity(Gravity.CENTER_VERTICAL);
		this.inflateHierarchy(context, R.layout.dialog_layout_title);
		final Resources resources = context.getResources();
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve tint color for icon if it specified within the current theme.
		if (theme.resolveAttribute(R.attr.dialogColorIcon, typedValue, true)) {
			if (mIconView instanceof ImageViewWidget) {
				final ColorStateList tintList;
				if (typedValue.resourceId != 0) {
					tintList = ResourceUtils.getColorStateList(resources, typedValue.resourceId, theme);
				} else {
					tintList = ColorStateList.valueOf(typedValue.data);
				}
				final ImageViewWidget iconView = (ImageViewWidget) mIconView;
				iconView.setImageTintList(tintList);
			}
		}
		// Resolve color for title text if it specified within the current theme.
		if (theme.resolveAttribute(R.attr.dialogColorTitle, typedValue, true)) {
			if (typedValue.resourceId != 0) {
				mTitleView.setTextColor(ResourceUtils.getColorStateList(resources, typedValue.resourceId, theme));
			} else {
				mTitleView.setTextColor(typedValue.data);
			}
		}
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mTitleView = (TextView) findViewById(R.id.dialog_text_view_title);
			this.mIconView = (ImageView) findViewById(R.id.dialog_image_view_icon);
			this.mProgressBar = findViewById(R.id.dialog_progress_bar_title);
		}
	}

	/**
	 */
	@Override public void setVectorIconResource(@DrawableRes final int resId) {
		this.mHasVectorIconRes = false;
		if (resId == 0) {
			setIconDrawable(null);
		} else if (mIconRes != resId) {
			this.mHasVectorIconRes = true;
			final Resources resources = getResources();
			setIconDrawable(ResourceUtils.getVectorDrawable(
					resources,
					mIconRes = resId,
					getContext().getTheme()
			));
		}
	}

	/**
	 */
	@Override public void setIconResource(@DrawableRes final int resId) {
		this.mHasVectorIconRes = false;
		if (resId == 0) {
			setIconDrawable(null);
		} else if (mIconRes != resId) {
			final Resources resources = getResources();
			setIconDrawable(ResourceUtils.getDrawable(
					resources,
					mIconRes = resId,
					getContext().getTheme()
			));
		}
	}

	/**
	 */
	@Override public void setIconDrawable(@Nullable final Drawable drawable) {
		if (drawable == null) {
			this.mIconRes = 0;
		}
		if (mIconView != null) {
			mIconView.setImageDrawable(drawable);
		}
		setIconVisible(drawable != null);
	}

	/**
	 */
	@Override @Nullable public Drawable getIconDrawable() {
		return mIconView != null ? mIconView.getDrawable() : null;
	}

	/**
	 */
	@Override public void setIconVisible(final boolean visible) {
		DialogWidgetUtils.updateViewVisibility(mIconView, visible ? VISIBLE : GONE);
	}

	/**
	 */
	@Override public boolean isIconVisible() {
		return DialogWidgetUtils.isVisible(mIconView);
	}

	/**
	 */
	@Override public void setTitle(@StringRes final int resId) {
		setTitle(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setTitle(@Nullable final CharSequence title) {
		if (mTitleView != null) mTitleView.setText(title);
	}

	/**
	 */
	@Override @NonNull public CharSequence getTitle() {
		return mTitleView != null ? mTitleView.getText() : "";
	}

	/**
	 */
	@Override public void setTitleVisible(final boolean visible) {
		DialogWidgetUtils.updateViewVisibility(mTitleView, visible ? VISIBLE : GONE);
	}

	/**
	 */
	@Override public boolean isTitleVisible() {
		return DialogWidgetUtils.isVisible(mTitleView);
	}

	/**
	 */
	@Override public void setTitleTypeface(@Nullable final Typeface typeface) {
		if (mTitleView != null) mTitleView.setTypeface(typeface);
	}

	/**
	 */
	@Override public void setIndeterminateProgressBarVisible(final boolean visible) {
		if (mProgressBar != null) mProgressBar.setVisibility(visible ? VISIBLE : INVISIBLE);
	}

	/**
	 */
	@Override public boolean isIndeterminateProgressBarVisible() {
		return mProgressBar != null && mProgressBar.getVisibility() == VISIBLE;
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.visible = getVisibility() == View.VISIBLE;
		savedState.iconRes = mIconRes;
		savedState.hasVectorIconRes = mHasVectorIconRes;
		savedState.iconVisible = DialogWidgetUtils.isVisible(mIconView);
		savedState.title = mTitleView != null ? mTitleView.getText() : "";
		savedState.titleVisible = DialogWidgetUtils.isVisible(mTitleView);
		savedState.progressBarVisible = DialogWidgetUtils.isVisible(mProgressBar);
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		if (!savedState.visible) {
			setVisibility(View.GONE);
		}
		if (savedState.hasVectorIconRes) {
			setVectorIconResource(savedState.iconRes);
		} else {
			setIconResource(savedState.iconRes);
		}
		setIconVisible(savedState.iconVisible);
		setTitle(savedState.title);
		setTitleVisible(savedState.titleVisible);
		setIndeterminateProgressBarVisible(savedState.progressBarVisible);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link SimpleDialogTitleView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		CharSequence title;

		/**
		 */
		int iconRes;

		/**
		 */
		boolean hasVectorIconRes, visible, titleVisible, iconVisible, progressBarVisible;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.visible = source.readInt() == 1;
			this.iconRes = source.readInt();
			this.hasVectorIconRes = source.readInt() == 1;
			this.iconVisible = source.readInt() == 1;
			this.title = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.titleVisible = source.readInt() == 1;
			this.progressBarVisible = source.readInt() == 1;
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(visible ? 1 : 0);
			dest.writeInt(iconRes);
			dest.writeInt(hasVectorIconRes ? 1 : 0);
			dest.writeInt(iconVisible ? 1 : 0);
			TextUtils.writeToParcel(title, dest, flags);
			dest.writeInt(titleVisible ? 1 : 0);
			dest.writeInt(progressBarVisible ? 1 : 0);
		}
	}
}