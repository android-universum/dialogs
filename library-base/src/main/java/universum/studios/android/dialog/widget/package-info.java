/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * Provides a set of basic widget implementations, like {@link universum.studios.android.dialog.widget.SimpleDialogTitleView},
 * {@link universum.studios.android.dialog.widget.SimpleDialogButtonsView}, used to build a view hierarchy
 * of a basic dialog along with widgets used by a specific types of dialogs.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.dialog.widget;