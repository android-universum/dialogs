/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.util;

import android.app.Dialog;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/**
 * Utility class for the Dialogs library.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class DialogUtils {

	/**
     */
    private DialogUtils() {
        // Creation of instances of this class is not publicly allowed.
    }

    /**
     * Shows the soft keyboard for the given <var>dialogFragment</var>.
     *
     * @param dialogFragment The dialog fragment for which window to request keyboard showing.
     * @return {@code True} if keyboard has been showed, {@code false} otherwise.
     * @see #hideSoftKeyboard(DialogFragment)
     */
    public static boolean showSoftKeyboard(@NonNull final DialogFragment dialogFragment) {
        if (dialogFragment.isAdded()) {
            final Dialog dialog =  dialogFragment.getDialog();
	        if (dialog == null) {
		        return false;
	        }
	        final Window window =  dialog.getWindow();
	        if (window == null) {
		        return false;
	        }
	        window.setSoftInputMode(
			        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
	        );
	        return true;
        }
        return false;
    }

    /**
     * Hides the soft keyboard for the given <var>dialogFragment</var>.
     *
     * @param dialogFragment The dialog fragment for which window to request keyboard hiding.
     * @return {@code True} if keyboard has been hided, {@code false} otherwise.
     * @see #showSoftKeyboard(DialogFragment)
     */
    public static boolean hideSoftKeyboard(@NonNull final DialogFragment dialogFragment) {
	    if (dialogFragment.isAdded()) {
		    final Dialog dialog =  dialogFragment.getDialog();
		    if (dialog == null) {
			    return false;
		    }
		    final Window window =  dialog.getWindow();
		    if (window == null) {
			    return false;
		    }
		    window.setSoftInputMode(
				    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
		    );
		    return true;
	    }
	    return false;
    }
}