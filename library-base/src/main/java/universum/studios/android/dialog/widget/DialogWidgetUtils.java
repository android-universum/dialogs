/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Set of utils used by extended widgets from the Dialogs library.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class DialogWidgetUtils {

	/**
	 */
	private DialogWidgetUtils() {
		// Creation of instances of this class is not publicly allowed.
	}

	/**
	 * Checks whether the given <var>view</var> is visible or not.
	 *
	 * @param view The view to check.
	 * @return {@code True} if view's visibility is {@link View#VISIBLE}, {@code false} otherwise.
	 */
	static boolean isVisible(final View view) {
		return view != null && view.getVisibility() == View.VISIBLE;
	}

	/**
	 * Updates a visibility of the given <var>view</var> to the specified one.
	 *
	 * @param view       The view of which visibility to update.
	 * @param visibility The visibility flag.
	 * @return {@code True} if view's visibility has been updated, {@code false} otherwise.
	 */
	static boolean updateViewVisibility(final View view, final int visibility) {
		if (view != null && view.getVisibility() != visibility) {
			view.setVisibility(visibility);
			return true;
		}
		return false;
	}

	/**
	 * Updates a layout gravity of the given <var>view</var> to the specified one.
	 *
	 * @param view    the view of which layout gravity to update.
	 * @param gravity The gravity flags.
	 * @return {@code True} if view's layout gravity has been updated, {@code false} otherwise.
	 */
	static boolean updateViewLayoutParamsGravity(final View view, final int gravity) {
		if (view != null) {
			final ViewGroup.LayoutParams params = view.getLayoutParams();
			if (params instanceof FrameLayout.LayoutParams) {
				((FrameLayout.LayoutParams) params).gravity = gravity;
				return true;
			}
		}
		return false;
	}
}