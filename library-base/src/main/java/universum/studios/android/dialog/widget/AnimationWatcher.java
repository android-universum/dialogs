/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.view.animation.Animation;

import androidx.annotation.NonNull;

/**
 * An {@link Animation.AnimationListener} implementation to simplify usage of such a listener, so we
 * do not need to implement all its callbacks, only those we need.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class AnimationWatcher implements Animation.AnimationListener {

	/**
	 */
	@Override public void onAnimationStart(@NonNull Animation animation) {}

	/**
	 */
	@Override public void onAnimationEnd(@NonNull Animation animation) {}

	/**
	 */
	@Override public void onAnimationRepeat(@NonNull Animation animation) {}
}