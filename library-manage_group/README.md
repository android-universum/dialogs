@Dialogs-Manage
===============

This module groups the following modules into one **single group**:

- [Manage-Core](https://bitbucket.org/android-universum/dialogs/src/main/library-manage-core)
- [Manage-Base](https://bitbucket.org/android-universum/dialogs/src/main/library-manage-base)
- [Manage-Xml](https://bitbucket.org/android-universum/dialogs/src/main/library-manage-xml)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adialogs/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adialogs/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:dialogs-manage:${DESIRED_VERSION}@aar"
