/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.content;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.model.DialogSample;
import universum.studios.android.samples.dialog.ui.adapter.SamplesAdapter;

/**
 * @author Martin Albedinsky
 */
public final class XmlDialogs extends DataSetProvider {

	@Override protected void onBindDialogsAdapter(@NonNull final SamplesAdapter adapter) {
		final List<DialogSample> items = new ArrayList<>();
		// Simple dialogs ==========================================================================
		items.add(DialogSample.create(
				R.xml.dialog,
				R.string.xml_dialog_label,
				R.string.xml_dialog_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_info,
				R.string.xml_info_dialog_label,
				R.string.xml_info_dialog_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_version_info,
				R.string.xml_version_info_dialog_label,
				R.string.xml_version_info_dialog_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_alert,
				R.string.xml_alert_dialog_label,
				R.string.xml_alert_dialog_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_error,
				R.string.xml_error_dialog_label,
				R.string.xml_error_dialog_desc
		));
		// Loading dialogs =========================================================================
		items.add(DialogSample.create(
				R.xml.dialog_loading,
				R.string.xml_loading_dialog_label,
				R.string.xml_loading_dialog_desc
		));
		// Progress dialogs ========================================================================
		items.add(DialogSample.create(
				R.xml.dialog_progress,
				R.string.xml_progress_dialog_label,
				R.string.xml_progress_dialog_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_progress_progress_and_time,
				R.string.xml_progress_dialog_progress_and_time_label,
				R.string.xml_progress_dialog_progress_and_time_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_progress_indeterminate,
				R.string.xml_progress_dialog_indeterminate_label,
				R.string.xml_progress_dialog_indeterminate_desc
		));
		// Web dialogs =============================================================================
		items.add(DialogSample.create(
				R.xml.dialog_web,
				R.string.xml_web_dialog_label,
				R.string.xml_web_dialog_desc
		));
		// Date/Time dialogs =======================================================================
		items.add(DialogSample.create(
				R.xml.dialog_time_picker,
				R.string.xml_time_picker_dialog_label,
				R.string.xml_time_picker_dialog_desc
		));
		items.add(DialogSample.create(
				R.xml.dialog_date_picker,
				R.string.xml_date_picker_dialog_label,
				R.string.xml_date_picker_dialog_desc
		));
		// Selection dialogs =======================================================================
		items.add(DialogSample.create(
				R.xml.dialog_selection,
				R.string.xml_selection_dialog_label,
				R.string.xml_selection_dialog_desc
		));
		// Edit dialogs ============================================================================
		items.add(DialogSample.create(
				R.xml.dialog_edit,
				R.string.xml_edit_dialog_label,
				R.string.xml_edit_dialog_desc
		));
		// SignIn dialogs ==========================================================================
		items.add(DialogSample.create(
				R.xml.dialog_signin,
				R.string.xml_sign_in_dialog_label,
				R.string.xml_sign_in_dialog_desc
		));
		adapter.changeItems(items);
	}
}