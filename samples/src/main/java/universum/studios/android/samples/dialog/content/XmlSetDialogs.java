/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.content;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.model.DialogSample;
import universum.studios.android.samples.dialog.ui.adapter.SamplesAdapter;

/**
 * @author Martin Albedinsky
 */
public final class XmlSetDialogs extends DataSetProvider {

	@Override protected void onBindDialogsAdapter(@NonNull final SamplesAdapter adapter) {
		final List<DialogSample> items = new ArrayList<>();
		items.add(DialogSample.create(
				R.id.dialogs_sing_up_loading,
				R.string.xml_set_loading_dialog_label,
				R.string.xml_set_loading_dialog_desc
		));
		items.add(DialogSample.create(
				R.id.dialogs_sing_up_wrong_name,
				R.string.xml_set_alert_dialog_label,
				R.string.xml_set_alert_dialog_desc
		));
		items.add(DialogSample.create(
				R.id.dialogs_sing_up_success,
				R.string.xml_set_info_dialog_label,
				R.string.xml_set_info_dialog_desc
		));
		items.add(DialogSample.create(
				R.id.dialog_error_unknown,
				R.string.xml_set_error_dialog_label,
				R.string.xml_set_error_dialog_desc
		));
		items.add(DialogSample.create(
				R.id.dialog_time_picker,
				R.string.xml_set_time_picker_dialog_label,
				R.string.xml_set_time_picker_dialog_desc
		));
		adapter.changeItems(items);
	}
}