/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.async;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.dialog.SignInDialog;

/**
 * @author Martin Albedinsky
 */
public final class SignInTask extends AsyncTask<SignInDialog.Credentials, Integer, Integer> {

	public interface Callback {

		void onAuthenticationServiceError(int error);

		void onAuthenticationServiceSuccess();
	}

	public static final int ERROR_WRONG_USERNAME = -0xe01;
	public static final int ERROR_WRONG_PASSWORD = -0xe02;
	private static final int SERVICE_RESPONSE_DELAY = 6000;
	private static final String USERNAME = "android@gmail.com";
	private static final String PASSWORD = "android";

	private final List<Callback> callbacks = new ArrayList<>(1);

	public void registerCallback(@NonNull final Callback callback) {
		callbacks.add(callback);
	}

	public void unregisterCallback(@NonNull final Callback callback) {
		callbacks.remove(callback);
	}

    @Override protected Integer doInBackground(@NonNull final SignInDialog.Credentials... params) {
        SignInDialog.Credentials credentials = params[0];
        // Simulate service response delay.
        try {
            Thread.sleep(SERVICE_RESPONSE_DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

	    // Check the user credentials.
	    final boolean userOK = USERNAME.equals(credentials.username);
	    final boolean passOK = PASSWORD.equals(credentials.password);

	    if (!userOK || !passOK) {
		    if (!userOK) {
			    publishProgress(ERROR_WRONG_USERNAME);
		    }
		    if (!passOK) {
			    publishProgress(ERROR_WRONG_PASSWORD);
		    }
		    return null;
	    }
        return 0;
    }

    @Override protected void onPostExecute(@NonNull final Integer result) {
	    if (result != null) {
		    for (Callback callback : callbacks) {
			    callback.onAuthenticationServiceSuccess();
		    }
	    }
    }

	@Override protected void onProgressUpdate(@NonNull final Integer... values) {
		for (Callback callback : callbacks) {
			callback.onAuthenticationServiceError(values[0]);
		}
	}
}