/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.ProgressBarDialog;
import universum.studios.android.dialog.SignInDialog;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.samples.dialog.async.ProgressTask;
import universum.studios.android.samples.dialog.async.SignInTask;
import universum.studios.android.samples.dialog.content.DataSetProvider;
import universum.studios.android.samples.dialog.ui.DialogTheme;
import universum.studios.android.samples.dialog.ui.adapter.SamplesAdapter;
import universum.studios.android.samples.ui.SamplesAdapterViewFragment;

/**
 * @author Martin Albedinsky
 */
public abstract class BaseDialogsFragment<P extends DataSetProvider> extends SamplesAdapterViewFragment<SamplesAdapter, ListView, TextView>
		implements
		AdapterView.OnItemClickListener,
		DialogController.OnRestoreListener,
		Dialog.OnDialogListener,
		Dialog.OnCancelListener,
		ProgressTask.Callback,
		SignInTask.Callback {

	@SuppressWarnings("unused") private static final String TAG = "BaseDialogsFragment";

	private static ProgressTask progressTask;
	private static SignInTask signInTask;

	DialogController dialogController;
	private DialogTheme dialogTheme;
	private P dataSetProvider;
	private SamplesAdapter adapter;

	@Override public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dialogTheme = DialogTheme.obtainFromAppPreferences(requireActivity());
		onSetUpDialogController(dialogController = DialogController.create(this));
		if (progressTask != null) {
			progressTask.registerCallback(this);
		}
		if (signInTask != null) {
			signInTask.registerCallback(this);
		}
	}

	@Override public void onDestroy() {
		super.onDestroy();
		if (progressTask != null) {
			progressTask.unregisterCallback(this);
		}
		if (signInTask != null) {
			signInTask.unregisterCallback(this);
		}
	}

	@Override public void onActivityCreated(@NonNull final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			onCheckRestoredDialogs();
		}
	}

	@Override public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
		return inflater.inflate(dialogTheme.listViewLayoutRes, null);
	}

	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.performBindDialogsAdapter();
	}

	@Override public void onItemClick(@NonNull final AdapterView<?> parent, @NonNull final View view, final int position, final long id) {
		onDialogSampleClick((int) id, dialogController);
	}

	@Override public void onDialogRestored(@NonNull final DialogFragment dialog) {
		Log.d(TAG, "onDialogRestored(" + dialog + ")");
	}

	@Override public boolean onDialogCancelled(@NonNull final Dialog dialog) {
		Log.d(TAG, "onDialogCancelled(" + dialog.getDialogId() + ")");
		return false;
	}

	@Override public boolean onDialogButtonClick(@NonNull final Dialog dialog, final int button) {
		Log.d(TAG, "onDialogButtonClick(" + dialog.getDialogId() + ", " + button + ")");
		return false;
	}

	@Override public void onProgressTaskCancelled(final int taskId, @NonNull final ProgressBarDialog progressDialog) {
		onProgressTaskFinished(taskId, progressDialog);
	}

	@Override public void onProgressTaskFinished(final int taskId, @NonNull final ProgressBarDialog progressDialog) {
		if (progressDialog.isVisible()) {
			progressDialog.dismiss();
		}
		progressTask = null;
	}

	@Override public void onAuthenticationServiceError(int error) {}

	@Override public void onAuthenticationServiceSuccess() {}

	protected void onSetUpDialogController(@NonNull final DialogController controller) {
		controller.registerOnRestoreListener(this);
	}

	protected void onCheckRestoredDialogs() {}

	protected void onDialogSampleClick(final int id, @NonNull final DialogController controller) {
		dialogController.newRequest(id).execute();
	}

	void setDataSetProvider(@NonNull final P provider) {
		this.dataSetProvider = provider;
		this.performBindDialogsAdapter();
	}

	boolean executeProgressTask(@NonNull final ProgressTask progressTask, @NonNull final ProgressBarDialog progressDialog) {
		if (progressTask != null) {
			if (BaseDialogsFragment.progressTask != null) {
				BaseDialogsFragment.progressTask.cancel();
			}
			BaseDialogsFragment.progressTask = progressTask;
			BaseDialogsFragment.progressTask.attachProgressDialog(progressDialog);
			BaseDialogsFragment.progressTask.registerCallback(this);
			BaseDialogsFragment.progressTask.execute();
			return true;
		}
		return false;
	}

	void reattachProgressDialogToTask(@NonNull final ProgressBarDialog progressDialog) {
		Log.d(TAG, "Reattaching progress task(" + progressTask + ") for dialog(" + progressDialog.getDialogId() + ", " + progressDialog + ").");
		if (progressTask != null) {
			progressTask.attachProgressDialog(progressDialog);
		}
	}

	void cancelProgressTask() {
		if (progressTask != null) {
			progressTask.cancel();
			progressTask = null;
		}
	}

	void executeSignInTask(@NonNull final SignInDialog.Credentials credentials) {
		if (credentials != null) {
			if (signInTask != null) {
				signInTask.cancel(true);
			}
			signInTask = new SignInTask();
			signInTask.registerCallback(this);
			signInTask.execute(credentials);
		}
	}

	private void performBindDialogsAdapter() {
		if (hasDataSetProvider()) {
			if (adapter == null) {
				adapter = new SamplesAdapter(getActivity());
				dataSetProvider.dispatchBindDialogsAdapter(adapter);
			} else {
				setAdapter(adapter);
			}
		}
	}

	private boolean hasDataSetProvider() {
		return dataSetProvider != null;
	}
}