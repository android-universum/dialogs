/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.fragment;

import universum.studios.android.fragment.annotation.FactoryFragment;
import universum.studios.android.fragment.manage.BaseFragmentFactory;

/**
 * @author Martin Albedinsky
 */
public final class FragmentsFactory extends BaseFragmentFactory {

	@FactoryFragment(JavaDialogsFragment.class) public static final int JAVA = 0x00;
	@FactoryFragment(XmlDialogsFragment.class) public static final int XML = 0x01;
	@FactoryFragment(XmlSetDialogsFragment.class) public static final int XML_SET = 0x02;
	@FactoryFragment(StringSetDialogsFragment.class) public static final int STRING_ARRAY = 0x03;
}