/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.dialog;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.manage.BaseDialogFactory;
import universum.studios.android.samples.dialog.R;

/**
 * @author Martin Albedinsky
 */
public final class DialogsStringSetFactory extends BaseDialogFactory {

    public enum Sample {
        DIALOG_1(0xb1001, R.array.dialog_options_1),
        DIALOG_2(0xb1002, R.array.dialog_options_2),
        DIALOG_3(0xb1003, R.array.dialog_options_3),
        DIALOG_4(0xb1004, R.array.dialog_options_4),
        DIALOG_5(0xb1005, R.array.dialog_options_5),
        DIALOG_6(0xb1006, R.array.dialog_options_6);

        public final int id;
        private final int stringArray;

        Sample(final int dialogId, final int string) {
            this.stringArray = string;
            this.id = dialogId;
        }

        static Sample parseDialog(final int dialogId) {
            for (Sample dialog : Sample.values()) {
                if (dialog.id == dialogId) {
                    return dialog;
                }
            }
            return null;
        }
    }

    public DialogsStringSetFactory(@NonNull final Context context) {
        super(context);
    }

	@Override protected boolean providesDialog(final int dialogId) {
		return true;
	}

    @Override protected DialogFragment onCreateDialog(final int dialogId, @Nullable final DialogOptions options) {
        final Sample dialog = Sample.parseDialog(dialogId);
        return dialog != null ? SimpleDialog.newInstance(new DialogOptions(mResources).fromStringArray(dialog.stringArray)) : null;
    }

    @Override @Nullable protected String onCreateDialogTag(final int dialogId) {
        return createDialogTag(getClass(), Integer.toString(dialogId));
    }
}