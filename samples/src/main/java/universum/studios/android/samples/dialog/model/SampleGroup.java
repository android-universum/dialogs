/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import universum.studios.android.dialog.adapter.DialogSelectionAdapter;

/**
 * @author Martin Albedinsky
 */
public final class SampleGroup implements DialogSelectionAdapter.Item {

	public static final Parcelable.Creator<SampleGroup> CREATOR = new Parcelable.Creator<SampleGroup>() {

		@Override public SampleGroup createFromParcel(@NonNull final Parcel source) { return new SampleGroup(source); }

		@Override public SampleGroup[] newArray(final int size) { return new SampleGroup[size]; }
	};

	public final int id;
	public final String name;

	public SampleGroup(final int id, final String name) {
		this.id = id;
		this.name = name;
	}

	SampleGroup(@NonNull final Parcel source) {
		this.id = source.readInt();
		this.name = source.readString();
	}

	@Override public long getId() {
		return id;
	}

	@Override public String toString() {
		return name;
	}

	@Override @NonNull public CharSequence getText() {
		return name;
	}

	@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
		dest.writeInt(id);
		dest.writeString(name);
	}

	public int describeContents() {
		return 0;
	}
}