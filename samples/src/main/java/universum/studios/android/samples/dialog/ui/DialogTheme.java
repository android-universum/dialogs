/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import androidx.annotation.NonNull;
import universum.studios.android.samples.dialog.R;

/**
 * @author Martin Albedinsky
 */
public enum DialogTheme {
	DEFAULT(
			R.id.menu_theme_default,
			R.style.Theme,
			R.string.menu_theme_default,
			R.color.theme_default,
			R.drawable.selector_list_default,
			R.layout.listview_default
	),
	DEFAULT_LIGHT(
			R.id.menu_theme_default_light,
			R.style.Theme_Light,
			R.string.menu_theme_default_light,
			R.color.theme_default_light,
			R.drawable.selector_list_default,
			R.layout.listview_default
	),
	ANDROID(
			R.id.menu_theme_android,
			R.style.Theme_Android,
			R.string.menu_theme_android,
			R.color.theme_android,
			R.drawable.selector_list_android,
			R.layout.listview_android
	),
	LIME(
			R.id.menu_theme_lime,
			R.style.Theme_Lime,
			R.string.menu_theme_lime,
			R.color.theme_lime,
			R.drawable.selector_list_lime,
			R.layout.listview_lime
	),
	SUNRISE(
			R.id.menu_theme_sunrise,
			R.style.Theme_Sunrise,
			R.string.menu_theme_sunrise,
			R.color.theme_sunrise,
			R.drawable.selector_list_sunrise,
			R.layout.listview_sunrise
	),
	ROSE(
			R.id.menu_theme_rose,
			R.style.Theme_Rose,
			R.string.menu_theme_rose,
			R.color.theme_rose,
			R.drawable.selector_list_rose,
			R.layout.listview_rose
	);

	public final int id;
	public final int themeRes;
	public final int titleRes;
	public final int colorRes;
	public final int listItemSelectorRes;
	public final int listViewLayoutRes;
	private static DialogTheme cached = null;

	DialogTheme(final int id, final int theme, final int title, final int color, final int listItemSelector, final int listViewLayout) {
		this.id = id;
		this.themeRes = theme;
		this.titleRes = title;
		this.colorRes = color;
		this.listItemSelectorRes = listItemSelector;
		this.listViewLayoutRes = listViewLayout;
	}

	public static void saveIntoAppPreferences(@NonNull final Context context, final int themeId) {
		cached = null;
		resolveInstance(themeId).saveIntoPreferences(context);
	}

	public static DialogTheme obtainFromAppPreferences(@NonNull final Context context) {
		if (cached == null) {
			cached = DialogTheme.values()[
					PreferenceManager.getDefaultSharedPreferences(context).getInt(
							context.getResources().getString(R.string.preference_key_selected_theme),
							DEFAULT_LIGHT.ordinal()
					)];
		}
		return cached;
	}

	public static Menu populateMenu(@NonNull final Menu menu, @NonNull final Resources resources) {
		// Set up sub-menu.
		final SubMenu subMenu = menu.addSubMenu(Menu.NONE, Menu.NONE, 0, resources.getString(R.string.menu_theme_title));
		final MenuItem menuItem = subMenu.getItem();
		menuItem.setIcon(R.drawable.ic_action_image_color_lens);
		menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		// Fill sub-menu with theme items.
		for (DialogTheme theme : DialogTheme.values()) {
			subMenu.add(Menu.NONE, theme.id, 0, resources.getString(theme.titleRes));
		}
		return menu;
	}

	public static DialogTheme resolveInstance(final int themeId) {
		for (DialogTheme theme : DialogTheme.values()) {
			if (theme.id == themeId) {
				return theme;
			}
		}
		return DialogTheme.DEFAULT;
	}

	public static boolean isSelected(final int themeId) {
		return (cached != null) && cached.id == themeId;
	}

	private boolean saveIntoPreferences(@NonNull final Context context) {
		final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		return editor.putInt(
				context.getResources().getString(R.string.preference_key_selected_theme),
				(this.ordinal())
		).commit();
	}
}