/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.model;

/**
 * @author Martin Albedinsky
 */
public final class DialogSample {

	public final int id, labelResId, descriptionResId;

	private DialogSample(final int id, final int labelRes, final int descRes) {
		this.id = id;
		this.labelResId = labelRes;
		this.descriptionResId = descRes;
	}

	public static DialogSample create(final int id, final int labelRes, final int descRes) {
		return new DialogSample(id, labelRes, descRes);
	}
}