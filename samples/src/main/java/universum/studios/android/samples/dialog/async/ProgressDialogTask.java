/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.async;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import universum.studios.android.dialog.ProgressBarDialog;

/**
 * Async task simulating downloading process of the images from the server.
 *
 * @author Martin Albedinsky
 */
public class ProgressDialogTask extends AsyncTask<Void, Integer, Void> implements ProgressTask {

	public static final int TASK_ID = 0xff001;
	public static final int ITEMS_TO_DOWNLOAD = 60;
	private static final int SLEEP_WORK = 300;
	private final Random RANDOM = new Random();

	private final List<Callback> callbacks = new ArrayList<>(1);
	ProgressBarDialog progressDialog;
	private int id = TASK_ID;
	private boolean publishCountProgress = false;

	public ProgressDialogTask() {
		this(TASK_ID);
	}

	ProgressDialogTask(final int taskId) {
		this.id = taskId;
	}

	@Override public void attachProgressDialog(@NonNull final ProgressBarDialog progressDialog) {
		this.progressDialog = progressDialog;
	}

	@Override public void registerCallback(@NonNull final Callback callback) {
		callbacks.add(callback);
	}

	@Override public void unregisterCallback(@NonNull final Callback callback) {
		callbacks.remove(callback);
		if (callbacks.size() == 0) {
			this.progressDialog = null;
		}
	}

	@Override public void execute() {
		super.execute();
	}

	@Override public void setPublishCountProgress(final boolean publish) {
		this.publishCountProgress = publish;
	}

	@Override protected Void doInBackground(@NonNull final Void... params) {
		int items = 0;
		int progress;

		publishProgress(ITEMS_TO_DOWNLOAD, 0);
		while (items <= ITEMS_TO_DOWNLOAD && !isCancelled() && !Thread.currentThread().isInterrupted()) {
			try {
				Thread.sleep(SLEEP_WORK);
			} catch (InterruptedException ignore) {
			}
			items += RANDOM.nextInt(3);
			progress = (int) ((items / (float) ITEMS_TO_DOWNLOAD) * 100);
			// As it is required by the progress dialog, progress value for the progress bar
			// must be at the last position of publishing progress values.
			publishProgress(Math.min(items, ITEMS_TO_DOWNLOAD), Math.min(progress, 100));
		}
		if (!isCancelled() || !Thread.currentThread().isInterrupted()) {
			publishProgress(ITEMS_TO_DOWNLOAD, 100);
		}
		return null;
	}

	@Override public void cancel() {
		cancel(true);
	}

	@Override protected void onCancelled() {
		for (Callback callback : callbacks) {
			callback.onProgressTaskCancelled(id, progressDialog);
		}
	}

	@Override protected void onPostExecute(final Void aVoid) {
		for (Callback callback : callbacks) {
			callback.onProgressTaskFinished(id, progressDialog);
		}
	}

	@Override protected void onProgressUpdate(@NonNull final Integer... values) {
		if (progressDialog != null) {
			if (publishCountProgress) {
				progressDialog.setProgress(values[0], values[1]);
			} else {
				progressDialog.setProgress(values[1]);
			}
		}
	}
}