/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.async;

import androidx.annotation.NonNull;

/**
 * @author Martin Albedinsky
 */
public final class LoadingDialogTask extends ProgressDialogTask {

    private static final int TASK_ID = 0xff002;

	private final String[] MESSAGES = {
            "Updating database ...",
            "Calculating directions ...",
            "Resolving destination ...",
            "Preparing route ..."
    };

    public LoadingDialogTask() {
        super(TASK_ID);
    }

    @Override protected void onProgressUpdate(@NonNull final Integer... values) {
        super.onProgressUpdate(values);
        if (progressDialog != null) {
            final int currProgress = values[values.length - 1];
            if (currProgress >= 90) {
	            progressDialog.setContent(MESSAGES[3]);
            } else if (currProgress >= 50) {
	            progressDialog.setContent(MESSAGES[2]);
            } else if (currProgress >= 35) {
	            progressDialog.setContent(MESSAGES[1]);
            } else if (currProgress >= 15) {
	            progressDialog.setContent(MESSAGES[0]);
            }
        }
    }
}