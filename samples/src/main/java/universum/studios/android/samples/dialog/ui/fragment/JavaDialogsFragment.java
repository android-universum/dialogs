/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.AdapterDialog;
import universum.studios.android.dialog.ColorPickerDialog;
import universum.studios.android.dialog.DatePickerDialog;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.EditDialog;
import universum.studios.android.dialog.LocalePickerDialog;
import universum.studios.android.dialog.ProgressBarDialog;
import universum.studios.android.dialog.SelectionDialog;
import universum.studios.android.dialog.SignInDialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.TimePickerDialog;
import universum.studios.android.dialog.UniqueDialog;
import universum.studios.android.dialog.adapter.DialogSelectionAdapter;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.dialog.manage.DialogRequest;
import universum.studios.android.dialog.util.DialogLocale;
import universum.studios.android.dialog.util.DialogLocales;
import universum.studios.android.graphics.Colors;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.async.LoadingDialogTask;
import universum.studios.android.samples.dialog.async.ProgressDialogTask;
import universum.studios.android.samples.dialog.async.ProgressTask;
import universum.studios.android.samples.dialog.async.SignInTask;
import universum.studios.android.samples.dialog.content.JavaDialogs;
import universum.studios.android.samples.dialog.ui.adapter.ListDialogAdapter;
import universum.studios.android.samples.dialog.ui.dialog.DialogsFactory;
import universum.studios.android.util.Toaster;

/**
 * @author Martin Albedinsky
 */
public final class JavaDialogsFragment extends BaseDialogsFragment<JavaDialogs>
        implements
        AdapterDialog.AdapterProvider,
        AdapterDialog.OnItemClickListener,
        SignInDialog.OnSignInListener {

    private long selectedGroupId = 1;
    private long selectedCountryId = -1;
    private long selectedLanguageId = -1;
    private int pickedColor;
    private Calendar calendar = Calendar.getInstance();

    @Override public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDataSetProvider(new JavaDialogs());
    }

    @Override protected void onSetUpDialogController(@NonNull final DialogController controller) {
        super.onSetUpDialogController(controller);
        controller.setFactory(new DialogsFactory(getActivity()));
    }

	@SuppressWarnings("ConstantConditions")
    @Override @Nullable public Object provideDialogAdapter(@NonNull final Dialog dialog) {
        switch (dialog.getDialogId()) {
            case DialogsFactory.LIST_DIALOG:
                return new ListDialogAdapter(getActivity(), dialog.getDialogLayoutInflater());
            case DialogsFactory.GRID_DIALOG:
                return null;
        }
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override public boolean onDialogItemClick(@NonNull final AdapterDialog dialog, @NonNull final AdapterView<?> adapterView, @NonNull final View view, final int position, final long id) {
        switch (dialog.getDialogId()) {
            case DialogsFactory.LIST_DIALOG:
                Toast.makeText(getActivity(), "Clicked: " + adapterView.getAdapter().getItem(position), Toast.LENGTH_SHORT).show();
                break;
            case DialogsFactory.LOCALE_DIALOG_COUNTRY:
                final DialogLocale country = (DialogLocale) ((LocalePickerDialog) dialog).getSelectedItem();
                this.selectedCountryId = country.getId();
                Toaster.showToast(getActivity(), "Selected =>" + country.countryName);
                break;
            case DialogsFactory.LOCALE_DIALOG_LANGUAGE:
                final DialogLocale language = (DialogLocale) ((LocalePickerDialog) dialog).getSelectedItem();
                this.selectedLanguageId = language.getId();
                Toaster.showToast(getActivity(), "Selected =>" + language.languageName);
                break;
            case DialogsFactory.ITEM_DIALOG_SINGLE_SELECTION:
                final DialogSelectionAdapter.Item item = ((SelectionDialog) dialog).getSelectedItem();
                this.selectedGroupId = item.getId();
                Toaster.showToast(getActivity(), "Selected => " + item);
                break;
        }
        dialog.dismiss();
        return true;
    }

    @Override public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.navigation_item_java);
    }

    @Override protected void onDialogSampleClick(final int id, @NonNull final DialogController controller) {
        switch (id) {
            case DialogsFactory.LOADING_DIALOG_WITH_PROGRESS_INDICATOR:
                executeProgressTask(
                        new LoadingDialogTask(),
                        (ProgressBarDialog) dialogController.newRequest(id).execute()
                );
                return;
            case DialogsFactory.PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR:
            case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR:
            case DialogsFactory.PROGRESS_DIALOG_WITH_TIME_INDICATOR:
                executeProgressTask(
                        new ProgressDialogTask(),
                        (ProgressBarDialog) dialogController.newRequest(id).execute()
                );
                return;
            case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR:
                final ProgressTask progressTask = new ProgressDialogTask();
                // We want to also show additional progress by count of already downloaded items.
                progressTask.setPublishCountProgress(true);
                executeProgressTask(
                        progressTask,
                        (ProgressBarDialog) dialogController.newRequest(id).execute()
                );
                return;
            case DialogsFactory.LOCALE_DIALOG_COUNTRY:
                final DialogLocale countryLocale = DialogLocales.obtainLocale((int) selectedCountryId);
                controller.newRequest(id).options(new LocalePickerDialog.LocaleOptions()
                        .selection(selectedCountryId)
                        .searchQuery(countryLocale != null ? countryLocale.countryName : null)
                ).execute();
                return;
            case DialogsFactory.LOCALE_DIALOG_LANGUAGE:
                controller.newRequest(id).options(new LocalePickerDialog.LocaleOptions()
                        .selection(selectedLanguageId)
                ).execute();
                return;
            case DialogsFactory.ITEM_DIALOG_SINGLE_SELECTION:
                controller.newRequest(id).options(new SelectionDialog.SelectionOptions().selection(new long[]{selectedGroupId})).execute();
                return;
            case DialogsFactory.COLOR_DIALOG:
                controller.newRequest(id).options(new ColorPickerDialog.ColorOptions().color(pickedColor)).execute();
                return;
        }
        super.onDialogSampleClick(id, controller);
    }

    @SuppressWarnings("ResourceType")
    @Override public boolean onDialogButtonClick(@NonNull final Dialog dialog, final int button) {
        switch (dialog.getDialogId()) {
            case DialogsFactory.DIALOG:
                if (button == Dialog.BUTTON_INFO) {
                    Toaster.showToast(getActivity(), "You can view all your payments in your account.");
                    return true;
                }
                break;
            case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR:
                if (button == Dialog.BUTTON_NEUTRAL) {
                    cancelProgressTask();
                    return true;
                }
                break;
            case DialogsFactory.EDIT_DIALOG:
                if (button == Dialog.BUTTON_POSITIVE) {
                    final EditDialog editDialog = (EditDialog) dialog;
                    // Simple input check.
                    if ("android".equals(editDialog.getEditableInput().toString())) {
                        editDialog.dismiss();
                        Toast.makeText(getActivity(), "New username saved", Toast.LENGTH_SHORT).show();
                    } else {
                        editDialog.setError("Not allowed (try 'android')");
                    }
                    return true;
                }
                break;
            case DialogsFactory.SIGN_IN_DIALOG:
                if (button == Dialog.BUTTON_POSITIVE) {
                    dialog.setIndeterminateProgressBarVisible(true);
                    dialog.setButtonEnabled(SimpleDialog.BUTTON_POSITIVE, false);
                    executeSignInTask(((SignInDialog) dialog).getCredentials());
                    return true;
                }
                break;
            case DialogsFactory.TIME_DIALOG:
                if (button == Dialog.BUTTON_POSITIVE) {
                    calendar.setTimeInMillis(((TimePickerDialog) dialog).getTime());
                    Toaster.showToast(
                            getActivity(),
                            "Picked time => " + calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) +
                                    " " + (calendar.get(Calendar.AM_PM) == Calendar.AM ? "am" : "pm")
                    );
                }
                return true;
            case DialogsFactory.TIME_DIALOG_WITH_ONLY_MINUTES:
                if (button == Dialog.BUTTON_POSITIVE) {
                    calendar.setTimeInMillis(((TimePickerDialog) dialog).getTime());
                    Toaster.showToast(
                            getActivity(),
                            "Picked minutes => " + calendar.get(Calendar.MINUTE)
                    );
                }
                return true;
            case DialogsFactory.DATE_DIALOG:
                if (button == Dialog.BUTTON_POSITIVE) {
                    calendar.setTimeInMillis(((DatePickerDialog) dialog).getDate());
                    Toaster.showToast(
                            getActivity(),
                            "Picked date => " + calendar.get(Calendar.YEAR) + "." + (calendar.get(Calendar.MONTH) + 1) +
                                    "." + calendar.get(Calendar.DAY_OF_MONTH)
                    );
                }
                return true;
            case DialogsFactory.ITEM_DIALOG_MULTI_SELECTION:
                final SelectionDialog selectionDialog = (SelectionDialog) dialog;
                Toaster.showToast(getActivity(), "Selected => " + Arrays.toString(selectionDialog.getSelectedItems().toArray()));
                return true;
            case DialogsFactory.COLOR_DIALOG:
                if (button == Dialog.BUTTON_POSITIVE) {
                    this.pickedColor = ((ColorPickerDialog) dialog).getColor();
                    Toaster.showToast(getActivity(), "Picked color => " + Colors.hexName(pickedColor));
                }
                return true;
        }
        return super.onDialogButtonClick(dialog, button);
    }

    @Override public boolean onDialogSignInError(@NonNull final SignInDialog dialog, final int error) {
        // There is only one sign in dialog, no id check needed.
        switch (error) {
            case SignInDialog.ERROR_INVALID_PASSWORD:
                dialog.setPasswordError("too short");
                break;
            case SignInDialog.ERROR_INVALID_USER_NAME:
                dialog.setUsernameError("invalid e-mail");
                break;
        }
        return true;
    }

    @Override public void onAuthenticationServiceSuccess() {
        this.dialogController.newRequest(DialogsFactory.SIGN_IN_DIALOG).intent(DialogRequest.DISMISS).execute();
        final Activity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, "Successfully signed in", Toast.LENGTH_SHORT).show();
        }
    }

    @Override public void onAuthenticationServiceError(final int error) {
        final SignInDialog signInDialog = (SignInDialog) dialogController.findDialogFragmentByFactoryId(DialogsFactory.SIGN_IN_DIALOG);
        if (signInDialog != null) {
            signInDialog.setIndeterminateProgressBarVisible(false);
            signInDialog.setButtonEnabled(SimpleDialog.BUTTON_POSITIVE, true);
            switch (error) {
                case SignInTask.ERROR_WRONG_USERNAME:
                    signInDialog.setUsernameError("Wrong username");
                    break;
                case SignInTask.ERROR_WRONG_PASSWORD:
                    signInDialog.setPasswordError("Wrong password");
                    break;
            }
        }
    }

    @Override public boolean onDialogCancelled(@NonNull final Dialog dialog) {
        switch (dialog.getDialogId()) {
            case DialogsFactory.LOADING_DIALOG_WITH_PROGRESS_INDICATOR:
            case DialogsFactory.PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR:
            case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR:
            case DialogsFactory.PROGRESS_DIALOG_WITH_TIME_INDICATOR:
            case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR:
                cancelProgressTask();
                return true;
        }
        return super.onDialogCancelled(dialog);
    }

    @Override protected void onCheckRestoredDialogs() {
        this.dialogController.checkRestoredDialogs(
		        DialogsFactory.LOADING_DIALOG_WITH_PROGRESS_INDICATOR,
		        DialogsFactory.PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR,
		        DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR,
		        DialogsFactory.PROGRESS_DIALOG_WITH_TIME_INDICATOR,
		        DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR
        );
    }

    @Override public void onDialogRestored(@NonNull final DialogFragment dialog) {
        super.onDialogRestored(dialog);
        if (dialog instanceof UniqueDialog) {
            switch (((UniqueDialog) dialog).getDialogId()) {
                case DialogsFactory.LOADING_DIALOG_WITH_PROGRESS_INDICATOR:
                case DialogsFactory.PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR:
                case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR:
                case DialogsFactory.PROGRESS_DIALOG_WITH_TIME_INDICATOR:
                case DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR:
                    reattachProgressDialogToTask((ProgressBarDialog) dialog);
                    break;
            }
        }
    }
}