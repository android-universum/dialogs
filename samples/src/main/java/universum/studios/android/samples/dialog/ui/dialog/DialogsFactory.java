/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.dialog;

import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.AlertDialog;
import universum.studios.android.dialog.ColorPickerDialog;
import universum.studios.android.dialog.DatePickerDialog;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.EditDialog;
import universum.studios.android.dialog.ErrorDialog;
import universum.studios.android.dialog.GridDialog;
import universum.studios.android.dialog.InfoDialog;
import universum.studios.android.dialog.IntentDialog;
import universum.studios.android.dialog.ListDialog;
import universum.studios.android.dialog.LoadingDialog;
import universum.studios.android.dialog.LocalePickerDialog;
import universum.studios.android.dialog.ProgressDialog;
import universum.studios.android.dialog.SelectionDialog;
import universum.studios.android.dialog.SignInDialog;
import universum.studios.android.dialog.SimpleDialog;
import universum.studios.android.dialog.TimePickerDialog;
import universum.studios.android.dialog.WebDialog;
import universum.studios.android.dialog.adapter.DialogSelectionAdapter;
import universum.studios.android.dialog.annotation.FactoryDialog;
import universum.studios.android.dialog.manage.BaseDialogFactory;
import universum.studios.android.dialog.view.DialogButtonsView;
import universum.studios.android.dialog.view.InputConfig;
import universum.studios.android.dialog.view.ProgressDialogView;
import universum.studios.android.dialog.view.TimePickerDialogView;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.async.ProgressDialogTask;
import universum.studios.android.samples.dialog.model.SampleGroup;
import universum.studios.android.ui.widget.EditLayout;

/**
 * @author Martin Albedinsky
 */
public final class DialogsFactory extends BaseDialogFactory {

	@FactoryDialog public static final int DIALOG_TEST = 0;
	@FactoryDialog public static final int DIALOG = 0xa0001;
	@FactoryDialog public static final int DIALOG_WITHOUT_TITLE = 0xa0002;
	@FactoryDialog public static final int DIALOG_WITHOUT_BUTTONS = 0xa0003;
	@FactoryDialog public static final int DIALOG_ONLY_MESSAGE = 0xa0004;
	@FactoryDialog public static final int INFO_DIALOG = 0xa0005;
	@FactoryDialog public static final int ALERT_DIALOG = 0xa0006;
	@FactoryDialog public static final int ERROR_DIALOG = 0xa0007;
	@FactoryDialog public static final int LOADING_DIALOG = 0xa1001;
	@FactoryDialog public static final int LOADING_DIALOG_WITH_PROGRESS_INDICATOR = 0xa1002;
	@FactoryDialog public static final int PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR = 0xa2001;
	@FactoryDialog public static final int PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR = 0xa2002;
	@FactoryDialog public static final int PROGRESS_DIALOG_WITH_TIME_INDICATOR = 0xa2003;
	@FactoryDialog public static final int PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR = 0xa2004;
	@FactoryDialog public static final int WEB_DIALOG_WITH_URL = 0xa3001;
	@FactoryDialog public static final int WEB_DIALOG_WITH_HTML = 0xa3002;
	@FactoryDialog public static final int LIST_DIALOG = 0xa4001;
	@FactoryDialog public static final int GRID_DIALOG = 0xa5001;
	@FactoryDialog public static final int INTENT_LIST_DIALOG = 0xa6001;
	@FactoryDialog public static final int INTENT_GRID_DIALOG = 0xa6002;
	@FactoryDialog public static final int TIME_DIALOG = 0xa7001;
	@FactoryDialog public static final int TIME_DIALOG_WITH_ONLY_MINUTES = 0xa7002;
	@FactoryDialog public static final int DATE_DIALOG = 0xa7003;
	@FactoryDialog public static final int COLOR_DIALOG = 0xa8001;
	@FactoryDialog public static final int LOCALE_DIALOG_COUNTRY = 0xa9001;
	@FactoryDialog public static final int LOCALE_DIALOG_LANGUAGE = 0xa9002;
	@FactoryDialog public static final int ITEM_DIALOG_SINGLE_SELECTION = 0xaa001;
	@FactoryDialog public static final int ITEM_DIALOG_MULTI_SELECTION = 0x0aa002;
	@FactoryDialog public static final int EDIT_DIALOG = 0xab001;
	@FactoryDialog public static final int SIGN_IN_DIALOG = 0xac001;

	public DialogsFactory(@NonNull final Context context) {
		super(context);
	}

	@Override
	protected DialogFragment onCreateDialog(final int dialogId, @Nullable DialogOptions options) {
		if (options == null) {
			options = new DialogOptions();
		}
		switch (dialogId) {
			case DIALOG_TEST:
				return SimpleDialog.newInstance(options
						.title("Invalid email or password or whatever is just wrong, completely wrong")
						.content("Try again.")
						.neutralButton("OK")
				);
			case DIALOG:
				return SimpleDialog.newInstance(options
						.title("Payment")
						.content("Your payment was <b>successfully</b> processed.")
						.contentFlag(DialogOptions.CONTENT_AS_HTML)
						.neutralButton("OK")
						.infoButton("Help")
				);
			case DIALOG_WITHOUT_TITLE:
				return SimpleDialog.newInstance(options
						.content("Operation can not be performed. Please check You Internet connection and try again.")
						.neutralButton("OK")
				);
			case DIALOG_WITHOUT_BUTTONS:
				return SimpleDialog.newInstance(options
						.title("Insufficient Funds")
						.content("There isn't enough funds at Your bank account."));
			case DIALOG_ONLY_MESSAGE:
				options.content("You have received new messages.");
				return SimpleDialog.newInstance(options);
			case INFO_DIALOG:
				return InfoDialog.newInstance(options
						.title("Connection")
						.content("Internet connection appears to be offline.")
						.neutralButton("OK")
				);
			case ALERT_DIALOG:
				return AlertDialog.newInstance(options
						.title("Low Battery")
						.content("Battery is getting down. Would You like to proceed.")
						.positiveButton("Proceed")
						.negativeButton("Terminate")
				);
			case ERROR_DIALOG:
				return ErrorDialog.newInstance(options
						.title("Error")
						.content("Unknown server errorDialog occurs. Please try again later.")
						.neutralButton("Dismiss")
						.buttonsWidthMode(DialogButtonsView.WIDTH_MODE_MATCH_PARENT)
				);
			case LOADING_DIALOG:
				return LoadingDialog.newInstance(new LoadingDialog.LoadingOptions()
						.content("Resolving dependencies ...")
				);
			case LOADING_DIALOG_WITH_PROGRESS_INDICATOR:
				return LoadingDialog.newInstance(new LoadingDialog.LoadingOptions()
						.content("Collecting data ...")
						.showProgressIndicator(true)
				);
			case PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR:
				return ProgressDialog.newInstance(new ProgressDialog.ProgressOptions()
						.content("Updating database")
						.progressIndicator(ProgressDialogView.INDICATOR_NONE)
				);
			case PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR:
				// Progress dialog style has as default set indicator to PROGRESS.
				return ProgressDialog.newInstance(new ProgressDialog.ProgressOptions()
						.vectorIcon(R.drawable.vc_upload)
						.title("Upload")
						.content("Uploading data ...")
						.neutralButton("Cancel")
						.progressIndicator(ProgressDialogView.INDICATOR_PROGRESS)
				);
			case PROGRESS_DIALOG_WITH_TIME_INDICATOR:
				return ProgressDialog.newInstance(new ProgressDialog.ProgressOptions()
						.title("Photo").content("Sending photo to server. Please wait until process finishes.")
						.progressIndicator(ProgressDialogView.INDICATOR_TIME)
				);
			case PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR:
				return ProgressDialog.newInstance(new ProgressDialog.ProgressOptions()
						.title("Download").content("Downloading images ...")
						.vectorIcon(R.drawable.vc_download)
						// There is fixed count of items set in the async task to download. But this value
						// will be in most cases dynamic, but fortunately, can be passed here in the bundle
						// data of dialog options from the activity context or directly from async task if
						// that async task is responsible for showing of a ProgressDialog.
						.progressFormat("%d/" + ProgressDialogTask.ITEMS_TO_DOWNLOAD + " [%d %%]")
				);
			case WEB_DIALOG_WITH_URL:
				return WebDialog.newInstance(new WebDialog.WebOptions(mResources)
						.title("Android Development")
						.content("http://developer.android.com")
						.neutralButton("Hide")
				);
			case WEB_DIALOG_WITH_HTML:
				final String html = "<body>" +
						"<h3 style=\"color: #4fc652;\">Android Development</h3>" +
						"<p>There are a few good pages which can help you when you are developing an applications for Android:</p>" +
						"<ul>" +
						"<li><a href=\"http://developer.android.com/\">developer.android.com</a> <b>(the most important)</b></li>" +
						"<li><a href=\"http://stackoverflow.com/\">stackoverflow.com</a> (some times it is very hard to find what you are looking for :), but there are a bunch of good examples)</li>" +
						"<li><a href=\"https://github.com\">github.com</a> (a lot of open source libraries)</li>" +
						"</ul>" +
						"<p>There is also a lot of good forums with lot of source code examples, just keep searching for them.</p>" +
						"</body>";
				return WebDialog.newInstance(new WebDialog.WebOptions(mResources)
						.title("Useful Android Pages")
						.content(html)
						.neutralButton("Hide")
				);
			case LIST_DIALOG:
				return ListDialog.newInstance(new ListDialog.ListOptions()
						.title("Users")
						.content("No users available.")
						.neutralButton("Hide")
				);
			case GRID_DIALOG:
				return GridDialog.newInstance(new GridDialog.GridOptions()
						.title("Images")
						.content("No images available.")
				);
			case INTENT_LIST_DIALOG:
				return IntentDialog.newInstance(new IntentDialog.IntentOptions()
						.intent(
								new Intent(Intent.ACTION_SEND)
										.setType("image/jpeg")
						)
						.title("Share image")
						.content("There are no applications to handle this action available.")
						.neutralButton("Cancel")
				);
			case INTENT_GRID_DIALOG:
				return IntentDialog.newInstance(new IntentDialog.IntentOptions()
						.title("Share via")
						.neutralButton("Cancel")
						.intent(new Intent(Intent.ACTION_SEND)
								.setType("text/plain")
								.putExtra(Intent.EXTRA_TITLE, "Sharing title")
								.putExtra(Intent.EXTRA_TEXT, "Sharing text")
						)
						.adapterViewType(IntentDialog.GRID)
				);
			case TIME_DIALOG:
				return TimePickerDialog.newInstance(new TimePickerDialog.TimeOptions()
						.time(TimePickerDialog.TimeOptions.newTime(10, 0, Calendar.AM))
						.negativeButton("Cancel")
						.positiveButton("OK")
				);
			case TIME_DIALOG_WITH_ONLY_MINUTES:
				return TimePickerDialog.newInstance(new TimePickerDialog.TimeOptions()
						.time(TimePickerDialog.TimeOptions.newTime(12, 30, Calendar.AM))
						.timePickers(TimePickerDialogView.PICKER_MINUTE)
						.timeQuantityText(R.plurals.dialog_plurals_minutes)
						.negativeButton("Cancel")
						.positiveButton("OK")
				);
			case DATE_DIALOG:
				return DatePickerDialog.newInstance(new DatePickerDialog.DateOptions()
						.minDate(DatePickerDialog.DateOptions.newDate(2010, Calendar.JANUARY, 1))
						.maxDate(DatePickerDialog.DateOptions.newDate(2030, Calendar.DECEMBER, 31))
						.negativeButton("Cancel")
						.positiveButton("OK")
				);
			case COLOR_DIALOG:
				final ColorPickerDialog.ColorOptions colorOptions = (ColorPickerDialog.ColorOptions) options;
				return ColorPickerDialog.newInstance(colorOptions
						.negativeButton("Cancel")
						.positiveButton("Done")
				);
			case LOCALE_DIALOG_COUNTRY:
				final LocalePickerDialog.LocaleOptions countryOptions = (LocalePickerDialog.LocaleOptions) options;
				return LocalePickerDialog.newInstance(countryOptions
						.mode(LocalePickerDialog.MODE_COUNTRY)
						.searchHint("Search countries")
						.content("No country found for the specified search text.")
				);
			case LOCALE_DIALOG_LANGUAGE:
				final LocalePickerDialog.LocaleOptions languageOptions = (LocalePickerDialog.LocaleOptions) options;
				return LocalePickerDialog.newInstance(languageOptions
						.mode(LocalePickerDialog.MODE_LANGUAGE)
						.searchHint("Search languages")
						.content("No language found for the specified search text.")
				);
			case ITEM_DIALOG_SINGLE_SELECTION:
				final SelectionDialog.SelectionOptions itemOptions = (SelectionDialog.SelectionOptions) options;
				return SelectionDialog.newInstance(itemOptions
						.emptySelectionAllowed(false)
						.items(new SampleGroup[]{
								new SampleGroup(0x01, "Musician"),
								new SampleGroup(0x02, "Athlete"),
								new SampleGroup(0x03, "Doctor"),
								new SampleGroup(0x04, "Programmer")
						})
				);
			case ITEM_DIALOG_MULTI_SELECTION:
				return SelectionDialog.newInstance(new SelectionDialog.SelectionOptions(mResources)
						.title("Shopping checklist")
						.selectionMode(DialogSelectionAdapter.MULTIPLE)
						.items(R.array.dialog_items_shopping_checklist)
						.neutralButton("Done")
				);
			case EDIT_DIALOG:
				return EditDialog.newInstance(new EditDialog.EditOptions(mResources)
						.title("Edit Username")
						.hint("New username")
						.inputConfig(new InputConfig()
								.lengthConstraint(10)
								.inputFeatures(EditLayout.FEATURE_LABEL |
										EditLayout.FEATURE_NOTE |
										EditLayout.FEATURE_CONSTRAINT)
						)
						.remain(true)
						.negativeButton("Do not want to change")
						.positiveButton("Update username")
				);
			case SIGN_IN_DIALOG:
				return SignInDialog.newInstance(new SignInDialog.SignInOptions()
						.title("Authentication")
						.negativeButton("Cancel")
						.positiveButton("SignIn")
						.usernameHint("e-mail address")
						.usernameInputConfig(new InputConfig()
								.inputFeatures(
										EditLayout.FEATURE_LABEL |
												EditLayout.FEATURE_NOTE
								)
						)
						.passwordRegExp("^.{4}(.)*$")
						.passwordInputConfig(new InputConfig()
								.maxLength(10)
								.inputFeatures(
										EditLayout.FEATURE_LABEL |
												EditLayout.FEATURE_NOTE |
												EditLayout.FEATURE_CONSTRAINT
								)
						)
				);
			default:
				return null;
		}
	}
}