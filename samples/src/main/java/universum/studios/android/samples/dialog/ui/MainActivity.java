/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import universum.studios.android.fragment.annotation.FragmentAnnotations;
import universum.studios.android.fragment.manage.FragmentController;
import universum.studios.android.fragment.manage.FragmentRequest;
import universum.studios.android.fragment.manage.FragmentRequestInterceptor;
import universum.studios.android.fragment.transition.FragmentTransitions;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.ui.fragment.FragmentsFactory;
import universum.studios.android.samples.ui.SamplesMainFragment;
import universum.studios.android.samples.ui.SamplesNavigationActivity;

/**
 * @author Martin Albedinsky
 */
public final class MainActivity extends SamplesNavigationActivity implements FragmentRequestInterceptor {

	private static final String EXTRA_THEME_ID = MainActivity.class.getName() + ".EXTRA.ThemeId";

	static {
		FragmentAnnotations.setEnabled(true);
	}

	private FragmentController fragmentController;

	public static void launch(@NonNull final Activity caller, @NonNull final DialogTheme theme) {
		final Intent intent = new Intent(caller, MainActivity.class);
		intent.putExtra(EXTRA_THEME_ID, theme.ordinal());
		caller.startActivity(intent);
		caller.overridePendingTransition(R.anim.window_fade_in, R.anim.window_fade_out);
		caller.finish();
	}

	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		final Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey(EXTRA_THEME_ID)) {
			setTheme(DialogTheme.values()[extras.getInt(EXTRA_THEME_ID)].themeRes);
		} else {
			final DialogTheme dialogTheme = DialogTheme.obtainFromAppPreferences(this);
			if (dialogTheme != null) {
				setTheme(dialogTheme.themeRes);
			}
		}
		super.onCreate(savedInstanceState);
		this.fragmentController = FragmentController.create(this);
		this.fragmentController.setFactory(new FragmentsFactory());
		this.fragmentController.setViewContainerId(R.id.samples_container);
	}

	@Override public boolean onCreateOptionsMenu(@NonNull final Menu menu) {
		DialogTheme.populateMenu(menu, getResources());
		return super.onCreateOptionsMenu(menu);
	}

	@Override public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
		final int itemId = item.getItemId();
		boolean processed = false;
		switch (itemId) {
			case R.id.menu_theme_default:
			case R.id.menu_theme_default_light:
			case R.id.menu_theme_android:
			case R.id.menu_theme_lime:
			case R.id.menu_theme_sunrise:
			case R.id.menu_theme_rose:
				if (!DialogTheme.isSelected(itemId)) {
					DialogTheme.saveIntoAppPreferences(this, itemId);
					launch(this, DialogTheme.resolveInstance(itemId));
				}
				processed = true;
				break;
		}
		return processed || super.onOptionsItemSelected(item);
	}

	@Override @Nullable public Fragment interceptFragmentRequest(@NonNull final FragmentRequest request) {
		request.transition(FragmentTransitions.CROSS_FADE).replaceSame(true);
		return null;
	}

	@Override protected boolean onHandleNavigationItemSelected(@NonNull final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.navigation_item_home:
				fragmentController.newRequest(new SamplesMainFragment()).execute();
				return true;
			case R.id.navigation_item_java:
				fragmentController.newRequest(FragmentsFactory.JAVA).execute();
				return true;
			case R.id.navigation_item_xml:
				fragmentController.newRequest(FragmentsFactory.XML).execute();
				return true;
			case R.id.navigation_item_xml_set:
				fragmentController.newRequest(FragmentsFactory.XML_SET).execute();
				return true;
			case R.id.navigation_item_string_array:
				fragmentController.newRequest(FragmentsFactory.STRING_ARRAY).execute();
				return true;
			default:
				return true;
		}
	}
}