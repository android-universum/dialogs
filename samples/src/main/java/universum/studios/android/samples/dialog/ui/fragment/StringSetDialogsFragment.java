/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.content.StringSetDialogs;
import universum.studios.android.samples.dialog.ui.dialog.DialogsStringSetFactory;

/**
 * @author Martin Albedinsky
 */
public final class StringSetDialogsFragment extends BaseDialogsFragment<StringSetDialogs> {

	@Override public void onCreate(@NonNull final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setDataSetProvider(new StringSetDialogs());
	}

	@Override protected void onSetUpDialogController(@NonNull final DialogController controller) {
		super.onSetUpDialogController(controller);
		controller.setFactory(new DialogsStringSetFactory(getActivity()));
	}

	@Override public void onResume() {
		super.onResume();
		getActivity().setTitle(R.string.navigation_item_string_array);
	}
}