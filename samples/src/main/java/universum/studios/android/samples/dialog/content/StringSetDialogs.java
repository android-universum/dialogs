/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.content;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.model.DialogSample;
import universum.studios.android.samples.dialog.ui.adapter.SamplesAdapter;
import universum.studios.android.samples.dialog.ui.dialog.DialogsStringSetFactory;

/**
 * @author Martin Albedinsky
 */
public final class StringSetDialogs extends DataSetProvider {

	@Override protected void onBindDialogsAdapter(@NonNull final SamplesAdapter adapter) {
		final List<DialogSample> items = new ArrayList<>();
		items.add(DialogSample.create(
				DialogsStringSetFactory.Sample.DIALOG_1.id,
				R.string.string_options_set_dialog_1_label,
				R.string.string_options_set_dialog_1_desc
		));
		items.add(DialogSample.create(
				DialogsStringSetFactory.Sample.DIALOG_2.id,
				R.string.string_options_set_dialog_2_label,
				R.string.string_options_set_dialog_2_desc
		));
		items.add(DialogSample.create(
				DialogsStringSetFactory.Sample.DIALOG_3.id,
				R.string.string_options_set_dialog_3_label,
				R.string.string_options_set_dialog_3_desc
		));
		items.add(DialogSample.create(
				DialogsStringSetFactory.Sample.DIALOG_4.id,
				R.string.string_options_set_dialog_4_label,
				R.string.string_options_set_dialog_4_desc
		));
		items.add(DialogSample.create(
				DialogsStringSetFactory.Sample.DIALOG_5.id,
				R.string.string_options_set_dialog_5_label,
				R.string.string_options_set_dialog_5_desc
		));
		items.add(DialogSample.create(
				DialogsStringSetFactory.Sample.DIALOG_6.id,
				R.string.string_options_set_dialog_6_label,
				R.string.string_options_set_dialog_6_desc
		));
		adapter.changeItems(items);
	}
}