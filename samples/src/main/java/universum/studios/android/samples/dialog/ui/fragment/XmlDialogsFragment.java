/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.AdapterDialog;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.ProgressDialog;
import universum.studios.android.dialog.UniqueDialog;
import universum.studios.android.dialog.manage.DialogController;
import universum.studios.android.dialog.manage.DialogXmlFactory;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.async.ProgressDialogTask;
import universum.studios.android.samples.dialog.content.XmlDialogs;
import universum.studios.android.util.Toaster;

/**
 * @author Martin Albedinsky
 */
public final class XmlDialogsFragment extends BaseDialogsFragment<XmlDialogs> implements AdapterDialog.OnItemClickListener {

	@Override public void onCreate(@NonNull final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setDataSetProvider(new XmlDialogs());
	}

	@Override public void onResume() {
		super.onResume();
		getActivity().setTitle(R.string.navigation_item_xml);
	}

	@Override protected void onDialogSampleClick(final int id, @NonNull final DialogController controller) {
		switch (id) {
			case R.xml.dialog_info:
				this.dialogController.newRequest(id).options(new DialogOptions().content(
						"Contact with name 'Thomas' was successfully added into your contact list."
				)).execute();
				return;
			case R.xml.dialog_progress:
			case R.xml.dialog_progress_progress_and_time:
				executeProgressTask(
						new ProgressDialogTask(),
						(ProgressDialog) dialogController.newRequest(id).execute()
				);
				return;
		}
		super.onDialogSampleClick(id, controller);
	}

	@Override protected void onSetUpDialogController(@NonNull final DialogController controller) {
		super.onSetUpDialogController(controller);
		// Here we use directly factory from library (not custom implementation) only to use it as a
		// "middle men" to inflate requested XML dialog from its xml file.
		final DialogXmlFactory factory = new DialogXmlFactory(getActivity());
		controller.setFactory(factory);
		// Note, that XML dialogs can be inflated directly via:
		// new DialogInflater(getActivity()).inflate(int, DialogOptions);
	}

	@Override protected void onCheckRestoredDialogs() {
		this.dialogController.checkRestoredDialogs(new int[] {
				R.xml.dialog_progress,
				R.xml.dialog_progress_progress_and_time
		});
	}

	@Override public void onDialogRestored(@NonNull final DialogFragment dialog) {
		super.onDialogRestored(dialog);
		if (dialog instanceof UniqueDialog) {
			switch (((UniqueDialog) dialog).getDialogId()) {
				case R.xml.dialog_progress:
				case R.xml.dialog_progress_progress_and_time:
					reattachProgressDialogToTask((ProgressDialog) dialog);
					break;
			}
		}
	}

	@Override public boolean onDialogCancelled(@NonNull final Dialog dialog) {
		switch (dialog.getDialogId()) {
			case R.xml.dialog_progress:
			case R.xml.dialog_progress_progress_and_time:
				cancelProgressTask();
				return true;
		}
		return super.onDialogCancelled(dialog);
	}

	@Override public boolean onDialogItemClick(@NonNull final AdapterDialog dialog, @NonNull final AdapterView<?> adapterView, @NonNull final View view, final int position, final long id) {
		switch (dialog.getDialogId()) {
			case R.xml.dialog_selection:
				Toaster.showToast(getActivity(), "Selected => " + adapterView.getAdapter().getItem(position));
				dialog.dismiss();
				return true;
		}
		return false;
	}
}