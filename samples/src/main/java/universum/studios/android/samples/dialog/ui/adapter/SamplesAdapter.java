/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.databinding.ItemListSampleBinding;
import universum.studios.android.samples.dialog.model.DialogSample;
import universum.studios.android.samples.dialog.ui.DialogTheme;
import universum.studios.android.widget.adapter.SimpleListAdapter;
import universum.studios.android.widget.adapter.holder.ViewHolder;

/**
 * @author Martin Albedinsky
 */
public final class SamplesAdapter extends SimpleListAdapter<SamplesAdapter, SamplesAdapter.ItemHolder, DialogSample> {

	private final DialogTheme theme;

	public SamplesAdapter(@NonNull final Context context) {
		super(context);
		this.theme = DialogTheme.obtainFromAppPreferences(context);
	}

	@Override public long getItemId(final int position) {
		return getItem(position).id;
	}

	@Override @NonNull protected ItemHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
		final ItemHolder holder = new ItemHolder(inflateView(R.layout.item_list_sample, parent));
		holder.binding.setTheme(theme);
		return holder;
	}

	@Override protected void onBindViewHolder(@NonNull final ItemHolder holder, final int position) {
		holder.binding.setSample(getItem(position));
		holder.binding.executePendingBindings();
	}

	final class ItemHolder extends ViewHolder {

		final ItemListSampleBinding binding;

		ItemHolder(@NonNull final View itemView) {
			super(itemView);
			this.binding = ItemListSampleBinding.bind(itemView);
		}
	}
}