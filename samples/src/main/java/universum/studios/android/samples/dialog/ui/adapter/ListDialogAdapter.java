/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import universum.studios.android.dialog.adapter.DialogBaseAdapter;
import universum.studios.android.samples.dialog.R;

/**
 * @author Martin Albedinsky
 */
public final class ListDialogAdapter extends DialogBaseAdapter<String, TextView> {

	public ListDialogAdapter(@NonNull final Context context, @NonNull final LayoutInflater layoutInflater) {
		super(context, layoutInflater, new String[] {
				"Thomas Carter",
				"Cate Done",
				"Jeffry Hold",
				"Daniel Bird",
				"John Lock",
				"Amy McDonald",
				"Jack O'Neill",
				"Olga Kurylenko",
				"Robert Junior",
				"John Lewrance",
				"Cate Blanchett"
		});
	}

	@Override @NonNull protected View onCreateView(@NonNull final ViewGroup parent, final int position) {
		return inflate(R.layout.item_list_dialog, parent);
	}

	@Override protected void onBindViewHolder(@NonNull final TextView view, final int position) {
		view.setText(getItem(position));
	}
}