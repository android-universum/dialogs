/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.content;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.samples.dialog.R;
import universum.studios.android.samples.dialog.model.DialogSample;
import universum.studios.android.samples.dialog.ui.adapter.SamplesAdapter;
import universum.studios.android.samples.dialog.ui.dialog.DialogsFactory;

/**
 * @author Martin Albedinsky
 */
public final class JavaDialogs extends DataSetProvider {

	@Override protected void onBindDialogsAdapter(@NonNull final SamplesAdapter adapter) {
		final List<DialogSample> items = new ArrayList<>();
		// Simple dialogs ==========================================================================
		items.add(DialogSample.create(
				DialogsFactory.DIALOG_TEST,
				R.string.java_dialog_test_label,
				R.string.java_dialog_test_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.DIALOG,
				R.string.java_dialog_label,
				R.string.java_dialog_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.DIALOG_WITHOUT_BUTTONS,
				R.string.java_dialog_without_buttons_label,
				R.string.java_dialog_without_buttons_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.DIALOG_WITHOUT_TITLE,
				R.string.java_dialog_without_title_label,
				R.string.java_dialog_without_title_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.DIALOG_ONLY_MESSAGE,
				R.string.java_dialog_without_title_and_buttons_label,
				R.string.java_dialog_without_title_and_buttons_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.INFO_DIALOG,
				R.string.java_info_dialog_label,
				R.string.java_info_dialog_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.ALERT_DIALOG,
				R.string.java_alert_dialog_label,
				R.string.java_alert_dialog_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.ERROR_DIALOG,
				R.string.java_error_dialog_label,
				R.string.java_error_dialog_desc
		));
		// Loading dialogs =========================================================================
		items.add(DialogSample.create(
				DialogsFactory.LOADING_DIALOG,
				R.string.java_loading_dialog_without_indicator_label,
				R.string.java_loading_dialog_without_indicator_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.LOADING_DIALOG_WITH_PROGRESS_INDICATOR,
				R.string.java_loading_dialog_label,
				R.string.java_loading_dialog_desc
		));
		// Progress dialogs ========================================================================
		items.add(DialogSample.create(
				DialogsFactory.PROGRESS_DIALOG_WITHOUT_ANY_INDICATOR,
				R.string.java_progress_dialog_label,
				R.string.java_progress_dialog_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_INDICATOR,
				R.string.java_progress_dialog_progress_indicator_label,
				R.string.java_progress_dialog_progress_indicator_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.PROGRESS_DIALOG_WITH_TIME_INDICATOR,
				R.string.java_progress_dialog_time_indicator_label,
				R.string.java_progress_dialog_time_indicator_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.PROGRESS_DIALOG_WITH_PROGRESS_AND_TIME_INDICATOR,
				R.string.java_progress_dialog_progress_and_time_indicator_label,
				R.string.java_progress_dialog_progress_and_time_indicator_desc
		));
		// Web dialogs =============================================================================
		items.add(DialogSample.create(
				DialogsFactory.WEB_DIALOG_WITH_URL,
				R.string.java_web_dialog_url_label,
				R.string.java_web_dialog_url_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.WEB_DIALOG_WITH_HTML,
				R.string.java_web_dialog_html_label,
				R.string.java_web_dialog_html_desc
		));
		// Adapter dialogs =========================================================================
		items.add(DialogSample.create(
				DialogsFactory.LIST_DIALOG,
				R.string.java_list_dialog_label,
				R.string.java_list_dialog_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.GRID_DIALOG,
				R.string.java_grid_dialog_label,
				R.string.java_grid_dialog_desc
		));
		// Intent dialogs ==========================================================================
		items.add(DialogSample.create(
				DialogsFactory.INTENT_GRID_DIALOG,
				R.string.java_intent_dialog_grid_label,
				R.string.java_intent_dialog_grid_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.INTENT_LIST_DIALOG,
				R.string.java_intent_dialog_list_label,
				R.string.java_intent_dialog_list_desc
		));
		// Date/Time dialogs =======================================================================
		items.add(DialogSample.create(
				DialogsFactory.TIME_DIALOG,
				R.string.java_time_dialog_label,
				R.string.java_time_dialog_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.TIME_DIALOG_WITH_ONLY_MINUTES,
				R.string.java_time_dialog_minute_label,
				R.string.java_time_dialog_minute_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.DATE_DIALOG,
				R.string.java_date_dialog_label,
				R.string.java_date_dialog_desc
		));
		// Color dialogs ===========================================================================
		items.add(DialogSample.create(
				DialogsFactory.COLOR_DIALOG,
				R.string.java_color_dialog_label,
				R.string.java_color_dialog_desc
		));
		// Locale dialogs ==========================================================================
		items.add(DialogSample.create(
				DialogsFactory.LOCALE_DIALOG_COUNTRY,
				R.string.java_locale_dialog_country_label,
				R.string.java_locale_dialog_country_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.LOCALE_DIALOG_LANGUAGE,
				R.string.java_locale_dialog_language_label,
				R.string.java_locale_dialog_language_desc
		));
		// Selection dialogs =======================================================================
		items.add(DialogSample.create(
				DialogsFactory.ITEM_DIALOG_SINGLE_SELECTION,
				R.string.java_selection_dialog_single_label,
				R.string.java_selection_dialog_single_desc
		));
		items.add(DialogSample.create(
				DialogsFactory.ITEM_DIALOG_MULTI_SELECTION,
				R.string.java_selection_dialog_multi_label,
				R.string.java_selection_dialog_multi_desc
		));
		// Edit dialogs ============================================================================
		items.add(DialogSample.create(
				DialogsFactory.EDIT_DIALOG,
				R.string.java_edit_dialog_label,
				R.string.java_edit_dialog_desc
		));
		// SignIn dialogs ==========================================================================
		items.add(DialogSample.create(
				DialogsFactory.SIGN_IN_DIALOG,
				R.string.java_sing_in_dialog_label,
				R.string.java_sing_in_dialog_desc
		));
		adapter.changeItems(items);
	}
}