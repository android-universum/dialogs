/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.dialog.async;

import androidx.annotation.NonNull;
import universum.studios.android.dialog.ProgressBarDialog;

/**
 * @author Martin Albedinsky
 */
public interface ProgressTask {

    void execute();

    void cancel();

    void attachProgressDialog(@NonNull ProgressBarDialog progressDialog);

    void registerCallback(@NonNull Callback callback);

    void unregisterCallback(@NonNull Callback callback);

    void setPublishCountProgress(boolean show);

    interface Callback {

        void onProgressTaskFinished(int taskId, @NonNull ProgressBarDialog progressDialog);

        void onProgressTaskCancelled(int taskId, @NonNull ProgressBarDialog progressDialog);
    }
}