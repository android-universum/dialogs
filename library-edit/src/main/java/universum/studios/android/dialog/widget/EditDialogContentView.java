/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.DialogsConfig;
import universum.studios.android.dialog.EditDialog;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.EditDialogView;
import universum.studios.android.dialog.view.InputConfig;
import universum.studios.android.ui.widget.EditLayout;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link EditDialogView} implementation used by {@link EditDialog EditDialog} as content view.
 * <p>
 * This class is only extended version of {@link EditLayout} and provides all input features of such
 * a layout. The extended feature is that you can specify a configuration for the EditText  of this
 * edit layout via {@link #setInputConfig(InputConfig)}, where such a config will provide all necessary
 * configuration options for the EditText.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutEditContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link EditLayout} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@link R.attr#uiEditLayoutStyle uiEditLayoutStyle}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class EditDialogContentView extends EditLayout implements EditDialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "EditDialogContentView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Edit text for user input.
	 */
	private EditText mEditText;

	/**
	 * Configuration for edit input.
	 */
	private InputConfig mInputConfig;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #EditDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public EditDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #EditDialogContentView(Context, AttributeSet, int)} with
	 * {@link R.attr#uiEditLayoutStyle uiEditLayoutStyle} as attribute for default style.
	 */
	public EditDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.uiEditLayoutStyle);
	}

	/**
	 * Same as {@link #EditDialogContentView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public EditDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of EditDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public EditDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		if (theme.resolveAttribute(R.attr.dialogLayoutEditContent, typedValue, true)) {
			this.inflateHierarchy(context, typedValue.resourceId);
		}
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			onFinishInflate();
		}
	}

	/**
	 */
	@Override public void setInputConfig(@NonNull final InputConfig inputConfig) {
		this.mInputConfig = inputConfig;
		setNote(inputConfig.note);
		if (inputConfig.lengthConstraint >= 0) {
			setLengthConstraint(inputConfig.lengthConstraint);
		}
		if (inputConfig.maxLength >= 0) {
			setMaxLength(inputConfig.maxLength);
		}
		if (inputConfig.inputFeatures >= 0) {
			requestInputFeatures(inputConfig.inputFeatures);
		}
		if (inputConfig.inputType > 0) {
			setInputType(inputConfig.inputType);
		}
	}

	/**
	 */
	@Override public void setInput(@StringRes final int resId) {
		setInput(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setInput(@Nullable final CharSequence input) {
		setText(input);
	}

	/**
	 */
	@Override @NonNull public CharSequence getInput() {
		final Editable editable = getEditableInput();
		return editable != null ? editable.toString() : "";
	}

	/**
	 */
	@Override @Nullable public Editable getEditableInput() {
		return mEditText != null ? mEditText.getText() : SpannableStringBuilder.valueOf("");
	}

	/**
	 */
	@Override public void clearInput() {
		if (mEditText != null) mEditText.setText("");
	}

	/**
	 */
	@Override protected void onInputViewChanged(@NonNull final View inputView) {
		super.onInputViewChanged(inputView);
		this.mEditText = getInputView();
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.inputConfig = mInputConfig;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		if (savedState.inputConfig != null) {
			setInputConfig(savedState.inputConfig);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link EditDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		InputConfig inputConfig;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.inputConfig = source.readParcelable(DialogsConfig.class.getClassLoader());
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeParcelable(inputConfig, flags);
		}
	}
}