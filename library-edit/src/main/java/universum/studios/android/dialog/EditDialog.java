/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.util.DialogUtils;
import universum.studios.android.dialog.view.EditDialogView;
import universum.studios.android.dialog.view.InputConfig;

/**
 * A {@link SimpleDialog} implementation that can be used to allow to a user to input its desired
 * editable text into text field or edit some already saved editable. This dialog uses default
 * {@link EditDialogView} implementation as its content view which consists of one
 * {@link universum.studios.android.ui.widget.EditLayout EditLayout} for user's input.
 * <p>
 * The current editable value can be obtained via {@link #getEditableInput()} or cleared via {@link #clearInput()}.
 * If needed, errors can be specified via {@link #setError(CharSequence)} or its resource equivalent
 * {@link #setError(int)} and cleared via {@link #clearError()}.
 * <p>
 * See {@link EditOptions} for options that can be supplied to the EditDialog from the desired
 * context via {@link #newInstance(EditOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogEditOptions dialogEditOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class EditDialog extends SimpleDialog {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "EditDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Edit dialog view implementation.
	 */
	private EditDialogView mEditView;

	/**
	 * Boolean flag indicating whether this dialog is enabled or not.
	 */
	private boolean mEnabled;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of EditDialog with single {@link android.widget.EditText} within body
	 * view.
	 */
	public EditDialog() {
		super(R.attr.dialogEditOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of EditDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New EditDialog instance.
	 */
	@NonNull public static EditDialog newInstance(@NonNull final EditOptions options) {
		final EditDialog dialog = new EditDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new EditOptions(resources);
	}

	/**
	 */
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof EditOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Edit);
			final EditOptions options = (EditOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Edit_dialogHint) {
					if (!options.isSet(EditOptions.HINT)) {
						options.hint(attributes.getText(index));
					}
				} else if (index == R.styleable.Dialog_Options_Edit_dialogInputStyle) {
					if (!options.isSet(EditOptions.INPUT_CONFIG)) {
						options.inputStyle(attributes.getResourceId(index, -1));
					}
				} else if (index == R.styleable.Dialog_Options_Edit_dialogShowSoftKeyboard) {
					if (!options.isSet(EditOptions.SHOW_SOFT_KEYBOARD)) {
						options.showSoftKeyboard(attributes.getBoolean(index, true));
					}
				}
			}
			attributes.recycle();
		}
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_edit, container, false);
	}

	/**
	 */
	@Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
		if (contentView instanceof EditDialogView && options instanceof EditOptions) {
			final EditOptions editOptions = (EditOptions) options;
			final EditDialogView editView = (EditDialogView) contentView;
			editView.setHint(editOptions.hint);
			if (!TextUtils.isEmpty(editOptions.mContent)) {
				editView.setInput(editOptions.mContent);
			}
			this.ensureOptionsInputConfig(editOptions);
			if (editOptions.inputConfig != null) {
				editView.setInputConfig(editOptions.inputConfig);
			}
		}
	}

	/**
	 * Ensures that the specified <var>options</var> has its edit configuration object initialized
	 * in case of use of style from which should be the config object created.
	 */
	private void ensureOptionsInputConfig(final EditOptions options) {
		final Context context = getActivity();
		if (options != null && context != null) options.ensureInputConfig(context);
	}

	/**
	 */
	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (mOptions instanceof EditOptions && ((EditOptions) mOptions).showSoftKeyboard) {
			DialogUtils.showSoftKeyboard(this);
		}
		final View contentView = getContentView();
		if (contentView instanceof EditDialogView) {
			this.mEditView = (EditDialogView) contentView;
		}
	}

	/**
	 * Updates the enabled state of this dialog.
	 * <p>
	 * <b>Note</b>, that while this dialog is <b>disabled</b>, the current input cannot be changed
	 * by the user.
	 *
	 * @param enabled {@code True} to enable this dialog, {@code false} otherwise.
	 * @see #isEnabled()
	 */
	public void setEnabled(final boolean enabled) {
		this.updateState(enabled);
	}

	/**
	 * Updates the current enabled state of this dialog.
	 *
	 * @param enabled {@code True} if this dialog should be enabled, {@code false} to be disabled, so
	 *                user cannot interact with it (<b>note</b>, that dialog will also be not cancelable).
	 */
	private void updateState(final boolean enabled) {
		if (mEnabled != enabled) {
			this.mEnabled = enabled;
			setCancelable(!enabled);
			if (mEditView != null) {
				mEditView.setEnabled(enabled);
			}
			final View buttonsView = getButtonsView();
			if (buttonsView != null) {
				buttonsView.setEnabled(enabled);
			}
		}
	}

	/**
	 * Returns a boolean flag indicating whether this dialog is enabled or not.
	 *
	 * @return {@code True} if this dialog is enabled, {@code false} otherwise.
	 * @see #setEnabled(boolean)
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 */
	@Override protected boolean onButtonClick(final int button) {
		DialogUtils.hideSoftKeyboard(this);
		return super.onButtonClick(button);
	}

	/**
	 * Same as {@link #setError(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired error text.
	 * @see #clearError()
	 */
	public void setError(@StringRes final int resId) {
		if (mEditView != null) mEditView.setError(resId);
	}

	/**
	 * Sets an error text for the input view of this dialog.
	 *
	 * @param error The desired error text.
	 * @see #clearError()
	 */
	public void setError(@NonNull final CharSequence error) {
		if (mEditView != null) mEditView.setError(error);
	}

	/**
	 * Clears the current error specified for this dialog (if any).
	 *
	 * @see #setError(CharSequence)
	 */
	public void clearError() {
		if (mEditView != null) mEditView.clearError();
	}

	/**
	 * Returns the current input value of this dialog.
	 *
	 * @return Input text. May be empty if no input has been specified yet.
	 * @see #getEditableInput()
	 */
	@NonNull public CharSequence getInput() {
		return mEditView != null ? mEditView.getInput() : "";
	}

	/**
	 * Returns the current editable input value of this dialog.
	 *
	 * @return Editable input value or {@code null} if no input has been specified yet.
	 * @see #getInput()
	 */
	@Nullable public Editable getEditableInput() {
		return mEditView != null ? mEditView.getEditableInput() : null;
	}

	/**
	 * Clears the current editable input value of this dialog.
	 *
	 * @see #getEditableInput()
	 */
	public void clearInput() {
		if (mEditView != null) mEditView.clearInput();
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link DialogOptions} implementation specific for the {@link EditDialog}.
	 * <p>
	 * Below are listed setters which may be used to supply desired data to {@link EditDialog}
	 * through these options:
	 * <ul>
	 * <li>{@link #content(CharSequence)} (used as initial editable value)</li>
	 * <li>{@link #content(int)} (used as initial editable value)</li>
	 * <li>{@link #hint(CharSequence)}</li>
	 * <li>{@link #hint(int)}</li>
	 * <li>{@link #inputConfig(InputConfig)}</li>
	 * <li>{@link #inputStyle(int)}</li>
	 * <li>{@link #showSoftKeyboard(boolean)}</li>
	 * </ul>
	 * <p>
	 * <h3>Xml attributes</h3>
	 * See {@link R.styleable#Dialog_Options_Edit EditOptions Attributes},
	 * {@link R.styleable#Dialog_Options DialogOptions Attributes}
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class EditOptions extends DialogOptions<EditOptions> {

		/**
		 * Creator used to create an instance or array of instances of EditOptions from {@link Parcel}.
		 */
		public static final Creator<EditOptions> CREATOR = new Creator<EditOptions>() {

			/**
			 */
			@Override public EditOptions createFromParcel(@NonNull final Parcel source) { return new EditOptions(source); }

			/**
			 */
			@Override public EditOptions[] newArray(final int size) { return new EditOptions[size]; }
		};

		/**
		 * Flag for <b>hint</b> parameter.
		 */
		static final int HINT = 0x00000001;

		/**
		 * Flag for <b>show soft keyboard</b> parameter.
		 */
		static final int SHOW_SOFT_KEYBOARD = 0x00000001 << 1;

		/**
		 * Flag for <b>edit config</b> parameter.
		 */
		static final int INPUT_CONFIG = 0x00000001 << 2;

		/**
		 * Hint text for EditText view. This text will be presented also as label view of EditLayout.
		 */
		CharSequence hint;

		/**
		 * Flag indicating whether a soft keyboard should be showed or not.
		 */
		boolean showSoftKeyboard = true;

		/**
		 * Configuration options for DialogEditLayout.
		 */
		InputConfig inputConfig;

		/**
		 * Resource id of the style holding configuration options for input.
		 */
		int inputConfigStyle = -1;

		/**
		 * Creates a new instance of EditOptions.
		 */
		public EditOptions() {
			super();
			remain(true);
		}

		/**
		 * Creates a new instance of EditOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public EditOptions(@NonNull final Resources resources) {
			super(resources);
			remain(true);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of EditOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected EditOptions(@NonNull final Parcel source) {
			super(source);
			this.hint = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.showSoftKeyboard = source.readInt() == 1;
			this.inputConfig = source.readParcelable(DialogsConfig.class.getClassLoader());
			this.inputConfigStyle = source.readInt();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			TextUtils.writeToParcel(hint, dest, flags);
			dest.writeInt(showSoftKeyboard ? 1 : 0);
			dest.writeParcelable(inputConfig, flags);
			dest.writeInt(inputConfigStyle);
		}

		/**
		 */
		@Override public EditOptions merge(@NonNull final DialogOptions options) {
			if (!(options instanceof EditOptions)) {
				return super.merge(options);
			}
			final EditOptions editOptions = (EditOptions) options;
			if (editOptions.isSet(HINT)) {
				this.hint = copyCharSequence(editOptions.hint);
				updateIsSet(HINT);
			}
			if (editOptions.isSet(SHOW_SOFT_KEYBOARD)) {
				this.showSoftKeyboard = editOptions.showSoftKeyboard;
				updateIsSet(SHOW_SOFT_KEYBOARD);
			}
			if (editOptions.isSet(INPUT_CONFIG)) {
				this.inputConfigStyle = editOptions.inputConfigStyle;
				if (editOptions.inputConfig != null) {
					this.inputConfig = new InputConfig(editOptions.inputConfig);
				} else {
					this.inputConfig = null;
				}
				updateIsSet(INPUT_CONFIG);
			}
			return super.merge(options);
		}

		/**
		 * Same as {@link #hint(CharSequence)} but with resource id.
		 * <p>
		 * Call this only if these options instance has been instantiated with valid application resources.
		 *
		 * @param resId Resource id of the desired hint text.
		 * @see R.attr#dialogHint dialog:dialogHint
		 * @see #hint()
		 */
		public EditOptions hint(@StringRes final int resId) {
			return hint(string(resId));
		}

		/**
		 * Sets the hint text for EditText of a dialog associated with these options.
		 *
		 * @param text The desired hint text.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogHint dialog:dialogHint
		 * @see #hint()
		 */
		public EditOptions hint(@Nullable final CharSequence text) {
			updateIsSet(HINT);
			if (hint == null || !hint.equals(text)) {
				this.hint = text;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the hint text, set to these options.
		 *
		 * @return Edit text hint or {@code null} by default.
		 * @see #hint(int)
		 * @see #hint(CharSequence)
		 */
		@Nullable public CharSequence hint() {
			return hint;
		}

		/**
		 * Sets a flag indicating whether to show soft keyboard for dialog associated with these
		 * options or not.
		 *
		 * @param show {@code True} to show, {@code false} otherwise.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogShowSoftKeyboard dialog:dialogShowSoftKeyboard
		 * @see #shouldShowSoftKeyboard()
		 */
		public EditOptions showSoftKeyboard(final boolean show) {
			updateIsSet(SHOW_SOFT_KEYBOARD);
			if (showSoftKeyboard != show) {
				this.showSoftKeyboard = show;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns a flag indicating whether to show soft keyboard for dialog or not.
		 * <p>
		 * Default value: <b>true</b>
		 *
		 * @return {@code True} to show, {@code false} otherwise.
		 * @see #showSoftKeyboard(boolean)
		 */
		public boolean shouldShowSoftKeyboard() {
			return showSoftKeyboard;
		}

		/**
		 * Specifies the configuration options for input of a dialog associated with these options.
		 *
		 * @param inputConfig The desired configuration options.
		 * @return These options to allow methods chaining.
		 * @see #inputConfig()
		 */
		public EditOptions inputConfig(@NonNull final InputConfig inputConfig) {
			updateIsSet(INPUT_CONFIG);
			if (this.inputConfig != inputConfig) {
				this.inputConfig = inputConfig;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Sets a resource id of the style containing attributes for configuration options for input
		 * of a dialog associated with these options.
		 *
		 * @param resId Resource id of the desired style.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogInputStyle dialog:dialogInputStyle
		 * @see #inputConfig()
		 */
		public final EditOptions inputStyle(@StyleRes final int resId) {
			updateIsSet(INPUT_CONFIG);
			if (inputConfigStyle != resId) {
				this.inputConfigStyle = resId;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Ensures that edit configuration object is initialized in case of use of style from which
		 * should be the config object created.
		 *
		 * @param context Context used to create edit config object from the current style (if any).
		 */
		final void ensureInputConfig(@NonNull final Context context) {
			if (inputConfigStyle != -1 && inputConfig == null) {
				this.inputConfig = InputConfig.fromStyle(context, inputConfigStyle);
			}
		}

		/**
		 * Returns the configuration options, set to these dialog options.
		 *
		 * @return Instance of InputConfig or {@code null} if no configuration has been specified.
		 * @see #inputConfig(InputConfig)
		 * @see #inputStyle(int)
		 */
		@Nullable public InputConfig inputConfig() {
			return inputConfig;
		}

		/**
		 */
		@Override protected void onParseXmlAttribute(
				@NonNull final XmlResourceParser xmlParser,
				final int index,
				@AttrRes final int attr,
				@NonNull final Resources resources,
				@Nullable final Resources.Theme theme
		) {
			if (attr == R.attr.dialogHint) {
				hint(obtainXmlAttributeText(xmlParser, index, resources));
			} else if (attr == R.attr.dialogInputStyle) {
				inputStyle(xmlParser.getAttributeResourceValue(index, -1));
			} else if (attr == R.attr.dialogShowSoftKeyboard) {
				showSoftKeyboard(xmlParser.getAttributeBooleanValue(index, showSoftKeyboard));
			} else {
				super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
			}
		}
	}
}