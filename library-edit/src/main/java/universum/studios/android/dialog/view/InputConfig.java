/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.ui.widget.EditLayout;

/**
 * Configuration options for an input component like {@link EditText} or {@link EditLayout}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("ResourceType")
public class InputConfig implements Parcelable {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "InputConfig";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Creator used to create an instance or array of instances of InputConfig from {@link Parcel}.
	 */
	public static final Creator<InputConfig> CREATOR = new Creator<InputConfig>() {

		/**
		 */
		@Override public InputConfig createFromParcel(@NonNull final Parcel source) { return new InputConfig(source); }

		/**
		 */
		@Override public InputConfig[] newArray(final int size) { return new InputConfig[size]; }
	};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Note text for input view.
	 *
	 * @see EditLayout#setNote(CharSequence)
	 */
	public CharSequence note;

	/**
	 * Constraint length (in characters) for input view.. This is only for informational purpose
	 * and  does not constraint number of characters that can be inputted by a user into input field.
	 * <p>
	 * If <b>negative</b>, this option is ignored.
	 *
	 * @see EditLayout#setLengthConstraint(int)
	 */
	public int lengthConstraint = -1;

	/**
	 * Max length (in characters) for text of input view.
	 * <p>
	 * If <b>negative</b>, this option is ignored.
	 *
	 * @see EditLayout#setMaxLength(int)
	 */
	public int maxLength = -1;

	/**
	 * Features for input view.
	 * <p>
	 * If <b>negative</b> this option is ignored.
	 *
	 * @see EditLayout#requestInputFeatures(int)
	 */
	@EditLayout.InputFeature
	public int inputFeatures = -1;

	/**
	 * Type of input for input view.
	 *
	 * @see EditText#setInputType(int)
	 */
	public int inputType;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of empty InputConfig.
	 */
	public InputConfig() {}

	/**
	 * Creates a new instance of InputConfig with the options from the specified <var>other</var>
	 * configuration.
	 *
	 * @param other InputConfig from which to create the new InputConfig.
	 */
	public InputConfig(@NonNull final InputConfig other) {
		this.note = other.note;
		this.lengthConstraint = other.lengthConstraint;
		this.maxLength = other.maxLength;
		this.inputFeatures = other.inputFeatures;
		this.inputType = other.inputType;
	}

	/**
	 * Called form {@link #CREATOR} to create an instance of InputConfig form the given parcel
	 * <var>source</var>.
	 *
	 * @param source Parcel with data for the new instance.
	 */
	protected InputConfig(@NonNull final Parcel source) {
		this.note = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
		this.lengthConstraint = source.readInt();
		this.maxLength = source.readInt();
		this.inputFeatures = source.readInt();
		this.inputType = source.readInt();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
		TextUtils.writeToParcel(note, dest, flags);
		dest.writeInt(lengthConstraint);
		dest.writeInt(maxLength);
		dest.writeInt(inputFeatures);
		dest.writeInt(inputType);
	}

	/**
	 * Initializes options of this edit configuration from a style with the specified <var>resId</var>.
	 *
	 * @param context Context used to parse data from the style.
	 * @param resId   Resource id of the desired style from which to obtain data.
	 */
	@NonNull public static InputConfig fromStyle(@NonNull final Context context, @StyleRes final int resId) {
		final InputConfig config = new InputConfig();
		final TypedArray attributes = context.obtainStyledAttributes(resId, R.styleable.Dialog_InputConfig);
		for (int i = 0; i < attributes.getIndexCount(); i++) {
			final int index = attributes.getIndex(i);
			if (index == R.styleable.Dialog_InputConfig_uiNote) {
				config.note = attributes.getText(index);
			} else if (index == R.styleable.Dialog_InputConfig_uiLengthConstraint) {
				config.lengthConstraint = attributes.getInt(index, config.lengthConstraint);
			} else if (index == R.styleable.Dialog_InputConfig_uiInputFeatures) {
				config.inputFeatures = attributes.getInteger(index, config.inputFeatures);
			} else if (index == R.styleable.Dialog_InputConfig_android_maxLength) {
				config.maxLength = attributes.getInt(index, config.maxLength);
			} else if (index == R.styleable.Dialog_InputConfig_android_inputType) {
				config.inputType = attributes.getInteger(index, config.inputType);
			}
		}
		attributes.recycle();
		return config;
	}

	/**
	 * Specifies value of {@link #note} configuration option.
	 *
	 * @return This configuration to allow methods chaining.
	 */
	@NonNull public InputConfig note(@Nullable final CharSequence note) {
		this.note = note;
		return this;
	}

	/**
	 * Specifies value of {@link #lengthConstraint} configuration option.
	 *
	 * @return This configuration to allow methods chaining.
	 */
	@NonNull public InputConfig lengthConstraint(final int lengthConstraint) {
		this.lengthConstraint = lengthConstraint;
		return this;
	}

	/**
	 * Specifies value of {@link #maxLength} configuration option.
	 *
	 * @return This configuration to allow methods chaining.
	 */
	@NonNull public InputConfig maxLength(final int maxLength) {
		this.maxLength = maxLength;
		return this;
	}

	/**
	 * Specifies value of {@link #inputFeatures} configuration option.
	 *
	 * @return This configuration to allow methods chaining.
	 */
	@NonNull public InputConfig inputFeatures(@EditLayout.InputFeature final int inputFeatures) {
		this.inputFeatures = inputFeatures;
		return this;
	}

	/**
	 * Specifies value of {@link #inputType} configuration option.
	 *
	 * @return This configuration to allow methods chaining.
	 */
	@NonNull public InputConfig inputType(final int inputType) {
		this.inputType = inputType;
		return this;
	}

	/**
	 */
	@Override public int describeContents() {
		return 0;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}