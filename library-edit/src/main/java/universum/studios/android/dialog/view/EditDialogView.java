/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.text.Editable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.EditDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link EditDialog EditDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.EditDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface EditDialogView extends DialogView {

	/**
	 * Updates the enabled state of this dialog view.
	 * <p>
	 * <b>Note</b>, that while this view is <b>disabled</b>, the current input within EditText cannot
	 * be updated by a user.
	 *
	 * @param enabled {@code True} to enable input, {@code false} otherwise.
	 * @see #isEnabled()
	 */
	void setEnabled(boolean enabled);

	/**
	 * Returns a boolean flag indicating whether this dialog view is enabled or not.
	 *
	 * @return {@code True} if it is enabled, {@code false} otherwise.
	 * @see #setEnabled(boolean)
	 */
	boolean isEnabled();

	/**
	 * Sets a configuration for the input view of this dialog view.
	 *
	 * @param inputConfig The desired configuration.
	 */
	void setInputConfig(@NonNull InputConfig inputConfig);

	/**
	 * Same as {@link #setHint(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired hint text.
	 * @see #getHint()
	 * @see #setInput(CharSequence)
	 */
	void setHint(@StringRes int resId);

	/**
	 * Sets a hint text for the input view of this dialog view.
	 *
	 * @param hint The desired hint text. May be {@code null} to clear the current one.
	 * @see #getHint()
	 * @see #setInput(CharSequence)
	 */
	void setHint(@Nullable CharSequence hint);

	/**
	 * Returns the hint text specified for this dialog view.
	 *
	 * @return Hint for input. May be empty if no hint has been specified yet.
	 * @see #setHint(int)
	 * @see #setHint(CharSequence)
	 */
	@NonNull CharSequence getHint();

	/**
	 * Same as {@link #setInput(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired input text.
	 * @see #getInput()
	 * @see #setHint(CharSequence)
	 */
	void setInput(@StringRes int resId);

	/**
	 * Sets an input text value for the input view of this dialog view.
	 *
	 * @param input The desired input text. May be {@code null} to clear the current one.
	 * @see #getInput()
	 * @see #setHint(CharSequence)
	 */
	void setInput(@Nullable CharSequence input);

	/**
	 * Returns the current input value of this dialog view.
	 *
	 * @return Input text. May be empty if no input has been specified yet.
	 * @see #setInput(int)
	 * @see #setInput(CharSequence)
	 * @see #getEditableInput()
	 */
	@NonNull CharSequence getInput();

	/**
	 * Returns the current editable input value of this dialog view.
	 *
	 * @return Editable input value or {@code null} if no input has been specified yet.
	 * @see #getInput()
	 * @see #clearInput()
	 */
	@Nullable Editable getEditableInput();

	/**
	 * Clears the current editable input value of this dialog view.
	 *
	 * @see #getEditableInput()
	 */
	void clearInput();

	/**
	 * Same as {@link #setError(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired error text.
	 * @see #getError()
	 * @see #clearError()
	 */
	void setError(@StringRes int resId);

	/**
	 * Sets an error text for the input view of this dialog view.
	 *
	 * @param error The desired error text. May be {@code null} to clear the current one.
	 * @see #getError()
	 * @see #clearError()
	 */
	void setError(@Nullable CharSequence error);

	/**
	 * Returns the current error specified for this dialog view.
	 *
	 * @return Error text. May be empty if no error has been specified yet.
	 * @see #setError(int)
	 * @see #setError(CharSequence)
	 */
	@NonNull CharSequence getError();

	/**
	 * Clears the current error specified for this dialog view (if any).
	 *
	 * @see #setError(CharSequence)
	 */
	void clearError();

	/**
	 * Clears the focus of this dialog view.
	 */
	void clearFocus();
}