/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import androidx.annotation.ColorInt;

import universum.studios.android.dialog.ColorPickerDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link ColorPickerDialog ColorPickerDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.ColorPickerDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface ColorPickerDialogView extends DialogView {

    /**
     * Sets a color that should be drawn by the ColorView of this dialog view.
     *
     * @param color The desired color.
     * @see #getColor()
     * @see #setCanvasColor(int)
     */
    void setColor(@ColorInt int color);

    /**
     * Returns the current color drawn by the ColorView of this dialog view.
     *
     * @return The color picked by a user via ARGB sliders or text fields, currently drawn by the
     * ColorView.
     * @see #setColor(int)
     */
    @ColorInt int getColor();

    /**
     * Sets a color that should be drawn as background for the color specified via {@link #setColor(int)}.
     * <p>
     * Default color is: <b>#FFFFFF</b>
     *
     * @param color The desired background canvas color.
     * @see #getCanvasColor()
     */
    void setCanvasColor(@ColorInt int color);

    /**
     * Returns the color that is used to draw a background of ColorView of this dialog view.
     *
     * @return Canvas color.
     * @see #setCanvasColor(int)
     */
    @ColorInt int getCanvasColor();

    /**
     * Sets a flag indicating whether an input that allows to pick the alpha value should be visible
     * (available to user) or not.
     *
     * @param visible {@code True} to allow to the user to pick also alpha value of color,
     *                {@code false} otherwise.
     * @see #isAlphaInputVisible()
     */
    void setAlphaInputVisible(boolean visible);

    /**
     * Returns the boolean flag indicating whether an input for the alpha value is visible or not.
     *
     * @return {@code True} if it is visible (available to a user), {@code false} otherwise.
     * @see #setAlphaInputVisible(boolean)
     */
    boolean isAlphaInputVisible();
}