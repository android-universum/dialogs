/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.DialogView;
import universum.studios.android.ui.widget.LinearLayoutWidget;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link LinearLayoutWidget} implementation that represents a container for one {@link TextView},
 * {@link SeekBar} and {@link EditText} used by {@link ColorPickerDialogContentView} view to allow to a user
 * to pick value for a particular color component (RED, GREEN, BLUE, [ALPHA]) from the range <b>[0, 255]</b>.
 * <p>
 * Initial value for a slider (SeekBar) can be specified via {@link #setValue(int)} and the current
 * one can be obtained via {@link #getValue()}. Text for label that should identify for which color
 * component can be value picked via a particular slider layout can be specified via {@link #setLabel(CharSequence)}.
 *
 * <h3>Callbacks</h3>
 * To listen for a changed value within DialogColorSliderLayout, register {@link OnValueChangeListener OnValueChangeListener}
 * via {@link #setOnValueChangeListener(OnValueChangeListener)}. This listener will fire its callback
 * ({@code OnValueChangeListener.onValueChanged(..., int)}) whenever the specified value has been
 * changed within the slider or entered into text field (EditText) by the user.
 *
 * <h3>Xml attributes</h3>
 * {@link R.styleable#Dialog_ColorSliderLayout DialogColorSliderLayout Attributes}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogColorSliderLayout extends LinearLayoutWidget
		implements
		DialogView,
		SeekBar.OnSeekBarChangeListener,
		TextWatcher {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "DialogColorSliderLayout";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Listener that can receive a callback about changed value of color component within
	 * {@link DialogColorSliderLayout}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public interface OnValueChangeListener {

		/**
		 * Invoked whenever the specified color component <var>value</var> has been changed within
		 * the <var>sliderLayout</var>.
		 * <p>
		 * This callback is fired whenever a user changes progress of slider or input text of text
		 * field.
		 *
		 * @param sliderLayout The color slider layout within which change occurred.
		 * @param value        The current changed value from the range {@code [0, 255]}.
		 */
		void onValueChanged(@NonNull DialogColorSliderLayout sliderLayout, int value);
	}

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Text view used to present name of the color component.
     */
    private TextView mLabelView;

    /**
     * Seek bar that allows to specify value of color component by dragging of its thumb.
     */
    private SeekBar mSeekBar;

    /**
     * Edit text that allows to specify value of color component by typing its decimal value.
     */
    private EditText mEditText;

    /**
     * Current picked value of color component from the range {@code [0, 255]}.
     */
    private int mValue;

    /**
     * Callback to be invoked whenever current picked value of color component is changed.
     */
    private OnValueChangeListener mValueChangeListener;

    /**
     * Boolean flag indicating whether a change in progress from the seek bar should be ignored or not.
     */
    private boolean mIgnoreSeekBarProgressChange;

    /**
     * Boolean flag indicating whether a change in input from the edit text should be ignored or not.
     */
    private boolean mIgnoreEditTextInputChange;

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #DialogColorSliderLayout(Context, AttributeSet)} without attributes.
     */
    public DialogColorSliderLayout(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #DialogColorSliderLayout(Context, AttributeSet, int)} with {@code 0} as attribute
     * for default style.
     */
    public DialogColorSliderLayout(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs, 0, 0);
    }

    /**
     * Same as {@link #DialogColorSliderLayout(Context, AttributeSet, int, int)} with {@code 0} as
     * default style.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public DialogColorSliderLayout(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    /**
     * Creates a new instance of DialogColorSliderLayout for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     * @param defStyleRes  Resource id of the default style for the new view.
     */
    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DialogColorSliderLayout(
    		@NonNull final Context context,
		    @Nullable final AttributeSet attrs,
		    @AttrRes final int defStyleAttr,
		    @StyleRes final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Called from one of constructors of this view to perform its initialization.
     * <p>
     * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
     * this view specific data from it that can be used to configure this new view instance. The
     * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
     * from the current theme provided by the specified <var>context</var>.
     */
    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        this.inflateHierarchy(context, R.layout.dialog_color_slider_layout);

        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.Dialog_ColorSliderLayout, defStyleAttr, defStyleRes);
        for (int i = 0; i < attributes.getIndexCount(); i++) {
            final int index = attributes.getIndex(i);
            if (index == R.styleable.Dialog_ColorSliderLayout_android_label) {
                setLabel(attributes.getText(index));
            }
        }
        attributes.recycle();
    }

    /**
     * Called to inflate a view hierarchy of this view.
     *
     * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
     *                 layout resource as view hierarchy for this view.
     * @param resource Resource id of the layout which should represent a view hierarchy of this view.
     */
    private void inflateHierarchy(final Context context, final int resource) {
        if (resource > 0) {
            LayoutInflater.from(context).inflate(resource, this);
            this.mLabelView = (TextView) getChildAt(0);
            this.mSeekBar = (SeekBar) getChildAt(1);
            this.mEditText = (EditText) getChildAt(2);
            if (mSeekBar != null) mSeekBar.setOnSeekBarChangeListener(this);
            if (mEditText != null) mEditText.addTextChangedListener(this);
            onFinishInflate();
        }
    }

    /**
     * Same as {@link #setLabel(CharSequence)} for resource id.
     *
     * @param resId Resource id of the desired label text.
     * @see #getLabel()
     */
    public void setLabel(@StringRes final int resId) {
        setLabel(getResources().getText(resId));
    }

    /**
     * Sets a text to be presented at the left of the slider as label. This label text should indicate
     * of which color component value can a user pick via this slider layout.
     *
     * @param text The desired label text. May be {@code null} to clear the current one.
     * @see #getLabel()
     */
    public void setLabel(@Nullable final CharSequence text) {
        if (mLabelView != null) mLabelView.setText(text);
    }

    /**
     * Returns the current label text presented at the left of the slider.
     *
     * @return Label text. May be empty if no text has been specified yet.
     * @see #setLabel(int)
     * @see #setLabel(CharSequence)
     */
    @NonNull public CharSequence getLabel() {
        return mLabelView != null ? mLabelView.getText() : "";
    }

    /**
     * Registers a callback to be invoked whenever the value of slider or text field are changed.
     *
     * @param listener Listener callback. May be {@code null} to clear the current one.
     * @see #setValue(int)
     */
    public void setOnValueChangeListener(@Nullable final OnValueChangeListener listener) {
        this.mValueChangeListener = listener;
    }

    /**
     * Sets a value of color component to be presented by this slider layout. The specified
     * <var>value</var> will be set as progress for the slider and as input for the text field.
     *
     * @param value The desired value from the range {@code [0, 255]}.
     * @see #getValue()
     * @see #setOnValueChangeListener(OnValueChangeListener)
     */
    public void setValue(@IntRange(from = 0, to = 255) final int value) {
        if (mValue != value && value >= 0 && value <= 255) {
            this.mValue = value;
            if (mSeekBar != null) {
                mSeekBar.setProgress(value);
            }
        }
    }

    /**
     * Returns the current color component value presented by this layout.
     *
     * @return Current picked value presented as progress by slider and input text within text field.
     * @see #setValue(int)
     */
    @IntRange(from = 0, to = 255) public int getValue() {
        return mValue;
    }

	/**
	 */
	@Override public void onStartTrackingTouch(@NonNull SeekBar seekBar) {
		// Ignored.
	}

	/**
	 */
	@SuppressLint("SetTextI18n")
	@Override public void onProgressChanged(@NonNull final SeekBar seekBar, final int progress, final boolean fromUser) {
		if (mIgnoreSeekBarProgressChange || mEditText == null) {
			return;
		}
		this.mIgnoreEditTextInputChange = true;
		mEditText.setText(Integer.toString(progress));
		this.mIgnoreEditTextInputChange = false;
		notifyValueChanged(mValue = progress);
	}

	/**
	 */
	@Override public void onStopTrackingTouch(@NonNull SeekBar seekBar) {
		// Ignored.
	}

	/**
	 */
	@Override public void beforeTextChanged(@NonNull CharSequence text, int start, int count, int after) {
		// Ignored.
	}

	/**
	 */
	@SuppressLint("SetTextI18n")
	@Override public void onTextChanged(@NonNull final CharSequence text, final int start, final int before, final int count) {
		if (TextUtils.isDigitsOnly(text)) {
			final int value = !TextUtils.isEmpty(text) ? Integer.parseInt(text.toString()) : -1;
			if (mIgnoreEditTextInputChange || mEditText == null || mSeekBar == null) {
				return;
			}
			if (value > 255 || value < 0) {
				mEditText.setText(Integer.toString(value > 255 ? 255 : 0));
			} else {
				this.mIgnoreSeekBarProgressChange = true;
				mSeekBar.setProgress(value);
				this.mIgnoreSeekBarProgressChange = false;
				notifyValueChanged(mValue = value);
			}
		}
	}

	/**
	 */
	@Override public void afterTextChanged(@NonNull Editable text) {
		// Ignored.
	}

    /**
     * Notifies the current OnValueChangeListener (if any), that the specified <var>value</var> has
     * been changed.
     *
     * @param value The current changed value.
     */
    private void notifyValueChanged(final int value) {
        if (mValueChangeListener != null) mValueChangeListener.onValueChanged(this, value);
    }

    /**
     */
    @Override protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.value = mValue;
        return savedState;
    }

    /**
     */
    @Override protected void onRestoreInstanceState(@NonNull final Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        final SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setValue(savedState.value);
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link WidgetSavedState} implementation used to ensure that the state of {@link DialogColorSliderLayout}
     * is properly saved.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class SavedState extends WidgetSavedState {

        /**
         * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
         */
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            /**
             */
            @Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

            /**
             */
            @Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
        };

        /**
         */
        int value;

        /**
         * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
         * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
         *
         * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
         *                   {@code onSaveInstanceState()}.
         */
        protected SavedState(@NonNull final Parcelable superState) {
            super(superState);
        }

        /**
         * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected SavedState(@NonNull final Parcel source) {
            super(source);
            this.value = source.readInt();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(value);
        }
    }
}