/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.ColorPickerDialogView;

/**
 * A {@link SimpleDialog} implementation that can be used to allow to a user to pick its desired color
 * via specifying values for <b>RGBA</b> color components using slider or text field. This dialog uses
 * default {@link ColorPickerDialogView} implementation as its content view which consists of
 * {@link universum.studios.android.dialog.widget.DialogColorView DialogColorView} used to present
 * picked color and 4 sliders with text input fields that can be used to input the desired value for
 * the mentioned color components.
 * <p>
 * The picked color may be obtained via {@link #getColor()}.
 * <p>
 * See {@link ColorOptions} for options that can be supplied to the ColorPickerDialog from the desired
 * context via {@link #newInstance(ColorOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogColorOptions dialogColorOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ColorPickerDialog extends SimpleDialog {

    /*
     * Constants ===================================================================================
     */

    /*
     * Log TAG.
     */
    // private static final String TAG = "ColorPickerDialog";

	/*
	 * Interface ===================================================================================
	 */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * ColorPicker dialog view implementation.
     */
    private ColorPickerDialogView mColorView;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of ColorPickerDialog with a {@link universum.studios.android.dialog.widget.DialogColorView DialogColorView}
     * and 4 layouts with sliders and text fields allowing to pick a value of each of the color components
     * (RGBA) respectively.
     */
    public ColorPickerDialog() {
        super(R.attr.dialogColorOptions);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Creates a new instance of ColorPickerDialog with the specified options.
     *
     * @param options Specific options for this type of dialog.
     * @return New ColorPickerDialog instance.
     */
    @NonNull public static ColorPickerDialog newInstance(@NonNull final ColorOptions options) {
        final ColorPickerDialog dialog = new ColorPickerDialog();
        dialog.setOptions(options);
        return dialog;
    }

    /**
     */
    @Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
        return new ColorOptions(resources);
    }

	/**
	 */
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof ColorOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Color);
			final ColorOptions options = (ColorOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Color_dialogColor) {
					if (!options.isSet(ColorOptions.COLOR)) {
						options.color(attributes.getColor(index, options.color));
					}
				} else if (index == R.styleable.Dialog_Options_Color_dialogColorCanvas) {
					if (!options.isSet(ColorOptions.CANVAS_COLOR)) {
						options.canvasColor(attributes.getColor(index, options.canvasColor));
					}
				}
			}
			attributes.recycle();
		}
	}

    /**
     */
    @Override @NonNull protected View onCreateContentView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.dialog_content_color, container, false);
    }

    /**
     */
    @Override @NonNull protected View onCreateButtonsView(
    		@NonNull final LayoutInflater inflater,
		    @NonNull final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        // Content view already contains buttons view.
        final View view = new Space(inflater.getContext());
        view.setVisibility(View.GONE);
        return view;
    }

    /**
     */
    @Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
        if (contentView instanceof ColorPickerDialogView && options instanceof ColorOptions) {
            final ColorOptions colorOptions = (ColorOptions) options;
            final ColorPickerDialogView colorView = (ColorPickerDialogView) contentView;
            if (colorOptions.color != 0) {
                colorView.setColor(colorOptions.color);
            }
            colorView.setCanvasColor(colorOptions.canvasColor);
        }
    }

    /**
     */
    @Override protected void onUpdateContentViewPadding() {
        // Do not update content view padding.
    }

    /**
     */
    @Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final View contentView = getContentView();
        if (contentView instanceof ColorPickerDialogView) {
            this.mColorView = (ColorPickerDialogView) contentView;
        }
    }

    /**
     * Returns the current value of <b>color</b> picked within this picker dialog.
     *
     * @return Picked color either by user or initially specified via options.
     */
    @ColorInt public int getColor() {
        return mColorView != null ? mColorView.getColor() : Color.TRANSPARENT;
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link DialogOptions} implementation specific for the {@link ColorPickerDialog}.
     * <p>
     * Below are listed setters which may be used to supply desired data to {@link ColorPickerDialog}
     * through these options:
     * <ul>
     * <li>{@link #color(int)}</li>
     * <li>{@link #canvasColor(int)}</li>
     * </ul>
     *
     * <h3>Xml attributes</h3>
     * See {@link R.styleable#Dialog_Options_Color ColorOptions Attributes},
     * {@link R.styleable#Dialog_Options DialogOptions Attributes}
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class ColorOptions extends DialogOptions<ColorOptions> {

        /**
         * Creator used to create an instance or array of instances of ColorOptions from {@link Parcel}.
         */
        public static final Creator<ColorOptions> CREATOR = new Creator<ColorOptions>() {

            /**
             */
            @Override public ColorOptions createFromParcel(@NonNull final Parcel source) { return new ColorOptions(source); }

            /**
             */
            @Override public ColorOptions[] newArray(final int size) { return new ColorOptions[size]; }
        };

        /**
         * Flag for <b>color</b> parameter.
         */
        static final int COLOR = 0x00000001;

        /**
         * Flag for <b>canvas color</b> parameter.
         */
        static final int CANVAS_COLOR = 0x00000001 << 1;

        /**
         * Initial color for ColorView.
         */
        int color;

        /**
         * Background color for ColorView.
         */
        int canvasColor = Color.WHITE;

        /**
         * Creates a new instance of ColorOptions.
         */
        public ColorOptions() {
            super();
        }

        /**
         * Creates a new instance of ColorOptions.
         *
         * @param resources Application resources used to obtain values for these options when they
         *                  are requested by theirs resource ids.
         */
        public ColorOptions(@NonNull final Resources resources) {
            super(resources);
        }

        /**
         * Called form {@link #CREATOR} to create an instance of ColorOptions form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected ColorOptions(@NonNull final Parcel source) {
            super(source);
            this.color = source.readInt();
            this.canvasColor = source.readInt();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(color);
            dest.writeInt(canvasColor);
        }

        /**
         */
        @Override public ColorOptions merge(@NonNull final DialogOptions options) {
            if (!(options instanceof ColorOptions)) {
                return super.merge(options);
            }
            final ColorOptions colorOptions = (ColorOptions) options;
            if (colorOptions.isSet(COLOR)) {
                this.color = colorOptions.color;
                updateIsSet(COLOR);
            }
            if (colorOptions.isSet(CANVAS_COLOR)) {
                this.canvasColor = colorOptions.canvasColor;
                updateIsSet(CANVAS_COLOR);
            }
            return super.merge(options);
        }

        /**
         * Sets the initial color for ColorView of a dialog associated with these options.
         * <p>
         * Default color: <b>{@link Color#TRANSPARENT}</b>
         *
         * @param color The desired initial color.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogColor dialog:dialogColor
         * @see #color()
         */
        public ColorOptions color(@ColorInt final int color) {
            updateIsSet(COLOR);
            if (this.color != color) {
                this.color = color;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the initial color for ColorView, set to these options.
         *
         * @return The initial color specified via {@link #color(int)} or {@link Color#TRANSPARENT}
         * by default.
         * @see #color(int)
         */
        @ColorInt public int color() {
            return color;
        }

        /**
         * Sets the color for canvas (background) of ColorView of a dialog associated with these
         * options.
         * <p>
         * Default color: <b>{@link Color#WHITE}</b>
         *
         * @param color The desired canvas (background) color.
         * @return These options to allow methods chaining.
         * @see R.attr#dialogColorCanvas dialog:dialogColorCanvas
         * @see #canvasColor()
         */
        public ColorOptions canvasColor(@ColorInt final int color) {
            updateIsSet(CANVAS_COLOR);
            if (this.canvasColor != color) {
                this.canvasColor = color;
                notifyChanged();
            }
            return this;
        }

        /**
         * Returns the canvas color of ColorView, set to these options.
         *
         * @return The canvas (background) color specified via {@link #canvasColor(int)} or
         * {@link Color#WHITE} by default.
         * @see #canvasColor(int)
         */
        @ColorInt public int canvasColor() {
            return canvasColor;
        }

        /**
         */
        @Override protected void onParseXmlAttribute(
        		@NonNull final XmlResourceParser xmlParser,
		        final int index,
		        @AttrRes final int attr,
		        @NonNull final Resources resources,
		        @Nullable final Resources.Theme theme
        ) {
            if (attr == R.attr.dialogColor) {
                color(xmlParser.getAttributeIntValue(index, color));
            } else if (attr == R.attr.dialogColorCanvas) {
                canvasColor(xmlParser.getAttributeIntValue(index, canvasColor));
            } else {
                super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
            }
        }
    }
}