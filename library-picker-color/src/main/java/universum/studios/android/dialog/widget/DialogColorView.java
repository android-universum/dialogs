/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.DialogView;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link View} implementation used to present a picked color within {@link DialogColorView}.
 * <p>
 * This view class simply draws a color specified via {@link #setColor(int)} on the {@link Canvas}.
 * You can use {@link #setBackgroundColor(int)} to present better contrast for the picked color and
 * the background on which is drawn.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogColorView extends View implements DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogColorView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Raw color specified by {@link #setColor(int)}. This color is used as base for color used to
	 * draw primary graphics of this view.
	 */
	private int mRawColor = Color.WHITE;

	/**
	 * Modified {@link #mRawColor} by the current alpha value of this view.
	 */
	private int mDrawColor = mRawColor;

	/**
	 * Maximum size dimension of this view.
	 */
	private int mMaxWidth, mMaxHeight;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #DialogColorView(Context, AttributeSet)} without attributes.
	 */
	public DialogColorView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #DialogColorView(Context, AttributeSet, int)} with
	 * {@link R.attr#dialogColorViewStyle dialogColorViewStyle} as attribute for default style.
	 */
	public DialogColorView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, R.attr.dialogColorViewStyle);
	}

	/**
	 * Same as {@link #DialogColorView(Context, AttributeSet, int, int)} with {@code 0} as default style.
	 */
	public DialogColorView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of DialogColorView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public DialogColorView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		this.mMaxWidth = mMaxHeight = Integer.MAX_VALUE;
		final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.Dialog_ColorView, defStyleAttr, defStyleRes);
		for (int i = 0; i < attributes.getIndexCount(); i++) {
			final int index = attributes.getIndex(i);
			if (index == R.styleable.Dialog_ColorView_android_color) {
				setColor(attributes.getColor(index, mRawColor));
			} else if (index == R.styleable.Dialog_ColorView_android_maxWidth) {
				this.mMaxWidth = attributes.getDimensionPixelSize(index, mMaxWidth);
			} else if (index == R.styleable.Dialog_ColorView_android_maxHeight) {
				this.mMaxHeight = attributes.getDimensionPixelSize(index, mMaxHeight);
			}
		}
		attributes.recycle();
	}

	/**
	 * Sets a color to be drawn by this color view.
	 *
	 * @param color The desired color.
	 * @see #getColor()
	 */
	public void setColor(@ColorInt final int color) {
		if (mRawColor != color || mDrawColor != color) {
			this.mRawColor = mDrawColor = color;
			invalidate();
		}
	}

	/**
	 * Returns the current color drawn by this color view.
	 *
	 * @return Current color.
	 * @see #setColor(int)
	 */
	@ColorInt public int getColor() {
		return mDrawColor;
	}

	/**
	 */
	@Override protected boolean onSetAlpha(final int alpha) {
		this.mDrawColor = (mRawColor << 8 >>> 8) | (alpha << 24);
		return true;
	}

	/**
	 */
	@Override protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int width = getMeasuredWidth();
		int height = getMeasuredHeight();
		// Take into count maximum size.
		width = Math.min(width, mMaxWidth);
		height = Math.min(height, mMaxHeight);
		setMeasuredDimension(width, height);
	}

	/**
	 */
	@Override protected void onDraw(@NonNull final Canvas canvas) {
		super.onDraw(canvas);
		if ((mDrawColor >>> 24) != 0) {
			canvas.drawColor(mDrawColor);
		}
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.color = mRawColor;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		setColor(savedState.color);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link DialogColorView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		int color;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.color = source.readInt();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(color);
		}
	}
}