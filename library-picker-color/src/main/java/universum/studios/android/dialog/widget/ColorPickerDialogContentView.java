/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.ColorPickerDialog;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.ColorPickerDialogView;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link ColorPickerDialogView} implementation used by {@link ColorPickerDialog ColorPickerDialog}
 * as content view.
 * <p>
 * This class represents a container for one {@link DialogColorView} and set of
 * {@link DialogColorSliderLayout DialogColorSliderLayouts} to allow picking of <b>red, green, blue</b>
 * and <b>alpha</b> components for RGBA color.
 * <p>
 * The current picked color within color view can be obtained via {@link #getColor()} or specified
 * via {@link #setColor(int)} which will also update the current progress within RGBA sliders. Also
 * the background color of the color view can be changed via {@link #setCanvasColor(int)} for color
 * that better contrasts with the current dialog background. To specify whether a user can also pick
 * value of the <b>alpha</b> color component, call {@link #setAlphaInputVisible(boolean)} with either
 * {@code true} or {@code false}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutColorContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_color}  for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ColorPickerDialogContentView extends LinearLayout implements ColorPickerDialogView {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "ColorPickerDialogContentView";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Color view used to present picked color.
     */
    private DialogColorView mColorView;

    /**
     * Slider layout for red color component.
     */
    private DialogColorSliderLayout mSliderLayoutR;

    /**
     * Slider layout for green color component.
     */
    private DialogColorSliderLayout mSliderLayoutG;

    /**
     * Slider layout for blue color component.
     */
    private DialogColorSliderLayout mSliderLayoutB;

    /**
     * Slider layout for alpha color component.
     */
    private DialogColorSliderLayout mSliderLayoutA;

    /**
     * Background color of the ColorView.
     */
    private int mCanvasColor;

    /**
     * Boolean flag indicating whether the change in value of one of color component sliders should
     * be ignored or not.
     */
    private boolean mIgnoreSliderChange = false;

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #ColorPickerDialogContentView(Context, AttributeSet)} without attributes.
     */
    public ColorPickerDialogContentView(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #ColorPickerDialogContentView(Context, AttributeSet, int)} with {@code 0} as
     * attribute for default style.
     */
    public ColorPickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs, 0, 0);
    }

    /**
     * Same as {@link #ColorPickerDialogContentView(Context, AttributeSet, int, int)} with {@code 0}
     * as default style.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ColorPickerDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    /**
     * Creates a new instance of ColorPickerDialogContentView for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     * @param defStyleRes  Resource id of the default style for the new view.
     */
    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ColorPickerDialogContentView(
            @NonNull final Context context,
            @Nullable final AttributeSet attrs,
            @AttrRes final int defStyleAttr,
            @StyleRes final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Called from one of constructors of this view to perform its initialization.
     * <p>
     * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
     * this view specific data from it that can be used to configure this new view instance. The
     * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
     * from the current theme provided by the specified <var>context</var>.
     */
    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        final Resources.Theme theme = context.getTheme();
        final TypedValue typedValue = new TypedValue();
        // Resolve layout resource from which to inflate view hierarchy.
        int layoutResource = R.layout.dialog_layout_color;
        if (theme.resolveAttribute(R.attr.dialogLayoutColorContent, typedValue, true)) {
            layoutResource = typedValue.resourceId;
        }
        this.inflateHierarchy(context, layoutResource);
        // Resolve default color.
        int color = Color.WHITE;
        if (theme.resolveAttribute(R.attr.colorAccent, typedValue, true)) {
            color = typedValue.data;
        }
        setColor(color);
    }

    /**
     * Called to inflate a view hierarchy of this view.
     *
     * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
     *                 layout resource as view hierarchy for this view.
     * @param resource Resource id of the layout which should represent a view hierarchy of this view.
     */
    private void inflateHierarchy(final Context context, final int resource) {
        if (resource > 0) {
            LayoutInflater.from(context).inflate(resource, this);
            this.mColorView = (DialogColorView) findViewById(R.id.dialog_color_view);
            this.mSliderLayoutR = (DialogColorSliderLayout) findViewById(R.id.dialog_color_slider_layout_r);
            this.mSliderLayoutG = (DialogColorSliderLayout) findViewById(R.id.dialog_color_slider_layout_g);
            this.mSliderLayoutB = (DialogColorSliderLayout) findViewById(R.id.dialog_color_slider_layout_b);
            this.mSliderLayoutA = (DialogColorSliderLayout) findViewById(R.id.dialog_color_slider_layout_a);
            final DialogColorSliderLayout.OnValueChangeListener valueChangeListener = new DialogColorSliderLayout.OnValueChangeListener() {

                /**
                 */
                @Override public void onValueChanged(@NonNull final DialogColorSliderLayout sliderLayout, final int value) {
                    handleSliderValueChange(sliderLayout.getId(), value);
                }
            };
            if (mSliderLayoutR != null) mSliderLayoutR.setOnValueChangeListener(valueChangeListener);
            if (mSliderLayoutG != null) mSliderLayoutG.setOnValueChangeListener(valueChangeListener);
            if (mSliderLayoutB != null) mSliderLayoutB.setOnValueChangeListener(valueChangeListener);
            if (mSliderLayoutA != null) mSliderLayoutA.setOnValueChangeListener(valueChangeListener);
            onFinishInflate();
        }
    }

    /**
     * Handles change in the specified <var>value</var> of the specified <var>colorComponent</var>.
     *
     * @param colorComponent Identifier of the color component of which value has been changed.
     * @param value          Changed value of the color component.
     */
    final void handleSliderValueChange(final int colorComponent, final int value) {
        if (mColorView == null || mIgnoreSliderChange) {
            return;
        }
        final int color = mColorView.getColor();
        // Isolate color components so we can update them separately.
        int alpha = color >>> 24;
        int red = (color >> 16) & 0xFF;
        int green = (color >> 8) & 0xFF;
        int blue = color & 0xFF;
        if (colorComponent == R.id.dialog_color_slider_layout_r) {
            red = value;
        } else if (colorComponent == R.id.dialog_color_slider_layout_g) {
            green = value;
        } else if (colorComponent == R.id.dialog_color_slider_layout_b) {
            blue = value;
        } else if (colorComponent == R.id.dialog_color_slider_layout_a) {
            alpha = value;
        }
        mColorView.setColor((alpha << 24) | (red << 16) | (green << 8) | blue);
    }

    /**
     */
    @Override public void setColor(@ColorInt final int color) {
        this.updateSliders(color);
        if (mColorView != null) mColorView.setColor(color);
    }

    /**
     * Updates color slider layouts with values of color components from the specified <var>color</var>.
     *
     * @param color The color used to extract values of components.
     */
    private void updateSliders(final int color) {
        if (mSliderLayoutR == null || mSliderLayoutG == null || mSliderLayoutB == null || mSliderLayoutA == null) {
            return;
        }
        this.mIgnoreSliderChange = true;
        mSliderLayoutR.setValue((color >> 16) & 0xFF);
        mSliderLayoutG.setValue((color >> 8) & 0xFF);
        mSliderLayoutB.setValue(color & 0xFF);
        mSliderLayoutA.setValue(color >>> 24);
        this.mIgnoreSliderChange = false;
    }

    /**
     */
    @Override @ColorInt public int getColor() {
        return mColorView != null ? mColorView.getColor() : 0;
    }

    /**
     */
    @Override public void setCanvasColor(@ColorInt final int color) {
        if (mCanvasColor != color) {
            this.mCanvasColor = color;
            if (mColorView != null) {
                mColorView.setBackgroundColor(color);
            }
        }
    }

    /**
     */
    @Override @ColorInt public int getCanvasColor() {
        return mCanvasColor;
    }

    /**
     */
    @Override public void setAlphaInputVisible(final boolean visible) {
        if (mSliderLayoutA != null) mSliderLayoutA.setVisibility(visible ? VISIBLE : GONE);
    }

    /**
     */
    @Override public boolean isAlphaInputVisible() {
        return mSliderLayoutA != null && mSliderLayoutA.getVisibility() == View.VISIBLE;
    }

    /**
     */
    @Override protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.canvasColor = mCanvasColor;
        savedState.alphaInputVisible = mSliderLayoutA != null && mSliderLayoutA.getVisibility() == View.VISIBLE;
        return savedState;
    }

    /**
     */
    @Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        final SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCanvasColor(savedState.canvasColor);
        setAlphaInputVisible(savedState.alphaInputVisible);
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link WidgetSavedState} implementation used to ensure that the state of {@link ColorPickerDialogContentView}
     * is properly saved.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class SavedState extends WidgetSavedState {

        /**
         * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
         */
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            /**
             */
            @Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

            /**
             */
            @Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
        };

        /**
         */
        int canvasColor;

        /**
         */
        boolean alphaInputVisible = true;

        /**
         * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
         * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
         *
         * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
         *                   {@code onSaveInstanceState()}.
         */
        protected SavedState(@NonNull final Parcelable superState) {
            super(superState);
        }

        /**
         * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected SavedState(@NonNull final Parcel source) {
            super(source);
            this.canvasColor = source.readInt();
            this.alphaInputVisible = source.readInt() == 1;
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(canvasColor);
            dest.writeInt(alphaInputVisible ? 1 : 0);
        }
    }
}