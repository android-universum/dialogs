/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcelable;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.WrapperListAdapter;

import universum.studios.android.dialog.AdapterDialog;
import universum.studios.android.dialog.view.AdapterDialogView;
import universum.studios.android.ui.widget.StatefulAdapter;

/**
 * A {@link BaseAdapterDialogContentView} implementation used by implementations of {@link AdapterDialog AdapterDialog}
 * as theirs content view.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @param <V> Type of the adapter view specific for the DialogAdapterContent implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class AdapterDialogContentView<V extends AdapterView> extends BaseAdapterDialogContentView<Adapter, V> implements AdapterDialogView<V> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "AdapterDialogContentView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #AdapterDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public AdapterDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #AdapterDialogContentView(Context, AttributeSet, int)} with {@code 0} as attribute
	 * for default style.
	 */
	public AdapterDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Same as {@link #AdapterDialogContentView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	public AdapterDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of AdapterDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public AdapterDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void setOnItemClickListener(@Nullable final AdapterView.OnItemClickListener listener) {
		if (mAdapterView != null) mAdapterView.setOnItemClickListener(listener);
	}

	/**
	 */
	@Override public void setOnItemLongClickListener(@Nullable final AdapterView.OnItemLongClickListener listener) {
		if (mAdapterView != null) {
			mAdapterView.setOnItemLongClickListener(listener);
			if (listener == null && mAdapterView.isLongClickable()) {
				mAdapterView.setLongClickable(false);
			}
		}
	}

	/**
	 */
	@Override public void setOnItemSelectedListener(@Nullable final AdapterView.OnItemSelectedListener listener) {
		if (mAdapterView != null) mAdapterView.setOnItemSelectedListener(listener);
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override protected void onAttachAdapter(@NonNull final AdapterView adapterView, @Nullable final Adapter adapter) {
		adapterView.setAdapter(adapter);
	}

	/**
	 */
	@Override protected void onAttachEmptyView(@NonNull final AdapterView adapterView, @Nullable final View emptyView) {
		adapterView.setEmptyView(emptyView);
	}

	/**
	 */
	@Override protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mAdapterView != null) {
			// Re-measure the adapter view so its items will take up its full width.
			mAdapterView.measure(
					View.MeasureSpec.makeMeasureSpec(
							mAdapterView.getMeasuredWidth(),
							View.MeasureSpec.EXACTLY
					),
					heightMeasureSpec
			);
		}
	}

	/**
	 */
	@Override @Nullable protected Parcelable onSaveAdapterState(@NonNull final Adapter adapter) {
		return saveAdapterState(adapter);
	}

	/**
	 * Saves the current state of the specified <var>adapter</var>.
	 *
	 * @param adapter The adapter of which state to save. Should be instance of {@link StatefulAdapter}
	 *                or one of wrapper adapter ({@link WrapperListAdapter}) implementations.
	 * @return Adapter's saved state or {@code null} or empty state if the adapter does not save its
	 * state or it is not a stateful adapter.
	 */
	private Parcelable saveAdapterState(final Adapter adapter) {
		if (adapter instanceof StatefulAdapter) {
			return ((StatefulAdapter) adapter).saveInstanceState();
		} else if (adapter instanceof WrapperListAdapter) {
			return saveAdapterState(((WrapperListAdapter) adapter).getWrappedAdapter());
		}
		return null;
	}

	/**
	 */
	@Override protected void onRestoreAdapterState(@NonNull final Adapter adapter, @NonNull final Parcelable adapterState) {
		this.restoreAdapterState(adapter, adapterState);
	}

	/**
	 * Restores the saved state of the specified <var>adapter</var>.
	 *
	 * @param adapter      The adapter of which state to restore. Should be instance of {@link StatefulAdapter}
	 *                     or one of wrapper adapter ({@link WrapperListAdapter}) implementations.
	 * @param adapterState The previously saved adapter state via {@link #saveAdapterState(Adapter)}.
	 */
	private void restoreAdapterState(final Adapter adapter, final Parcelable adapterState) {
		if (adapter instanceof StatefulAdapter) {
			((StatefulAdapter) adapter).restoreInstanceState(adapterState);
		} else if (adapter instanceof WrapperListAdapter) {
			restoreAdapterState(((WrapperListAdapter) adapter).getWrappedAdapter(), adapterState);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}