/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

import universum.studios.android.dialog.R;
import universum.studios.android.ui.UiConfig;

/**
 * Simple manager used to tint over-scroll graphics.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class OverScrollTintManager {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "OverScrollTintManager";

	/**
	 * Name of the <b>edge</b> over-scroll drawable placed within Android resources.
	 */
	private static final String OVER_SCROLL_EDGE_DRAWABLE_NAME = "overscroll_edge";

	/**
	 * Name of the <b>glow</b> over-scroll drawable placed within Android resources.
	 */
	private static final String OVER_SCROLL_GLOW_DRAWABLE_NAME = "overscroll_glow";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Boolean flag indicating whether a tint has been applied to the over-scroll drawables or not.
	 */
	private static boolean sOverScrollDrawablesTinted;

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private OverScrollTintManager() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Applies a tint determined by the <b>edge effect</b> color presented within a theme of the
     * specified <var>context</var> to the Android over-scroll drawables.
	 * <p>
	 * <b>Note</b>, that for the Android versions above {@link android.os.Build.VERSION_CODES#LOLLIPOP LOLLIPOP}
     * (including) this method does nothing.
	 *
	 * @param context The context used to obtain current theme and also drawables from the Android
	 *                resources that will be tinted.
	 */
	static void applyOverScrollTint(final Context context) {
		if (!UiConfig.MATERIALIZED && !sOverScrollDrawablesTinted) {
            OverScrollTintManager.sOverScrollDrawablesTinted = true;
            final Resources.Theme theme = context.getTheme();
            final TypedValue typedValue = new TypedValue();
            int edgeEffectColor = Color.TRANSPARENT;
            if (theme.resolveAttribute(R.attr.dialogColorEdgeEffect, typedValue, true)) {
                edgeEffectColor = typedValue.data;
            }
			if (edgeEffectColor != Color.TRANSPARENT) {
                setOverScrollDrawablesTint(context.getResources(), edgeEffectColor);
            }
		}
	}

	/**
	 * Sets a tint color for the over-scroll drawables (edge + glow) obtained from the Android
	 * resources.
	 *
	 * @param resources Resources used to obtain particular drawables to be tinted.
	 * @param tintColor The color used as tint.
	 */
	private static void setOverScrollDrawablesTint(final Resources resources, final int tintColor) {
		final Drawable androidEdge = obtainAndroidDrawable(resources, OVER_SCROLL_EDGE_DRAWABLE_NAME);
		if (androidEdge != null) {
			androidEdge.setColorFilter(tintColor, PorterDuff.Mode.SRC_IN);
		}
		final Drawable androidGlow = obtainAndroidDrawable(resources, OVER_SCROLL_GLOW_DRAWABLE_NAME);
		if (androidGlow != null) {
			androidGlow.setColorFilter(tintColor, PorterDuff.Mode.SRC_IN);
		}
	}

	/**
	 * Obtains the drawable with the specified <var>name</var> from the Android resources.
	 *
	 * @param resources Resources used to obtain the desired drawable.
	 * @param name      Name of the drawable to obtain.
	 * @return Android drawable or {@code null} if no such a drawable exists in the Android resources.
	 */
	private static Drawable obtainAndroidDrawable(final Resources resources, final String name) {
		final int drawableId = resources.getIdentifier(name, "drawable", "android");
		return drawableId != 0 ? resources.getDrawable(drawableId) : null;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}