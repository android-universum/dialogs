/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link BaseAdapter} implementation that specifies simple API and structure to manage data set
 * of custom items. This adapter supports changing of the current data set via {@link #changeItems(Object[])}
 * and obtaining an item for a requested position via {@link #getItem(int)}.
 * <p>
 * In the simplest implementation case of this adapter, only {@link #onCreateView(ViewGroup, int)}
 * and {@link #onBindViewHolder(Object, int)} methods are required to be implemented to take a full
 * advantage of this adapter class.
 *
 * @param <I>  Type of the item presented within a data set of a subclass of this DialogBaseAdapter.
 * @param <VH> Type of the view holder used within a subclass of this SimpleAdapter.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class DialogBaseAdapter<I, VH> extends BaseAdapter {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogBaseAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Context in which is this adapter presented.
	 */
	protected final Context mContext;

	/**
	 * Application resources.
	 */
	protected final Resources mResources;

	/**
	 * Layout inflater used to inflate new views for this adapter.
	 */
	protected final LayoutInflater mLayoutInflater;

	/**
	 * Data set of this adapter.
	 */
	List<I> mItems;

	/**
	 * Type of view for the current iterated position.
	 */
	int mCurrentViewType;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #DialogBaseAdapter(Context, LayoutInflater, List)} so the given <var>items</var>
	 * array will be converted to List.
	 */
	public DialogBaseAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @NonNull final I[] items) {
		this(context, inflater, Arrays.asList(items));
	}

	/**
	 * Creates a new instance of DialogBaseAdapter with the given <var>items</var> data set.
	 *
	 * @param context  Context in which will be this adapter used.
	 * @param inflater Layout inflater that should be used to inflate views for the adapter.
	 * @param items    List of items to be used as initial data set for this adapter.
	 */
	public DialogBaseAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater, @NonNull final List<I> items) {
		this(context, inflater);
		this.mItems = items;
	}

	/**
	 * Creates a new instance of DialogBaseAdapter without initial data set.
	 *
	 * @param context  Context in which will be this adapter used.
	 * @param inflater Layout inflater that should be used to inflate views for the adapter.
	 */
	public DialogBaseAdapter(@NonNull final Context context, @NonNull final LayoutInflater inflater) {
		this.mContext = context;
		this.mResources = context.getResources();
		this.mLayoutInflater = inflater;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the current data set of this adapter.
	 *
	 * @return Data set of this adapter or {@code null} if there is no data set presented within
	 * this adapter.
	 */
	@Nullable public List<I> getItems() {
		return mItems;
	}

	/**
	 * Like {@link #changeItems(List)}, but this will also return the old data set.
	 */
	@Nullable public List<I> swapItems(@Nullable final List<I> items) {
		final List<I> oldItems = mItems;
		changeItems(items);
		return oldItems;
	}

	/**
	 * Same as {@link #changeItems(List)} so the given <var>items</var> array will be converted
	 * to List.
	 */
	public void changeItems(@NonNull final I[] items) {
		changeItems(Arrays.asList(items));
	}

	/**
	 * Changes the current data set of this adapter.
	 * <p>
	 * This will also notify data set change if the given <var>items</var> are valid, otherwise will
	 * notify data set invalidation.
	 *
	 * @param items A set of items to set as the current data set for this adapter.
	 * @see #swapItems(List)
	 * @see #clearItems()
	 */
	public void changeItems(@Nullable final List<I> items) {
		this.mItems = items;
		if (items != null) {
			notifyDataSetChanged();
		} else {
			notifyDataSetInvalidated();
		}
	}

	/**
	 * Clears the current data set of this adapter.
	 * <p>
	 * This will also notify data set change.
	 */
	public void clearItems() {
		if (mItems != null) {
			mItems.clear();
			notifyDataSetChanged();
		}
	}

	/**
	 */
	@Override public int getCount() {
		return mItems != null ? mItems.size() : 0;
	}

	/**
	 */
	@Override @NonNull public I getItem(final int position) {
		final int itemsCount = getCount();
		if (position < 0 || position >= itemsCount) {
			throw new IndexOutOfBoundsException(
					"Requested item at invalid position(" + position + "). " +
							"Current data set has items in count of(" + itemsCount + ")."
			);
		}
		return mItems.get(position);
	}

	/**
	 */
	@Override public long getItemId(final int position) {
		return position;
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
		Object viewHolder;
		// Obtain current item view type.
		this.mCurrentViewType = getItemViewType(position);
		if (convertView == null) {
			// Dispatch to create new view.
			convertView = onCreateView(parent, position);
			// Resolve holder for the newly created view.
			final Object holder = onCreateViewHolder(convertView, position);
			if (holder != null) {
				convertView.setTag(viewHolder = holder);
			} else {
				viewHolder = convertView;
			}
		} else {
			final Object holder = convertView.getTag();
			viewHolder = holder != null ? holder : convertView;
		}
		ensureViewHolderPosition(viewHolder, position);
		// Dispatch to bind view holder with data.
		onBindViewHolder((VH) viewHolder, position);
		return convertView;
	}

	/**
	 * Returns a type of an item's view for the currently iterated position.
	 *
	 * @return View type provided by {@link #getItemViewType(int)} for the currently iterated position.
	 */
	protected final int currentViewType() {
		return mCurrentViewType;
	}

	/**
	 * Invoked to create a view for an item from the current data set at the specified position.
	 * <p>
	 * This is invoked only if <var>convertView</var> for the specified <var>position</var> in
	 * {@link #getView(int, View, ViewGroup)} was {@code null}.
	 *
	 * @param parent   A parent view, to resolve correct layout params for the newly creating view.
	 * @param position Position of the item from the current data set for which should be a new view
	 *                 created.
	 * @return New instance of the requested view.
	 * @see #inflate(int, ViewGroup)
	 */
	@NonNull protected abstract View onCreateView(@NonNull ViewGroup parent, int position);

	/**
	 * Inflates a new view hierarchy from the given xml resource.
	 *
	 * @param resource Resource id of a view to inflate.
	 * @param parent   A parent view, to resolve correct layout params for the newly creating view.
	 * @return The root view of the inflated view hierarchy.
	 * @see LayoutInflater#inflate(int, ViewGroup)
	 */
	@NonNull protected final View inflate(@LayoutRes final int resource, @NonNull final ViewGroup parent) {
		return mLayoutInflater.inflate(resource, parent, false);
	}

	/**
	 * Invoked to create a view holder for a view of an item from the current data set at the
	 * specified position.
	 * <p>
	 * This is invoked only if <var>convertView</var> for the specified <var>position</var> in
	 * {@link #getView(int, View, ViewGroup)} was {@code null}, so as
	 * view also holder need to be created.
	 *
	 * @param itemView An instance of the same view as obtained from  {@link #onCreateView(ViewGroup, int)}
	 *                 for the specified position.
	 * @param position Position of the item from the current data set for which should be a new view
	 *                 holder created.
	 * @return New instance of the requested view holder.
	 */
	@SuppressWarnings("unchecked")
	@Nullable protected VH onCreateViewHolder(@NonNull final View itemView, final int position) {
		// Return null holder, so view created by onCreateView(..) will be passed as holder to onBindViewHolder(..).
		return null;
	}

	/**
	 * Ensures that the specified <var>viewHolder</var> has updated position to the specified one.
	 * <p>
	 * <b>Note</b>, that position will be updated only for holder instance of {@link DialogViewHolder}.
	 *
	 * @param viewHolder The view holder of which position to update.
	 * @param position   The position of the holder.
	 */
	final void ensureViewHolderPosition(final Object viewHolder, final int position) {
		if (viewHolder instanceof DialogViewHolder)
			((DialogViewHolder) viewHolder).updateAdapterPosition(position);
	}

	/**
	 * Invoked to set up and populate a view of an item from the current data set at the specified
	 * position. This is invoked whenever {@link #getView(int, View, ViewGroup)}
	 * is called.
	 * <p>
	 * <b>Note</b>, that if {@link #onCreateViewHolder(View, int)} returns {@code null}
	 * for the specified <var>position</var> here passed <var>viewHolder</var> will be the view created
	 * by {@link #onCreateView(ViewGroup, int)} for the specified position or just recycled
	 * view for such position. This approach can be used, when a view hierarchy of the specific item
	 * is represented by one custom view, where such a view represents a holder for all its child views.
	 *
	 * @param viewHolder An instance of the same holder as provided by {@link #onCreateViewHolder(View, int)}
	 *                   for the specified position or converted view as holder as described above.
	 * @param position   Position of the item from the current data set of which view to set up.
	 */
	protected abstract void onBindViewHolder(@NonNull VH viewHolder, int position);

	/*
	 * Inner classes ===============================================================================
	 */
}