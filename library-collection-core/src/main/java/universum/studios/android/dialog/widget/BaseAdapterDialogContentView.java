/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.AttrRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import universum.studios.android.dialog.BaseAdapterDialog;
import universum.studios.android.dialog.DialogsConfig;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.BaseAdapterDialogView;
import universum.studios.android.ui.widget.FrameLayoutWidget;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * An {@link BaseAdapterDialogView} implementation used by inheritance hierarchies of
 * {@link BaseAdapterDialog BaseAdapterDialog} as base for theirs content view.
 * <p>
 * This class represents a layout container for a single adapter view along with its empty view.
 * Adapter for adapter view can be specified via {@link #setAdapter(Object)} and text for the empty
 * view can be specified via {@link #setEmptyText(CharSequence)} or its resource equivalent
 * {@link #setEmptyText(int)}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @param <A> Type of the adapter that can be attached to the related adapter view.
 * @param <V> Type of the adapter view specific for the DialogBaseAdapterContent implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class BaseAdapterDialogContentView<A, V extends ViewGroup> extends FrameLayoutWidget implements BaseAdapterDialogView<A, V> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseAdapterDialogContentView";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Layout resource used to inflate {@link #mEmptyView}.
	 */
	private int mEmptyViewResource;

	/**
	 * View used as empty for {@link #mAdapterView}.
	 */
	private View mEmptyView;

	/**
	 * Text specified for {@link #mEmptyView}.
	 */
	private CharSequence mEmptyText;

	/**
	 * Adapter view specific for this base dialog view.
	 */
	V mAdapterView;

	/**
	 * Adapter specified for {@link #mAdapterView}.
	 */
	A mAdapter;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseAdapterDialogContentView(Context, AttributeSet)} without attributes.
	 */
	public BaseAdapterDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #BaseAdapterDialogContentView(Context, AttributeSet, int)} with {@code 0} as
	 * attribute for default style.
	 */
	public BaseAdapterDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Same as {@link #BaseAdapterDialogContentView(Context, AttributeSet, int, int)} with {@code 0}
	 * as default style.
	 */
	public BaseAdapterDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * Creates a new instance of BaseAdapterDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public BaseAdapterDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	@SuppressWarnings("unchecked")
	final void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			if (getChildCount() > 0) {
				removeAllViews();
			}
			LayoutInflater.from(context).inflate(resource, this);
			final View adapterView = findViewById(R.id.dialog_adapter_view);
			if (onHierarchyInflationFinished(adapterView)) {
				this.mAdapterView = (V) adapterView;
			}
			this.mEmptyView = findViewById(R.id.dialog_empty_view);
			onFinishInflate();
			if (mAdapterView != null) onAttachEmptyView(mAdapterView, mEmptyView);
		}
	}

	/**
	 * Invoked whenever {@link #inflateHierarchy(Context, int)} has been called and a new view
	 * hierarchy for this dialog content view has been inflated.
	 *
	 * @param adapterView The view found by {@link R.id#dialog_adapter_view} id.
	 * @return {@code True} whenever the adapter view is of proper type for this adapter content view,
	 * {@code false} if not.
	 */
	boolean onHierarchyInflationFinished(@Nullable final View adapterView) {
		return false;
	}

	/**
	 */
	@Override public void setEmptyView(@LayoutRes final int resource) {
		if (resource == 0) {
			setEmptyView(null);
		} else if (mEmptyViewResource != resource) {
			setEmptyView(LayoutInflater.from(getContext()).inflate(
					mEmptyViewResource = resource,
					this,
					false
			));
		}
	}

	/**
	 */
	@Override public void setEmptyView(@Nullable final View view) {
		if (mEmptyView != null) {
			removeView(mEmptyView);
		}
		if ((mEmptyView = view) != null) {
			if (view instanceof TextView) {
				((TextView) view).setText(mEmptyText);
			}
			addView(view, 0);
		} else {
			this.mEmptyViewResource = 0;
		}
		if (mAdapterView != null) onAttachEmptyView(mAdapterView, mEmptyView);
	}

	/**
	 * Invoked to attach the given <var>emptyView</var> to the specified <var>adapterView</var>.
	 * <p>
	 * This method will be called whenever {@link #setEmptyView(View)} is called.
	 *
	 * @param adapterView Adapter view type of specific for this base dialog view to which to attach
	 * @param emptyView   The empty view passed to {@link #setEmptyView(View)} method.
	 *                    the empty view.
	 */
	protected void onAttachEmptyView(@NonNull V adapterView, @Nullable View emptyView) {
		// May be implemented by the inheritance hierarchies.
	}

	/**
	 */
	@Override @Nullable public View getEmptyView() {
		return mEmptyView;
	}

	/**
	 */
	@Override public void setEmptyText(@StringRes final int resId) {
		setEmptyText(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setEmptyText(@Nullable final CharSequence text) {
		this.mEmptyText = text;
		if (mEmptyView instanceof TextView) {
			((TextView) mEmptyView).setText(text);
		}
	}

	/**
	 */
	@Override @NonNull public CharSequence getEmptyText() {
		return mEmptyText != null ? mEmptyText : "";
	}

	/**
	 */
	@Override @NonNull public V getAdapterView() {
		return mAdapterView;
	}

	/**
	 */
	@Override public void setAdapter(@Nullable final A adapter) {
		this.mAdapter = adapter;
		if (mAdapterView != null) onAttachAdapter(mAdapterView, mAdapter);
	}

	/**
	 * Invoked to attach the given <var>adapter</var> to the specified <var>adapterView</var>.
	 * <p>
	 * This method will be called whenever {@link #setAdapter(Object)} is called.
	 *
	 * @param adapterView Adapter view type of specific for this base dialog view to which to attach
	 * @param adapter     The adapter passed to {@link #setAdapter(Object)} method.
	 *                    the adapter.
	 */
	protected abstract void onAttachAdapter(@NonNull V adapterView, @Nullable A adapter);

	/**
	 */
	@Override @Nullable public A getAdapter() {
		return mAdapter;
	}

	/**
	 */
	@Override public void setPadding(final int left, final int top, final int right, final int bottom) {
		if (mAdapterView != null) mAdapterView.setPadding(left, top, right, bottom);
	}

	/**
	 */
	@Override public int getPaddingLeft() {
		return mAdapterView != null ? mAdapterView.getPaddingLeft() : super.getPaddingLeft();
	}

	/**
	 */
	@Override public int getPaddingTop() {
		return mAdapterView != null ? mAdapterView.getPaddingTop() : super.getPaddingTop();
	}

	/**
	 */
	@Override public int getPaddingRight() {
		return mAdapterView != null ? mAdapterView.getPaddingRight() : super.getPaddingRight();
	}

	/**
	 */
	@Override public int getPaddingBottom() {
		return mAdapterView != null ? mAdapterView.getPaddingBottom() : super.getPaddingBottom();
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.emptyViewResource = mEmptyViewResource;
		savedState.emptyText = mEmptyView instanceof TextView ? ((TextView) mEmptyView).getText() : mEmptyText;
		if (mAdapter != null) {
			final Parcelable adapterState = onSaveAdapterState(mAdapter);
			if (adapterState != null) {
				savedState.adapterState = adapterState;
			}
		}
		return savedState;
	}

	/**
	 * Called from {@link #onSaveInstanceState()} to save the current state of the given <var>adapter</var>.
	 *
	 * @param adapter The current adapter of which state to save.
	 * @return Adapter's saved state or {@code null} if the adapter does not save its state.
	 * @see #onRestoreAdapterState(Object, Parcelable)
	 */
	@Nullable protected Parcelable onSaveAdapterState(@NonNull final A adapter) {
		// May be implemented by the inheritance hierarchies.
		return null;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@Nullable final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		if (savedState.emptyViewResource > 0) {
			setEmptyView(savedState.emptyViewResource);
		}
		setEmptyText(savedState.emptyText);
		if (mAdapter != null && savedState.adapterState != null) {
			onRestoreAdapterState(mAdapter, savedState.adapterState);
		}
	}

	/**
	 * Called from {@link #onRestoreInstanceState(Parcelable)} to restore the previously saved state.
	 * of the given <var>adapter</var>.
	 *
	 * @param adapter      The adapter of which state to restore.
	 * @param adapterState The previously saved adapter state. Will be the same as obtained via
	 *                     {@link #onSaveAdapterState(Object)}.
	 */
	protected void onRestoreAdapterState(@NonNull A adapter, @NonNull Parcelable adapterState) {
		// May be implemented by the inheritance hierarchies.
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link BaseAdapterDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		int emptyViewResource;

		/**
		 */
		CharSequence emptyText;

		/**
		 */
		Parcelable adapterState;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.emptyViewResource = source.readInt();
			this.emptyText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.adapterState = source.readParcelable(DialogsConfig.class.getClassLoader());
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(emptyViewResource);
			TextUtils.writeToParcel(emptyText, dest, flags);
			dest.writeParcelable(adapterState, flags);
		}
	}
}