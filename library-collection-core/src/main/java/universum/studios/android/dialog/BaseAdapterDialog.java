/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.app.Activity;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.AttrRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.BaseAdapterDialogView;
import universum.studios.android.dialog.widget.DialogDynamicLayout;

/**
 * A {@link SimpleDialog} base implementation for dialogs that can be used to present a data set of
 * items within an adapter view.
 * <p>
 * The desired adapter can be supplied via {@link #setAdapter(Object)} or by implementing {@link AdapterProvider}
 * interface by the context (Activity or Fragment) within which will be a specific BaseAdapterDialog
 * shown. The BaseAdapterDialog will automatically detect such a provider implementation and will use
 * it to obtain an instance of adapter via {@link AdapterProvider#provideDialogAdapter(Dialog)} method.
 * <p>
 * See {@link BaseAdapterOptions} for options that can be supplied to the BaseAdapterDialog
 * implementation from the desired context via its {@code #newInstance(Options)} factory method
 * or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * @param <A>  Type of the adapter that can be attached to the related adapter view.
 * @param <AV> Type of the adapter view specific for the BaseAdapterDialog implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class BaseAdapterDialog<A, AV extends ViewGroup> extends SimpleDialog {

    /*
     * Interface ===================================================================================
     */

    /**
     * Provider used by {@link BaseAdapterDialog} to provide an instance of <b>adapter</b> with data
     * set for <b>adapter view</b> of such a dialog.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public interface AdapterProvider {

        /**
         * Called by the specified <var>dialog</var> to obtain an instance of adapter with data set
         * for the dialog's adapter view.
         * <p>
         * This method is called only once from {@link android.app.DialogFragment#onActivityCreated(Bundle) onActivityCreated(Bundle)}
         * method of the calling dialog.
         * <p>
         * <b>Note, that it is a responsibility of this provider to provide adapter that is of a proper
         * type for the requesting dialog. If the provided adapter is not of the proper type, the dialog
         * may throw an exception.</b>
         *
         * @param dialog The dialog instance that requests this provider for its associated adapter.
         * @return Instance of dialog adapter with data set or {@code null} if this provider does not
         * provide adapter for the dialog.
         */
        @Nullable Object provideDialogAdapter(@NonNull Dialog dialog);
    }

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "BaseAdapterDialog";

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Adapter with data set for this dialog.
     */
    A mAdapter;

    /**
     * Adapter view presented as body view of this dialog.
     */
    private BaseAdapterDialogView<A, AV> mAdapterView;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of BaseAdapterDialog to present data from an adapter.
     *
     * @param optionsStyleAttr An attribute which contains a reference to the options (style) specific
     *                         for this type of dialog within the current dialogs theme.
     */
    protected BaseAdapterDialog(@AttrRes final int optionsStyleAttr) {
        super(optionsStyleAttr);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     */
    @Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
        return new BaseAdapterOptions(resources);
    }

    /**
     * Sets an adapter for adapter view of this dialog instance.
     *
     * @param adapter The desired adapter with data set. May be {@code null} to clear the current one.
     * @see #getAdapter()
     */
    public void setAdapter(@Nullable final A adapter) {
        this.mAdapter = adapter;
        if (mAdapterView != null) mAdapterView.setAdapter(mAdapter);
    }

    /**
     * Returns the current adapter with data set of this dialog instance.
     *
     * @return Current adapter or {@code null} if there was no adapter specified/provided yet.
     * @see #setAdapter(Object)
     */
    @Nullable public A getAdapter() {
        return mAdapter;
    }

    /**
     */
    @Override public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Check for provided adapter.
        Object adapter = null;
        final Fragment fragment = findContextFragment();
        if (fragment instanceof AdapterProvider) {
            adapter = ((AdapterProvider) fragment).provideDialogAdapter(this);
        }
        if (adapter == null) {
            final Activity activity = getActivity();
            if (activity instanceof AdapterProvider) {
                adapter = ((AdapterProvider) activity).provideDialogAdapter(this);
            }
        }
        if (adapter != null) onAttachProvidedAdapter(adapter);
    }

    /**
     * Invoked whenever there was obtained the given <var>adapter</var> from an implementation of
     * {@link AdapterProvider} for this dialog instance.
     * <p>
     * <b>Note, that it is up to the inherited dialog class to check if the provided adapter is
     * a proper type of specific for this adapter dialog. If the provided adapter is not of valid type
     * an exception should be thrown to inform the dialog provider.</b>
     *
     * @param adapter The provided adapter to be attached to the adapter view of this dialog.
     */
    protected abstract void onAttachProvidedAdapter(@NonNull Object adapter);

    /**
     */
    @Override @NonNull protected ViewGroup onCreateLayout(
    		@NonNull final LayoutInflater inflater,
		    @Nullable final ViewGroup container,
		    @Nullable final Bundle savedInstanceState
    ) {
        return new DialogDynamicLayout(inflater.getContext());
    }

    /**
     */
    @Override @NonNull protected abstract View onCreateContentView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     */
    @Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
        if (contentView instanceof BaseAdapterDialogView) {
            final BaseAdapterDialogView adapterView = (BaseAdapterDialogView) contentView;
            if (options instanceof BaseAdapterOptions) {
                final BaseAdapterOptions adapterOptions = (BaseAdapterOptions) options;
                if (adapterOptions.emptyViewResource > 0) {
                    adapterView.setEmptyView(adapterOptions.emptyViewResource);
                }
                adapterView.setEmptyText(options.mContent);
            }
        }
    }

    /**
     */
    @Override protected void onUpdateContentViewPadding() {
        // Do not update content view padding.
    }

    /**
     */
    @SuppressWarnings("unchecked")
    @Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final View contentView = getContentView();
        if (contentView instanceof BaseAdapterDialogView) {
            this.mAdapterView = (BaseAdapterDialogView) contentView;
            if (mAdapter != null) {
                mAdapterView.setAdapter(mAdapter);
            }
        }
    }

    /**
     * Returns the adapter view of this dialog instance that presents data set of the current adapter.
     *
     * @return Adapter view type of specific for this type of adapter dialog.
     */
    @Nullable public AV getAdapterView() {
        return mAdapterView != null ? mAdapterView.getAdapterView() : null;
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A {@link DialogOptions} implementation specific for the {@link BaseAdapterDialog}.
     * <p>
     * Below are listed all adapter related options that can be supplied to the BaseAdapterDialog
     * through these options:
     * <ul>
     * <li>{@link #emptyViewResource(int)}</li>
     * </ul>
     * <p>
     * <h3>Xml attributes</h3>
     * <ul>
     * <li>{@link R.attr#dialogEmptyView dialogEmptyView}</li>
     * </ul>
     *
     * @param <Options> Type of the BaseAdapterOptions implementation to allow proper methods chaining.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    protected static class BaseAdapterOptions<Options extends BaseAdapterOptions> extends DialogOptions<Options> {

        /**
         * Creator used to create an instance or array of instances of BaseAdapterOptions from {@link Parcel}.
         */
        public static final Creator<BaseAdapterOptions> CREATOR = new Creator<BaseAdapterOptions>() {

        	/**
             */
            @Override public BaseAdapterOptions createFromParcel(@NonNull final Parcel source) { return new BaseAdapterOptions(source); }

            /**
             */
            @Override public BaseAdapterOptions[] newArray(final int size) { return new BaseAdapterOptions[size]; }
        };

        /**
         * Flag for <b>empty view resource</b> parameter.
         */
        static final int EMPTY_VIEW_RESOURCE = 0x00000001 << 16;

        /**
         * Layout resource id with empty view for adapter view.
         */
        int emptyViewResource = -1;

        /**
         * Creates a new instance of BaseAdapterOptions.
         */
        public BaseAdapterOptions() {
            super();
        }

        /**
         * Creates a new instance of BaseAdapterOptions.
         *
         * @param resources Application resources used to obtain values for these options when they
         *                  are requested by theirs resource ids.
         */
        public BaseAdapterOptions(@NonNull final Resources resources) {
            super(resources);
        }

        /**
         * Called form {@link #CREATOR} to create an instance of BaseAdapterOptions form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected BaseAdapterOptions(@NonNull final Parcel source) {
            super(source);
            this.emptyViewResource = source.readInt();
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(emptyViewResource);
        }

        /**
         */
        @Override public Options merge(@NonNull final DialogOptions options) {
            if (!(options instanceof BaseAdapterOptions)) {
                return super.merge(options);
            }
            final BaseAdapterOptions adapterOptions = (BaseAdapterOptions) options;
            if (adapterOptions.isSet(EMPTY_VIEW_RESOURCE)) {
                this.emptyViewResource = adapterOptions.emptyViewResource;
                updateIsSet(EMPTY_VIEW_RESOURCE);
            }
            return super.merge(options);
        }

        /**
         * Sets the layout resource for adapter's empty view of a dialog associated with these options.
         *
         * @return These options to allow methods chaining.
         * @see R.attr#dialogEmptyView dialog:dialogEmptyView
         * @see #emptyViewResource()
         */
        @SuppressWarnings("unchecked")
        public Options emptyViewResource(@LayoutRes final int resource) {
            updateIsSet(EMPTY_VIEW_RESOURCE);
            if (emptyViewResource != resource) {
                this.emptyViewResource = resource;
                notifyChanged();
            }
            return (Options) this;
        }

        /**
         * Returns the empty view resource, set to these options.
         *
         * @return Layout resource or {@code -1} by default.
         * @see #emptyViewResource(int)
         */
        @LayoutRes public int emptyViewResource() {
            return emptyViewResource;
        }

        /**
         */
        @Override protected void onParseXmlAttribute(
        		@NonNull final XmlResourceParser xmlParser,
		        final int index,
		        @AttrRes final int attr,
		        @NonNull final Resources resources,
		        @Nullable final Resources.Theme theme
        ) {
            if (attr == R.attr.dialogEmptyView) {
                emptyViewResource(xmlParser.getAttributeResourceValue(index, emptyViewResource));
            } else {
                super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
            }
        }
    }
}