/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.widget.Adapter;
import android.widget.AdapterView;

import androidx.annotation.Nullable;
import universum.studios.android.dialog.AdapterDialog;

/**
 * A {@link BaseAdapterDialogView} extension used by {@link AdapterDialog AdapterDialog}
 * as interface layer for its content view.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.AdapterDialogContentView implementation}
 * for more info.
 *
 * @param <V> Type of the adapter view specific for the AdapterDialogView implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface AdapterDialogView<V extends AdapterView> extends BaseAdapterDialogView<Adapter, V> {

	/**
	 * Registers a callback to be invoked whenever an item is clicked within the AdapterView of this
	 * dialog view.
	 *
	 * @param listener Listener callback. May be {@code null} to clear the current one.
	 * @see AdapterView#setOnItemClickListener(AdapterView.OnItemClickListener)
	 */
	void setOnItemClickListener(@Nullable AdapterView.OnItemClickListener listener);

	/**
	 * Registers a callback to be invoked whenever an item is long-clicked within the AdapterView of
	 * this dialog view.
	 *
	 * @param listener Listener callback. May be {@code null} to clear the current one.
	 * @see AdapterView#setOnItemLongClickListener(AdapterView.OnItemLongClickListener)
	 */
	void setOnItemLongClickListener(@Nullable AdapterView.OnItemLongClickListener listener);

	/**
	 * Registers a callback to be invoked whenever an item is selected within the AdapterView of
	 * this dialog view.
	 *
	 * @param listener Listener callback. May be {@code null} to clear the current one.
	 * @see AdapterView#setOnItemSelectedListener(AdapterView.OnItemSelectedListener)
	 */
	void setOnItemSelectedListener(@Nullable AdapterView.OnItemSelectedListener listener);
}