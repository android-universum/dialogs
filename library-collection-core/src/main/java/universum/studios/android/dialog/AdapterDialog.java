/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.AttrRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import universum.studios.android.dialog.view.AdapterDialogView;

/**
 * A {@link BaseAdapterDialog} implementation for dialogs that can be used to present a data set of
 * items within an {@link AdapterView}.
 * <p>
 * <h3>Callbacks</h3>
 * All listeners related to AdapterView that can be specified via {@link #setOnDialogListener(OnDialogListener)}
 * are listed below:
 * <ul>
 * <li>{@link OnItemClickListener}</li>
 * <li>{@link OnItemLongClickListener}</li>
 * <li>{@link OnItemSelectedListener}</li>
 * </ul>
 * As in case of {@link AdapterProvider} you do not need to specify these listeners via setter method
 * but just implementing them by the context within which will be your AdapterDialog shown and the
 * dialog will automatically detect those implementations and will use them to dispatch all callbacks
 * received from AdapterView to them.
 * <p>
 * <b>Note</b>, that you need to explicitly specify which listeners you want to attach to AdapterView
 * via {@link #requestListeners(int)}. By default only {@link AdapterView.OnItemClickListener}
 * is attached.
 *
 * @param <AV> Type of the adapter view specific for the AdapterDialog implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class AdapterDialog<AV extends AdapterView> extends BaseAdapterDialog<Adapter, AV> {

    /*
     * Interface ===================================================================================
     */

    /**
     * Listener that can receive a callback about clicked item within {@link AdapterDialog}.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public interface OnItemClickListener extends OnDialogListener {

        /**
         * Invoked whenever an item at the specified <var>position</var> within the specified <var>dialog</var>
         * has been clicked.
         *
         * @param dialog      The dialog in which was item clicked.
         * @param adapterView AdapterView of the specified dialog in which is clicked item presented.
         * @param view        A view of the clicked item.
         * @param position    The item position.
         * @param id          Item id as provided by adapter's {@link Adapter#getItemId(int)}.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         * @see AdapterView.OnItemClickListener#onItemClick(AdapterView, View, int, long)
         */
        boolean onDialogItemClick(@NonNull AdapterDialog dialog, @NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id);
    }

    /**
     * Listener that can receive a callback about long-clicked item within {@link AdapterDialog}.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public interface OnItemLongClickListener extends OnDialogListener {

        /**
         * Invoked whenever an item at the specified <var>position</var> within the specified <var>dialog</var>
         * has been long-clicked.
         *
         * @param dialog      The dialog in which was item long-clicked.
         * @param adapterView AdapterView of the specified dialog in which is long-clicked item presented.
         * @param view        A view of the long-clicked item.
         * @param position    The item position.
         * @param id          Item id as provided by adapter's {@link Adapter#getItemId(int)}.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         * @see AdapterView.OnItemLongClickListener#onItemLongClick(AdapterView, View, int, long)
         */
        boolean onDialogItemLongClick(@NonNull AdapterDialog dialog, @NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id);
    }

    /**
     * Listener that can receive a callback about selected item within {@link AdapterDialog}.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public interface OnItemSelectedListener extends OnDialogListener {

        /**
         * Invoked whenever an item at the specified <var>position</var> within the specified <var>dialog</var>
         * has been selected.
         *
         * @param dialog      The dialog in which was item selected.
         * @param adapterView AdapterView of the specified dialog in which is selected item presented.
         * @param view        A view of the selected item.
         * @param position    The item position.
         * @param id          Item id as provided by adapter's {@link Adapter#getItemId(int)}.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         * @see AdapterView.OnItemSelectedListener#onItemSelected(AdapterView, View, int, long)
         */
        boolean onDialogItemSelected(@NonNull AdapterDialog dialog, @NonNull AdapterView<?> adapterView, @NonNull View view, int position, long id);

        /**
         * Invoked whenever nothing has been selected within the specified <var>dialog</var>.
         *
         * @param dialog      The dialog in which was nothing selected.
         * @param adapterView AdapterView of the specified dialog.
         * @return {@code True} if this event has been processed here, {@code false} to be propagated
         * further.
         * @see AdapterView.OnItemSelectedListener#onNothingSelected(AdapterView)
         */
        boolean onDialogNothingSelected(@NonNull AdapterDialog dialog, @NonNull AdapterView<?> adapterView);
    }

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "AdapterDialog";

    /**
     * Defines an annotation for determining set of allowed flags for {@link #requestListeners(int)}
     * method.
     */
    @IntDef(
            flag = true,
            value = {ON_ITEM_CLICK_LISTENER, ON_ITEM_LONG_CLICK_LISTENER, ON_ITEM_SELECTED_LISTENER}
    )
    @Retention(RetentionPolicy.SOURCE)
    public @interface Listeners {}

    /**
     * Flag for {@link #requestListeners(int)} to request registration of {@link AdapterView.OnItemClickListener}
     * upon the AdapterView presented within an instance of {@link AdapterDialog} implementation.
     */
    public static final int ON_ITEM_CLICK_LISTENER = 0x00000001;

    /**
     * Flag for {@link #requestListeners(int)} to request registration of {@link AdapterView.OnItemLongClickListener}
     * upon the AdapterView presented within an instance of {@link AdapterDialog} implementation.
     */
    public static final int ON_ITEM_LONG_CLICK_LISTENER = 0x00000001 << 1;

    /**
     * Flag for {@link #requestListeners(int)} to request registration of {@link AdapterView.OnItemSelectedListener}
     * upon the AdapterView presented within an instance of {@link AdapterDialog} implementation.
     */
    public static final int ON_ITEM_SELECTED_LISTENER = 0x00000001 << 2;

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Adapter view listener instance.
     */
    private final AdapterViewListener ADAPTER_VIEW_LISTENER = new AdapterViewListener();

    /**
     * Adapter view presented as body view of this dialog.
     */
    private AdapterDialogView mAdapterView;

    /**
     * Set of requested listeners flags.
     */
    private int mRequestedListeners = ON_ITEM_CLICK_LISTENER;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of AdapterDialog to present data from adapter.
     *
     * @param optionsStyleAttr An attribute which contains a reference to the options (style) specific
     *                         for this type of dialog within the current dialogs theme.
     */
    protected AdapterDialog(@AttrRes final int optionsStyleAttr) {
        super(optionsStyleAttr);
    }

    /*
     * Methods =====================================================================================
     */

    /**
     */
    @Override protected void onAttachProvidedAdapter(@NonNull final Object adapter) {
        if (!(adapter instanceof Adapter)) {
            throw new IllegalArgumentException(
                    "Provided adapter of unsupported type for AdapterDialog!" +
                            " Dialog adapter must be instance of " + Adapter.class.getName() + "."
            );
        }
        setAdapter((Adapter) adapter);
    }

    /**
     * Requests depends on the specified <var>listeners</var> flags, which item listeners should be
     * attached to the current adapter view of this dialog.
     *
     * @param listeners {@link #ON_ITEM_CLICK_LISTENER}, {@link #ON_ITEM_LONG_CLICK_LISTENER},
     *                  {@link #ON_ITEM_SELECTED_LISTENER} or theirs combination.
     */
    protected void requestListeners(@Listeners final int listeners) {
        this.mRequestedListeners = listeners;
        this.updateListeners();
    }

    /**
     */
    @Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final View contentView = getContentView();
        if (contentView instanceof AdapterDialogView) {
            this.mAdapterView = (AdapterDialogView) contentView;
            this.updateListeners();
        }
    }

    /**
     * Attaches/detaches listeners to/from the current adapter view depends on the {@link #mRequestedListeners}
     * flags.
     */
    private void updateListeners() {
        if (mAdapterView == null) {
            return;
        }
        if ((mRequestedListeners & ON_ITEM_CLICK_LISTENER) != 0) {
            mAdapterView.setOnItemClickListener(ADAPTER_VIEW_LISTENER);
        } else {
            mAdapterView.setOnItemClickListener(null);
        }
        if ((mRequestedListeners & ON_ITEM_LONG_CLICK_LISTENER) != 0) {
            mAdapterView.setOnItemLongClickListener(ADAPTER_VIEW_LISTENER);
        } else {
            mAdapterView.setOnItemLongClickListener(null);
        }
        if ((mRequestedListeners & ON_ITEM_SELECTED_LISTENER) != 0) {
            mAdapterView.setOnItemSelectedListener(ADAPTER_VIEW_LISTENER);
        } else {
            mAdapterView.setOnItemSelectedListener(null);
        }
    }

    /**
     * Delivers the item click event to this dialog instance.
     */
    final void dispatchItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        if (!onItemClick(parent, view, position, id)) {
            // Traverse event to all available listeners.
            boolean processed = mListener instanceof OnItemClickListener && ((OnItemClickListener) mListener).onDialogItemClick(this, parent, view, position, id);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnItemClickListener && ((OnItemClickListener) fragment).onDialogItemClick(this, parent, view, position, id);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnItemClickListener) {
                    ((OnItemClickListener) activity).onDialogItemClick(this, parent, view, position, id);
                }
            }
        }
    }

    /**
     * Invoked whenever {@link #ON_ITEM_CLICK_LISTENER} was requested to be set and
     * {@link AdapterView.OnItemClickListener#onItemClick(AdapterView, View, int, long)}
     * was fired for listener of this dialog instance.
     *
     * @return {@code True} if item click event was handled here, {@code false} to
     * let available listeners (listener, fragment, activity) handle this event.
     */
    protected boolean onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
        return false;
    }

    /**
     * Delivers the item long click event to this dialog instance.
     */
    final boolean dispatchItemLongClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        if (!onItemLongClick(parent, view, position, id)) {
            // Traverse event to all available listeners.
            boolean processed = mListener instanceof OnItemLongClickListener && ((OnItemLongClickListener) mListener).onDialogItemLongClick(this, parent, view, position, id);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnItemLongClickListener && ((OnItemLongClickListener) fragment).onDialogItemLongClick(this, parent, view, position, id);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnItemLongClickListener) {
                    ((OnItemLongClickListener) activity).onDialogItemLongClick(this, parent, view, position, id);
                }
            }
        }
        return true;
    }

    /**
     * Invoked whenever {@link #ON_ITEM_LONG_CLICK_LISTENER} was requested to be set and
     * {@link AdapterView.OnItemLongClickListener#onItemLongClick(AdapterView, View, int, long)}
     * was fired for listener of this dialog instance.
     *
     * @return {@code True} if item long click event was handled here, {@code false} to
     * let available listeners (listener, fragment, activity) handle this event.
     */
    protected boolean onItemLongClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
        return false;
    }

    /**
     * Delivers the item selection event to this dialog instance.
     */
    final void dispatchItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
        if (!onItemSelected(parent, view, position, id)) {
            // Traverse event to all available listeners.
            boolean processed = mListener instanceof OnItemSelectedListener && ((OnItemSelectedListener) mListener).onDialogItemSelected(this, parent, view, position, id);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnItemSelectedListener && ((OnItemSelectedListener) fragment).onDialogItemSelected(this, parent, view, position, id);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnItemSelectedListener) {
                    ((OnItemSelectedListener) activity).onDialogItemSelected(this, parent, view, position, id);
                }
            }
        }
    }

    /**
     * Invoked whenever {@link #ON_ITEM_SELECTED_LISTENER} was requested to be set and
     * {@link AdapterView.OnItemSelectedListener#onItemSelected(AdapterView, View, int, long)}
     * was fired for listener of this dialog instance.
     *
     * @return {@code True} if item selection event was handled here, {@code false} to
     * let available listeners (listener, fragment, activity) handle this event.
     */
    protected boolean onItemSelected(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
        return false;
    }

    /**
     * Delivers the nothing selected event to this dialog instance.
     */
    final void dispatchNothingSelected(final AdapterView<?> parent) {
        if (!onNothingSelected(parent)) {
            // Traverse event to all available listeners.
            boolean processed = mListener instanceof OnItemSelectedListener && ((OnItemSelectedListener) mListener).onDialogNothingSelected(this, parent);
            if (!processed) {
                final Fragment fragment = findContextFragment();
                processed = fragment instanceof OnItemSelectedListener && ((OnItemSelectedListener) fragment).onDialogNothingSelected(this, parent);
            }
            if (!processed) {
                final Activity activity = getActivity();
                if (activity instanceof OnItemSelectedListener) {
                    ((OnItemSelectedListener) activity).onDialogNothingSelected(this, parent);
                }
            }
        }
    }

    /**
     * Invoked whenever {@link #ON_ITEM_SELECTED_LISTENER} was requested to be set and
     * {@link AdapterView.OnItemSelectedListener#onNothingSelected(AdapterView)}
     * was fired for listener of this dialog instance.
     *
     * @return {@code True} if no selection event was handled here, {@code false} to
     * let available listeners (listener, fragment, activity) handle this event.
     */
    protected boolean onNothingSelected(@NonNull final AdapterView<?> parent) {
        return false;
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * This class implements all AdapterView item listeners and received callbacks dispatches to
     * instance of AdapterDialog.
     */
    private final class AdapterViewListener
            implements
            AdapterView.OnItemClickListener,
            AdapterView.OnItemLongClickListener,
            AdapterView.OnItemSelectedListener {

        /**
         */
        @Override public void onItemClick(@NonNull final AdapterView<?> parent, @Nullable final View view, final int position, final long id) {
            dispatchItemClick(parent, view, position, id);
        }

        /**
         */
        @Override public boolean onItemLongClick(@NonNull final AdapterView<?> parent, @Nullable final View view, final int position, final long id) {
            return dispatchItemLongClick(parent, view, position, id);
        }

        /**
         */
        @Override public void onItemSelected(@NonNull final AdapterView<?> parent, @Nullable final View view, final int position, final long id) {
            dispatchItemSelected(parent, view, position, id);
        }

        /**
         */
        @Override public void onNothingSelected(@NonNull final AdapterView<?> parent) {
            dispatchNothingSelected(parent);
        }
    }
}