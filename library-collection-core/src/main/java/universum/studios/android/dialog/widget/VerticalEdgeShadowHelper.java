/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

import universum.studios.android.dialog.R;
import universum.studios.android.ui.util.ResourceUtils;

/**
 * A helper that may be used to draw a shadow at vertical (top and bottom) edges of a particular view.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class VerticalEdgeShadowHelper {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "VerticalEdgeShadowHelper";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * The view that is using this helper to draw its vertical edge shadows.
	 */
	private final View mView;

	/**
	 * View's size dimension.
	 */
	private int mViewWidth, mViewHeight;

	/**
	 * Size of the shadow to be drawn.
	 */
	private int mShadowSize;

	/**
	 * Drawable used to draw shadow at one of vertical edges.
	 */
	private Drawable mDrawableTop, mDrawableBottom;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of VerticalEdgeShadowHelper for the given <var>view</var>.
	 *
	 * @param view The view that wants to use the new helper to draw its vertical edge shadows.
	 */
	VerticalEdgeShadowHelper(final View view) {
		this.mView = view;
		final Resources resources = view.getResources();
		final Resources.Theme theme = view.getContext().getTheme();
		this.mDrawableTop = ResourceUtils.getDrawable(resources, R.drawable.dialog_shadow_edge_top, theme);
		this.mDrawableBottom = ResourceUtils.getDrawable(resources, R.drawable.dialog_shadow_edge_bottom, theme);
		this.mShadowSize = resources.getDimensionPixelSize(R.dimen.dialog_collection_edge_shadow_size);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a size for the shadow to be drawn.
	 *
	 * @param size The desired size in pixels.
	 */
	void setShadowSize(final int size) {
		if (mShadowSize != size) {
			this.mShadowSize = size;
			mView.invalidate();
		}
	}

	/**
	 * Sets a drawables that will be used to draw the edge shadow.
	 *
	 * @param drawableTop    The drawable to be used to draw shadow at the top edge. May be {@code null}
	 *                       to not draw top shadow.
	 * @param drawableBottom The drawable to be used to draw shadow at the bottom edge. May be {@code null}
	 *                       to not draw bottom shadow.
	 * @see #setShadowSize(int)
	 */
	void setShadowDrawables(final Drawable drawableTop, final Drawable drawableBottom) {
		if (mDrawableTop != null) {
			mDrawableTop.setCallback(null);
		}
		if ((mDrawableTop = drawableTop) != null) {
			mDrawableTop.setCallback(mView);
		}
		if (mDrawableBottom != null) {
			mDrawableBottom.setCallback(null);
		}
		if ((mDrawableBottom = drawableBottom) != null) {
			mDrawableBottom.setCallback(mView);
		}
		onViewSizeChanged(mViewWidth, mViewHeight);
	}

	/**
	 * Verifies whether the given <var>drawable</var> is either top or bottom shadow drawable.
	 * <p>
	 * This method should be called from {@link View#verifyDrawable(Drawable)} of the associated view.
	 *
	 * @param drawable The drawable to verify.
	 * @return {@code True} if the drawable is top or bottom shadow drawable, {@code false} otherwise.
	 */
	boolean verifyShadowDrawable(final Drawable drawable) {
		return drawable == mDrawableTop || drawable == mDrawableBottom;
	}

	/**
	 * Sets a boolean flag indicating whether the top shadow drawable should be visible or not.
	 *
	 * @param visible {@code True} to make top shadow visible, {@code false} to hide it.
	 */
	void setTopShadowVisible(final boolean visible) {
		if (mDrawableTop != null) mDrawableTop.setVisible(visible, false);
	}

	/**
	 * Sets a boolean flag indicating whether the bottom shadow drawable should be visible or not.
	 *
	 * @param visible {@code True} to make bottom shadow visible, {@code false} to hide it.
	 */
	void setBottomShadowVisible(final boolean visible) {
		if (mDrawableBottom != null) mDrawableBottom.setVisible(visible, false);
	}

	/**
	 * Called to dispatch that a size of the associated view has changed.
	 * <p>
	 * This method should be called from {@link View#onSizeChanged(int, int, int, int)} of the
	 * associated view.
	 *
	 * @param width New width of the associated view.
	 * @param height New height of the associated view.
	 */
	void onViewSizeChanged(final int width, final int height) {
		this.mViewWidth = width;
		this.mViewHeight = height;
		this.updateShadowBounds(mDrawableTop,
				0,
				0,
				width,
				mShadowSize
		);
		this.updateShadowBounds(mDrawableBottom,
				0,
				height - mShadowSize,
				width,
				height
		);
	}

	/**
	 * Updates bounds of the given <var>shadowDrawable</var> if it is not {@code null}.
	 *
	 * @param shadowDrawable The shadow drawable of which bounds to update.
	 * @param left           Left bound for the shadow.
	 * @param top            Top bound for the shadow.
	 * @param right          Right bound for the shadow.
	 * @param bottom         Bottom bound for the shadow.
	 */
	private void updateShadowBounds(final Drawable shadowDrawable, final int left, final int top, final int right, final int bottom) {
		if (shadowDrawable != null) shadowDrawable.setBounds(left, top, right, bottom);
	}

	/**
	 * Draws the top and bottom shadows (if they are visible).
	 *
	 * @param canvas Canvas where to draw shadows.
	 * @see #setTopShadowVisible(boolean)
	 * @see #setBottomShadowVisible(boolean)
	 */
	void drawShadows(final Canvas canvas) {
		this.drawShadow(canvas, mDrawableTop);
		this.drawShadow(canvas, mDrawableBottom);
	}

	/**
	 * Draws the given <var>shadowDrawable</var> on the given <var>canvas</var> if the drawable is not
	 * {@code null}, is visible and its alpha is grater than {@code 0}.
	 *
	 * @param canvas         The canvas where to draw the shadow.
	 * @param shadowDrawable The drawable that represents the shadow to be drawn.
	 */
	@TargetApi(Build.VERSION_CODES.KITKAT)
	private void drawShadow(final Canvas canvas, final Drawable shadowDrawable) {
		if (shadowDrawable != null && shadowDrawable.isVisible()) {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT || shadowDrawable.getAlpha() > 0) {
				shadowDrawable.draw(canvas);
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}