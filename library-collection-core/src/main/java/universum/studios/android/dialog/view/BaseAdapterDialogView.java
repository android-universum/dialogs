/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * Interface specifying API layer for a view that can be used as content view for a dialog with
 * adapter view to ensure proper working of binding process of the content view for such a dialog.
 *
 * @param <A> Type of the adapter that can be attached to the related adapter view.
 * @param <V> Type of the adapter view specific for the BaseAdapterDialogView implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface BaseAdapterDialogView<A, V extends ViewGroup> extends DialogView {

	/**
	 * Sets a layout resource to be inflated and used as empty view for this dialog view.
	 *
	 * @param resource Resource id of the desired layout containing empty view.
	 * @see #setEmptyView(View)
	 */
	void setEmptyView(@LayoutRes int resource);

	/**
	 * Sets an empty view for this dialog view to be shown when there is no data set available.
	 *
	 * @param view The desired view to be used as empty view.
	 * @see #getEmptyView()
	 */
	void setEmptyView(@Nullable View view);

	/**
	 * Returns the empty view specified to this dialog view.
	 *
	 * @return Empty view or {@code null} if no empty view has been specified yet.
	 * @see #setEmptyView(View)
	 */
	@Nullable View getEmptyView();

	/**
	 * Same as {@link #setEmptyText(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired text.
	 */
	void setEmptyText(@StringRes int resId);

	/**
	 * Sets a text for the empty view of this dialog view.
	 * <p>
	 * <b>Note</b>, that the specified empty text will be set to the current empty view only if it
	 * is instance of {@link android.widget.TextView}.
	 *
	 * @param text The desired text for empty view. May be {@code null} to clear the current one.
	 * @see #setEmptyText(int)
	 * @see #getEmptyText()
	 */
	void setEmptyText(@Nullable CharSequence text);

	/**
	 * Returns the empty text specified for this dialog view.
	 *
	 * @return Current empty text.
	 * @see #setEmptyText(int)
	 * @see #setEmptyText(CharSequence)
	 */
	@NonNull CharSequence getEmptyText();

	/**
	 * Returns the adapter view presented within a view hierarchy of this dialog view.
	 *
	 * @return Adapter view of type specific for this dialog view.
	 * @see #setAdapter(Object)
	 */
	@NonNull V getAdapterView();

	/**
	 * Sets an adapter to provide data set for the adapter view of this dialog view.
	 *
	 * @param adapter The desired adapter with data set. May be {@code null} to clear the current one.
	 * @see #getAdapter()
	 */
	void setAdapter(@Nullable A adapter);

	/**
	 * Returns the current adapter of this dialog view.
	 *
	 * @return Adapter specified via {@link #setAdapter(Object)} or {@code null} if no adapter
	 * has been specified yet.
	 */
	@Nullable A getAdapter();
}