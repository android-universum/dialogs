/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.adapter;

import android.view.View;

import androidx.annotation.NonNull;

/**
 * Holder for a single item view of a specific Dialog Adapter that can be used to support the optimized
 * <b>holder</b> pattern for {@link android.widget.Adapter#getView(int, View, android.view.ViewGroup)
 * Adapter#getView(int, android.view.View, android.view.ViewGroup)} method.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogViewHolder {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DialogViewHolder";

	/**
	 * Flag used to determine that no position has been specified for a particular instance of
	 * {@link DialogViewHolder}.
	 *
	 * @see #getAdapterPosition()
	 */
	public static final int NO_POSITION = -1;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * View with which has been this holder created.
	 */
	public final View itemView;

	/**
	 * Position of an item from associated adapter's data set of which view is hold by this holder
	 * instance.
	 */
	private int mAdapterPosition = NO_POSITION;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DialogViewHolder for the specified position to hold the given <var>itemView</var>.
	 *
	 * @param itemView Instance of view to be hold by the holder.
	 */
	public DialogViewHolder(@NonNull final View itemView) {
		this.itemView = itemView;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Updates the current position of this holder instance.
	 *
	 * @param position The new adapter position for this holder.
	 * @see #getAdapterPosition()
	 */
	final void updateAdapterPosition(final int position) {
		this.mAdapterPosition = position;
	}

	/**
	 * Returns the current position of this holder instance.
	 *
	 * @return The position of item within associated adapter's data set of which view is hold by
	 * this holder or {@link #NO_POSITION} if no position has been specified yet.
	 */
	public final int getAdapterPosition() {
		return mAdapterPosition;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}