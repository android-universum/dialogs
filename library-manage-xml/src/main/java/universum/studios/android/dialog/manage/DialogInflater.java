/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.AndroidRuntimeException;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.XmlRes;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.Dialog;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.DialogsConfig;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.XmlDialog;

/**
 * Instantiates a dialog Xml file (in case of dialogs set, a specific part of Xml file) into its
 * corresponding {@link DialogFragment} object. It is used by {@link DialogXmlFactory} to instantiate
 * its provided dialogs but may be also used across Android application elsewhere.
 * <p>
 * The desired dialog can be inflated via {@link #inflate(int, DialogOptions)}
 * (in case when Xml file contains single dialog) or {@link #inflate(int, int, DialogOptions)}
 * (in case when Xml file contains set of dialogs).
 *
 * <h3>Supported dialog tags:</h3>
 * The DialogInflater need to know which dialogs can be inflated from Xml file. By default, dialogs
 * listed below can be inflated by specifying theirs corresponding tag within Xml:
 * <ul>
 * <li>{@code SimpleDialog} = &lt;Dialog/&gt;</li>
 * <li>{@code InfoDialog} = &lt;InfoDialog/&gt;</li>
 * <li>{@code AlertDialog} = &lt;AlertDialog/&gt;</li>
 * <li>{@code ErrorDialog} = &lt;ErrorDialog/&gt;</li>
 * <li>{@code LoadingDialog} = &lt;LoadingDialog/&gt;</li>
 * <li>{@code ProgressDialog} = &lt;ProgressDialog/&gt;</li>
 * <li>{@code TimePickerDialog} = &lt;TimePickerDialog/&gt;</li>
 * <li>{@code DatePickerDialog} = &lt;DatePickerDialog/&gt;</li>
 * <li>{@code ColorPickerDialog} = &lt;ColorPickerDialog/&gt;</li>
 * <li>{@code LocalePickerDialog} = &lt;LocalePickerDialog/&gt;</li>
 * <li>{@code SelectionDialog} = &lt;SelectionDialog/&gt;</li>
 * <li>{@code IntentDialog} = &lt;IntentDialog/&gt;</li>
 * <li>{@code ListDialog} = &lt;ListDialog/&gt;</li>
 * <li>{@code GridDialog} = &lt;GridDialog/&gt;</li>
 * <li>{@code RecyclerDialog} = &lt;RecyclerDialog/&gt;</li>
 * <li>{@code EditDialog} = &lt;EditDialog/&gt;</li>
 * <li>{@code VersionDialog} = &lt;VersionDialog/&gt;</li>
 * <li>{@code WebDialog} = &lt;WebDialog/&gt;</li>
 * <li>{@code SignInDialog} = &lt;SignInDialog/&gt;</li>
 * </ul>
 * If you have your own dialog implementation that you want to be available for inflation via Xml
 * file, you need to implement {@link XmlDialog} interface and than register that dialog by its class
 * via {@link #registerDialog(Class)}. Simple name ({@link Class#getSimpleName()}) of the specified
 * dialog class will be than associated with its class as Xml tag, so when the dialog inflater finds
 * this tag within dialogs Xml file, it will instantiate a new dialog instance using its associated
 * class and will call {@link XmlDialog#inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)}
 * upon it.
 * <p>
 * <b>Note</b>, that from the static definition of {@link #registerDialog(Class)} it is obvious that
 * the dialog classes are globally cached, so you do not need to register your custom dialogs every
 * time you want to inflate them with the dialog inflater.
 *
 * <h3>Xml structure</h3>
 * Below are listed some samples of Xml files containing single dialog structure and also structure
 * representing set of dialogs.
 * <p>
 * <b>Single dialog:</b>
 * <pre>
 *  &lt;ErrorDialog xmlns:android="http://schemas.android.com/apk/res/android"
 *               xmlns:dialog="http://schemas.android.com/apk/res-auto"
 *               dialog:dialogIcon="@drawable/dialog_icon_error"
 *               dialog:dialogTitle="Unknown error"
 *               dialog:dialogContent="Server has some troubles to finish your request."
 *               dialog:dialogNegativeButton="Cancel"
 *               dialog:dialogPositiveButton="Try again"/&gt;
 * </pre>
 * <p>
 * <b>Set of dialogs:</b>
 * <pre>
 *  &lt;Dialogs xmlns:android="http://schemas.android.com/apk/res/android"
 *           xmlns:dialog="http://schemas.android.com/apk/res-auto"&gt;
 *
 *           &lt;LoadingDialog
 *                  dialog:dialogId="@+id/dialog_uploading_photos"
 *                  dialog:dialogContent="Uploading photos to the cloud."/&gt;
 *
 *           &lt;Dialog
 *                  dialog:dialogId="@+id/dialog_photos_upload_finished"
 *                  dialog:dialogTitle="Upload finished"
 *                  dialog:dialogContent="Upload of your photos has been finished."
 *                  dialog:dialogNeutralButton="OK"/&gt;
 *
 *           &lt;include dialog:dialog="@xml/dialog_error"/&gt;
 *
 *           &lt;include dialog:dialogs="...REFERENCE_TO_XML_FILE_CONTAINING_MULTIPLE_DIALOGS..."/&gt;
 *  &lt;/Dialogs&gt;
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Dialog
 * @see XmlDialog
 * @see DialogOptions
 * @see DialogXmlFactory
 */
public final class DialogInflater {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    private static final String TAG = "DialogInflater";

    /**
     * Required root tag for xml file with set of multiple dialogs.
     */
    public static final String DIALOGS_ROOT_TAG = "Dialogs";

    /**
     * Include tag for xml file with set of multiple dialogs to include set of dialogs from another
     * xml file.
     */
    private static final String DIALOGS_INCLUDE_TAG = "include";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /**
     * Set of registered dialogs which can be inflated from xml file.
     */
    private static final HashMap<String, Class<? extends XmlDialog>> DIALOGS = new HashMap<>();

    static {
        // Register default dialogs provided by this library that can be inflated from an Xml file.
	    try {
		    //noinspection unchecked
		    DIALOGS.put("Dialog", (Class<? extends XmlDialog>) Class.forName(DialogsConfig.class.getPackage().getName() + ".SimpleDialog"));
	    } catch (ClassNotFoundException e) {
		    if (DialogsConfig.LOG_ENABLED) {
			    Log.v(TAG, "Skipping registration of default dialog with name(Dialog). Dialog class not found.");
		    }
	    }
	    registerDefaultDialog("InfoDialog");
        registerDefaultDialog("ErrorDialog");
        registerDefaultDialog("AlertDialog");
        registerDefaultDialog("LoadingDialog");
        registerDefaultDialog("ProgressDialog");
        registerDefaultDialog("TimePickerDialog");
        registerDefaultDialog("DatePickerDialog");
        registerDefaultDialog("ColorPickerDialog");
        registerDefaultDialog("LocalePickerDialog");
        registerDefaultDialog("SelectionDialog");
        registerDefaultDialog("IntentDialog");
        registerDefaultDialog("ListDialog");
        registerDefaultDialog("GridDialog");
        registerDefaultDialog("RecyclerDialog");
        registerDefaultDialog("EditDialog");
        registerDefaultDialog("WebDialog");
        registerDefaultDialog("VersionDialog");
        registerDefaultDialog("SignInDialog");
    }

    /**
     * Registers a default dialog provided by this library with the specified <var>dialogClassName</var>.
     * <p/>
     * The given dialog simple class name will be used to obtain full qualified class name that will
     * be registered. If such class cannot be found, for example if this library is used only with
     * few of its separate modules, the requested dialog name will not be registered and thus cannot
     * be inflated via Xml file.
     *
     * @param dialogSimpleClassName Name of the dialog class to register as default dialog.
     */
    @SuppressWarnings("unchecked")
    private static void registerDefaultDialog(final String dialogSimpleClassName) {
        final Class<?> dialogClass;
        try {
            dialogClass = Class.forName(DialogsConfig.class.getPackage().getName() + "." + dialogSimpleClassName);
        } catch (ClassNotFoundException e) {
            if (DialogsConfig.LOG_ENABLED) {
	            Log.v(TAG, "Skipping registration of default dialog with name(" + dialogSimpleClassName + "). Dialog class not found.");
            }
            return;
        }
        // We assume that all default dialogs that are to be registered via DialogInflater are
        // properly implemented inheritance hierarchies of XmlDialog interface.
        DIALOGS.put(dialogClass.getSimpleName(), (Class<? extends XmlDialog>) dialogClass);
    }

    /*
     * Members =====================================================================================
     */

    /**
     * Context with which has been this inflater created.
     */
    private final Context mContext;

    /**
     * Application resources used during dialogs inflation process.
     */
    private final Resources mResources;

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates a new instance of DialogInflater.
     *
     * @param context Context used when inflating dialogs from Xml.
     */
    private DialogInflater(Context context) {
        this.mContext = context;
        this.mResources = context.getResources();
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Registers the given <var>classOfDialog</var> as dialog which can be inflated from xml file.
     * Whenever there will be found within the currently processed xml file dialog element with same
     * name as name of here specified class, instance of this class will be instantiated and its
     * {@link XmlDialog#inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)} will
     * be invoked upon the created instance.
     * <p>
     * <b>Note</b>, that as tag for the given class will be used its name under which will be saved
     * within the set of registered dialogs and can be obtained by {@link #getClassOfDialog(String)}.
     * </p>
     *
     * @param classOfDialog The class of dialog to register.
     */
    public static <D extends DialogFragment & XmlDialog> void registerDialog(@NonNull final Class<D> classOfDialog) {
        final String simpleName = classOfDialog.getSimpleName();
        if (!DIALOGS.containsKey(simpleName)) {
            DIALOGS.put(simpleName, classOfDialog);
        }
    }

    /**
     * Obtains a new instance of DialogInflater using the specified <var>context</var>.
     *
     * @param context The context used to initialize the new inflater.
     * @return DialogInflater instance that can be used to inflate desired dialogs from Xml files.
     */
    @NonNull public static DialogInflater from(@NonNull final Context context) {
        return new DialogInflater(context);
    }

    /**
     * Returns the context specified for this inflater during its initialization.
     *
     * @return This inflater's context.
     */
    @NonNull public final Context getContext() {
        return mContext;
    }

    /**
     * Checks whether a dialog with the specified <var>dialogTag</var> can be inflated by this inflater
     * instance form Xml file or not.
     *
     * @param dialogTag The tag of a dialog to check.
     * @return {@code True} if dialog with the specified tag is registered so its instances can be
     * inflated from Xml file, {@code false} otherwise.
     * @see #registerDialog(Class)
     */
    public static boolean isDialogSupported(@NonNull final String dialogTag) {
        return DIALOGS.get(dialogTag) != null;
    }

    /**
     * Returns the class of a dialog from the current registered dialogs associated with the specified
     * <var>dialogTag</var>.
     *
     * @param dialogTag Dialog tag that is associated with the desired dialog class.
     * @return Requested dialog class or {@code null} if there is no mapping for such class associated
     * with the specified tag.
     * @see #registerDialog(Class)
     * @see #isDialogSupported(String)
     */
    @Nullable public static Class<? extends XmlDialog> getClassOfDialog(@NonNull final String dialogTag) {
        return DIALOGS.get(dialogTag);
    }

    /**
     * Inflates a new instance of XmlDialog from an Xml file with the specified <var>xmlRes</var>
     * containing single dialog.
     *
     * @param xmlRes  Resource id of the desired Xml file containing the dialog to inflate.
     * @param options Options for the new dialog instance. These options will be passed to dialog via
     *                {@link XmlDialog#inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)}.
     * @param <D>     Type of the dialog to be inflated.
     * @return A new instance of inflated dialog or {@code null} if some error has occurred during
     * inflation process.
     * @throws InflationException          If there is no such a dialog registered for the root tag
     *                                     of Xml file with the specified resource id.
     * @throws Resources.NotFoundException If Xml file with the specified resource id does not exist.
     * @see #isDialogSupported(String)
     * @see #inflate(int, int, DialogOptions)
     */
    @Nullable public <D extends DialogFragment & XmlDialog> D inflate(@XmlRes final int xmlRes, @Nullable final DialogOptions options) {
	    long time = 0;
	    if (DialogsConfig.DEBUG_LOG_ENABLED) {
		    time = System.currentTimeMillis();
	    }
        final XmlResourceParser xmlParser = mResources.getXml(xmlRes);
        D dialog = null;
        try {
            do {
                switch (xmlParser.getEventType()) {
                    case XmlResourceParser.START_TAG:
                        dialog = inflateFromParser(xmlParser, options);
                        break;
                }
            } while (xmlParser.next() != XmlResourceParser.END_DOCUMENT);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        xmlParser.close();
	    if (DialogsConfig.DEBUG_LOG_ENABLED) {
		    Log.d(TAG, "Inflation of single dialog took " + Long.toString(System.currentTimeMillis() - time) + "ms.");
	    }
        return dialog;
    }

    /**
     * Inflates a new instance of XmlDialog from the given parser containing data for a single dialog.
     * <p/>
     * <b>Note, that the parser must be already moved to the correct position with data for the
     * requested dialog.</b>
     *
     * @param xmlParser Xml parser moved to the position with data for the new dialog.
     * @param options   Initial options for the new dialog passed from outside of the inflater.
     * @param <D>       Type of the dialog to be inflated.
     * @return A new instance of inflated dialog or {@code null} if some error occurs during inflation.
     * @throws InflationException If there is no such a dialog registered for the name of the given parser.
     */
    @SuppressWarnings("unchecked") private <D extends DialogFragment & XmlDialog> D inflateFromParser(final XmlResourceParser xmlParser, final DialogOptions options) {
        final String dialogTag = xmlParser.getName();
        if (isDialogSupported(dialogTag)) {
            final D dialog = instantiateDialogWithTag(dialogTag);
            if (dialog != null) {
                dialog.inflate(xmlParser, mResources, mContext.getTheme(), options);
            }
            return dialog;
        }
        throw new InflationException("Failed to inflate dialog. Unknown dialog tag('" + dialogTag + "').");
    }

    /**
     * Inflates a new instance of XmlDialog from an Xml file with the specified <var>xmlRes</var>
     * containing set of multiple dialogs.
     *
     * @param xmlRes   Resource id of the desired Xml file containing the dialog to inflate.
     *                 <b>Note, that the Xml file must start with {@link #DIALOGS_ROOT_TAG} root tag.</b>
     * @param dialogId Id of the desired dialog to inflate. This id is used to search for the requested
     *                 dialog among the set of dialogs contained within the Xml file.
     * @param options  Options for the new dialog instance. These options will be passed to dialog via
     *                 {@link XmlDialog#inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)}.
     * @param <D>      Type of the dialog to be inflated.
     * @return A new instance of inflated dialog or {@code null} if some error has occurred during
     * inflation process.
     * @throws InflationException          If Xml file with the specified resource id does not have
     *                                     root tag specified as {@link #DIALOGS_ROOT_TAG} or there
     *                                     is no such a dialog registered for the tag with the specified
     *                                     dialog id.
     * @throws Resources.NotFoundException If Xml file with the specified resource id does not exist.
     * @see #isDialogSupported(String)
     * @see #inflate(int, DialogOptions)
     */
    @Nullable public <D extends DialogFragment & XmlDialog> D inflate(@XmlRes final int xmlRes, final int dialogId, @Nullable final DialogOptions options) {
        long time = 0;
	    if (DialogsConfig.DEBUG_LOG_ENABLED) {
		    time = System.currentTimeMillis();
	    }
	    final XmlResourceParser xmlParser = mResources.getXml(xmlRes);
        final D dialog = inflateFromParser(xmlParser, dialogId, options);
        xmlParser.close();
        if (DialogsConfig.DEBUG_LOG_ENABLED) {
            Log.d(TAG, "Inflation of dialog from dialogs set took " + Long.toString(System.currentTimeMillis() - time) + "ms.");
        }
        return dialog;
    }

    /**
     * Inflates a new instance of XmlDialog from the given parser containing data for multiple dialogs.
     *
     * @param xmlParser Xml parser with data for the new dialog.
     * @param dialogId  Id of the desired dialog to inflate. This id is used to search for the dialog
     *                  via {@link android.R.attr#id android:id} attribute.
     * @param options   Initial options for the new dialog passed from outside of the inflater.
     * @param <D>       Type of the dialog to be inflated.
     * @return A new instance of inflated dialog or {@code null} if some error occurs during inflation.
     * @throws InflationException If some unexpected error occurs during inflation process.
     */
    private <D extends DialogFragment & XmlDialog> D inflateFromParser(final XmlResourceParser xmlParser, final int dialogId, final DialogOptions options) {
        try {
            boolean checkRootTag = true;
            do {
                switch (xmlParser.getEventType()) {
                    case XmlResourceParser.START_TAG:
                        final String tag = xmlParser.getName();
                        if (checkRootTag) {
                            if (isDialogSupported(tag)) {
                                final D dialog = inflateFromParserById(xmlParser, dialogId, options);
                                if (dialog != null) return dialog;
                            } else if (!DIALOGS_ROOT_TAG.equals(tag)) {
                                throw new InflationException(
                                        "Failed to inflate dialog. Unknown dialog tag('" + tag + "'). " +
                                                "Only '" + DIALOGS_ROOT_TAG + "' tag or name of the desired dialog are allowed as root tags for dialogs Xml set."
                                );
                            }
                            checkRootTag = false;
                        } else if (xmlParser.getAttributeCount() == 0) {
                            throw new InflationException("Empty dialog tags in dialogs Xml set are not allowed.");
                        } else {
                            if (DIALOGS_INCLUDE_TAG.equals(tag)) {
                                D dialog = null;
                                // Obtain resource id of the included dialog/-s Xml file.
                                final int n = xmlParser.getAttributeCount();
                                for (int i = 0; i < n; i++) {
                                    final int attrNameRes = xmlParser.getAttributeNameResource(i);
                                    if (attrNameRes == R.attr.dialogs || attrNameRes == R.attr.dialog) {
                                        dialog = inflateFromParser(mResources.getXml(xmlParser.getAttributeResourceValue(i, 0)), dialogId, options);
                                        break;
                                    }
                                }
                                if (dialog != null) {
                                    return dialog;
                                }
                            } else {
                                final D dialog = inflateFromParserById(xmlParser, dialogId, options);
                                if (dialog != null) return dialog;
                            }
                        }
                        break;
                }
            } while (xmlParser.next() != XmlResourceParser.END_DOCUMENT);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Inflates a new instance of XmlDialog from the given parser if value of {@link android.R.attr#id android:id}
     * attribute of the given <var>xmlParser</var> is same as the specified <var>dialogId</var>.
     *
     * @param xmlParser Xml parser with data for the new dialog.
     * @param dialogId  Id of the desired dialog to inflate.
     * @param options   Initial options for the new dialog passed from outside of the inflater.
     * @param <D>       Type of the dialog to be inflated.
     * @return A new instance of inflated dialog or {@code null} if some error occurs during inflation
     * or the Xml parser does not point to the requested dialog at its current position.
     * @throws InflationException If some unexpected error occurs during inflation process.
     */
    private <D extends DialogFragment & XmlDialog> D inflateFromParserById(final XmlResourceParser xmlParser, final int dialogId, final DialogOptions options) {
        final int n = xmlParser.getAttributeCount();
        for (int i = 0; i < n; i++) {
            if (xmlParser.getAttributeNameResource(i) == android.R.attr.id) {
				final int id = xmlParser.getAttributeResourceValue(i, 0);
				if (dialogId == id) return inflateFromParser(xmlParser, options);
				break;
			}
        }
        return null;
    }

    /**
     * Instantiates a new instance of XmlDialog with the specified <var>dialogTag</var>.
     *
     * @param dialogTag The tag that is associated with the class of the requested dialog to be
     *                  instantiated.
     * @param <D>       Type of the dialog to be instantiated.
     * @return New dialog instance or {@code null} if instantiation has failed.
     */
    @SuppressWarnings("unchecked")
    private <D extends DialogFragment & XmlDialog> D instantiateDialogWithTag(final String dialogTag) {
        try {
            return (D) DIALOGS.get(dialogTag).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Exception used to indicate error occurred during inflation process of dialog from Xml file.
     *
     * @see DialogInflater
     */
    public static final class InflationException extends AndroidRuntimeException {

        /**
         * Creates a new instance of InflationException with the specified <var>message</var>.
         */
        InflationException(final String message) {
            super(message);
        }
    }
}