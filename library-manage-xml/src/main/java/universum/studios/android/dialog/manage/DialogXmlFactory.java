/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.manage;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.XmlRes;
import androidx.fragment.app.DialogFragment;
import universum.studios.android.dialog.DialogOptions;
import universum.studios.android.dialog.XmlDialog;

/**
 * A {@link BaseDialogFactory} implementation that can be used to provide dialogs that can be inflated
 * from an Xml file as single dialog or from a set of dialogs.
 * <p>
 * Whenever dialog should be inflated from single Xml file or set of dialogs within Xml file is
 * resolved within {@link #createDialog(int, DialogOptions)}
 * method, where the requested dialog will be first looked up (by its id) within set of dialogs (if any)
 * and then within the single Xml file where the dialog's id passed to {@code createDialog(...)}
 * will be used as resource id of Xml file from which to inflate the dialog. Xml file containing set
 * of Xml dialogs can be specified via {@link DialogXmlFactory#DialogXmlFactory(Context, int)}.
 * <p>
 * DialogXmlFactory may be also used to directly inflate a desired dialog via {@link #inflateDialog(Context, int, DialogOptions)}
 * or {@link #inflateDialogFromSet(Context, int, int, DialogOptions)}.
 * <p>
 * <b>Note</b>, that when inflating dialogs implemented within the Dialogs library, remember that
 * options passed to the Xml factory via {@link #createDialog(int, DialogOptions)} will be passed to
 * dialog during its inflation via {@link XmlDialog#inflate(XmlResourceParser, Resources, Resources.Theme, DialogOptions)}
 * and merged with inflated options with data from Xml file via {@link DialogOptions#merge(DialogOptions)}.
 * The options passed to the factory via Java code have the higher priority as the inflated options.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class DialogXmlFactory extends BaseDialogFactory {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "DialogXmlFactory";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Inflater used to inflate dialogs from Xml file.
	 */
	protected final DialogInflater mInflater;

	/**
	 * Resource id of xml file containing set of xml dialogs where each of these dialogs can be
	 * inflated by its associated id.
	 */
	protected final int mXmlDialogsSet;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Like {@link #DialogXmlFactory(Context, int)} creates a new instance of DialogXmlFactory with
	 * the given context. This factory may be used to create <b>only</b> Xml dialogs which are presented,
	 * each respectively, in single Xml file.
	 *
	 * @param context Context in which will be this factory used.
	 */
	public DialogXmlFactory(@NonNull final Context context) {
		this(context, -1);
	}

	/**
	 * Crates a new instance of DialogXmlFactory with the given context and the resource id of xml
	 * file with set of xml dialogs. This factory may be used to create xml dialogs which are presented,
	 * each respectively, in single Xml file by resource id of that file, or by id of the requested
	 * dialog within the Xml dialogs set file.
	 *
	 * @param context       Context in which will be this factory used.
	 * @param xmlDialogsSet Resource id of Xml file with set of inflatable dialogs.
	 * @see #DialogXmlFactory(Context)
	 */
	public DialogXmlFactory(@NonNull final Context context, @XmlRes final int xmlDialogsSet) {
		super(context);
		this.mInflater = DialogInflater.from(context);
		this.mXmlDialogsSet = xmlDialogsSet;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Delegates to {@link DialogInflater#inflate(int, DialogOptions)}.
	 *
	 * @param context Context used to initialize inflater.
	 * @see #inflateDialogFromSet(Context, int, int, DialogOptions)
	 */
	@Nullable public static <D extends DialogFragment & XmlDialog> D inflateDialog(
			@NonNull final Context context,
			@XmlRes final int xmlRes,
			@Nullable final DialogOptions options
	) {
		return inflateDialogInner(DialogInflater.from(context), xmlRes, options);
	}

	/**
	 * Inflates dialog fragment from an Xml file with the specified <var>xmlRes</var> id.
	 *
	 * @param inflater Xml dialog inflater used to inflate the requested dialog.
	 * @param xmlRes   Resource if of the Xml file containing desired xml dialog.
	 * @param options  Options for the dialog to be inflated.
	 * @return Inflated instance of dialog fragment.
	 * @see DialogInflater#inflate(int, DialogOptions)
	 */
	private static <D extends DialogFragment & XmlDialog> D inflateDialogInner(
			final DialogInflater inflater,
			final int xmlRes,
			final DialogOptions options
	) {
		try {
			return inflater.inflate(xmlRes, options);
		} catch (Resources.NotFoundException e) {
			Log.e(TAG, "Failed to inflate dialog from Xml. No Xml file found with the specified resource id(" + xmlRes + ").");
		}
		return null;
	}

	/**
	 * Delegates to {@link DialogInflater#inflate(int, int, DialogOptions)}.
	 *
	 * @param context Context used to initialize inflater.
	 * @see #inflateDialog(Context, int, DialogOptions)
	 */
	@Nullable public static <D extends DialogFragment & XmlDialog> D inflateDialogFromSet(
			@NonNull final Context context,
			@XmlRes final int xmlSetRes,
			final int dialogId,
			@Nullable final DialogOptions options
	) {
		return inflateDialogFromSetInner(DialogInflater.from(context), xmlSetRes, dialogId, options);
	}

	/**
	 * Inflates dialog fragment from an Xml file with the specified <var>xmlRes</var> id.
	 *
	 * @param inflater  Xml dialog inflater to inflate dialog.
	 * @param xmlSetRes The resource id of xml file containing desired xml dialog.
	 * @param dialogId  The id of desired dialog within xml file.
	 * @param options   Options for the dialog to be inflated.
	 * @return Inflated instance of dialog fragment or {@code null} if there is no such a dialog
	 * within the Xml file with the specified <var>xmlRes</var>
	 * @see DialogInflater#inflate(int, int, DialogOptions)
	 */
	private static <D extends DialogFragment & XmlDialog> D inflateDialogFromSetInner(
			final DialogInflater inflater,
			final int xmlSetRes,
			final int dialogId,
			final DialogOptions options
	) {
		try {
			return inflater.inflate(xmlSetRes, dialogId, options);
		} catch (Resources.NotFoundException e) {
			Log.e(TAG, "Failed to inflate dialog from set of Xml dialogs. No Xml file found with the specified resource id(" + xmlSetRes + ").");
		}
		return null;
	}

	/**
	 * Returns the resource id of Xml file containing set of xml dialogs for this factory.
	 *
	 * @return Xml dialogs set resource id or {@code -1} if no set was specified.
	 */
	@XmlRes public final int getXmlDialogsSet() {
		return mXmlDialogsSet;
	}

	/**
	 */
	@Override protected boolean providesDialog(final int dialogId) {
		return dialogId >= 0;
	}

	/**
	 */
	@Override public DialogFragment createDialog(final int dialogId, @Nullable final DialogOptions options) {
		DialogFragment dialog;
		// First dispatch to inflate from dialogs set.
		if ((dialog = onCreateDialogFromSet(dialogId, options)) == null) {
			// Dialog not inflated from set, dispatch to inflate from single xml file or just create
			// it within factory.
			dialog = onCreateDialog(dialogId, options);
		}
		return attachDialogId(dialog, dialogId);
	}

	/**
	 * Invoked whenever {@link #createDialog(int, DialogOptions)} is called.
	 * <p>
	 * <b>Note</b>, that this is invoked before {@link #onCreateDialog(int, DialogOptions)}.
	 */
	@Nullable protected DialogFragment onCreateDialogFromSet(final int dialogId, @Nullable final DialogOptions options) {
		return mXmlDialogsSet == -1 ? null : inflateDialogFromSetInner(mInflater, mXmlDialogsSet, dialogId, options);
	}

	/**
	 * Invoked whenever {@link #createDialog(int, DialogOptions)} is called and
	 * {@link #onCreateDialogFromSet(int, DialogOptions)} returns {@code null} for the specified
	 * <var>dialogId</var>.
	 */
	@Override protected DialogFragment onCreateDialog(final int dialogId, @Nullable final DialogOptions options) {
		return inflateDialogInner(mInflater, dialogId, options);
	}

	/**
	 */
	@Override @Nullable protected String onCreateDialogTag(final int dialogId) {
		return createDialogTag(getClass(), Integer.toString(dialogId));
	}

	/*
	 * Inner classes ===============================================================================
	 */
}