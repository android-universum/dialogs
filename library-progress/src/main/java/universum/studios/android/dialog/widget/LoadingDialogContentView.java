/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.LoadingDialog;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.LoadingDialogView;
import universum.studios.android.ui.widget.CircularProgressBar;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link LoadingDialogView} implementation used by {@link LoadingDialog LoadingDialog} as content
 * view.
 * <p>
 * This class represents a container for a {@link CircularProgressBar} and content view ({@link TextView})
 * positioned side by side horizontally to show progress bar and content message to a user. Content
 * content can be specified via {@link #setContent(CharSequence)} or its resource equivalent
 * {@link #setContent(int)}. You can also specify a progress value that will be presented in the middle
 * of circular progress bar via {@link #setProgress(int...)} and its current value can be obtained
 * via {@link #getProgress()}. Specified progress value will be showed as raw value in TextView
 * converted to String, but can be formatted using format that can be specified via
 * {@link #setProgressFormat(String)} or its resource equivalent {@link #setProgressFormat(String)}.
 * <p>
 * Whether to show or hide an indicator showing current progress value can be specified via
 * {@link #setProgressIndicatorVisible(boolean)}.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutLoadingContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_loading} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class LoadingDialogContentView extends LinearLayout implements LoadingDialogView {

    /*
     * Constants ===================================================================================
     */

    /**
     * Log TAG.
     */
    // private static final String TAG = "LoadingDialogContentView";

    /*
     * Interface ===================================================================================
     */

    /*
     * Static members ==============================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Current progress values.
     */
    private int[] mProgress = new int[0];

    /**
     * Format used when formatting current progress values.
     */
    private String mProgressFormat;

    /**
     * Text view to present current progress values.
     */
    private TextView mProgressIndicatorView;

    /**
     * Flag indicating whether the progress indicator view is visible or not.
     */
    private boolean mProgressIndicatorVisible;

    /**
     * Text view to present content (message text) of dialog.
     */
    private TextView mContentView;

    /*
     * Constructors ================================================================================
     */

    /**
     * Same as {@link #LoadingDialogContentView(Context, AttributeSet)} without attributes.
     */
    public LoadingDialogContentView(@NonNull final Context context) {
        this(context, null);
    }

    /**
     * Same as {@link #LoadingDialogContentView(Context, AttributeSet, int)} with {@code 0} as attribute
     * for default style.
     */
    public LoadingDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs, 0, 0);
    }

    /**
     * Same as {@link #LoadingDialogContentView(Context, AttributeSet, int, int)}  with {@code 0} as
     * default style.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public LoadingDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs, defStyleAttr, 0);
    }

    /**
     * Creates a new instance of LoadingDialogContentView for the given <var>context</var>.
     *
     * @param context      Context in which will be the new view presented.
     * @param attrs        Set of Xml attributes used to configure the new instance of this view.
     * @param defStyleAttr An attribute which contains a reference to a default style resource for
     *                     this view within a theme of the given context.
     * @param defStyleRes  Resource id of the default style for the new view.
     */
    @SuppressWarnings("unused")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LoadingDialogContentView(
    		@NonNull final Context context,
		    @Nullable final AttributeSet attrs,
		    @AttrRes final int defStyleAttr,
		    @StyleRes final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Methods =====================================================================================
     */

    /**
     * Called from one of constructors of this view to perform its initialization.
     * <p>
     * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
     * this view specific data from it that can be used to configure this new view instance. The
     * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
     * from the current theme provided by the specified <var>context</var>.
     */
    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        final Resources.Theme theme = context.getTheme();
        final TypedValue typedValue = new TypedValue();
        // Resolve layout resource from which to inflate view hierarchy.
        int layoutResource = R.layout.dialog_layout_loading;
        if (theme.resolveAttribute(R.attr.dialogLayoutLoadingContent, typedValue, true)) {
            layoutResource = typedValue.resourceId;
        }
        this.inflateHierarchy(context, layoutResource);
    }

    /**
     * Called to inflate a view hierarchy of this view.
     *
     * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
     *                 layout resource as view hierarchy for this view.
     * @param resource Resource id of the layout which should represent a view hierarchy of this view.
     */
    private void inflateHierarchy(final Context context, final int resource) {
        if (resource > 0) {
            LayoutInflater.from(context).inflate(resource, this);
            this.mContentView = (TextView) findViewById(R.id.dialog_text_view_content);
            this.mProgressIndicatorView = (TextView) findViewById(R.id.dialog_text_view_progress_indicator);
            onFinishInflate();
        }
    }

    /**
     */
    @Override public void setProgressIndicatorVisible(final boolean visible) {
        if (mProgressIndicatorView != null) {
            this.mProgressIndicatorVisible = visible;
            mProgressIndicatorView.setVisibility(visible ? VISIBLE : GONE);
        }
    }

    /**
     */
    @Override public boolean isProgressIndicatorVisible() {
        return mProgressIndicatorVisible;
    }

    /**
     */
    @Override public void setProgressFormat(@StringRes final int resId) {
        setProgressFormat(getResources().getString(resId));
    }

    /**
     */
    @Override public void setProgressFormat(@Nullable final String format) {
        this.mProgressFormat = format;
        this.handleProgressUpdate(mProgress);
    }

    /**
     */
    @Override @NonNull public String getProgressFormat() {
        return mProgressFormat;
    }

    /**
     */
    @Override public void setProgress(@NonNull final int... progress) {
        this.handleProgressUpdate(mProgress = progress);
    }

    /**
     * Updates the progress indicator with the specified progress values.
     *
     * @param progress The current progress values.
     */
    @SuppressLint("SetTextI18n")
    private void handleProgressUpdate(final int[] progress) {
        if (progress.length > 0 && mProgressIndicatorView != null) {
            if (!TextUtils.isEmpty(mProgressFormat)) {
                mProgressIndicatorView.setText(String.format(mProgressFormat, progress[0]));
            } else {
                mProgressIndicatorView.setText(Integer.toString(progress[0]));
            }
        }
    }

    /**
     */
    @Override @NonNull public int[] getProgress() {
        return mProgress;
    }

    /**
     */
    @Override public void setContent(@StringRes final int resId) {
        setContent(getResources().getText(resId));
    }

    /**
     */
    @Override public void setContent(@Nullable final CharSequence text) {
        if (mContentView != null) mContentView.setText(text);
    }

    /**
     */
    @Override @NonNull public CharSequence getContent() {
        return mContentView != null ? mContentView.getText() : "";
    }

    /**
     */
    @Override protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.content = mContentView != null ? mContentView.getText() : "";
        savedState.progressFormat = mProgressFormat;
        savedState.progress = mProgress;
        savedState.progressIndicatorVisible = mProgressIndicatorVisible;
        return savedState;
    }

    /**
     */
    @Override protected void onRestoreInstanceState(@NonNull final Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        final SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setContent(savedState.content);
        setProgressFormat(savedState.progressFormat);
        setProgress(savedState.progress);
        setProgressIndicatorVisible(savedState.progressIndicatorVisible);
    }

    /**
     * Inner classes ===============================================================================
     */

    /**
     * A {@link WidgetSavedState} implementation used to ensure that the state of {@link LoadingDialogContentView}
     * is properly saved.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    public static class SavedState extends WidgetSavedState {

        /**
         * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
         */
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            /**
             */
            @Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

            /**
             */
            @Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
        };

        /**
         */
        CharSequence content;

        /**
         */
        int[] progress = new int[0];

        /**
         */
        String progressFormat;

        /**
         */
        boolean progressIndicatorVisible;

        /**
         * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
         * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
         *
         * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
         *                   {@code onSaveInstanceState()}.
         */
        protected SavedState(@NonNull final Parcelable superState) {
            super(superState);
        }

        /**
         * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
         * <var>source</var>.
         *
         * @param source Parcel with data for the new instance.
         */
        protected SavedState(@NonNull final Parcel source) {
            super(source);
            this.content = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
            this.progressFormat = source.readString();
            this.progressIndicatorVisible = source.readInt() == 1;
            source.readIntArray(progress);
        }

        /**
         */
        @Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
            super.writeToParcel(dest, flags);
            TextUtils.writeToParcel(content, dest, flags);
            dest.writeString(progressFormat);
            dest.writeInt(progressIndicatorVisible ? 1 : 0);
            dest.writeIntArray(progress);
        }
    }
}