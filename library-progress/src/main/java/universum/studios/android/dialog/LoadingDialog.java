/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.view.LoadingDialogView;
import universum.studios.android.dialog.view.ProgressDialogView;

/**
 * A {@link SimpleDialog} implementation that can be used to show a loading bar with content text.
 * This dialog uses default {@link LoadingDialogView} implementation as its content view which consists
 * of {@link universum.studios.android.ui.widget.CircularProgressBar CircularProgressBar} with progress
 * indicator in the middle and text view for content text at the right of the loading bar.
 * <p>
 * Current progress within indicator (only shows text value of progress) can be specified via
 * {@link #setProgress(int...)} and the current value can be obtained via {@link #getProgress()}.
 * You can also change the content text dynamically via {@link #setContent(CharSequence)} or its
 * resource equivalent {@link #setContent(int)}.
 * <p>
 * See {@link LoadingOptions} for options that can be supplied to the LoadingDialog from the desired
 * context via {@link #newInstance(LoadingOptions)} or via {@link #setOptions(DialogOptions)}.
 * <p>
 * <b>Note</b>, that current state of this dialog will be always preserved after orientation change,
 * as all dialog views from this library save theirs current state for that matter.
 *
 * <h3>Xml options</h3>
 * This dialog parses its global options from the current dialog theme referenced via
 * {@link R.attr#dialogLoadingOptions dialogLoadingOptions} attribute.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class LoadingDialog extends SimpleDialog implements ProgressBarDialog {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LoadingDialog";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Loading dialog view implementation.
	 */
	private LoadingDialogView mLoadingView;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of LoadingDialog with single circular {@link android.widget.ProgressBar}.
	 */
	public LoadingDialog() {
		super(R.attr.dialogLoadingOptions);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of LoadingDialog with the specified options.
	 *
	 * @param options Specific options for this type of dialog.
	 * @return New LoadingDialog instance.
	 */
	@NonNull public static LoadingDialog newInstance(@NonNull final LoadingOptions options) {
		final LoadingDialog dialog = new LoadingDialog();
		dialog.setOptions(options);
		return dialog;
	}

	/**
	 */
	@Override @NonNull protected DialogOptions onCreateDefaultOptions(@NonNull final Resources resources) {
		return new LoadingOptions(resources);
	}

	/**
	 */
	@Override protected void onProcessOptionsStyle(@NonNull final Context context, @StyleRes final int optionsStyle) {
		super.onProcessOptionsStyle(context, optionsStyle);
		if (mOptions instanceof LoadingOptions) {
			final TypedArray attributes = context.obtainStyledAttributes(optionsStyle, R.styleable.Dialog_Options_Loading);
			final LoadingOptions options = (LoadingOptions) mOptions;
			for (int i = 0; i < attributes.getIndexCount(); i++) {
				int index = attributes.getIndex(i);
				if (index == R.styleable.Dialog_Options_Loading_dialogProgressFormat) {
					if (!options.isSet(LoadingOptions.PROGRESS_FORMAT)) {
						options.progressFormat(attributes.getString(index));
					}
				} else if (index == R.styleable.Dialog_Options_Loading_dialogProgressIndicator) {
					if (!options.isSet(LoadingOptions.PROGRESS_INDICATOR_VISIBLE)) {
						options.showProgressIndicator(
								attributes.getInteger(index, 0) == ProgressDialogView.INDICATOR_PROGRESS
						);
					}
				}
			}
			attributes.recycle();
		}
	}

	/**
	 */
	@Override @NonNull protected View onCreateContentView(
			@NonNull final LayoutInflater inflater,
			@NonNull final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.dialog_content_loading, container, false);
	}

	/**
	 */
	@Override void onBindContentView(@NonNull final View contentView, @Nullable final DialogOptions options) {
		super.onBindContentView(contentView, options);
		if (contentView instanceof LoadingDialogView && options instanceof LoadingOptions) {
			final LoadingDialogView loadingView = (LoadingDialogView) contentView;
			final LoadingOptions loadingOptions = (LoadingOptions) options;
			loadingView.setProgressIndicatorVisible(loadingOptions.progressIndicatorVisible);
			if (loadingOptions.progressFormat != null) {
				loadingView.setProgressFormat(loadingOptions.progressFormat);
			}
		}
	}

	/**
	 */
	@Override public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final View contentView = getContentView();
		if (contentView instanceof LoadingDialogView) {
			this.mLoadingView = (LoadingDialogView) contentView;
		}
	}

	/**
	 */
	@Override public void setProgress(@NonNull final int... progress) {
		if (mLoadingView != null) mLoadingView.setProgress(progress);
	}

	/**
	 */
	@Override @NonNull public int[] getProgress() {
		return mLoadingView != null ? mLoadingView.getProgress() : new int[0];
	}

	/**
	 */
	@Override public void setContent(@StringRes final int resId) {
		setContent(getText(resId));
	}

	/**
	 */
	@Override public void setContent(@Nullable final CharSequence content) {
		if (mLoadingView != null) mLoadingView.setContent(content);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link DialogOptions} implementation specific for the {@link LoadingDialog}.
	 * <p>
	 * Below are listed setters which may be used to supply desired data to {@link LoadingDialog}
	 * through these options:
	 * <ul>
	 * <li>{@link #showProgressIndicator(boolean)}</li>
	 * <li>{@link #progressFormat(String)}</li>
	 * <li>{@link #progressFormat(int)}</li>
	 * </ul>
	 *
	 * <h3>Xml attributes</h3>
	 * See {@link R.styleable#Dialog_Options_Loading LoadingOptions Attributes},
	 * {@link R.styleable#Dialog_Options DialogOptions Attributes}
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class LoadingOptions extends DialogOptions<LoadingOptions> {

		/**
		 * Creator used to create an instance or array of instances of LoadingOptions from {@link Parcel}.
		 */
		public static final Creator<LoadingOptions> CREATOR = new Creator<LoadingOptions>() {

			/**
			 */
			@Override public LoadingOptions createFromParcel(@NonNull final Parcel source) { return new LoadingOptions(source); }

			/**
			 */
			@Override public LoadingOptions[] newArray(final int size) { return new LoadingOptions[size]; }
		};

		/**
		 * Flag for <b>progress indicator visible</b> parameter.
		 */
		static final int PROGRESS_INDICATOR_VISIBLE = 0x00000001;

		/**
		 * Flag for <b>progress format</b> parameter.
		 */
		static final int PROGRESS_FORMAT = 0x00000001 << 1;

		/**
		 * Flag indicating whether a progress indicator within progress bar should be visible or not.
		 */
		boolean progressIndicatorVisible;

		/**
		 * Format for progress value.
		 */
		String progressFormat;

		/**
		 * Creates a new instance of LoadingOptions.
		 */
		public LoadingOptions() {
			super();
		}

		/**
		 * Creates a new instance of LoadingOptions.
		 *
		 * @param resources Application resources used to obtain values for these options when they
		 *                  are requested by theirs resource ids.
		 */
		public LoadingOptions(@NonNull final Resources resources) {
			super(resources);
		}

		/**
		 * Called form {@link #CREATOR} to create an instance of LoadingOptions form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected LoadingOptions(@NonNull final Parcel source) {
			super(source);
			this.progressIndicatorVisible = source.readInt() == 1;
			this.progressFormat = source.readString();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(progressIndicatorVisible ? 1 : 0);
			dest.writeString(progressFormat);
		}

		/**
		 */
		@Override public LoadingOptions merge(@NonNull final DialogOptions options) {
			if (!(options instanceof LoadingOptions)) {
				return super.merge(options);
			}
			final LoadingOptions loadingOptions = (LoadingOptions) options;
			if (loadingOptions.isSet(PROGRESS_INDICATOR_VISIBLE)) {
				this.progressIndicatorVisible = loadingOptions.progressIndicatorVisible;
				updateIsSet(PROGRESS_INDICATOR_VISIBLE);
			}
			if (loadingOptions.isSet(PROGRESS_FORMAT)) {
				this.progressFormat = loadingOptions.progressFormat;
				updateIsSet(PROGRESS_FORMAT);
			}
			return super.merge(options);
		}

		/**
		 * Sets a flag indicating whether the progress indicator of a dialog associated with this
		 * options should be visible or not.
		 *
		 * @param show {@code True} to show progress indicator, {@code false} to hide it.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogProgressMode dialog:dialogProgressMode
		 * @see #shouldShowProgressIndicator()
		 */
		public LoadingOptions showProgressIndicator(final boolean show) {
			updateIsSet(PROGRESS_INDICATOR_VISIBLE);
			if (progressIndicatorVisible != show) {
				this.progressIndicatorVisible = show;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns a flag indicating whether the progress indicator should be visible, set to this
		 * options.
		 * <p>
		 * Default value: <b>false</b>
		 *
		 * @return {@code True} to show progress indicator, {@code false} otherwise.
		 * @see #showProgressIndicator(boolean)
		 */
		public boolean shouldShowProgressIndicator() {
			return progressIndicatorVisible;
		}

		/**
		 * Same as {@link #progressFormat(String)} but with resource id.
		 *
		 * @param resId Resource id of the desired progress format.
		 * @see R.attr#dialogProgressFormat dialog:dialogProgressFormat
		 * @see #progressFormat()
		 */
		public LoadingOptions progressFormat(@StringRes final int resId) {
			return progressFormat(string(resId));
		}

		/**
		 * Sets the format used when formatting progress value of a dialog associated with these options.
		 *
		 * @param format The desired progress format.
		 * @return These options to allow methods chaining.
		 * @see R.attr#dialogProgressFormat dialog:dialogProgressFormat
		 * @see #progressFormat()
		 */
		public LoadingOptions progressFormat(@Nullable final String format) {
			updateIsSet(PROGRESS_FORMAT);
			if (progressFormat == null || !progressFormat.equals(format)) {
				this.progressFormat = format;
				notifyChanged();
			}
			return this;
		}

		/**
		 * Returns the progress format, set to these options.
		 *
		 * @return Progress format.
		 * @see #progressFormat(int)
		 * @see #progressFormat(String)
		 */
		@Nullable public String progressFormat() {
			return progressFormat;
		}

		/**
		 */
		@Override protected void onParseXmlAttribute(
				@NonNull final XmlResourceParser xmlParser,
				final int index,
				@AttrRes final int attr,
				@NonNull final Resources resources,
				@Nullable final Resources.Theme theme
		) {
			if (attr == R.attr.dialogProgressIndicator) {
				showProgressIndicator(xmlParser.getAttributeIntValue(index, ProgressDialogView.INDICATOR_NONE) == ProgressDialogView.INDICATOR_PROGRESS);
			} else if (attr == R.attr.dialogProgressFormat) {
				progressFormat(obtainXmlAttributeString(xmlParser, index, resources));
			} else {
				super.onParseXmlAttribute(xmlParser, index, attr, resources, theme);
			}
		}
	}
}