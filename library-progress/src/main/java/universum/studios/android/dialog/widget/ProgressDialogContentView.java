/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import universum.studios.android.dialog.ProgressDialog;
import universum.studios.android.dialog.R;
import universum.studios.android.dialog.view.ProgressDialogView;
import universum.studios.android.ui.widget.LinearProgressBar;
import universum.studios.android.ui.widget.WidgetSavedState;

/**
 * A {@link ProgressDialogView} implementation used by {@link ProgressDialog ProgressDialog} as
 * content view.
 * <p>
 * This class represents a container for a content view (TextView) and {@link LinearProgressBar}
 * with also set of TextViews used as progress indicator and time indicator. Content message can
 * be specified via {@link #setContent(CharSequence)} or its resource equivalent {@link #setContent(int)}.
 * Progress value for progress bar can be specified via {@link #setProgress(int...)} and the current
 * one obtained via {@link #getProgress()}. The specified progress value will be set to progress bar
 * and also formatted to string value to be showed by the progress indicator. By default is used
 * {@link #DEFAULT_PROGRESS_FORMAT} for formatting process, but the desired format can be specified
 * via {@link #setProgressFormat(String)} or its resource equivalent {@link #setProgressFormat(int)}.
 * If you want more control above the formatting process, you can specify progress formatter via
 * {@link #setProgressFormatter(ProgressDialogView.ProgressFormatter)}.
 * <p>
 * Whether to show the progress indicator or time indicator can be specified via {@link #setProgressIndicator(int)}
 * supplying one of {@link #INDICATOR_PROGRESS}, {@link #INDICATOR_TIME} or theirs combination or
 * {@link #INDICATOR_NONE} to hide all indicators.
 * <p>
 * <b>Note</b>, that this view saves its current state, so it can be later restored (in case of
 * orientation change for example).
 *
 * <h3>Custom view hierarchy</h3>
 * Custom layout resource, that will be inflated as view hierarchy for this layout, can be specified
 * via {@link R.attr#dialogLayoutProgressContent} within the current dialog theme. As long as the default
 * view hierarchy (same view types with same ids) is preserved the proper working of this view is
 * guaranteed. See {@link R.layout#dialog_layout_progress} for the info about the default view hierarchy.
 *
 * <h3>Xml attributes</h3>
 * {@code none}
 *
 * <h3>Default style attribute</h3>
 * {@code none}
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ProgressDialogContentView extends LinearLayout implements ProgressDialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ProgressDialogContentView";

	/**
	 * Default format used to format progress values.
	 */
	public static final String DEFAULT_PROGRESS_FORMAT = "%d %%";

	/**
	 * Default format used to format time value.
	 */
	public static final String DEFAULT_TIME_FORMAT = "mm:ss";

	/**
	 * One second in milliseconds.
	 */
	private static final int ONE_SECOND = 1000;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Runnable task which updates a value of the current progress time for this dialog.
	 */
	private final Runnable UPDATE_TIME = new Runnable() {

		/**
		 */
		@Override public void run() {
			if (mTimeRunning && mTime != null) {
				mTime.setTime(mTime.getTime() + mTimeUpdateInterval);
				handleTimeUpdate(mTime);
				mTimeHandler.postDelayed(this, mTimeUpdateInterval);
			}
		}
	};

	/**
	 * Type of progress indicator presented within this dialog body view.
	 */
	@Indicator private int mProgressIndicator = INDICATOR_PROGRESS | INDICATOR_TIME;

	/**
	 * Text view used to present content (message text) of dialog.
	 */
	private TextView mContentView;

	/**
	 * Progress bar used to display current progress.
	 */
	private LinearProgressBar mProgressBar;

	/**
	 * Current progress values.
	 */
	private int[] mProgress = new int[0];

	/**
	 * Format used when formatting current progress values.
	 */
	private String mProgressFormat = DEFAULT_PROGRESS_FORMAT;

	/**
	 * Formatter used to format current progress values.
	 */
	private ProgressFormatter mProgressFormatter = new ProgressFormatter() {

		/**
		 */
		@SuppressLint("DefaultLocale")
		@Override @NonNull public CharSequence formatProgress(@NonNull final int... progress) {
			if (TextUtils.isEmpty(mProgressFormat)) {
				return Integer.toString(progress[0]);
			}
			if (progress.length != 0 && progress[0] != 0) {
				switch (progress.length) {
					case 1:
						return String.format(mProgressFormat, progress[0]);
					case 2:
						return String.format(mProgressFormat, progress[0], progress[1]);
					case 3:
						return String.format(mProgressFormat, progress[0], progress[1], progress[2]);
				}
			}
			return String.format(Locale.getDefault(), DEFAULT_PROGRESS_FORMAT, 0);
		}
	};

	/**
	 * Layout which contains all progress indicators of this dialog body view.
	 */
	private ViewGroup mLayoutProgressIndicators;

	/**
	 * Text view to present current progress values.
	 */
	private TextView mTextViewProgressIndicator;

	/**
	 * Text view to present current progress time value.
	 */
	private TextView mTextViewTimeIndicator;

	/**
	 * Current progress time.
	 */
	private Date mTime;

	/**
	 * Handler used to count progress time.
	 */
	private Handler mTimeHandler;

	/**
	 * Boolean flag indicating whether a time counting is running or not.
	 */
	boolean mTimeRunning = false;

	/**
	 * Date format object used when formatting current progress time value.
	 */
	private SimpleDateFormat mSimpleTimeFormat = new SimpleDateFormat(DEFAULT_TIME_FORMAT, Locale.US);

	{
		mSimpleTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	/**
	 * Formatter used to format current progress time value.
	 */
	private TimeFormatter mTimeFormatter = new TimeFormatter() {

		/**
		 */
		@Override @NonNull public CharSequence formatTime(final long time) {
			if (mTime == null) {
				return "";
			}
			if (mSimpleTimeFormat == null) {
				return Long.toString(time / 1000L) + "s";
			}
			return mSimpleTimeFormat.format(mTime);
		}
	};

	/**
	 * Format used when formatting current progress time value.
	 */
	private String mTimeFormat = DEFAULT_TIME_FORMAT;

	/**
	 * Interval that determines how often the progress time should be updated.
	 */
	private long mTimeUpdateInterval = ONE_SECOND;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #ProgressDialogContentView(Context, AttributeSet)}  without attributes.
	 */
	public ProgressDialogContentView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #ProgressDialogContentView(Context, AttributeSet, int)} with {@code 0} as attribute
	 * for default style.
	 */
	public ProgressDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		super(context, attrs);
		this.init(context, attrs, 0, 0);
	}

	/**
	 * Same as {@link #ProgressDialogContentView(Context, AttributeSet, int, int)} with {@code 0} as
	 * default style.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public ProgressDialogContentView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init(context, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of ProgressDialogContentView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of this view.
	 * @param defStyleAttr An attribute which contains a reference to a default style resource for
	 *                     this view within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the new view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public ProgressDialogContentView(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.init(context, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from one of constructors of this view to perform its initialization.
	 * <p>
	 * Initialization is done via parsing of the specified <var>attrs</var> set and obtaining for
	 * this view specific data from it that can be used to configure this new view instance. The
	 * specified <var>defStyleAttr</var> and <var>defStyleRes</var> are used to obtain default data
	 * from the current theme provided by the specified <var>context</var>.
	 */
	private void init(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
		final Resources.Theme theme = context.getTheme();
		final TypedValue typedValue = new TypedValue();
		// Resolve layout resource from which to inflate view hierarchy.
		int layoutResource = R.layout.dialog_layout_progress;
		if (theme.resolveAttribute(R.attr.dialogLayoutProgressContent, typedValue, true)) {
			layoutResource = typedValue.resourceId;
		}
		this.inflateHierarchy(context, layoutResource);
	}

	/**
	 * Called to inflate a view hierarchy of this view.
	 *
	 * @param context  Context used to obtain an instance of LayoutInflater used to inflate a desired
	 *                 layout resource as view hierarchy for this view.
	 * @param resource Resource id of the layout which should represent a view hierarchy of this view.
	 */
	private void inflateHierarchy(final Context context, final int resource) {
		if (resource > 0) {
			LayoutInflater.from(context).inflate(resource, this);
			this.mContentView = (TextView) findViewById(R.id.dialog_text_view_content);
			this.mLayoutProgressIndicators = (ViewGroup) findViewById(R.id.dialog_progress_indicators);
			this.mTextViewProgressIndicator = (TextView) findViewById(R.id.dialog_text_view_progress_indicator);
			this.mTextViewTimeIndicator = (TextView) findViewById(R.id.dialog_text_view_time_indicator);
			this.mProgressBar = (LinearProgressBar) findViewById(R.id.dialog_progress_bar);
			onFinishInflate();
		}
	}

	/**
	 */
	@Override public void setProgressMode(@Mode final int mode) {
		if (mProgressBar != null) mProgressBar.setProgressMode(mode);
	}

	/**
	 */
	@Override public int getProgressMode() {
		return mProgressBar != null ? mProgressBar.getProgressMode() : MODE_DETERMINATE;
	}

	/**
	 */
	@Override public void setProgressIndicator(@Indicator final int indicator) {
		if (mProgressIndicator != indicator) {
			this.mProgressIndicator = indicator;
			onProgressIndicatorUpdated(indicator);
		}
	}

	/**
	 * Invoked whenever the progress indicator type has been updated.
	 *
	 * @param indicator The current progress indicator type.
	 */
	private void onProgressIndicatorUpdated(@Indicator final int indicator) {
		if (mLayoutProgressIndicators != null && mTextViewProgressIndicator != null && mTextViewTimeIndicator != null) {
			// Update indicator views visibility and layout position (gravity).
			switch (indicator) {
				case INDICATOR_PROGRESS:
					DialogWidgetUtils.updateViewVisibility(mLayoutProgressIndicators, View.VISIBLE);
					DialogWidgetUtils.updateViewVisibility(mTextViewProgressIndicator, View.VISIBLE);
					DialogWidgetUtils.updateViewVisibility(mTextViewTimeIndicator, View.GONE);
					DialogWidgetUtils.updateViewLayoutParamsGravity(mTextViewProgressIndicator, Gravity.CENTER);
					break;
				case INDICATOR_TIME:
					DialogWidgetUtils.updateViewVisibility(mLayoutProgressIndicators, View.VISIBLE);
					DialogWidgetUtils.updateViewVisibility(mTextViewProgressIndicator, View.GONE);
					DialogWidgetUtils.updateViewVisibility(mTextViewTimeIndicator, View.VISIBLE);
					DialogWidgetUtils.updateViewLayoutParamsGravity(mTextViewTimeIndicator, Gravity.CENTER);
					break;
				case INDICATOR_PROGRESS | INDICATOR_TIME:
					DialogWidgetUtils.updateViewVisibility(mLayoutProgressIndicators, View.VISIBLE);
					DialogWidgetUtils.updateViewVisibility(mTextViewProgressIndicator, View.VISIBLE);
					DialogWidgetUtils.updateViewVisibility(mTextViewTimeIndicator, View.VISIBLE);
					DialogWidgetUtils.updateViewLayoutParamsGravity(mTextViewProgressIndicator, Gravity.LEFT | Gravity.CENTER_VERTICAL);
					DialogWidgetUtils.updateViewLayoutParamsGravity(mTextViewTimeIndicator, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
					break;
				case ProgressDialogView.INDICATOR_NONE:
				default:
					mLayoutProgressIndicators.setVisibility(View.GONE);
					break;
			}
		}
	}

	/**
	 */
	@Override @Indicator public int getProgressIndicator() {
		return mProgressIndicator;
	}

	/**
	 */
	@Override public void setProgressFormat(@StringRes final int resId) {
		setProgressFormat(getResources().getString(resId));
	}

	/**
	 */
	@Override public void setProgressFormat(@NonNull final String format) {
		this.mProgressFormat = format;
		this.handleProgressUpdate(mProgress);
	}

	/**
	 */
	@Override @NonNull public String getProgressFormat() {
		return mProgressFormat;
	}

	/**
	 */
	@Override public void setProgressFormatter(@NonNull final ProgressFormatter formatter) {
		this.mProgressFormatter = formatter;
		this.handleProgressUpdate(mProgress);
	}

	/**
	 */
	@Override @NonNull public ProgressFormatter getProgressFormatter() {
		return mProgressFormatter;
	}

	/**
	 */
	@Override public void setProgress(@NonNull final int... progress) {
		this.handleProgressUpdate(mProgress = progress);
	}

	/**
	 * Updates the progress indicator with the specified progress values.
	 *
	 * @param progress The current progress values.
	 */
	private void handleProgressUpdate(final int[] progress) {
		if (progress.length > 0) {
			this.mProgress = progress;
			if (!mTimeRunning && (mProgressIndicator & INDICATOR_TIME) != 0) {
				this.startTime(mTimeUpdateInterval);
				handleTimeUpdate(mTime);
			}
			if (mProgressBar != null) {
				mProgressBar.setProgress(progress[progress.length - 1]);
			}
			if (mTextViewProgressIndicator != null) {
				mTextViewProgressIndicator.setText(mProgressFormatter.formatProgress(progress));
			}
		}
	}

	/**
	 */
	@Override @NonNull public int[] getProgress() {
		return mProgress;
	}

	/**
	 */
	@Override public void setTimeFormat(final int resId) {
		setTimeFormat(getResources().getString(resId));
	}

	/**
	 */
	@Override public void setTimeFormat(@NonNull final String format) {
		this.mSimpleTimeFormat = new SimpleDateFormat(mTimeFormat, Locale.US);
		this.mSimpleTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		this.handleTimeUpdate(mTime);
	}

	/**
	 */
	@Override @NonNull public String getTimeFormat() {
		return mTimeFormat;
	}

	/**
	 */
	@Override public void setTimeFormatter(@NonNull final TimeFormatter formatter) {
		this.mTimeFormatter = formatter;
		this.handleTimeUpdate(mTime);
	}

	/**
	 */
	@Override @NonNull public TimeFormatter getTimeFormatter() {
		return mTimeFormatter;
	}

	/**
	 */
	@Override public void setTimeUpdateInterval(final long interval) {
		if (mTimeUpdateInterval != interval) this.mTimeUpdateInterval = interval;
	}

	/**
	 */
	@Override public long getTimeUpdateInterval() {
		return mTimeUpdateInterval;
	}

	/**
	 * Starts counting of the progress time.
	 *
	 * @param delay Delay in milliseconds to start the counting.
	 */
	final void startTime(final long delay) {
		if (mTimeHandler == null) {
			this.mTimeHandler = new Handler();
		}
		if (mTime == null) {
			this.mTime = new Date(0);
		}
		this.mTimeRunning = true;
		mTimeHandler.postDelayed(UPDATE_TIME, delay);
	}

	/**
	 * Updates the time indicator with the specified <var>time</var> value.
	 *
	 * @param time The current progress time.
	 */
	final void handleTimeUpdate(final Date time) {
		if (mTextViewTimeIndicator != null) {
			if (time != null) {
				mTextViewTimeIndicator.setText(mTimeFormatter.formatTime(time.getTime()));
			} else {
				mTextViewTimeIndicator.setText(mTimeFormatter.formatTime(0));
			}
		}
	}

	/**
	 */
	@Override public void pauseTime() {
		this.mTimeRunning = false;
	}

	/**
	 */
	@Override public void resumeTime() {
		if (!mTimeRunning) this.startTime(0);
	}

	/**
	 */
	@Override public long getTime() {
		return mTime != null ? mTime.getTime() : -1;
	}

	/**
	 */
	@Override public void setContent(@StringRes final int resId) {
		setContent(getResources().getText(resId));
	}

	/**
	 */
	@Override public void setContent(@Nullable final CharSequence content) {
		if (mContentView != null) mContentView.setText(content);
	}

	/**
	 */
	@Override @NonNull public CharSequence getContent() {
		return mContentView != null ? mContentView.getText() : "";
	}

	/**
	 *
	 */
	@Override protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		onProgressIndicatorUpdated(mProgressIndicator);
	}

	/**
	 */
	@Override protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		this.mTimeRunning = false;
	}

	/**
	 */
	@Override protected Parcelable onSaveInstanceState() {
		final SavedState savedState = new SavedState(super.onSaveInstanceState());
		savedState.content = mContentView != null ? mContentView.getText() : "";
		savedState.progressFormat = mProgressFormat;
		savedState.timeFormat = mTimeFormat;
		savedState.progress = mProgress;
		savedState.progressIndicator = mProgressIndicator;
		savedState.time = mTimeRunning ? mTime.getTime() : -1;
		return savedState;
	}

	/**
	 */
	@Override protected void onRestoreInstanceState(@NonNull final Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());
		setContent(savedState.content);
		setProgressFormat(savedState.progressFormat);
		setTimeFormat(savedState.timeFormat);
		setProgress(savedState.progress);
		setProgressIndicator(savedState.progressIndicator);
		if (savedState.time >= 0 && (mProgressIndicator & INDICATOR_TIME) != 0) {
			this.mTime = new Date(savedState.time);
			handleTimeUpdate(mTime);
			this.mTimeHandler = new Handler();
			resumeTime();
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link WidgetSavedState} implementation used to ensure that the state of {@link ProgressDialogContentView}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class SavedState extends WidgetSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) { return new SavedState(source); }

			/**
			 */
			@Override public SavedState[] newArray(final int size) { return new SavedState[size]; }
		};

		/**
		 */
		CharSequence content;

		/**
		 */
		int[] progress = new int[0];

		/**
		 */
		String progressFormat, timeFormat;

		/**
		 */
		int progressIndicator;

		/**
		 */
		long time;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #onSaveInstanceState()} and also in {@link #onRestoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.onSaveInstanceState()} within
		 *                   {@code onSaveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.content = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
			this.progressFormat = source.readString();
			this.timeFormat = source.readString();
			this.progressIndicator = source.readInt();
			source.readIntArray(progress);
			this.time = source.readLong();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			TextUtils.writeToParcel(content, dest, flags);
			dest.writeString(progressFormat);
			dest.writeString(timeFormat);
			dest.writeInt(progressIndicator);
			dest.writeIntArray(progress);
			dest.writeLong(time);
		}
	}
}