/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import universum.studios.android.dialog.LoadingDialog;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link LoadingDialog LoadingDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.LoadingDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface LoadingDialogView extends DialogView {

    /**
     * Updates a visibility of the progress indicator.
     *
     * @param visible {@code True} to set indicator visible, {@code false} otherwise.
     * @see #isProgressIndicatorVisible()
     */
    void setProgressIndicatorVisible(boolean visible);

    /**
     * Returns a flag indicating whether the progress indicator is visible or not.
     *
     * @return {@code True} if indicator is visible, {@code false} otherwise.
     * @see #setProgressIndicatorVisible(boolean)
     */
    boolean isProgressIndicatorVisible();

    /**
     * Same as {@link #setProgressFormat(String)} for resource id.
     *
     * @param resId Resource id of the desired format.
     * @see #getProgressFormat()
     */
    void setProgressFormat(@StringRes int resId);

    /**
     * Sets the format used when formatting current progress values.
     *
     * @param format The desired format for progress.
     * @see #getProgressFormat()
     */
    void setProgressFormat(@Nullable String format);

    /**
     * Returns the format used when formatting progress.
     *
     * @return Current progress format.
     * @see #setProgressFormat(int)
     * @see #setProgressFormat(String)
     */
    @NonNull String getProgressFormat();

    /**
     * Sets the current progress for this dialog view.
     *
     * @param progress Current progress values.
     * @see #getProgress()
     */
    void setProgress(@NonNull int... progress);

    /**
     * Returns the current progress set to this dialog view.
     *
     * @return Current progress values.
     * @see #setProgress(int...)
     */
    @NonNull int[] getProgress();

    /**
     * Same as {@link #setContent(CharSequence)} for resource id.
     *
     * @param resId Resource id of the desired content text.
     * @see #getContent()
     */
    void setContent(@StringRes int resId);

    /**
     * Sets a content text for this dialog view.
     *
     * @param text The desired content text. May be {@code null} to clear the current one.
     * @see #getContent()
     */
    void setContent(@Nullable CharSequence text);

    /**
     * Returns the current content text presented by this dialog view.
     *
     * @return Current content text.
     * @see #setContent(int)
     * @see #setContent(CharSequence)
     */
    @NonNull CharSequence getContent();
}