/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog.view;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.dialog.ProgressDialog;
import universum.studios.android.ui.widget.LinearProgressBar;

/**
 * Interface specifying API layer for a view that can be used as content view for {@link ProgressDialog ProgressDialog}
 * to ensure proper working of binding process of the content view for such a dialog.
 * <p>
 * See default {@link universum.studios.android.dialog.widget.ProgressDialogContentView implementation}
 * for more info.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface ProgressDialogView extends DialogView {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Flag copied from {@link LinearProgressBar#MODE_DETERMINATE}.
	 */
	int MODE_DETERMINATE = LinearProgressBar.MODE_DETERMINATE;

	/**
	 * Flag copied from {@link LinearProgressBar#MODE_INDETERMINATE}.
	 */
	int MODE_INDETERMINATE = LinearProgressBar.MODE_INDETERMINATE;

	/**
	 * Flag copied from {@link LinearProgressBar#MODE_BUFFER}.
	 */
	int MODE_BUFFER = LinearProgressBar.MODE_BUFFER;

	/**
	 * Flag copied from {@link LinearProgressBar#MODE_QUERY_INDETERMINATE_DETERMINATE}.
	 */
	int MODE_QUERY_INDETERMINATE_DETERMINATE = LinearProgressBar.MODE_QUERY_INDETERMINATE_DETERMINATE;

	/**
	 * Defines an annotation for determining set of allowed modes for {@link #setProgressMode(int)}
	 * method.
	 */
	@IntDef({
			MODE_DETERMINATE,
			MODE_INDETERMINATE,
			MODE_QUERY_INDETERMINATE_DETERMINATE,
			MODE_BUFFER
	})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface Mode {}

	/**
	 * Indicator flag for {@link #setProgressIndicator(int)}. Use this to not show any indicator within
	 * progress dialog view.
	 */
	int INDICATOR_NONE = 0x00000000;

	/**
	 * Indicator flag for {@link #setProgressIndicator(int)}. Use this to show indicator which presents
	 * the current progress value.
	 */
	int INDICATOR_PROGRESS = 0x00000001;

	/**
	 * Indicator flag for {@link #setProgressIndicator(int)}. Use this to show indicator which presents
	 * the current progress time value.
	 */
	int INDICATOR_TIME = 0x00000001 << 1;

	/**
	 * Defines an annotation for determining set of allowed flags for {@link #setProgressIndicator(int)}
	 * method.
	 */
	@IntDef(flag = true, value = {
			INDICATOR_NONE,
			INDICATOR_PROGRESS,
			INDICATOR_TIME
	})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface Indicator {}

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Formatter that can be used to supply custom formatting logic of progress value for
	 * implementation of {@link ProgressDialogView}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 *
	 * @see #setProgressFormatter(ProgressFormatter)
	 */
	interface ProgressFormatter {

		/**
		 * Formats the given set of progress values.
		 *
		 * @param progress The desired progress values to format.
		 * @return Formatted progress.
		 */
		@NonNull CharSequence formatProgress(@NonNull int... progress);
	}

	/**
	 * Formatter that can be used to supply custom formatting logic of progress time value for
	 * implementation of {@link ProgressDialogView}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 *
	 * @see #setTimeFormatter(TimeFormatter)
	 */
	interface TimeFormatter {

		/**
		 * Formats the given time value.
		 *
		 * @param time The desired time to format.
		 * @return Formatted time.
		 */
		@NonNull CharSequence formatTime(long time);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a mode for {@link LinearProgressBar} determining how should be the progress graphics
	 * presented in this dialog view.
	 *
	 * @param mode The desired mode. One of {@link #MODE_DETERMINATE}, {@link #MODE_INDETERMINATE},
	 *             {@link #MODE_BUFFER} or {@link #MODE_QUERY_INDETERMINATE_DETERMINATE}.
	 */
	void setProgressMode(@Mode int mode);

	/**
	 * Returns the progress mode specified for this dialog view.
	 * <p>
	 * Default value: <b>{@link #MODE_DETERMINATE}</b>
	 *
	 * @return One of {@link #MODE_DETERMINATE}, {@link #MODE_INDETERMINATE}, {@link #MODE_BUFFER}
	 * or {@link #MODE_QUERY_INDETERMINATE_DETERMINATE}.
	 */
	@Mode int getProgressMode();

	/**
	 * Sets a progress indicator flags for this dialog view.
	 *
	 * @param indicator The desired indicator flags to determine which indicators to show within
	 *                  this progress dialog view. One of {@link #INDICATOR_PROGRESS}, {@link #INDICATOR_TIME}
	 *                  or theirs combination or {@link #INDICATOR_NONE} to not show any indicator.
	 * @see #getProgressIndicator()
	 */
	void setProgressIndicator(@Indicator int indicator);

	/**
	 * Returns the current progress indicator flags.
	 * <p>
	 * Default value: <b>{@link ProgressDialogView#INDICATOR_PROGRESS} | {@link ProgressDialogView#INDICATOR_TIME}</b>
	 *
	 * @return One of {@link #INDICATOR_PROGRESS}, {@link #INDICATOR_TIME} or theirs combination or
	 * {@link #INDICATOR_NONE} if no indicator should be displayed.
	 * @see #setProgressIndicator(int)
	 */
	@Indicator int getProgressIndicator();

	/**
	 * Same as {@link #setProgressFormat(String)} for resource id.
	 *
	 * @param resId Resource id of the desired format.
	 * @see #getProgressFormat()
	 */
	void setProgressFormat(@StringRes int resId);

	/**
	 * Sets the format used when formatting progress values.
	 *
	 * @param format The desired format for progress.
	 * @see #getProgressFormat()
	 * @see #setProgressFormatter(ProgressFormatter)
	 */
	void setProgressFormat(@NonNull String format);

	/**
	 * Returns the current progress format.
	 *
	 * @return Format for progress values.
	 * @see #setProgressFormat(int)
	 * @see #setProgressFormat(String)
	 */
	@NonNull String getProgressFormat();

	/**
	 * Sets a formatter used to format progress values.
	 *
	 * @param formatter The desired formatter.
	 * @see #getProgressFormatter()
	 * @see #setProgressFormat(String)
	 */
	void setProgressFormatter(@NonNull ProgressFormatter formatter);

	/**
	 * Returns the current progress formatter.
	 *
	 * @return Formatter for progress values.
	 * @see #setProgressFormatter(ProgressFormatter)
	 */
	@NonNull ProgressFormatter getProgressFormatter();

	/**
	 * Sets the current progress for this dialog view.
	 * <p>
	 * <b>Note</b>, that if {@link #INDICATOR_TIME} flag has been requested and this method is called
	 * for the first time, this will start counting of the progress time. Progress time can be later
	 * paused ({@link #pauseTime()}) and resumed ({@link #resumeTime()}). Counting stops when the
	 * associated dialog is dismissed.
	 *
	 * @param progress The desired progress values.
	 * @see #getProgress()
	 */
	void setProgress(@NonNull int... progress);

	/**
	 * Returns the current progress of this dialog view.
	 *
	 * @return Current progress values.
	 * @see #setProgress(int...)
	 */
	@NonNull int[] getProgress();

	/**
	 * Same as {@link #setTimeFormat(String)} for resource id.
	 *
	 * @param resId Resource id of the desired format.
	 * @see #getTimeFormat()
	 */
	void setTimeFormat(@StringRes int resId);

	/**
	 * Sets the format used when formatting current progress time.
	 *
	 * @param format The desired format for progress time.
	 * @see #getTimeFormat()
	 * @see #setTimeFormatter(TimeFormatter)
	 */
	void setTimeFormat(@NonNull String format);

	/**
	 * Returns the current progress time format.
	 *
	 * @return Format for progress time.
	 * @see #setTimeFormat(int)
	 * @see #setTimeFormat(String)
	 */
	@NonNull String getTimeFormat();

	/**
	 * Sets a formatter used to format progress time.
	 *
	 * @param formatter The desired formatter.
	 * @see #getTimeFormatter()
	 * @see #setTimeFormat(String)
	 */
	void setTimeFormatter(@NonNull TimeFormatter formatter);

	/**
	 * Returns the current time formatter.
	 *
	 * @return Formatter for progress time value.
	 * @see #setTimeFormatter(TimeFormatter)
	 */
	@NonNull TimeFormatter getTimeFormatter();

	/**
	 * Sets an interval that determines how often the progress time should be updated.
	 *
	 * @param interval The desired update interval in milliseconds.
	 * @see #getTimeUpdateInterval()
	 */
	void setTimeUpdateInterval(long interval);

	/**
	 * Returns the update interval for progress time.
	 * <p>
	 * Default value: <b>1 000 millis</b>
	 *
	 * @return Update interval in milliseconds
	 * @see #setTimeUpdateInterval(long) .
	 */
	long getTimeUpdateInterval();

	/**
	 * Pauses counting of the progress time.
	 *
	 * @see #resumeTime()
	 */
	void pauseTime();

	/**
	 * Resumes counting of the progress time.
	 *
	 * @see #pauseTime()
	 */
	void resumeTime();

	/**
	 * Returns the current progress time in milliseconds.
	 *
	 * @return Progress time or {@code -1} if there was no progress time counted yet.
	 */
	long getTime();

	/**
	 * Same as {@link #setContent(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired content text.
	 * @see #getContent()
	 */
	void setContent(@StringRes int resId);

	/**
	 * Sets a content text for this dialog view.
	 *
	 * @param text The desired content text. May be {@code null} to clear the current one.
	 * @see #getContent()
	 */
	void setContent(@Nullable CharSequence text);

	/**
	 * Returns the current content text presented by this dialog view.
	 *
	 * @return Current content text.
	 * @see #setContent(int)
	 * @see #setContent(CharSequence)
	 */
	@NonNull CharSequence getContent();
}