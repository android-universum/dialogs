/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.dialog;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * Interface specifying API required for dialog implementations that are showing some progress to a
 * user.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface ProgressBarDialog extends Dialog {

	/**
	 * Sets the current progress for this progress dialog instance.
	 *
	 * @param progress Current progress.
	 * @see #getProgress()
	 */
	void setProgress(@NonNull int... progress);

	/**
	 * Returns the current progress of this progress dialog instance.
	 *
	 * @return Current progress.
	 * @see #setProgress(int...)
	 */
	@NonNull int[] getProgress();

	/**
	 * Same as {@link #setContent(CharSequence)} for resource id.
	 *
	 * @param resId Resource id of the desired content text.
	 */
	void setContent(@StringRes int resId);

	/**
	 * Sets a content text for this progress dialog instance.
	 *
	 * @param content The desired content text.
	 */
	void setContent(@Nullable CharSequence content);
}